def f(x):
  return x**2

x=0
while f(x)<=10**6:
  x+=1

print("f(",x,")=",f(x))
