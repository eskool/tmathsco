# TMaths Co: Limites de Fonctions

## Limite d'une fonction en l'infini

### Limite $+\infty$ en l'infini ( $\pm \infty$ )

!!! exp "Introduction graphique pour Limite $+\infty$ en $+\infty$"
    !!! col __60
        :one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=x^2$

        Il semble que les images $f(x)$ soient aussi ***grandes*** (comprendre proches de $+\infty$) que l'on veut, lorque $x$ est *suffisamment <b>grand</b>* (comprendre proche de $+\infty$):

        * $f(x)>10^6$ pour $x>1000$
        * $f(x)>10^{10}$ pour $x>10^5$

        **Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **monte haut** (aussi *haut* que l'on veut)...

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} x^2=+\infty$</enc>

        :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=x^3$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} x^3=+\infty$</enc>

    !!! col __40
        <center>

        <enc>Exemples de fonctions $f$ de limite $+\infty$ en $+\infty$</enc>

        <figure>

        <img src="img/x2.png" alt="">

        <figcaption>

        fonction $x\mapsto x^2$

        </figcaption>

        </figure>

        <figure>

        <img src="img/x3.png" alt="">

        <figcaption>

        fonction $x\mapsto x^3$

        </figcaption>

        </figure>

        </center>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $+\infty$, quand $x$ tend vers $+\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to +\infty} f(x) = +\infty$
    * ou encore que: $f(x) \rightarrow +\infty$ quand $x \rightarrow +\infty$

!!! exp "Introduction graphique pour Limite $+\infty$ en $-\infty$"
    !!! col __60
        :one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=x^2$

        Il semble que les images $f(x)$ soient aussi ***grandes*** (comprendre proches de $+\infty$) que l'on veut, lorque $x$ est *suffisamment <b>petit</b>* (comprendre proche de $-\infty$):

        * $f(x)>10^6$ pour $x< -1000$
        * $f(x)>10^{12}$ pour $x< -10^6$

        **Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **monte haut** (aussi *haut* que l'on veut)...

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} x^2=+\infty$</enc>

        :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=-x^3$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -x^3=+\infty$</enc>

    !!! col __40
        <center>

        <encadre>Exemples de fonctions $f$ de limite $+\infty$ en $-\infty$</encadre>

        <figure>

        <img src="img/x2.png">

        <figcaption>

        fonction $x\mapsto x^2$

        </figcaption>

        </figure>

        <figure>

        <img src="img/moinsx3.png">

        <figcaption>

        fonction $x\mapsto -x^3$

        </figcaption>

        </figure>

        </center>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $+\infty$, quand $x$ tend vers $-\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to -\infty} f(x) = +\infty$
    * ou encore que: $f(x) \rightarrow +\infty$ quand $x \rightarrow -\infty$

### Limite $-\infty$ en l'infini ( $\pm \infty$ )

!!! exp "Introduction graphique pour Limite $-\infty$ en $+\infty$"

    !!! col __60
        :one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=-x^2$

        Il semble que les images $f(x)$ soient aussi ***petites*** (comprendre proches de $-\infty$) que l'on veut, lorque $x$ est *suffisamment <b>grand</b>* (comprendre proche de $+\infty$):

        * $f(x)< -10^6$ pour $x>1000$
        * $f(x)< -10^{12}$ pour $x>10^6$

        **Language Humain :** plus on va vers la **droite**, et plus la *"hauteur"* **descend bas** (aussi *bas* que l'on veut)...

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -x^2=-\infty$</enc>

        :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=-x^3$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -x^3=-\infty$</enc>

    !!! col __40
        <center>

        <encadre>Exemples de fonctions $f$ de limite $-\infty$ en $+\infty$</encadre>

        <figure>

        <img src="img/moinsx2.png">

        <figcaption>

        fonction $x\mapsto -x^2$

        </figcaption>

        </figure>

        <figure>

        <img src="img/moinsx3.png">

        <figcaption>

        fonction $x\mapsto -x^3$

        </figcaption>

        </figure>

        </center>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $+\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to +\infty} f(x) = -\infty$
    * ou encore que: $f(x) \rightarrow -\infty$ quand $x \rightarrow +\infty$

!!! exp "Introduction graphique pour Limite $-\infty$ en $-\infty$"

    !!! col __60
        :one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=-x^2$

        Il semble que les images $f(x)$ soient aussi ***petites*** (comprendre proches de $-\infty$) que l'on veut, lorque $x$ est *suffisamment <b>petit</b>* (comprendre proche de $-\infty$):

        * $f(x)< -10^6$ pour $x< -1000$
        * $f(x)< -10^{12}$ pour $x< -10^6$

        **Language Humain :** plus on va vers la **gauche**, et plus la *"hauteur"* **descend bas** (aussi *bas* que l'on veut)...

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -x^2=-\infty$</enc>

        :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=x^3$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} x^3=-\infty$</enc>

    !!! col __40
        <center>

        <encadre>Exemples de fonctions $f$ de limite $-\infty$ en $-\infty$</encadre>

        <figure>

        <img src="img/moinsx2.png">

        <figcaption>

        fonction $x\mapsto -x^2$

        </figcaption>

        </figure>

        <figure>

        <img src="img/x3.png">

        <figcaption>

        fonction $x\mapsto x^3$

        </figcaption>

        </figure>

        </center>

!!! def
    On dit dans ce cas que **la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $-\infty$**, et on note:

    * $\displaystyle \lim_{x\to -\infty} f(x) = -\infty$
    * ou encore que: $f(x) \rightarrow -\infty$ quand $x \rightarrow -\infty$

!!! pte "Fonctions de Référence de Limite infinie en l'infini"
    <center>

    |>|>|>|Fonctions de Référence de Limite Infinie en l'infini|
    |:-:|:-:|:-:|:-:|
    |>|$\displaystyle \lim_{x\to +\infty} ln(x)=+\infty$|>|$\displaystyle \lim_{x\to +\infty} \sqrt x=+\infty$|
    |$\displaystyle \lim_{x\to +\infty} x^2=+\infty$|$\displaystyle \lim_{x\to +\infty} x^3=+\infty$|$\displaystyle \lim_{x\to +\infty} x^n=+\infty$<br>pour $n$ entier $>0$|$\displaystyle \lim_{x\to +\infty} e^x=+\infty$|
    |$\displaystyle \lim_{x\to -\infty} x^2=+\infty$|$\displaystyle \lim_{x\to -\infty} x^3=-\infty$|$\displaystyle \lim_{x\to -\infty} x^n=+\infty$<br>pour $n$ entier <b>pair</b> $>0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{e^x}=+\infty$|
    |^|^|^|^|$\displaystyle \lim_{x\to -\infty} x^n=-\infty$<br>pour $n$ entier <b>impair</b> $>0$|^|
    {.petit}

    </center>

### Limite $0$ en l'infini ( $\pm \infty$ )


!!! exp "Introduction graphique pour Limite $0$ en $+\infty$"
    !!! col __100
        !!! col __60
            :one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=\dfrac 1x$

            Il semble que les images $f(x)$ soient aussi ***"petites", au sens proches de $0$*** que l'on veut, lorque $x$ est *suffisamment <b>grand</b>* (comprendre proche de $+\infty$):

            * $0<f(x)<10^{-3}$ pour $x>1000$
            * $0<f(x)<10^{-5}$ pour $x>10^5$

            **Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **est proche de $0$** (aussi *proche* que l'on veut)...
            $\Leftrightarrow$ plus on va vers la **droite**, et plus ***la (hauteur de la) courbe s'approche de l'axe des $x$*** (aussi proche que l'on veut)

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} \dfrac 1x = 0$</enc>

        !!! col __40 center
            <encadre style="margin-bottom:1em;">Exemples de fonctions $f$ de limite $0$ en $+\infty$</encadre>

            <figure>
            <img src="img/1surx_enplusinfini.png">
            <figcaption>
            fonction $x\mapsto \dfrac 1x$
            </figcaption>
            </figure>


    !!! col __100
        !!! col __60
            :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=-\dfrac 1x$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -\dfrac 1x = 0$</enc>
        !!! col __40 center
            <figure>

            <img src="img/moins1surx_enplusinfini.png">

            <figcaption>

            fonction $x\mapsto -\dfrac 1x$

            </figcaption>

            </figure>

    !!! col __100
        !!! col __60
            :three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= \dfrac {sin(100x)}{x}$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} \dfrac {sin(100x)}{x} = 0$</enc>
        !!! col __40 center
            <figure>

            <img src="img/sinxsurx_enplusinfini.png">

            <figcaption>

            fonction $x\mapsto \dfrac {sin(100x)}{x}$

            </figcaption>

            </figure>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $0$, quand $x$ tend vers $+\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to +\infty} f(x) = 0$
    * ou encore que: $f(x) \rightarrow 0$ quand $x \rightarrow +\infty$

<!-- Exemple suivant -->

!!! exp "Introduction graphique pour Limite $0$ en $-\infty$"
    !!! col __100
        !!! col __60
            :one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=-\dfrac 1x$

            Il semble que les images $f(x)$ soient aussi ***"petites", au sens proches de $0$*** que l'on veut, lorque $x$ est *suffisamment <b>petit</b>* (comprendre proche de $-\infty$):

            * $0<f(x)<10^{-3}$ pour $x< -1000$
            * $0<f(x)<10^{-5}$ pour $x< -10^5$

            **Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **est proche de $0$** (aussi *proche* que l'on veut)...
            $\Leftrightarrow$ plus on va vers la **gauche**, et plus ***la (hauteur de la) courbe s'approche de l'axe des $x$*** (aussi proche que l'on veut)

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -\dfrac 1x = 0$</enc>
        !!! col __40 center
            <encadre style="margin-bottom:1em;">Exemples de fonctions $f$ de limite $0$ en $-\infty$</encadre>
            <figure>
            <img src="img/1surx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto \dfrac 1x$
            </figcaption>
            </figure>

    !!! col __100
        !!! col __60
            :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=\dfrac 1x$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} \dfrac 1x = 0$</enc>
        !!! col __40
            <figure>
            <img src="img/sinxsurx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto \dfrac {sin(100x)}{x}$
            </figcaption>
            </figure>

    !!! col __100
        !!! col __60
            :three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= \dfrac {sin(100x)}{x}$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} \dfrac {sin(100x)}{x} = 0$</enc>
        !!! col __40
            <figure>
            <img src="img/moins1surx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto -\dfrac 1x$
            </figcaption>
            </figure>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $0$, quand $x$ tend vers $-\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to -\infty} f(x) = 0$
    * ou encore que: $f(x) \rightarrow 0$ quand $x \rightarrow -\infty$

!!! pte "Fonctions de Référence de Limite $0$ en l'infini"
    |>|>|>|>|Fonctions de Référence de Limite $0$ en l'infini|
    |:-:|:-:|:-:|:-:|:-:|
    |>|>|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{ln(x)}=0$|>|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{\sqrt x}=0$|
    |$\displaystyle \lim_{x\to +\infty} \dfrac 1x=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^2}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^3}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^n}=0$<br/>pour $n$ entier $>0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{e^x}=0$|
    |$\displaystyle \lim_{x\to -\infty} \dfrac 1x=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^2}=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^3}=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^n}=0$<br/>pour $n$ entier $>0$|$\displaystyle \lim_{x\to -\infty} {e^x}=0$|
    {.petit}

### Limite $\ell$ en l'infini ( $\pm\infty$ )

!!! exp "Introduction graphique pour Limite $\ell$ en $+\infty$"
    !!! col __100
        !!! col __60
            :one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+\dfrac 1x$

            Il semble que les images $f(x)$ soient ***aussi proches de $2$*** que l'on veut, lorque $x$ est *suffisamment <b>grand</b>* (comprendre proche de $+\infty$):

            * $2<f(x)<2+10^{-3}$ pour $x>1000$
            * $2<f(x)<2+10^{-5}$ pour $x>10^5$

            **Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **est proche de $2$** (aussi *proche* que l'on veut)...
            $\Leftrightarrow$ plus on va vers la **droite**, et plus ***la (hauteur de la) courbe s'approche de la droite horizontale d'équation $y=2$*** (aussi proche que l'on veut)

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2+\dfrac 1x = 2$</enc>
        !!! col __40 center
            <encadre style="margin-bottom:1em;">Exemples de fonctions $f$ de limite $\ell$ en $+\infty$</encadre>
            <figure>
            <img src="img/2plus1surx_enplusinfini.png">
            <figcaption>
            fonction $x\mapsto 2+\dfrac 1x$
            </figcaption>
            </figure>
    !!! col __100
        !!! col __60
            :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2-\dfrac 1x$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2-\dfrac 1x = 2$</enc>
        !!! col __40 center
            <figure>
            <img src="img/2moins1surx_enplusinfini.png">
            <figcaption>
            fonction $x\mapsto 2-\dfrac 1x$
            </figcaption>
            </figure>
    !!! col __100
        !!! col __60
            :three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+ \dfrac {sin(100x)}{x}$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2+\dfrac {sin(100x)}{x} = 2$</enc>
        !!! col __40 center
            <figure>
            <img src="img/2plussinxsurx_enplusinfini.png">
            <figcaption>
            fonction $x\mapsto 2+\dfrac {sin(100x)}{x}$
            </figcaption>
            </figure>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $\ell$, quand $x$ tend vers $+\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to +\infty} f(x) = \ell$
    * ou encore que: $f(x) \rightarrow \ell$ quand $x \rightarrow +\infty$

!!! def
    On dit aussi dans ce cas que la droite $d$ d'équation $y=\ell$ est une <red>asymptote horizontale en $+\infty$</red> à la courbe $\mathcal C_f$ de $f$.

!!! exp
    La droite $d$ d'équation $y=2$ est une asymptote horizontale en $+\infty$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=2+\dfrac 1x$

!!! exp "Introduction graphique pour Limite $\ell$ en $-\infty$"
    !!! col __100
        !!! col __60
            :one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2-\dfrac 1x$

            Il semble que les images $f(x)$ soient **aussi proches de $2$** que l'on veut, lorque $x$ est *suffisamment <b>petit</b>* (comprendre proche de $-\infty$):

            * $2<f(x)<2+10^{-3}$ pour $x< -1000$
            * $2<f(x)<2+10^{-5}$ pour $x< -10^5$

            **Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **est proche de $2$** (aussi *proche* que l'on veut)...
            $\Leftrightarrow$ plus on va vers la **gauche**, et plus ***la (hauteur de la) courbe s'approche de la droite horizontale d'équation $y=2$*** (aussi proche que l'on veut)

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2-\dfrac 1x = 2$</enc>
        !!! col __40 center
            <encadre style="margin-bottom:1em;">Exemples de fonctions $f$ de limite $\ell$ en $-\infty$</encadre>
            <figure>
            <img src="img/2moins1surx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto 2-\dfrac 1x$
            </figcaption>
            </figure>
    !!! col __100
        !!! col __60
            :two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+\dfrac 1x$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2+\dfrac 1x = 2$</enc>
        !!! col __40 center
            <figure>
            <img src="img/2plus1surx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto 2+\dfrac 1x$
            </figcaption>
            </figure>
    !!! col __100
        !!! col __60
            :three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= 2+\dfrac {sin(100x)}{x}$

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2+\dfrac {sin(100x)}{x} = 2$</enc>
        !!! col __40 center
            <figure>
            <img src="img/2plussinxsurx_enmoinsinfini.png">
            <figcaption>
            fonction $x\mapsto 2+\dfrac {sin(100x)}{x}$
            </figcaption>
            </figure>

!!! def
    On dit dans ce cas que <red>la fonction $f(x)$ a pour limite $\ell$, quand $x$ tend vers $-\infty$</red>, et on note:

    * $\displaystyle \lim_{x\to -\infty} f(x) = \ell$
    * ou encore que: $f(x) \rightarrow \ell$ quand $x \rightarrow -\infty$

!!! def
    On dit aussi dans ce cas que la droite $d$ d'équation $y=\ell$ est une <red>asymptote horizontale en $-\infty$</red> à la courbe $\mathcal C_f$ de $f$.

!!! exp
    La droite $d$ d'équation $y=2$ est une asymptote horizontale en $-\infty$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=2+\dfrac 1x$

## Lien entre Limite $\ell$ et limite $0$ en l'infini

L'étude des Limites $\ell$ se ramène en quelque sorte à l'étude des limites $0$, grâce à la propriété suivante:

!!! pte "Lien entre Limite L et Limite 0 en l'infini"
    |Lien entre Limite $\ell$ et Limite $0$|
    |:-:|
    |$\displaystyle \lim_{x\to +\infty} f(x)=\ell \Leftrightarrow \displaystyle \lim_{x\to +\infty} (f(x)-\ell)=0$|
    |$\displaystyle \lim_{x\to -\infty} f(x)=\ell \Leftrightarrow \displaystyle \lim_{x\to -\infty} (f(x)-\ell)=0$|

!!! mth "Comment montrer qu'une limite vaut $\ell$ en l'infini ?"
    * Pour démontrer que la limite de $f(x)$ vaut $\ell $ en l'infini ($\pm \infty$), il suffit de démontrer que la limite de $f(x)-\ell$ vaut $0$ en l'infini ($\pm \infty$)
    * Pour cela on dispose de fonctions de références de limite $0$ en l'infini ($\pm \infty$)

!!! exp
    Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup ]0;+\infty[$, par $f(x)=3-\dfrac {1}{x^2}$

    On a $f(x)-3=-\dfrac {1}{x^2}$ donc : 

    * <bd>Limite en $+\infty$</bd>

        On sait que la fonction $x\mapsto \dfrac {1}{x^2}$ est une fonction de référence pour les limites $0$ en $+\infty$.
        donc $\displaystyle \lim_{x\to +\infty} (f(x)-3)= \lim_{x\to +\infty} \left( -\dfrac {1}{x^2} \right) = -0 = 0$
        Ceci prouve que $\displaystyle \lim_{x\to +\infty} f(x)=3$

    * <bd>Limite en $-\infty$</bd>

        On sait que la fonction $x\mapsto \dfrac {1}{x^2}$ est une fonction de référence pour les limites $0$ en $-\infty$.
        donc $\displaystyle \lim_{x\to -\infty} (f(x)-3)= \lim_{x\to -\infty} \left( -\dfrac {1}{x^2} \right) = -0 = 0$
        Ceci prouve que $\displaystyle \lim_{x\to -\infty} f(x)=3$

## Limite d'une fonction en un réel $a$ fini

### Limite $+\infty$ à droite en $0$

!!! exp "Introduction graphique pour Limite $+\infty$ en $0$"
    !!! col __100
        !!! col __60
            :one: Soit $f$ la fonction définie sur $D=]0;+\infty[$ par $f(x)=\dfrac 1x$

            Il semble que les images $f(x)$ soient ***aussi grandes*** que l'on veut (comprendre proches de $+\infty$), lorque $x$ est *suffisamment <b>"petit", au sens de proche de $0$ (tout en restant $>0$, à cause de l'ensemble de définition $D$ )</b>* :

            * $f(x)>10^{3}$ pour $0<x<0,001$
            * $f(x)>10^{5}$ pour $0<x<10^{-5}$

            **Language Humain :** plus on **s'approche de $0$, tout en restant $>0$**, et plus la *hauteur* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut)
            $\Leftrightarrow$ plus on **s'approche de zéro en venant de la droite**, et plus ***la (hauteur de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut)

            <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to0} \dfrac 1x = +\infty$</enc>

        !!! col __40 center
            <encadre style="margin-bottom:1em;">Exemples de fonctions $f$ de limite $+\infty$ en $0$</encadre>

            <figure>

            <img src="img/1surx_en0plus.png">

            <figcaption>

            fonction $x\mapsto \dfrac 1x$

            </figcaption>

            </figure>

!!! remarques
    * Il faudrait noter <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} \dfrac 1x=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} \dfrac 1x = +\infty$</enc> .  
    Il s'agit en effet d'une notation plus précise, mais plus lourde. En fait, il s'agit d'un sous-entendu de la notation. 
    Dans une notation de limite "lorsque $x\to0$", il est toujours sous-entendu que $x$ n'est jamais égal à $0$ : ainsi, <enc>$x$ tend vers $0$ **sans jamais l'atteindre**</enc>
    * De manière plus générale, en notant $D$ l'ensemble de définition de la fonction $f$:  
    Il faudrait noter <enc>$\lim\limits_{\substack{x \to 0 \\ x\in D}} \dfrac 1x=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} \dfrac 1x = +\infty$</enc> .  
    Il s'agit encore d'une notation plus précise, mais plus lourde. En fait, il s'agit encore d'un sous-entendu de la notation. 
    Dans une notation de limite "lorsque $x\to0$", il est toujours sous-entendu que <enc>$x$ tend vers $0$ (avec $x\neq 0$) **tout en restant dans l'ensemble de définition $D$ de $f$**</enc> .
        
        Cela suppose néanmoins que ce soit possible..., ce qui peut dépendre de l'ensemble $D$ considéré...  
        Remarquons que c'est en particulier possible lorsque $D$ est un intervalle ou une réunion d'intervalles, ET lorsque :

        * ou bien, $0\in D$ c'est-à-dire lorsque $0$ est à l'intérieur de l'ensemble $D$ (ce qui n'est PAS le cas dans cet exemple)
        * ou bien, $0$ est l'une des extrémités de $D$, sans pour autant appartenir à $D$ (c'est qui EST le cas dans cet exemple)

    Nous n'utiliserons pas ces lourdeurs innécessaires, **sauf dans les situations exigant davantage de précision** 
    
    * <bd @bleu>Note</bd> La situation de l'exemple précédent EXIGE davantage de précision... car $x\in D \Leftrightarrow x>0$, et on voit bien que la situation graphique n'est pas la même à gauche de $0$, et à droite de $0$..

!!! def "Notion de Limite à droite de $f$ en $0$ et Notation $0^+$"
    On note quelquefois, pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac 1x=+\infty$</enc> $\,\,$ ou encore <enc>$\displaystyle \lim_{x\to 0^+} \dfrac {1}{x}=+\infty$</enc>  

    Plus généralement, pour une fonction $f$ quelconque dans cette même situation graphique, on dit que:

    * <red>la limite à droite de $f$ en $0$ vaut $+\infty$</red>, et on note:
    
        <center><enc>$\displaystyle \lim_{x\to 0^+} f(x) = \lim\limits_{\substack{x \to 0 \\ x>0}} f(x) = +\infty$</enc> $\,$ </center>
   
    * <red>la fonction $f$ tend vers $+\infty$ quand **$x$ tend vers $0$ par la droite</red>** (i.e. tout en restant strictement positif), et on note:

    <center><enc>$f(x)\to +\infty$ quand $x\to 0^+$</enc></center>

!!! def "Asymptote Verticale (à droite)"
    On dit dans ce cas que la **droite verticale** d'équation $x=0$ est <red>une asymptote verticale (à droite) à la courbe $\mathcal C_f$ de $f$</red>

### Limite $+\infty$ à gauche en $0$

!!! exp "Limite infinie en $0$ (suite)"
    !!! col __60
        :two: on pourrait dire de même pour la fonction $g$ définie sur $D=]-\infty;0[$ par $g(x)=-\dfrac 1x$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to0} -\dfrac 1x = +\infty$</enc>
    !!! col __40 center
        <figure>
        <img src="img/moins1surx_en0moins.png">
        <figcaption>
        fonction $x\mapsto -\dfrac 1x$
        </figcaption>
        </figure>

!!! Remarques
    * Il faudrait noter <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} -\dfrac 1x=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} -\dfrac 1x = +\infty$</enc> .  
    Idem que précédemment, il s'agit d'un sous-entendu de la notation.
    * De manière plus générale, en notant $D$ l'ensemble de définition de la fonction $f$ :  
    Il faudrait noter <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in D}} -\dfrac 1x=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} -\dfrac 1x = +\infty$</enc> .  
    Idem que précédemment, il s'agit d'un sous-entendu de la notation.
    Nous n'utiliserons pas ces lourdeurs innécessaires, **sauf dans les situations exigeant davantage de précision** 
    
    * <bd @bleu>Note</bd> La situation de l'exemple précédent EXIGE davantage de précision... car $x\in D \Leftrightarrow x<0$, et on voit bien que la situation graphique n'est pas la même à gauche de $0$, et à droite de $0$..

!!! def "Notion de Limite à gauche de $f$ en $0$ et Notation $0^-$"
    On note quelquefois, pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} -\dfrac 1x=+\infty$</enc> $\,\,$ ou encore <enc>$\displaystyle \lim_{x\to 0^-} -\dfrac {1}{x}=+\infty$</enc>  

    Plus généralement, pour une fonction $f$ quelconque dans cette même situation graphique, on dit que:
    
    * <red>la limite à gauche en $0$ de $f$ vaut $+\infty$</red> $\,$ et on note :  

        <center><enc>$\displaystyle \lim_{x\to 0^-} f(x) = \lim\limits_{\substack{x \to 0 \\ x<0}} f(x) = +\infty$</enc> $\,$ </center>

    * <red>la fonction $f$ tend vers $+\infty$ quand **$x$ tend vers $0$ par la gauche</red>** (i.e. tout en restant strictement négatif) $\,$ et on note:

    <center><enc>$f(x)\to +\infty$ quand $x\to 0^-$</enc></center>

!!! def "Asymptote Verticale (à gauche)"
    On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une <red>asymptote verticale (à gauche) à la courbe $\mathcal C_f$</red>

### Limite $+\infty$ en $0$

#### Introduction

!!! exp
    !!! col __60
        :three: on pourrait dire de même pour la fonction $h$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $h(x)=\dfrac {1}{x^2}$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to0} \dfrac {1}{x^2} = +\infty$</enc>
    !!! col __40 center
        <figure>
        <img src="img/1surx2_en0.png">
        <figcaption>
        fonction $x\mapsto \dfrac {1}{x^2}$
        </figcaption>
        </figure>

!!! Remarques
    * Il faudrait noter <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} \dfrac {1}{x^2}=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} \dfrac {1}{x^2} = +\infty$</enc>  
    Il s'agit d'un sous-entendu de la notation, plus précis mais plus lourd.  
    * De manière plus générale, en notant $D$ l'ensemble de définition de $f$:  
    Il faudrait noter, de manière plus précise mais plus lourde:   
    <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in D}} \dfrac {1}{x^2}=+\infty$</enc> $\,\,$ au lieu de <enc>$\displaystyle \lim_{x\to0} \dfrac {1}{x^2} = +\infty$</enc>   
    Idem que précédemment, il s'agit d'un sous-entendu de la notation. Plus précis mais plus lourd.  
    Nous n'utiliserons pas ces lourdeurs innécessaires, **sauf dans les situations exigeant davantage de précision**.
    * <bd @bleu>Note</bd> La situation précédente N'EXIGE PAS davantage de précision, car $x\in D \Leftrightarrow x\neq 0$, ce qui est déjà un sous-entendu de la notation. Et la situation graphique est symétrique à gauche de $0$ et à droite de $0$.

#### Limite vs Limite à droite vs Limite à gauche

!!! info "Point Info : Limite en $0$ vs Limite <b>à gauche</b> en $0$ vs Limite <b>à droite</b> en $0$"
    Remarquer que dans le cas général pour une fonction $f$, tous les cas de figure suivants sont possibles:  

    * la limite à gauche ET la limite à droite existent toutes les deux, mais elles ne sont **pas forcément** égales entre elles:  
        * ou bien elles sont égales entre elles
        * ou bien elles sont différentes
    * seule la limite à gauche existe, ou bien, 
    * seule la limite à droite existe, ou bien,
    * aucune des deux limites n'existe (ni la limite à gauche, ni la limite à droite)

#### Existence d'une Limite en $0$

!!! exp "Existence d'une Limite $+\infty$ en $0$"
    !!! col __65
        Pour la fonction $f(x) = \dfrac {1}{x^2}$ définie sur $D=]-\infty;0[ \cup ]0;+\infty[$

        * La limite à gauche en $0$ existe, et vaut $+\infty$
        
        <center>
        <enc>$\displaystyle \lim_{x\to 0^-} \dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^2}=+\infty$</enc>
        </center>

        * La limite à droite en $0$ existe, et vaut $+\infty$

        <center>
        $\,\,$ <enc>$\displaystyle \lim_{x\to 0^+} \dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^2}=+\infty$</enc>
        </center>

        * DONC La limite en $0$ existe, et vaut $+\infty$

        <center>
        $\,\,$ <enc>$\displaystyle \lim_{x\to 0} \dfrac {1}{x^2}= \lim_{x\to 0^-} \dfrac {1}{x^2} = \lim_{x\to 0^+} \dfrac {1}{x^2} = +\infty$</enc>
        </center>
    !!! col __35 center
        <figure>
        <img src="img/1surx2_en0.png">
        <figcaption>
        fonction $x\mapsto \dfrac {1}{x^2}$
        </figcaption>
        </figure>

!!! def "Existence de la limite en $0$"
    On dit que <red>la limite de $f$ en $0$ existe</red> lorsque :
    
    * la limite de $f$ à gauche en $0$ existe, et 
    * la limite de $f$ à droite en $0$ existe, et
    * elles sont égales entre elles

    Dans ce cas, on dit que la limite de $f$ en $0$ (existe et qu'elle) est égale à la limite à gauche en $0$ et à la limite à droite en $0$, (ici $+\infty$), et (dans notre exemple) on note:

    <center><enc>$\displaystyle \lim_{x\to 0} f(x) = +\infty$</enc> $\,$</center>
    
    ou encore que
    
    <center><enc>$f(x) \rightarrow +\infty$ quand $x \rightarrow 0$</enc></center>

!!! def "Asymptote Verticale"
    Lorsque la limite en $0$ existe et est infinie ($+\infty$ ou $-\infty$), on dit que la **droite verticale** d'équation $x=0$ est <red>une asymptote verticale à la courbe $\mathcal C_f$ en $0$</red>. Ainsi, 
    
!!! pte
    La **droite verticale** $d$ d'équation $x=0$ est une <red>asymptote verticale en $0$</red> (resp. en $0^-$, ou en $0^+$, quand on veut être plus précis) à la courbe $\mathcal C_f$ de $f$, lorsque la situation graphique correspond à n'importe lequel des trois cas suivants:

    * ou bien la limite en $0$ existe et vaut $+\infty$ (donc la limite à gauche et à droite existent aussi)
    * ou bien seulement la limite à gauche en $0$ existe et vaut $+\infty$
    * ou bien seulement la limite à droite en $0$ existe et vaut $+\infty$

#### Non Existence d'une Limite en $0$

!!! exp "NON Existence d'une Limite $+\infty$ en $0$"
    !!! col __60
        :four: <b>Contre-Exemple</b> : La fonction $k$ définie sur $D=]-\infty;0[\cup ]0;+\infty[$ par $k(x)=\dfrac 1x$

        * <bd>Limite à gauche en $0$</bd>
        
            <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac 1x = \lim_{x\to 0^-} \dfrac 1x = \displaystyle \lim\limits_{\substack{x \to 0 \\ x\in ]-\infty;0[}} \dfrac 1x = -\infty$</enc>

            La limite à gauche en $0$ existe et vaut $-\infty$
        
        * <bd>Limite à droite en $0$</bd>
        
            <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac 1x = \lim_{x\to 0^+} \dfrac 1x = \displaystyle \lim\limits_{\substack{x \to 0 \\ x\in ]0;+\infty[}} \dfrac 1x = +\infty$</enc>
            
            La limite à droite en $0$ existe et vaut $+\infty$
    !!! col __40 clear
        <center>
        <figure>
        <img src="img/1surx_en0.png">
        <figcaption>
        fonction $x\mapsto \dfrac 1x$
        </figcaption>
        </figure>
        </center>

    * <bd>PAS de Limite en $0$</bd>  
        
        La notation $\displaystyle "\lim_{x\to 0} \dfrac 1x"$ est ambigüe, en effet:
        
        * la limite à gauche en $0$ existe, et 
        * la limite à droite en $0$ existe, 
        * mais elles ne sont pas égales entre elles

        * Remarquer qu'on peut toujours utiliser une notation $\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in I}} \dfrac 1x$ pour lever les ambigüités, où $I\subseteq D$ désigne un ensemble inclus ou égal à l'ensemble de définition $D$

    * <bd>Asymptote Verticale</bd> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une **asymptote verticale à la courbe $\mathcal C_f$**

!!! def "NON Existence de la limite en $0$"
    On dit que <red>la limite en $0$ n'existe pas</red> dans le cas contraire, càd lorsque l'on se trouve dans n'importe laquelle des $3$ situations distinctes suivantes :  

    <bd @bleu>Situation 1</bd> ou bien:  

    * La limite à gauche en $0$ n'existe pas

    <bd @bleu>Situation 2</bd> ou bien  

    * La limite à droite en $0$ n'existe pas
    
    <bd @bleu>Situation 3</bd> ou bien  

    * La limite à gauche en $0$ existe
    * La limite à droite en $0$ existe 
    * mais elles ne sont pas égales entre elles

!!! exp
    La droite $d$ d'équation $x=0$ est une asymptote verticale en $0$ à la courbe $\mathcal C_f$ de la fonction $f$ définie sur $D=]0;+\infty[$ par $f(x)=\dfrac {1}{\sqrt x}$

### Limite $-\infty$ en $0$

!!! exp "Introduction graphique pour Limite $-\infty$ en $0$"
    !!! col __60
        <bd @vert>:one: Limite $-\infty$ à gauche en $0$</bd>
        
        Soit $f$ la fonction définie sur $D=]-\infty;0[$ par $f(x)=\dfrac 1x$

        Il semble que les images $f(x)$ soient ***aussi petites*** que l'on veut (comprendre proches de $-\infty$), lorque $x$ est *suffisamment **"petit", au sens de proche de $0$ tout en restant $<0$*** :

        * $f(x)< -10^{3}$ pour $-0,001<x<0$
        * $f(x)< -10^{5}$ pour $-10^{-5}<x<0$

        **Language Humain :** plus on **s'approche de $0$, tout en restant $<0$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut)
        $\Leftrightarrow$ plus on **s'approche de zéro en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut)

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac 1x = \lim_{x\to 0^-} \dfrac 1x = \lim_{x\to 0} \dfrac 1x = -\infty$</enc></enc>
    !!! col __40
        <center>
        <encadre>Exemples de fonctions $f$ de limite $-\infty$ en $0$</encadre>
        <figure>
        <img src="img/1surx_en0moins.png">
        <figcaption>
        fonction $x\mapsto \dfrac 1x$
        </figcaption>
        </figure>
        </center>

    !!! col __100
        <bd>Remarque</bd> $\,$mêmes remarques sur les notations que pour $+\infty$ (etc... pour toute la suite)

        <bd>Asymptote Verticale</bd> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**
    !!! col __60
        <bd @vert>:two: Limite $-\infty$ à droite en $0$</bd>

        On pourrait dire de même pour la fonction $g$ définie sur $D=]0;+\infty[$ par $g(x)=-\dfrac 1x$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} -\dfrac 1x = \lim_{x\to 0^+} -\dfrac 1x =\lim_{x\to 0} -\dfrac 1x=-\infty$</enc></enc>

        <bd>Asymptote Verticale</bd> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une **asymptote verticale à la courbe $\mathcal C_f$**
    !!! col __40
        <center>
        <figure>
        <img src="img/moins1surx_en0plus.png">
        <figcaption>
        fonction $x\mapsto -\dfrac 1x$
        </figcaption>
        </figure>
        </center>
    !!! col __60
        <bd @vert>:three: Limite $-\infty$ en $0$</bd>

        On pourrait dire de même pour la fonction $h$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $h(x)=-\dfrac {1}{x^2}$

        <bd>Notation</bd> $\,\,$ <enc>$\displaystyle \lim_{x\to 0} -\dfrac {1}{x^2}=-\infty$</enc></enc>

        * La limite à gauche en $0$ existe et vaut $-\infty$ 
        
        <center>
        <enc>$\displaystyle \lim_{x\to 0^-} -\dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x<0}} -\dfrac {1}{x^2}=-\infty$</enc>
        </center>

        * La limite à droite en $0$ existe et vaut $-\infty$ 
        
        <center>
        <enc>$\displaystyle \lim_{x\to 0^+} -\dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x>0}} -\dfrac {1}{x^2}=-\infty$</enc>
        </center>

        * DONC La limite en $0$ existe, et vaut $-\infty$

        <center>

        <enc>$\displaystyle \lim_{x\to 0} -\dfrac {1}{x^2} = \lim_{x\to 0^-} -\dfrac {1}{x^2} = \lim_{x\to 0^+} -\dfrac {1}{x^2} = -\infty$</enc>

        </center>

    !!! col __40 clear
        <center>
        <figure>
        <img src="img/moins1surx2_en0.png">
        <figcaption>
        fonction $x\mapsto -\dfrac {1}{x^2}$
        </figcaption>
        </figure>
        </center>

    <bd>Asymptote Verticale</bd> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**

!!! def "Limite $-\infty$ en $0$"
    On dit dans les cas précédents (non ambigüs) que **la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $0$**, et on note:

    <center><enc>$\displaystyle \lim_{x\to 0} f(x) = -\infty$</center></center>

    ou encore

    <center><enc>$f(x) \rightarrow -\infty$ quand $x \rightarrow 0$</enc></center>

!!! def "Asymptote Verticale en $0$ (deuxième situation)"
    On dit que la **droite verticale** $d$ d'équation $x=0$ est une <red>asymptote verticale en $0$</red> (resp. en $0^-$, ou en $0^+$, quand on veut être plus précis) à la courbe $\mathcal C_f$ de $f$, lorsque l'on se trouve dans n'importe lequel des trois cas suivants:

    * ou bien la limite en $0$ existe et vaut $-\infty$ (donc la limite à gauche et à droite existent aussi)
    * ou bien seulement la limite à gauche en $0$ existe et vaut $-\infty$
    * ou bien seulement la limite à droite en $0$ existe et vaut $-\infty$

!!! exp
    La droite verticale $d$ d'équation $x=0$ est une asymptote verticale en $0$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=-\dfrac {1}{\sqrt x}$

#### Fonctions de Référence de Limite infinie en $0$

!!! pte "Limites infinies de Référence en $0$"
    |>|>|Fonctions de Référence de Limite $\pm\infty$ en $0$|
    |:-:|:-:|:-:|
    |Limite à gauche en $0$ | Limite à droite en $0$ | Limite en $0$|
    |$\varnothing$ | $\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{\sqrt{x}} = \lim_{x\to 0^+} \dfrac {1}{\sqrt {x}} = \lim_{x\to 0} \dfrac {1}{\sqrt {x}} = +\infty$ | $\displaystyle \lim_{x\to 0} \dfrac {1}{\sqrt {x}} = +\infty$ |
    |$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x} = \lim_{x\to 0^-} \dfrac {1}{x} = -\infty$ | $\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x} = \lim_{x\to 0^+} \dfrac {1}{x} = +\infty$ | (n'existe pas) |
    |Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^n} = \lim_{x\to 0^-} \dfrac {1}{x^n} = -\infty$ | Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^n} = \lim_{x\to 0^+} \dfrac {1}{x^n} = +\infty$ | (n'existe pas) |
    |$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^2} = \lim_{x\to 0^-} \dfrac {1}{x^2} = +\infty$ | $\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^2} = \lim_{x\to 0^+} \dfrac {1}{x^2} = +\infty$ | $\displaystyle \lim_{x\to 0} \dfrac {1}{x^2} = +\infty$ |
    |Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^n} = \lim_{x\to 0^-} \dfrac {1}{x^n} = +\infty$ | Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^n} = \lim_{x\to 0^+} \dfrac {1}{x^n} = +\infty$ | Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim_{x\to 0} \dfrac {1}{x^n} = +\infty$|
    {.petit}

### Limite infinie en un réel $a$

Les mêmes cas de figure existent, lorsque l'on calcule la limite de $f(x)$, quand $x$ tend vers un réel $a$ avec $x$ restant dans l'ensemble de définition $D$ de $f$.

#### Différents Cas de Figure

!!! col __25
    <center>
    <figure style="width:95%;">
    <img src="img/1surxmoins2plus1_en2.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f(x) = \dfrac {1}{x-2}+1$
    </figcaption>
    </figure>
    </center>
    <bd>Limite à gauche en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} f(x) = \lim_{x\to2^-} f(x) = -\infty$  
    <bd>Limite à droite en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} f(x) = \lim_{x\to2^+} f(x) = +\infty$  
    <bd>Limite en $2$</bd>  
    Limite à gauche $\ne$ Limite à droite  
    donc $\displaystyle \lim_{x\to2} f(x)$ n'existe pas.  
    Asymptote verticale : $d:x=2$
!!! col __25
    <center>
    <figure style="width:95%;">
    <img src="img/moins1surxmoins2plus1_en2.png" style="margin-bottom:1em;width:100%;">
    <figcaption>
    $g(x) = -\dfrac {1}{x-2}+1$
    </figcaption>
    </figure>
    </center>
    <bd>Limite à gauche en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} g(x) = \lim_{x\to2^-} g(x) = +\infty$  
    <bd>Limite à droite en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} g(x) = \lim_{x\to2^+} g(x) = -\infty$  
    <bd>Limite en $2$</bd>  
    Limite à gauche $\ne$ Limite à droite  
    donc $\displaystyle \lim_{x\to2} g(x)$ n'existe pas.  
    Asymptote verticale : $d:x=2$
!!! col __25
    <center>
    <figure style="width:95%;">
    <img src="img/1surxmoins2carreplus1_en2.png" style="margin-bottom:1em;width:100%;">
    <figcaption>
    $h(x) = \dfrac {1}{(x-2)^2}+1$
    </figcaption>
    </figure>
    </center>
    <bd>Limite à gauche en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} h(x) = \lim_{x\to2^-} h(x) = +\infty$  
    <bd>Limite à droite en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} h(x) = \lim_{x\to2^+} h(x) = +\infty$  
    <bd>Limite en $2$</bd>  
    Limite à gauche $=$ Limite à droite  
    donc $\displaystyle \lim_{x\to2} h(x)$ existe et vaut $+\infty$  
    Asymptote verticale : $d:x=2$
!!! col __25 clear
    <center>
    <figure style="width:95%;">
    <img src="img/moins1surxmoins2carreplus1_en2.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $k(x) = -\dfrac {1}{(x-2)^2}+1$
    </figcaption>
    </figure>
    </center>
    <bd>Limite à gauche en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} k(x) = \lim_{x\to2^-} k(x) = -\infty$  
    <bd>Limite à droite en $2$</bd>  
    $\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} k(x) = \lim_{x\to2^+} k(x) = -\infty$  
    <bd>Limite en $2$</bd>  
    Limite à gauche $=$ Limite à droite  
    donc $\displaystyle \lim_{x\to2} k(x)$ existe et vaut $-\infty$  
    Asymptote verticale : $d:x=2$

#### Définitions

!!! def "Limite infinie à gauche ou à droite en un réel $a$"
    <bd>Limite $+\infty$ à gauche en $a$</bd> plus on **s'approche de $a$, tout en restant $<a$**, et plus la *"hauteur"* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut). On note :  
    <center><enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = +\infty$</enc></center>  
    <bd>Limite $+\infty$ à droite en $a$</bd> plus on **s'approche de $a$, tout en restant $>a$**, et plus la *"hauteur"* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la droite**, et plus ***la ("hauteur" de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut). On note :  
    <center><enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x) = +\infty$</enc></center>  
    <bd>Limite $-\infty$ à gauche en $a$</bd> plus on **s'approche de $a$, tout en restant $<a$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut). On note :  
    <center><enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = -\infty$</enc></center>  
    <bd>Limite $-\infty$ à droite en $a$</bd> plus on **s'approche de $a$, tout en restant $>a$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la droite**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut). On note :  
    <center><enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x) = -\infty$</enc></center>

!!! def "Asymptote verticale d'équation $(d) : x=a$"
    Lorsqu'au moins l'une des 4 limites précédentes existe et est infinie, on dit que la droite verticale <enc>$d:x=a$</enc> $\,$ est une <red>asymptote verticale en $a$ à la courbe $\mathcal C_f$</red>

!!! def "Limite infinie en a et lien avec la Limite (infinie) à gauche (resp. à droite) en $a$"
    Soit $a$ un réel tel que $x$ puisse tendre vers $a$ tout en restant dans $D$.

    <bd @vert>:one: $\,$ ou bien $a$ est une extrémité de $D$</bd>, auquel cas:

    * Si $a$ est une **extrémité gauche** de $D$ (donc $x\rightarrow a$ par la droite...) et que la limite infinie **à droite** en $a$ existe,  Alors dans ce cas on dit que la limite infinie en $a$ existe, et vaut la limite à droite en $a$  
    On note  :
    <center><enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x)$</enc></center>
    * Si $a$ est une **extrémité droite** de $D$ (donc $x\rightarrow a$ par la gauche...) et que la limite infinie **à gauche** en $a$ existe, Alors dans ce cas on dit que la limite infinie en $a$ existe, et vaut la limite à gauche en $a$  
    On note  
    <center><enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x)$</enc></center>

    <bd @vert>:two: $\,$ ou bien $a\in D$</bd> (c'est-à-dire $a$ est à l'intérieur de $D$), auquel cas:  
    La limite inifinie en $a$ existe $\Leftrightarrow$ :

    * Les limites infinies à gauche <b>ET</b> à droite en $a$ existent toutes les deux, et
    * Limite à gauche en $a$ = Limite à droite en $a$
    Dans ce cas on dit que la limite en $a$ existe, et on pose: Limite en $a=$ Limite à gauche en $a = $ Limite à droite en $a$
    On note <enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x)$</enc>

#### Fonctions de Référence de Limite infinie en $a$

<pte data="Fonctions de Référence de Limite infinie en a">

!!! pte "Fonctions de Référence de Limite infinie en $a$"
    |>|>|Fonctions de Référence de Limite $\pm\infty$ en $a$|
    |:-:|:-:|:-:|
    |Limite à gauche en $a$|Limite à droite en $a$|Limite en $a$|
    |$\varnothing$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{\sqrt{x-a}} = \lim_{x\to a^+} \dfrac {1}{\sqrt {x-a}} = \lim_{x\to a} \dfrac {1}{\sqrt {x-a}} = +\infty$|$\displaystyle \lim_{x\to a} \dfrac {1}{\sqrt {x-a}} = +\infty$||
    |$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{x-a} = \lim_{x\to a^-} \dfrac {1}{x-a} = -\infty$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{x-a} = \lim_{x\to a^+} \dfrac {1}{x-a} = +\infty$|(n'existe pas)|
    |Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^-} \dfrac {1}{(x-a)^n} = -\infty$|Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^+} \dfrac {1}{(x-a)^n} = +\infty$|(n'existe pas)|
    |$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^2} = \lim_{x\to a^-} \dfrac {1}{(x-a)^2} = +\infty$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^2} = \lim_{x\to a^+} \dfrac {1}{(x-a)^2} = +\infty$|$\displaystyle \lim_{x\to a} \dfrac {1}{(x-a)^2} = +\infty$|
    |Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^-} \dfrac {1}{(x-a)^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^+} \dfrac {1}{(x-a)^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim_{x\to a} \dfrac {1}{(x-a)^n} = +\infty$|
    {.petit}

### Limite finie en un réel $a$

Soit $f$ une fonction définie sur un intervalle $I$, et $\ell\in \mathbb R$ un nombre réel.
Soit un réel $a$ tel que:

* ou bien $a\in I$ est intérieur à $I$, 
* ou bien $a$ est une extrémité de $I$ si $a\notin I$

!!! def
    Dire que : &laquo; plus $x$ **s'approche de $a$**, et plus la *"hauteur"* $f(x)$ **est proche de $\ell$** &raquo; (aussi *proche* que l'on veut)  
    $\Leftrightarrow$ &laquo; plus $x$ **s'approche de $a$**, et plus ***la "hauteur" $f(x)$ de la courbe est proche de $\ell$*** &raquo; (aussi proche que l'on veut).  
    Dans ce cas, on dit que la limite de $f(x)$ en $a$ existe et vaut le réel fini $\ell$.  
    On note :  
    <center><enc>$\displaystyle \lim_{x\to a} f(x) = \ell$</enc></center>

!!! Remarque
    Cette notation sous-entend que :  
    
    <center><enc>$\displaystyle \lim_{x\to a} f(x) = \lim\limits_{\substack{x \to a \\ x\ne a}} f(x)$</enc></center> 
    
    $\,$et plus généralement que :
    
    <center><enc>$\,$ $\displaystyle \lim_{x\to a} f(x) = \lim\limits_{\substack{x \to a \\ x\in I, x\ne a}} f(x)$</enc></center>

!!! pte
    Dire que la limite de $f$ en $a$ existe et vaut $\ell$ est équivalent à dire que:

    * la limite à gauche $a$ existe et vaut $\ell$
    * la limite à droite $a$ existe et vaut $\ell$

!!! exp "Fonction pour laquelle Limite en $a$ n'existe pas"
    !!! col __60
        Soit $f$ la fonction définie sur $D=\mathbb R$ par $f(x)=E(x)=\lfloor x \rfloor$

        <bd>Limites</bd>  
        <center><enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \lfloor x \rfloor = \lim_{x\to 0^-} \lfloor x \rfloor = -1$</enc> et <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \lfloor x \rfloor = \lim_{x\to 0^+} \lfloor x \rfloor = 0$</enc></center>  
        <bd>Limite à gauche en $0$</bd> La Limite à gauche existe et vaut $-1$,  
        <bd>Limite à droite en $0$</bd> Limite à droite existe et vaut $0$  
        <bd>PAS de limite en $0$</bd> donc la limite en $0$ n'existe pas.  
        De la même manière, il n'existe pas de limite de $f$ en $a$ lorsque $a$ est un nombre entier $n$.
    !!! col __40
        <center>
        <encadre>Exemple d'une fonction $f$ n'ayant pas de limite finie : <br/>ni en $0$, ni en $1$, etc...</encadre>
        <figure>
        <img src="img/partieentiere.png">
        <figcaption>
        fonction partie Entière $x\mapsto E(x)=\lfloor x \rfloor$
        </figcaption>
        </figure>
        </center>

## Opérations sur les Limites de fonctions

### Notion de Forme Indéterminée (FI)

C'est exactement la même notion appiquée aux limites de fonctions

### Limite d'une Somme

<center>

|>|>|>|>|>|>|Limite d'une Somme de fonctions|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x)+g(x) \right)=...$|$\ell+\ell'$|$+\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|$-\infty$|

</center>

<bd>Résumé</bd>  (Idem que pour les Suites)

* Une seule FI pour l'Addition: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)+(-\infty)$ ou bien $(-\infty)+(+\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour l'addition
* la Règle des signes pour l'addition est préservée: $+(+\infty)=+\infty$ et $+(-\infty)=-\infty$

### Limite d'une Soustraction

<center>

|>|>|>|>|>|>|Limite d'une Soustraction de fonctions|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x)-g(x) \right)=...$|$\ell-\ell'$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|$+\infty$|<span style="color:red;">$FI$</span>|

</center>

<bd>Résumé</bd>  (Idem que pour les Suites)

* Une seule FI pour la Soutraction: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)-(+\infty)$ ou $(-\infty)-(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Soustraction
* la Règle des signes pour la soustraction est préservée: $-(+\infty)=-\infty$ et $-(-\infty)=+\infty$

### Limite d'un Produit

<center>

|>|>|>|>|>|>|>|>|>|Limite d'un Produit de fonctions|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell>0$|$\ell>0$|$\ell<0$|$\ell<0$|$+\infty$|$+\infty$|$-\infty$|$0$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x) \times g(x) \right)=...$|$\ell \times \ell'$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|$+\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

</center>

<bd>Résumé</bd> (Idem que pour les Suites)

* Une seule FI pour la Multiplication: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$0\times \infty$</red></encadre> $\quad($comprendre : $0\times(+\infty)$ ou $0\times(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Multiplication, <bd @vert>SAUF SUR LE RÉEL $\ell=0$ !!</bd> (FI)
* la Règle des signes pour le produit est préservée: $(+)\times(+)=+$, $(-)\times(-)=+$, $(+)\times(-)=-$, $(-)\times(+)=-$

### Limite d'un Quotient

|>|>|>|>|>|>|>|<bd @vert>Cas I</bd> Limite d'un Quotient de Fonctions $\dfrac {f(x)}{g(x)}$ avec <bd @jaune>$\displaystyle \lim_{x \to a} g(x) \ne 0$</bd> <br/>(et $\forall x \in \mathcal{V(a)} , g(x) \ne 0$)|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|et si $\displaystyle \lim_{x \to a} g(x)=...$|$\ell' \neq 0$|$+\infty$ ou $-\infty$|$\ell'>0$|$\ell'<0$|$\ell'>0$|$\ell'<0$|$+\infty$ ou $-\infty$|
|<nobr>alors  $\displaystyle \lim_{x \to a} \left( \frac{f(x)}{g(x)} \right)=...$</nobr>|$\dfrac \ell {\ell'}$|$0$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

|>|>|>|>|>|<bd @vert>Cas II</bd> Limite d'un Quotient de Suites $\dfrac {f(x)}{g(x)}$ avec <bd @jaune>$\displaystyle \lim_{x \to a} g(x) = 0$</bd> <br/>(et $\forall x \in \mathcal{V(a)} , g(x) \ne 0$)|
|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{x \to a} f(x)=...$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$0$|
|et si $\displaystyle \lim_{x \to a} g(x)=...$|$0^{+}$<br/>$=0$ en restant positif|$0^{+}$<br/>$=0$ en restant positif|$0^{-}$<br/>$=0$ en restant négatif|$0^{-}$<br/>$=0$ en restant négatif|$0$|
|<nobr>alors  $\displaystyle \lim_{x \to a} \left( \frac{f(x)}{g(x)} \right)=...$</nobr>|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

<bd>Résumé</bd> des deux tableaux précédents sous la forme d'un tableau à double entrée: (si vous préférez)

<center>

|>|>|>|>|>|>|>|Tableau résumant la Limite d'un Quotient $\dfrac {f(x)}{g(x)}$ avec $\forall x \in \mathcal{V(a)} , g(x) \ne 0$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=\ell$<br/>$\displaystyle \lim_{x \to a} g(x)=\ell'$|>|>|$0$|$\ell'>0$|$\ell'<0$|$+\infty$|$-\infty$|
|^|$0^{+}$|$0^{-}$|$0^{+/-}$|^|^|^|^|
|$\ell=0$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|$0$|$0$|$0$|$0$|
|$\ell>0$|$+\infty$|$-\infty$ |<bd @bleu>$PDL$</bd>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{+}$|$0^{-}$|
|$\ell<0$|$-\infty$|$+\infty$ |<bd @bleu>$PDL$</bd>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{-}$|$0^{+}$|
|$+\infty$|$+\infty$|$-\infty$|<bd @bleu>$PDL$</bd>|$+\infty$|$-\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|
|$-\infty$|$-\infty$|$+\infty$|<bd @bleu>$PDL$</bd>|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|

</center>

<bd @bleu>$PDL=$Pas De Limite</bd>voulant dire que l'on est sûr que la fonction $\left( \dfrac {f(x)}{g(x)} \right)$ n'admet <b>P</b>as <b>D</b>e <b>L</b>imite (ni réelle $\ell$, ni infinie).

**Résumé: (Idem pour les Suites)**

* Deux FI pour le Quotient: <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{0}{0}$</red></encadre> $\,\,\,$ et $\,\,$ <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{\infty}{\infty}$</red></encadre>
comprendre $\dfrac{0^+}{0^+}$, $\dfrac{0^-}{0^-}$, $\dfrac{0^+}{0^-}$, $\dfrac{0^-}{0^+}$, $\dfrac{0^{+/-}}{0^+}$, $\dfrac{0^{+/-}}{0^-}$, $\dfrac{0^{+}}{0^{+/-}}$, etc...$\,\,$ et $\,\,$ $\dfrac{+\infty}{+\infty}$, $\dfrac{-\infty}{-\infty}$, $\dfrac{+\infty}{-\infty}$, $\dfrac{-\infty}{+\infty}$, 
* Pour le quotient, l'$\infty$ **AU NUMÉRATEUR** :
  * ne l'emporte pas sur $\ell'=0^{+/-}$ **AU DÉNOMINATEUR**. Dans ce cas: PAS DE LIMITE
  * l'emporte sur tout réel $\ell'\ne 0$ **AU DÉNOMINATEUR**, 
  * ne l'emporte PAS sur l'$\infty$ **AU DÉNOMINATEUR** !! Dans ce cas : on a une FI, 
* Pour le quotient, $0$ **AU NUMÉRATEUR** l'emporte sur tout réel $\ell$ **AU DÉNOMINATEUR**, **SAUF SUR $\ell=0$ !!** Dans ce cas: on a une FI,
* la Règle des signes pour le quotient est préservée:
 $\dfrac{+}{+}=+$, $\dfrac{-}{-}=+$, $\dfrac{+}{-}=-$, $\dfrac{-}{+}=-$

### Résumé des Formes indéterminées (FI)

<center>

|>|>|>|>|Résumé des Formes indéterminées usuelles pour les Fonctions|
|:-:|:-:|:-:|:-:|:-:|
|$\infty-\infty$|$0\times \infty$|$\dfrac{0}{0}$|$\dfrac{\infty}{\infty}$|$1^{\infty}$|

</center>

En résumé, ce sont les mêmes que pour les suites.
La forme indéterminée $1^{\infty}$ peut sembler moins naturelle pour le moment, mais pourra être mieux comprise durant cette année, grâce à :

* l'étude de la fonction exponentielle $exp$, vue en 1ère,
* et l'étude de la fonction $ln$ qui sera étudiée en peu plus tard, 
* et grâce à la formule <encadre>$x^y=e^{y ln(x)}$</encadre>

