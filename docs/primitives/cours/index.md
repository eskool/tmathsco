# TMaths Co: Primitives

## Primitive.s d'une fonction $f$ sur $I$

!!! def
    Soit $f$ une fonction définie sur un intervalle $I$.
    Dire qu'une fonction $F$ est <red>UNE primitive de $f$ sur $I$</red>, signifie que:

    * $F$ est dérivable sur $I$
    * $F'=f$ sur $I$

!!! thm "d'Existence - provisoirement admis"
    Toute fonction $f$ **continue** sur $I$ admet (au moins) une primitive $F$ de $f$ sur un $I$

**Remarques:**

* Dire que $F'=f$ sur $I$ signifie que : $\forall x\in I, \quad F'(x)=f(x)$
* Par définition même, une primitive $F$ est forcément dérivable.
* Une fonction dérivable $F$ est une primitive de $f$ sur $I$ $\Leftrightarrow$ $f$ est la dérivée de $F$ sur $I$.  
  Autrement dit :
  <env>Si $f$ est dérivable (de dérivée $f'$) sur $I$, alors $f$ est UNE primitive de $f'$ sur $I$.
  </env>
  
  Intuitivement : 
  >la notion de fonction primitive peut être vue intuitivement comme **une dérivation à l'envers**</enc>
  >
  > **Issac Newton**

!!! mth
    Soit $F$ une fonction **dérivable**, **supposée connue explicitement en fonction de $x$**.  
    et soit $f$ une fonction **supposée connue explicitement en fonction de $x$**.
    Pour montrer que cette fonction $F$ explicite est UNE primitive de $f$ sur un intervalle $I$, il suffit donc de:
    * Calculer la dérivée de $F$ sur $I$
    * et vérifier que l'on a bien l'égalité $F'=f$ sur $I$

!!! ex
    Démontrer (dans la case *Démonstration*), pour chaque ligne du tableau, qu'UNE primitive de la fonction $f$ est la fonction $F$ correspondante, sur l'intervalle $I$ (à déterminer s'il n'est pas précisé). Proposer une autre primitive $G$ de la fonction $f$ sur $I$.

    |UNE primitive de $f(x)=...$|est $F(x)=...$|sur l'intervalle $I=...$|Démonstration|Une autre primitive $G$ ?|
    |:-:|:-:|:-:|:-:|:-:|
    |$1$|$x$|$\mathbb R$|$\cdots$|$\cdots$|
    |$1$|$x+4$|$\mathbb R$| $\cdots$|$\cdots$|
    |$2$|$2x$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2$|$2x+5$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2x$|$x^2$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x$|$\dfrac{x^2}{2}$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2x$|$x^2-3$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2x$|$x^2+\dfrac 14$|$\mathbb R$|$\cdots$|$\cdots$|
    |$6x-5$|$3x^2-5x+2$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2x+1$|$(x-3)(x+4)$|$\mathbb R$|$\cdots$|$\cdots$|
    |$24x-1$|$(3x-1)(4x+1)$|$\mathbb R$|$\cdots$|$\cdots$|
    |$3x^2$|$x^3$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^2$|$\dfrac{x^3}{3}$|$\mathbb R$|$\cdots$|$\cdots$|
    |$6x^2+10x$|$2x^3+5x^2-7$|$\mathbb R$|$\cdots$|$\cdots$|
    |$12x^2-6x+5$|$4x^3-3x^2+5x+8$|$\cdots$|$\cdots$|$\cdots$|
    |$\dfrac{-3}{x^2-4x+4}$|$\dfrac{x+1}{x-2}$|$\cdots$|$\cdots$|$\cdots$|
    |$\dfrac{1}{\sqrt x}$|$2\sqrt x$|$\cdots$|$\cdots$|$\cdots$|
    |$\dfrac{1}{x^2}$|$-\dfrac{1}{x}$|$\cdots$|$\cdots$|$\cdots$|
    |$e^x$|$e^x$|$\cdots$|$\cdots$|$\cdots$|
    |$e^{3x+2}$|$\dfrac{e^{3x+2}}{3}$|$\cdots$|$\cdots$|$\cdots$|
    |$e^{2x-5}(2x+1)$|$xe^{2x-5}$|$\cdots$|$\cdots$|$\cdots$|
    {.petit}

!!! exp
    1. On sait que la dérivée de $x\mapsto x^2$ est $x\mapsto 2x$ sur $I=\mathbb R$
    Autrement dit, la fonction $F$ définie par $F(x)=x^2$ est une fonction primitive de la fonction $f$ définie $f(x)=2x$ sur $I=\mathbb R$
    Nous venons donc de trouver (et prouver) une primitive $F$ de $f$ sur $I$  
    2. La fonction $F_1$ définie par $F_1(x)=x^2+1$ est une **autre** primitive de la fonction $f(x)=2x$ sur $I= \mathbb R$
    car pour tout réel $x\in I, \quad F_1'(x)=2x$  
    3. La fonction $F_3$ définie par $F_3(x)=x^2+3$ est une **autre** primitive de la fonction $f(x)=2x$ sur $I= \mathbb R$
    car pour tout réel $x\in I, \quad F_3'(x)=2x$  
    4. La fonction $F_{-2}$ définie par $F_{-2}(x)=x^2-2$ est une **autre** primitive de la fonction $f(x)=2x$ sur $I= \mathbb R$
    car pour tout réel $x\in I, \quad F_{-2}'(x)=2x$  
    5. Par contre, une seule parmi les $3$ primitives $F$ précédentes vérifie l'égalité $F(0)=3$. Laquelle? 
    **Conclusion de cet Exemple:**

    * Une primitive particulière $F$ de $f$ sur $I$ étant connue, on comprend par ce procédé reproductible qu'il est aisé d'inventer une infinité de primitives $F_k$ de $f$ sur $I$, définies par $F_k(x)=F(x)+k$ avec $k \in \mathbb R$ est un nombre réel constant. Ainsi,
    * S'il existe une primitive $F$ de $f$ sur $I$, Alors il en existe (au moins...) une infinité sous la forme $F_k=F+k$
    * Pour chaque valeur distincte de la constante $k\in \mathbb R$, il existe une fonction primitive $F_k$ distincte de toutes les autres. ATTENTION : rien ne prouve pour le moment que nous n'en avons pas oublié, c'est-à-dire que rien ne prouve pour le moment qu'il n'existe pas d'autres primitives $G$ de $f$ sur $I$ mais qui ne s'écriraient pas sous la forme $F_k=F+k$ avec $k\in \mathbb R$  constant. En fait, il n'en existe pas d'autres :

!!! pte "Forme Générale des Primitives d'une fonction f"
    Soit $f$ une fonction définie sur un intervalle $I$.
    Soit $F$ une primitive quelconque de $f$ sur $I$.
    L'ensemble de toutes les primitives de $f$ sur $I$ est constitué par les fonctions $F_k=F+k$ où $k$ décrit $\mathbb R$
    (sous-entendu, il n'en existe pas d'autres, que sous cette forme)

<env>Conséquences</env>

* On dit que **deux primitives quelconques d'une même fonction $f$ sur un intervalle $I$ diffèrent forcément d'une constante**.
* Si une fonction $f$ admet (au moins) une primitive $F$ de $f$ sur $I$, alors $f$ admet une infinité de primitives $F_k$ sur $I$.

??? demo
    Soit $F$ une primitive quelconque de $f$ sur $I$. 
    Il s'agit de montrer les deux sens suivants:
    <env>$\Rightarrow$</env> Montrons que Si $G$ est une fonction de la forme $G=F+k$, Alors $G$ est une primitive de $f$ sur $I$:
    Soit $G=F+k$ avec $k\in\mathbb R$, alorsG est dérivable sur $I$ car $F$ est dérivable sur $I$, et l'on a: $G'=F'=f$.
    donc $G$ est une primitive de $f$ sur $I$.
    <env>$\Leftarrow$</env> Montrons que Si $G$ est une primitive de $f$ sur $I$, Alors il existe une unique constante $k$ telle que $G=F+k$
    Soit $G$ une primitive de $f$ sur $I$, alors $G'=f$ sur $I$ et $F'=f$ sur $I$ également, donc $G'-F'=f-f=0$ sur $I$.
    Donc $(G-F)'=0$ sur $I$, ceci prouve que la fonction $G-F$ est une fonction constante sur $I$, ce qui prouve l'existence et l'unicité de la constante réelle $k$ telle que: $G-F=k$ sur $I$, donc $G=F+k$ sur $I$

!!! ex
    Renseigner le tableau de sorte que, pour chaque ligne, la fonction $F$ soit UNE primitive de $f$ sur l'intervalle $I$.

    <center>

    |UNE primitive de $f(x)=...$|est $F(x)=...$|sur l'intervalle $I=...$|$\quad \quad \quad $ Exemple d'autre primitive $G$ de $f$ $\quad \quad \quad $| Forme Générale de TOUTES les primitives de $f$|
    |:-:|:-:|:-:|:-:|:-:|
    |$3$|$\cdots$|$\mathbb R$|$\cdots$<stylo inline><rep>Hello</rep></stylo>|$\cdots$|
    |$-4$|$\cdots$|$\mathbb R$|$\cdots$<stylo><rep>Hello</rep></stylo>|$\cdots$|
    |$\dfrac 23$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$\sqrt 2$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x+5$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^2$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$4x^2$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$2x^2+3x$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^3$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^4$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^5$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$x^2+3x+1$|$\cdots$|$\mathbb R$|$\cdots$|$\cdots$|
    |$...$|$5x^4$|$\mathbb R$|$\cdots$|$\cdots$|
    |$...$|$5x^4$|$\mathbb R$|$\cdots$|$\cdots$|
    |$\dfrac 1x$|$...$|$]-\infty; 0[$ ou $]0;+\infty[$|$\cdots$|$\cdots$|
    |$...$|$\dfrac 1x$|$]0;+\infty[$|$\cdots$|$\cdots$|
    |$...$|$\dfrac 1x$|$]0;+\infty[$|$\cdots$<stylo><rep>Hello</rep></stylo>|$\cdots$|

    </center>

!!! pte "Unicité de la Primitive avec Conditions Initiales"
    Soit $f$ une fonction définie sur un intervalle $I$, (admettant au moins une primitive $F$ sur $I$).
    Il existe une **unique primitive** $G$ de $f$ sur $I$ vérifiant $G(x_0)=y_0$ où $x_0$ est un nombre réel donné de $I$ et $y_0$ est un nombre réel donné.

??? demo
    Supposons qu'il existe (au moins) une primitive $F$, quelconque, de $f$ sur $I$.
    On sait que la forme générale des primitives $G$ de $f$ est $G(x)=F_k(x)=F(x)+k$ où $k\in \mathbb R$ est une constante. 
    Autrement dit, une primitive $G$ quelconque de $f$ est forcément égale à l'une des fonctions $F_k$ pour une certaine valeur de $k$. 
    Montrer l'existence et l'unicité de $G$ vérifiant $G(x_0)=y_0$ est donc équivalent à montrer qu'il existe une unique valeur de $k$ telle que $G(x_0)=F_k(x_0)=F(x_0)+k=y_0$
    Or l'équation $F(x_0)+k=y_0 \Leftrightarrow k=y_0 - F(x_0)$ admet une unique solution. CQFD

## Formulaire

### Primitives de Référence (des fonctions usuelles)

|UNE primitive de $f(x)=...$|est $F(x)=...$|Forme Générale $F_k(x)=...$|sur l'intervalle $I=...$|
|:-:|:-:|:-:|:-:|
|$0$|$0$|$k \quad(=0+k)$|$\mathbb R$|
|$1$|$x$|$x+k$|$\mathbb R$|
|$2$|$2x$|$2x+k$|$\mathbb R$|
|$a\quad$ <br/>(avec $a\in\mathbb R$ constante)|$ax$|$ax+k$|$\mathbb R$|
|$x$ |$\dfrac{x^2}{2}$|$\dfrac{x^2}{2}+k$|$\mathbb R$|
|$x^2$ |$\dfrac{x^3}{3}$|$\dfrac{x^3}{3}+k$|$\mathbb R$|
|$x^n$ <br/>($n$ entier positif $n\ge0$, ou :<br/>$n$ entier négatif et $n\ne-1$) |$\dfrac{x^{n+1}}{n+1}$|$\dfrac{x^{n+1}}{n+1}+k$|<ul style="text-align:left;"><li>$\mathbb R\quad$ si $n$ entier positif $(n\ge0)$</li><li>$]-\infty;0[$ ou $]0;+\infty[$ si $n$ entier négatif $(n\lt0)$</li></ul>|
|$\dfrac 1x$ |$ln(x)$|$ln(x)+k$|$]0;+\infty[$|
|$\dfrac {1}{x^2}$ |$\dfrac {-1}{x}$|$\dfrac {-1}{x}+k$|$]-\infty;0[$ ou $]0;+\infty[$|
|$\dfrac{1}{\sqrt x}$ |$2\sqrt x$|$2\sqrt x+k$|$]0;+\infty[$|
|$e^x$ |$e^x$|$e^x+k$|$\mathbb R$|

### Opérations sur les Primitives

<env>Notations & Suppositions</env>
On suppose que l'on dispose de:

* une fonction $u$ qui admet une primitive $U$ sur $I$
* une fonction $v$ qui admet une primitive $V$ sur $I$

|UNE primitive de $f(x)=...$|est $F(x)=...$|Forme Générale $F_k(x)=...$|sur l'intervalle ...|
|:-:|:-:|:-:|:-:|
|$u+v$|$U+V$|$U+V+k$|$I$|
|$u-v$|$U-V$|$U-V+k$|$I$|
|$\alpha u$<br/> (avec $\alpha\in\mathbb R$ constante)|$\alpha U$|$\alpha U+k$|$I$|

!!! ex
    (à faire)

|UNE primitive de $f(x)=...$|est $F(x)=...$|Forme Générale $F_k(x)=...$|sur l'intervalle ..<br/>(ou l'ensemble...)|
|:-:|:-:|:-:|:-:|
|$u'u$|$\dfrac{u^2}{2}$|$\dfrac{u^2}{2}+k$|$I$|
|$u'u^{n}$<br/>($n$ entier positif $n\ge0$, ou :<br/>$n$ entier négatif et $n\ne-1$)|$\dfrac{u^{n+1}}{n+1}$|$\dfrac{u^{n+1}}{n+1}+k$|<ul style="text-align:left;"><li>$I$ pour $n\ge0$</li><li>$\{x\in I/ u(x)\ne0 \}$, pour $n\le-2$ </li></ul>|
|$\dfrac{u'}{u^2}$|$\dfrac{-1}{u}$|$\dfrac{-1}{u}+k$|$\{ x\in I$ pour lesquels $u(x)\ne0$ \}|
|$\dfrac{u'}{u}$|$ln(u)$|$ln(u)+k$|$\{x\in I$ pour lesquels $u(x)>0\}$|
|$e^{kx}$|$\dfrac{e^{kx}}{k}$|$\dfrac{e^{kx}}{k}+C^{te}$|$I$|
|$u'e^u$|$e^u$|$e^u+k$|$I$|

