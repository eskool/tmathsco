# TMATHSCO, Cours Probablités : Loi Géométrique

## Définition

!!! def "Loi Géométrique"
    On répète une épreuve de Bernoulli dont la probabilité de succès est $p$.  
    On note $X$ la variable aléatoire qui compte le **nombre d'épreuves nécessaires pour avoir le premier succès**.  
    La loi de probabilité de la variable aléatoire $X$ est appelée <red>Loi Géométrique de paramètre $p$</red>.  
    On dit aussi que $X$ <red>suit la loi Géométrique de paramètre $p$</red>, et on note <enc>$X \suitloi \mc G (p)$</enc>

!!! pte "Calcul de $P(X=k)$"
    Soit $X$ une variable aléatoire qui suit la loi Géométrique de paramètre $p$.  
    Pour tout entier naturel $k\ge 1$, $\quad$ <enc>$P(X=k) = p(1-p)^{k-1}$</enc>

??? demo
    On répète $k$ fois une épreuve de Bernoulli de paramètre $p$, càd telle que $P(S)=p$.  
    L'événement $\{X=k\}$ est réalisé lorsqu'on obtient exactement $k-1$ échecs $\overline{S}$, suivis d'un (premier) succès $S$. Donc $P(X=k) = (1-p)^{k-1}\times p$

## Espérance et Variance de la Loi Géométrique

!!! pte "Espérance et Variance de la Loi Géométrique"
    Soit $X$ une variable aléatoire qui suit la loi Géométrique de paramètre $p$.  
    :one: L'<red>Espérance</red> $E(X)$ est donnée par :  
    <center><enc>$E(X) = \dfrac 1p$</enc></center>
    :two: La <red>Variance</red> $V(X)$ est donnée par :  
    <center><enc>$V(X) = \dfrac {1-p}{p^2}$</enc></center>


??? demo "(Hors Programme) :rocket:"
    <bd @gris>Fonctions Utiles</bd>  
    Soit $f(x)$ la fonction définie sur $I=[0;1[$ par :  
    $
    \begin{equation}
    \displaystyle f(x) = \lim_{n \to +\infty} \left( 1+x+x^2+...+x^n \right) = \lim_{n \to +\infty} \left( \sum_{k=0}^n x^k \right) = \lim_{n \to +\infty} \left( \frac{1-x^{n+1}}{1-x} \right) = \frac{1}{1-x}
    \label{f}
    \end{equation}
    $  
    On peut montrer que :  
    $
    \begin{equation}
    \displaystyle f'(x) = \lim_{n \to +\infty} \left( 1+2x+3x^2+...+nx^{n-1} \right) = \lim_{n \to +\infty} \left( \sum_{k=1}^{n-1} kx^{k-1} \right) = \frac{1}{(1-x)^2}
    \end{equation}
    $  
    $
    \begin{equation}
    \displaystyle f''(x) = \lim_{n \to +\infty} \left( 2+3\times 2x+...+n(n-1)x^{n-2} \right) = \lim_{n \to +\infty} \left( \sum_{k=2}^{n-2} k(k-1)x^{k-2} \right) = \frac{2}{(1-x)^3}
    \end{equation}
    $  

    En particulier,  
    $
    \begin{align*}
    \displaystyle \lim_{n \to +\infty} \sum_{k=1}^{n-1} k^2x^{k-1} &= \lim_{n \to +\infty} x\sum_{k=2}^{n-1} k(k-1)x^{k-2} + \lim_{n \to +\infty} \sum_{k=1}^{n-1} kx^{k-1} \\
        &= \dfrac{2x}{(1-x)^3} + \dfrac{1}{(1-x)^2} \\
        &= \dfrac{2}{(1-x)^3} - \dfrac{1}{(1-x)^2} \\
    \end{align*}
    $  

    donc :

    $
    \begin{equation}
    \label{fvariance}
    \lim_{n \to +\infty} \sum_{k=1}^{n-1} k^2x^{k-1} = \dfrac{2}{(1-x)^3} - \dfrac{1}{(1-x)^2}
    \end{equation}
    $

    <bd @gris>Calcul de l'Espérance $E(X)$</bd>  
    Soit $X$ une variable aléatoire qui suit la Loi Géométrique de paramètre $p$.  
    On a :  

    $
    \begin{align*}
    \displaystyle E(X) &= \lim_{n \to +\infty} \left( P(X=1)\times 1 + P(X=2)\times 2 + \cdots + P(X=n)\times n \right) \\
        &= \lim_{n \to +\infty} \sum_{k=1}^n P(X=k)\times k \\
        &= \lim_{n \to +\infty} \sum_{k=1}^n p(1-p)^{k-1}\times k \\
        &= f'(1-p) \text{ d'après la formule } \ref{f} \\
        &= \dfrac{1}{1-(1-p)} \\
        &= \dfrac{1}{p} \\
    \end{align*}
    $

    <bd @gris>Calcul de la Variance $V(X)$</bd>  
    Soit $X$ une variable aléatoire qui suit la Loi Géométrique de paramètre $p$.  
    On a :  

    $
    \begin{align*}
    \displaystyle V(X) &= \lim_{n \to +\infty} \left[ P(X=1)\times 1^2 + P(X=2)\times 2^2 + \cdots + P(X=n)\times n^2 - E(X)^2 \right] \\
        &= \left( \lim_{n \to +\infty} \sum_{k=1}^n P(X=k)\times k^2 \right) - E(X)^2 \\
        &= \lim_{n \to +\infty} \sum_{k=1}^n k^2 p(1-p)^{k-1} - \left( \dfrac{1}{p} \right)^2 \text{ car } E(X) = \dfrac 1p \\
        &= \lim_{n \to +\infty} p\sum_{k=1}^n k^2(1-p)^{k-1} - \dfrac{1}{p^2} \\
        &= p \left[ \dfrac{2}{(1-(1-p))^3} - \dfrac{1}{(1-(1-p))^2} \right] - \dfrac{1}{p^2} \text{ d'après la formule } \ref{fvariance} \\
        &= p \left[ \dfrac{2}{p^3} - \dfrac{1}{p^2} \right] - \dfrac{1}{p^2} \\
        &= \dfrac{2}{p^2} - \dfrac{1}{p} - \dfrac{1}{p^2} \\
        &= \dfrac{1}{p^2} - \dfrac{p}{p^2} \\
        &= \dfrac{1}{p^2} - \dfrac{p}{p^2} \\
        &= \dfrac{1-p}{p^2} \\
    \end{align*}
    $

!!! exp
    On lance une pièce truquée dont la probabilité d'obtenir Pile est $0,3$.
    Soit $X$ la variable aléatoire qui compte le nombre de lancers nécessaires pour obtenir Pile.  
    $X$ suit la Loi Géométrique de paramètre $p=0,3$. Donc :
    
    * $E(X) = \dfrac{1}{0,3} \approx 3,33$.  
    En moyenne, donc, il faudra $3,33$ lancers pour obtenir le premier Succès $S$ (Pile)
    * $V(X) = \dfrac{1-0,3}{0,3^2} \approx 7,78$.  
    * $\sigma(X) = \sqrt{V(X)} = \sqrt{\dfrac{1-0,3}{0,3^2}} \approx 2,78$.  
