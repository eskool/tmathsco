# TMATHSCO, Probablités : Cours Loi Uniforme (Discrète)

## Définition

!!! notation
    L'ensemble de tous les nombres entiers de $1$ à $n$ est noté classiquement de l'une des deux manières suivantes :  

    * $\{1;2;...;n\}$
    * $\llb  1;n \rrb$

Dans toute la suite, nous noterons $\llb 1;n \rrb$ cet ensemble, mais on aurait tout aussi bien pu le noter $\{1;2;...;n\}$ à chaque fois.

!!! def
    Soit $X$ est une variable alétoire prenant ses valeurs dans $\{1;2;...;n\}$ avec $n\in \m N^*$.  
    On dit que <red>la loi de probablité de $X$ est la loi uniforme discrète</red>, ou que <red>la v.a. $X$ suit la loi uniforme discrète</red>, et on note quelquefois <enc>$X \suitloi \mc U(\llb 1;n \rrb)$</enc> $\,\,$ ou $\,\,$ <enc>$X \simarrow \mc U(\llb 1;n \rrb))$</enc> $\,\,$ ou $\,\,$ <enc>$X \sim \mc U(\llb 1;n \rrb))$</enc> $\,\,$ , lorque :
    
    * toutes les valeurs $k\in \llb 1;n \rrb$ prises par la v.a. $X$ sont équiprobables.  
    Autrement dit, lorsque  
    * $\forall k \in \llb 1;n \rrb, \quad P(X=k)=\dfrac 1n$

!!! pte "Espérance d'une va discrète suivant la loi Uniforme sur $\llb 1;n \rrb$"
    Soit $X$ une v.a. qui suit la loi uniforme sur $\llb 1;n \rrb$  
    Alors <enc>$E(X)=\dfrac {n+1}{2}$</enc>

!!! demo
    $\begin{align*}
    \displaystyle
    E(X) &= p_1 x_1 + p_2 x_2 +...+ p_r x_r \\
        &= \dfrac 1n\times 1+\dfrac 1n\times 2+...+\dfrac 1n\times n \\
        &= \dfrac 1n (1+2+...+n) \\
        &= \dfrac 1{\cancel n} \dfrac {\cancel{n}(n+1)}{2} \\
        &= \dfrac {n+1}{2}
    \end{align*}$

!!! pte "Variance d'une va discrète suivant la loi Uniforme sur $\llb 1;n \rrb$"
    Soit $X$ une v.a. qui suit la loi uniforme sur $\llb 1;n \rrb$  
    Alors <enc>$V(X)=\dfrac {n^2-1}{12}$</enc>

!!! demo
    $\begin{align*}
    \displaystyle
    V(X) &= p_1 x_1^2 + p_2 x_2^2 +...+ p_r x_r^2 - E(X)^2 \\
        &= \dfrac 1n\times 1^2+\dfrac 1n\times 2^2+...+\dfrac 1n\times n^2 -E(X)^2 \\
        &= \dfrac 1n (1^2+2^2+...+n^2) -E(X)^2 \\
        &= \dfrac 1{\cancel n} \dfrac {\cancel{n}(n+1)(2n+1)}{6} -E(X)^2 \quad \text{ car } 1^2+2^2+...=\dfrac {n(n+1)(2n+1)}{6} \\
        &= \dfrac {(n+1)(2n+1)}{6} - \left(\dfrac{n+1}{2}\right)^2 \\
        &= \dfrac {(n+1)(2n+1)}{6} - \dfrac{(n+1)^2}{4} \\
        &= \dfrac {2(n+1)(2n+1)}{12} - \dfrac{3(n+1)^2}{12} \\
        &= \dfrac {2(n+1)(2n+1)-3(n+1)(n+1)}{12} \\
        &= \dfrac {(n+1)[2(2n+1)-3(n+1)]}{12} \\
        &= \dfrac {(n+1)[4n+2-3n-3]}{12} \\
        &= \dfrac {(n+1)(n-1)}{12} \\
        &= \dfrac {(n^2-1)}{12} \\
    \end{align*}$
