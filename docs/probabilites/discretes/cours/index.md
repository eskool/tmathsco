# TMATHSCO : Probabilités. Variables Aléatoires Discrètes

## Variable Aléatoire Discrète

### Deux Exemples Introductoires

* Voici une Expérience aléatoire: 
    On lance deux fois de suite une pièce de monnaie supposée équilibrée , et on note à chaque lancer :
    
    * $F$ quand on obtient « FACE » et
    * $P$ lorsqu'on obtient « PILE »  
    L'ensemble des issues, appelé l'<red>univers (des possibles)</red>, est $\Omega=\{FF;FP;PF;PP\}$.  
    On note $\omega$ chacune des <red>issues</red> de $\Omega$.
    Toutes ces issues sont **équiprobables**, la loi est donc **équirépartie**.

!!! exp "Une variable aléatoire discrète $X$ sur $\Omega$"
    On convient que chaque « PILE » obtenu fait gagner $1\euro$ et que chaque « FACE » fait perdre $2\euro$.  
    La fonction $X$ qui à chaque issue $\omega$ de $\Omega$, associe le gain $x_i$ du joueur, prend comme valeurs $x_1=2$, $x_2=-1$ et $x_3=-4$ avec des probabilités $p_1$, $p_2$ et $p_3$ que l'on peut calculer :

    |Gain $x_i$|$2$|$-1$|$-4$|
    |:-:|-:-|:-:|:-:|
    |Probabilité<br>$p_i=P(X=x_i)$|$\dfrac 14$|$\dfrac 12$|$\dfrac 14$|
    {.nocolor}

    !!! col __50
        ![Pile ou Face](./img/img1_pileFace.png)

    !!! col __50
        Exemple de Calcul Détaillé :  
        $\begin{align*}
        p_2 &= P(X=-1) \\
            &= P(\{FP;PF\}) \\ 
            &= \dfrac 14+\dfrac 14 \\
            &= \dfrac 12
        \end{align*}$

!!! def
    $X$ est appelé une <red>variable aléatoire discrète</red>, en abrégé <red>v.a.</red> (ou <red>v.a.d.</red> si on veut préciser)

On résume cette loi de probabilité (associée à $X$) dans le tableau ci-dessus.

!!! notation "Événement $(X=x)$"
    On préfère donc noter : 

    |notation| au lieu de ..|
    |:-:|:-:|
    |$(X=4)$| $\{FF\}$|
    |$(X=-1)$| $\{FP;PF\}$|
    |$(X=2)$| $\{PP\}$|
    
    Plus précisément, $(X=x)$ désigne donc la réunion de toutes les issues $\omega$ telles que $X(\omega)=x$, ainsi :  
    <center><enc>$(X=x)=\{\omega\in \Omega\ \\/ X(\omega)=x\}$</enc></center>
    c'est donc un événement au sens des probabilités.

!!! exp "Une variable aléatoire discrète $Y$ sur $\Omega$"
    Soit $Y=
    \begin{cases}
    +2, \quad \text{Si deux fois la même face} \\
    -1, \quad \text{Sinon}
    \end{cases}
    $

    ![Une v.a.d. Y](./img/img2_vadY.png)

    On peut résumer la loi de Probabilité de $Y$ sur $\Omega$ par le tableau :

    |Gain $y_i$|$2$|$-1$|
    |:-:|-:-|:-:|
    |Probabilité<br>$p_i=P(X=y_i)$|$\dfrac 12$|$\dfrac 12$|
    {.nocolor}

### Variable Aléatoire Discrète et Loi de Probabilité associée

!!! def
    On considère un **ensemble fini** $\Omega$ et une loi de probabilité $P$ sur $\Omega$.
    
    * Une <red>variable aléatoire (discrète)</red> sur un univers $\Omega$ (fini) est une fonction définie sur $\Omega$ à valeurs dans $[0;1]$.
    * Si $x_1$, $x_2$, ..., et $x_r$ désignent les valeurs prises par $X$,  
    alors on note <enc>$X=x_i$</enc> $\quad$ ou $\quad$ <enc>$\left( X=x_i \right)$</enc> $\quad$ ou $\quad$ <enc>$\left\{X=x_i\right\}$</enc> $\quad$ ou $\quad$ <enc>$\left[X=x_i\right]$</enc> l'événement « $X$ prend la valeur $x_i$ » $=\{\omega\in \Omega \\/ X(\omega)=x_i\}\subset \Omega$
    * $X(\Omega)= \{x_1;x_2;...;x_r\}$ est appelé <red>Univers Image</red>, c'est l'ensemble des valeurs prises par $X$,  
    C'est-à-dire ce sont les issues de l'univers image.
    * La connaissance d'une loi de probabilité $P$ sur $\Omega$ permet de définir, de manière induite, une nouvelle <red>loi de probabilité, notée $P_X$, associée à $X$ (ou, de $X$) sur $X(\Omega)=\{x_1;x_2;...;x_r\}$ </red> par la donnée des réels $x_i$ et des probabilités $p_i=P(X=x_i)=P_X(x_i)$ pour $1\leq i \leq r$ (pour $i=1, .., r$).  
    On dit encore que la loi de probabilité de $X$ est $(x_i;p_i)$ pour $1 \leq i \leq r$.  
    
On présente la loi de la v.a.  très fréquemment sous forme de tableau :

|$x_i$|$x_1$|$x_2$|$...$|$x_r$|
|:-:|:-:|:-:|:-:|:-:|
|Probabilité<br/>$p_i=P(X=x_i)$|$p_1$|$p_2$|$...$|$p_r$|
{.nocolor}

!!! remarque
    1. Les événements $(X=x_i)$ et $(X=x_j)$ sont incompatibles pour $i \neq j$, donc :  
    $\begin{align*}
    P_X(x_1)+P_X(x_2)+...+P_X(x_r) &= P(X=x_1)+P(X=x_2)+...+P(X=x_r) \\
        &= P((X=x_1) \cup ... \cup (X=x_r)) \\
        &= P(\{\Omega\}) \\
        &=1
    \end{align*}$  
    car $X(\Omega)=\{x_1;x_2;...;x_r\}$  
    ce qui prouve que $P_X$ est une loi de probabilité sur $X(\Omega)=\{x_1;x_2;...x_r\}$  
    1. Soit $X$ est une variable aléatoire de loi de probabilité $(x_i;p_i)$ pour $i=1,2,...,r$.  
    Si $a$ et $b$ sont deux réels, on peut définir une nouvelle variable aléatoire $Y=aX+b$, en posant $y_i=ax_i+b$ pour $i=1,..,r$  
    La loi de probabilité de $Y$ est alors $(y_i;p_i)$ pour $i=1,...,r$.  
    Preuve : $P(Y=y_i)=P(aX+b=ax_i+b)=P(X=x_i)=p_i$

### Opérations sur les variables aléatoires

#### Somme de deux variables aléatoires $X+Y$

Soit $X$ la variable aléatoire de loi $(x_i;p_i)$ pour $i=1,...,r$ et $Y$ de loi $(y_j;q_j)$ pour 
$y_j=1,2,..,s$  
$X$ prend donc ses valeurs sur $X(\Omega)=\{x_1;x_2;..;x_r\}$ et $Y$ sur $Y(\Omega)=\{y_1;y_2;..;y_s\}$.  
En posant $Z=X+Y$, on définit une nouvelle variable aléatoire, appelée la somme des deux variables aléatoires $X$ et $Y$, elle prend donc ses valeurs sur :  
$Z(\Omega) \quad \symbover{=}{noté} \quad (X+Y)(\Omega)=\{X(\omega)+Y(\omega)$ pour $\omega\in \Omega$ $\}$

Attention, certaines valeurs de $x_i$ ne seront jamais ajoutées à certaines autres $y_j$, car elles ne pourront pas se réaliser simultanément (elles ne viennent pas du même $\omega$), donc :  
$(X+Y)(\Omega) \neq \{ z_{ij}=x_i+y_j$ pour $1\leq x_i \leq r$ et $1\leq y_j \leq s\}$

Même en connaissant la loi de probabilité de $X$ et celle de $Y$,  la loi de probabilité de $X+Y$
n'est pas simple à calculer avec des formules générales lorsque $X$ et $Y$ sont quelconques. Il faut en général avoir des conditions supplémentaires sur $X$ et $Y$ (par exemple, que $X$ et $Y$ soient « indépendantes », voir plus loin) pour pouvoir la déterminer avec des formules un peu plus simples.

On peut néanmoins la déterminer « à la main » sur des exemples précis.

!!! exp
    Reprenons les v.a. $X$ et $Y$ des Exemples 1 et 2 précédents. Pour rappel :

    !!! col __50
        |$x_i$|$2$|$-1$|$-4$|
        |:-:|:-:|:-:|:-:|
        |Probabilité<br/>$p_i=P(X=x_i)$|$\dfrac 14$|$\dfrac 12$|$\dfrac 14$|
        {.nocolor}
    !!! col __50
        |$y_j$|$2$|$-1$|
        |:-:|:-:|:-:|
        |Probabilité<br/>$q_j=P(Y=y_j)$|$\dfrac 12$|$\dfrac 12$|
        {.nocolor}

    Détaillons comment trouver l'ensemble $(X+Y)(\Omega)$ ainsi que la loi de probabilité pour la v.a. $X+Y$ :

    |$\omega$|$FF$|$FP$|$PF$|$PP$|
    |:-:|:-:|:-:|:-:|:-:|
    |$x_i+y_j=X(\omega)+Y(\omega)$|$-4+2$<br/>$=-2$|$(-1)+(-1)$<br/>$=-2$|$(-1)+(-1)$<br/>$=-2$|$2+2$<br/>$=4$|
    {.nocolor}

    On a donc , d'où la loi de probabilité pour la variable  :

    |$x_i+y_j$|$-2$|$4$|
    |:-:|:-:|:-:|
    |Probabilité<br/>$p_{ij}=P(X+Y=x_i+y_j)$|$\dfrac 34$|$\dfrac 14$|
    {.nocolor}

#### Produit de deux variables aléatoires $XY$

Soit $X$ la variable aléatoire de loi $(x_i;p_i)$ pour $i=1,...,r$ et $Y$ de loi $(y_j;q_j)$ pour $j=1,...,s$  
$X$ prend donc ses valeurs sur $X(\Omega)=\{x_1;x_2;..;x_r\}$ et $Y$ sur $Y(\Omega)=\{y_1;y_2;..;y_s\}$.  
En posant $Z=XY$, on définit une nouvelle variable aléatoire, appelé le produit des variables aléatoires $X$ et $Y$. Elle prend donc ses valeurs sur :  
<center>$Z(\Omega) \quad \symbover{=}{noté} \quad (XY)(\Omega)=\{\}$</center>

Attention, certaines valeurs de $x_i$ ne seront jamais multipliées à certaines autres $y_i$, car elles ne pourront pas se réaliser simultanément (elles ne viennent pas du même $\omega$), donc :  
<center>$(XY)(\Omega) \neq \{ z_{ij}=x_i y_j$ pour $1 \leq x_i \leq r$ et $1 \leq y_j \leq s \}$</center>

Même en connaissant la loi de probabilité de $X$ et celle de $Y$, la loi de probabilité de $XY$ n'est pas simple à calculer avec des formules générales lorsque $X$ et $Y$ sont quelconques. Il faut en général avoir des conditions supplémentaires sur $X$ et $Y$ (comme par exemple que $X$ et $Y$ soient « indépendantes », voir plus loin) pour pouvoir la déterminer avec des formules un peu plus simples.

On peut néanmoins la déterminer « à la main » sur des exemples précis.

!!! exp
    Reprenons les v.a. $X$ et $Y$ des Exemples 1 et 2 précédents. Pour rappel :

    !!! col __50
        |$x_i$|$2$|$-1$|$-4$|
        |:-:|:-:|:-:|:-:|
        |Probabilité<br/>$p_i=P(X=x_i)$|$\dfrac 14$|$\dfrac 12$|$\dfrac 14$|
        {.nocolor}
    !!! col __50
        |$y_j$|$2$|$-1$|
        |:-:|:-:|:-:|
        |Probabilité<br/>$q_j=P(Y=y_j)$|$\dfrac 12$|$\dfrac 12$|
        {.nocolor}

    Détaillons comment trouver l'ensemble $(XY)(\Omega)$ ainsi que la loi de probabilité pour la v.a. $XY$ :

    |$\omega$|$FF$|$FP$|$PF$|$PP$|
    |:-:|:-:|:-:|:-:|:-:|
    |$x_i y_j=X(\omega)Y(\omega)$|$-4\times 2=-8$|$(-1)\times (-1)=+1$|$(-1)\times (-1)=+1$|$2\times 2=4$|
    {.nocolor}

    On a donc $(XY)(\Omega)=\{-8;1;4\}$, d'où la loi de probabilité pour la variable $XY$ :
    
    |$x_i y_j$|$-8$|$1$|$4$|
    |:-:|:-:|:-:|:-:|
    |Probabilité<br/>$p_{ij}=P(XY=x_i y_j)$|$\dfrac 14$|$\dfrac 24=\dfrac 12$|$\dfrac 14$|
    {.nocolor}

## Espérance, Variance, Écart-Type d'une Variable Aléatoire

### Définition

Soit $X$ une variable aléatoire de loi de probabilité $(x_i;p_i)$ pour $1\leq x_i \leq r$. 

!!! def
    On appelle :

    * <red>Espérance de $X$</red> le nombre réel, noté $E(X)$ ou quelquefois $\m E(X)$ ou $\overline X$, défini par :  
   	
        <center><enc>$E(X)=p_1 x_1+p_2 x_2+...+p_r x_r$</enc> $\quad$ ou encore  $\quad$ <enc>$\displaystyle E(X)=\sum_{i=1}^r p_i x_i$</enc></center>  
    L'espérance en probabilités est l'équivalent de la valeur moyenne en statistiques.

    * <red>Variance de $X$</red> le nombre réel positif, noté $V(X)$ ou quelquefois $V_X$ ou $\sigma_X^2$, défini par :  
        <center><enc>$V(X)=p_1(x_1 -E(X))^2+p_2(x_2 -E(X))^2+...+p_r(x_r -E(X))^2$</enc> $\quad$
        ou $\quad$ <enc>$\quad$ $\displaystyle V(X)=\sum_{i=1}^r p_i (x_i-E(X))^2$</enc></center>
    * <red>Écart-type de $X$</red>, le nombre réel positif, noté $\sigma(X)$ ou quelquefois $\sigma_X$, défini par :  
        <center><enc>$\sigma(X)=\sqrt{V(X)}$</enc></center>
 
!!! remarque
    1. Puisque $X(\Omega)=\{x_1;x_2;...;x_r\}$ alors $\forall \omega\in\Omega, \, \exists!i$ tq $X(\omega)=x_i$ et $\displaystyle p_i=\sum_{\omega\in (X=x_i)} P(\omega)$  
    donc $\displaystyle E(X)=\sum_{i=1}^r p_i x_i$ peut s'écrire plus généralement : 
    
        * $\displaystyle E(X)=\sum_{\omega \in \Omega} P(\omega) X(\omega)$ $\quad$ ou encore
        * $\displaystyle E(X)=\sum_{x \in X(\Omega)} P(X=x) x$

    1. Plus généralement, si $f: \m R \mapsto \m R$ est une fonction, Alors :  
    
        <center>$\displaystyle E(f(X))=\sum_{x \in X(\Omega)} P(X=x) f(x)$</center> 
    1. Une définition équivalente de $V(X)$ est donc :  
        <center>$\displaystyle 
        \begin{align*}
        V(X) &= E\left((X-E(X))^2\right) \\
            &= \sum_{x \in X(\Omega)} P(X=x) (x-E(X))^2
            \end{align*}$</center> 

### Théorème de Koenig-Huygens

!!! thm "Théorème de Koenig-Huygens"
    <center><enc>$V(X)=p_1 x_1^2+p_2 x_2^2+...+p_r x_r^2-E(X)^2$</enc></center>
    ou <center><enc>$\displaystyle V(X) = \left( \sum_{i=1}^r p_i x_i^2\right) -E(X)^2$</enc></center>  
    ou <center><enc>$\displaystyle V(X) = E(X^2) -E(X)^2$</enc></center>

??? demo
    * <bd @gris>Avec Espérance</bd> Calculs plus simples, mais plus abstraits :

        $\begin{align*}
        V(X) &= E([X-E(X)]^2) \\
            &= E(X^2-2XE(X)+E(X)^2) \\
            &= E(X^2)-2E(X)E(X)+E(E(X)^2) \\
            &= E(X^2)-2E(X)^2+E(X)^2 \\
            &= E(X^2)-E(X)^2 \\
        \end{align*}$

    * <bd @gris>Sans Espérance (:cry:)</bd> Calculs plus compliqués, mais plus « compréhensibles » :

        $\begin{align*}
        \displaystyle
        V(X) &= \sum_{i=1}^r p_i (x_i-E(X))^2 \\
            &= \sum_{i=1}^r p_i \left( x_i^2 - 2x_iE(X) + E(X)^2 \right) \\
            &= \sum_{i=1}^r \left[ p_i x_i^2 - 2p_i x_i E(X) + p_i E(X)^2 \right] \\
            &= \sum_{i=1}^r p_i x_i^2 - \sum_{i=1}^r 2p_i x_i E(X) + \sum_{i=1}^r p_i E(X)^2 \\
            &= E(X^2) - 2E(X)\sum_{i=1}^r p_i x_i + E(X)^2 \sum_{i=1}^r p_i \\
            &= E(X^2) - 2E(X)E(X) + E(X)^2 \times 1 \\
            &= E(X^2) - 2E(X)^2 + E(X)^2 \\
            &= E(X^2) - E(X)^2 \\
            &= \sum_{i=1}^r p_i x_i^2 - E(X)^2
        \end{align*}$

### Théorie des Jeux : Jeux Favorables vs Défavorables

<bd>Joueurs et Espérance de gain</bd>  
Le nom « Espérance mathématique » est issu de la **Théorie des jeux**.
L'espérance de gain s'interprète comme la moyenne des gains obtenus en répétant le jeu un grand nombre de fois.  

* Le jeu est **favorable** au joueur si son espérance de gain est strictement positive, 
* Le jeu est **défavorable** au joueur si son espérance de gain est strictement négative. 
* Le jeu est **équitable** si l'espérance est nulle. 
* L'écart-type du gain mesure la dispersion des gains autour de cette moyenne : plus il est grand, plus le degré de risque du jeu est grand. En théorie des jeux, donc, l'écart-type est un paramètre de mesure des risques.

!!! exp
    On reprend la v.a. $X$ de l'Exemple 1.
    
    * <bd>Espérance</bd>  
    $\begin{align*}
    E(X) &= p_1 x_1+p_2 x_2+p_3 x_3 \\
        &= \dfrac 14\times2+\dfrac 12\times(-1)+\dfrac 14\times(-4) \quad \euro\\
        &= \dfrac 12-\dfrac 12-1 \quad \euro\\
        &= -1 \quad \euro
    \end{align*}$  
    L'espérance de gain est strictement négative, donc **le jeu est défavorable au joueur**. (on s'en doutait..).
    <bd grey>Interprétation Intuitive</bd>  
    On retrouve qu'en moyenne, pour une partie, le joueur peut « espérer gagner » :
    $\dfrac {2+(-1)+(-4)}{3}=\dfrac {-3}{3}=-1 \quad \euro$.
    * <bd>Variance</bd>  
    $\begin{align*}
    V(X) &= p_1 x_1^2+p_2 x_2^2+p_3 x_3^2 -E(X)^2\\
        &= \dfrac 14\times2^2+ \dfrac 12\times(-1)^2+\dfrac 14\times(-4)^2-(-1)^2 \quad \euro^2\\
        &= \dfrac 44+\dfrac 12+\dfrac {16}{4}-1 \quad \euro^2\\
        &= 1+0,5+4-1 \quad \euro^2\\
        &= 4,5 \quad \euro^2
    \end{align*}$
    * <bd>Ecart-Type</bd>  
    $\sigma(X)=\sqrt{V(X)}=\sqrt{4,5}\approx 2,12$ à $10^{-2}$ près (en $\euro$).

!!! exp
    On reprend la v.a. $Y$ de l'Exemple 2.

    * <bd>Espérance</bd>  
    $\begin{align*}
    E(Y) &= p_1 x_1+p_2 x_2 \\
        &= \dfrac 12\times2+\dfrac 12\times(-1) \quad \euro\\
        &= 1-\dfrac 12 \quad \euro\\
        &= \dfrac 12 \quad \euro
    \end{align*}$  
    L'espérance de gain est strictement positive, donc **le jeu est favorable au joueur**. (on s'en doutait..).
    <bd grey>Interprétation Intuitive</bd>  
    On retrouve qu'en moyenne, pour une partie, le joueur peut « espérer gagner » :
    $\dfrac {2+(-1)}{2}=\dfrac 12 \quad \euro$.
    * <bd>Variance</bd>  
    $\begin{align*}
    V(Y) &= p_1 x_1^2+p_2 x_2^2 -E(Y)^2 \\
        &= \dfrac 12\times2^2+ \dfrac 12\times(-1)^2-\left(\dfrac 12 \right)^2 \quad \euro^2 \\
        &= \dfrac 42+\dfrac 12-\dfrac 14 \quad \euro^2 \\
        &= 2+0,5-0,25 \quad \euro^2 \\
        &= 2,25 \quad \euro^2
    \end{align*}$
    * <bd>Ecart-Type</bd>  
    $\sigma(Y)=\sqrt{V(Y)}=\sqrt{2,25}= 1,5$ (en $\euro$).

### Propriétés de l'Espérance, Variance et Écart-Type

!!! pte
    Soit $X$ et $Y$ deux variables aléatoires de lois de probabilités respect. $(x_i;p_i)_{1\leq i\leq r}$ et $(y_j;q_j)_{1\leq j\leq s}$  
    Soit $a$ et $b$ des réels. Alors :

    * <bd>Linéarité de l'Espérance</bd> $\quad$ <enc>$E(aX+bY)=aE(X)+bE(Y)$</enc> $\quad$ En particulier :
        * $E(X+Y)=E(X)+E(Y)$
        * $E(aX)=aE(X)$
        * $E(X+b)=E(X)$
        * $E(b)=b$
        * $E(aX+b)=aE(X)+b$
    * <bd>Semi-Linéarité de la Variance</bd>  
        Attention, sans conditions supplémentaires sur $X$ et $Y$, les égalités suivantes sont fausses :  

        * <enc>$V(aX+bY) \neq a^2V(X)+b^2V(Y)$</enc>  
        * <enc>$V(X+Y) \neq V(X)+V(Y)$</enc>  
        Il peut néanmoins arriver que l'une de ces égalités soit vraie, mais dans des cas particuliers très précis, comme par exemple lorsque $X$ et $Y$ sont indépendantes entre elles.  
    
        Par contre, les égalités suivantes sont toujours vraies, sans conditions supplémentaires sur $X$ et $Y$ :

        * $V(aX+b)=a^2V(X)$
        * $V(aX)=a^2V(X)$
        * $V(X+b)=V(X)$
        * $V(b)=0$
    * <bd>Écart-Type</bd>  
        <enc>$\sigma(aX+b)=\left\vert a \right\vert \sigma(X)$</enc> $\quad$ En particulier  

        * $\sigma(aX)=\vert a \vert \sigma(X)$
        * $\sigma(X+b)=\sigma(X)$
        * $\sigma(b)=0$

??? demo
    <bd>Pour les formules sur l'Espérance</bd>

    * <bd @gris>Démonstration « avec des $\omega$»</bd> Calculs plus simples à écrire, mais plus abstraits (toutes les formules d'un coup !)  
        $\begin{align*}
        \displaystyle E(aX+bY) &=\sum_{\omega \in \Omega} P(\omega)(aX+bY)(\omega) \\
            &= a\sum_{\omega \in \Omega} P(\omega) X(\omega) + b\sum_{\omega \in \Omega} P(\omega)Y(\omega) \quad \text{par linéarité du « } \Sigma \text{ »} \\
            &= aE(X)+bE(Y)
            \end{align*}$  
    * <bd @gris>Démonstration « sans $\omega$ »</bd> Calculs plus compliqués à écrire, mais plus « compréhensibles » (?)  
    Soit $Z$ la v.a. $Z=aX+bY$.  
    La loi de $X$ est $(x_i;p_i)$ pour $i=1,2,..,r$  
    La loi de $Y$ est $(y_j;q_j)$ pour $i=1,2,..,s$  
    La loi de $Z$ est donc donnée par $(z_{ij};p_{ij})$ où $z_{ij}=ax_i + b y_j$  
    donc :  
    $\begin{align*}
    \displaystyle
    E(Z) &= E(aX+bY) \\
        &= \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} p_{ij} z_{ij} \\
        &= \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} P(Z=z_{ij}) z_{ij} \\
        &= \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} P(aX+bY=ax_i+b y_j) (ax_i+b y_j) \\
        &= \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} P(aX+bY=ax_i+b y_j) (ax_i+b y_j) \\
        &= \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} P(aX+bY=ax_i+b y_j) (ax_i) + \sum_{\begin{matrix} i\in \{1;2;..;r\} \\ j\in \{1,2,..,s\} \end{matrix}} P(aX+bY=ax_i+b y_j) (b y_j) \\
        &= \sum_{ i\in \{1;2;..;r\} } \sum_{ j\in \{1,2,..,s\} } P(aX+bY=ax_i+b y_j) (ax_i) + \sum_{ j\in \{1,2,..,s\} } \sum_{ i\in \{1;2;..;r\} } P(aX+bY=ax_i+b y_j) (b y_j) \\
        &= \sum_{i=1}^r \sum_{j=1}^s P(aX+bY=ax_i+b y_j) (ax_i) + \sum_{j=1}^s \sum_{i=1}^r P(aX+bY=ax_i+b y_j) (b y_j) \\
    \end{align*}$

        De plus :  

        * $\displaystyle \sum_{j=1}^s P(aX+bY=ax_i+b y_j)=P(aX=ax_i)=P(X=x_i)$ car $\{y_j\}_{1\leq j \leq s}$ est une partition de $\Omega$
        * $\displaystyle \sum_{i=1}^r P(aX+bY=ax_i+b y_j)=P(bY=by_j)=P(Y=y_j)$ car $\{x_i\}_{1\leq i \leq r}$ est une partition de $\Omega$

        Donc :  
        $\begin{align*}
        \displaystyle
        E(Z) &= E(aX+bY) \\
            &= \sum_{i=1}^r P(X=x_i)(ax_i) + \sum_{j=1}^s P(Y=y_j) (b y_j) \\
            &= a\sum_{i=1}^r P(X=x_i)(x_i) + b\sum_{j=1}^s P(Y=y_j) (y_j) \\
            &= aE(X) + bE(Y) \\
        \end{align*}$  
    * <bd @gris>Pour les cas particuliers de l'Espérance</bd>
        * Commencer par remarquer que $E(1)=1$, ce qui est vrai car :  
        $\begin{align*}
        \displaystyle E(1) &= \sum_{\omega \in \Omega} P(\omega)(1)(\omega) \quad \text{ici, 1 désigne la fonction valant toujours 1 quel que soit } \omega\\
            &= \sum_{\omega \in \Omega} P(\omega)\times 1 \\
            &= \sum_{\omega \in \Omega} P(\omega) \\
            &= P(\Omega) \\
            &= 1 \quad \text{car } \Omega \text{ est l'Univers des Possibles}
        \end{align*}
        $
        
        * On obtient alors les cas particuliers de l'Espérance en utilisant la formule précédente $E(aX+bY)=aE(X)+bE(Y)$ avec les valeurs respectives suivantes:  
            * $a=1$ et $b=1$ pour montrer que $E(X+Y)=E(X)+E(Y)$ car dans ce cas :  
                $E(X+Y)=E(1\times X+1\times Y)=1\times E(X)+1\times E(Y) = E(X)+E(Y)$
            * $b=0$ pour montrer que $E(aX)=aE(X)$ car dans ce cas :  
                $E(aX)=E(a\times X+0\times Y)=a\times E(X)+0\times E(Y) = aE(X)$
            * $a=1$ et $Y=1$ pour montrer que $E(X+b)=E(X)+b$ car dans ce cas :  
                $E(X+b)=E(1\times X+b\times 1)=1\times E(X)+b\times E(1) = E(X)+b$ car $E(1)=1$
            * $a=0$ et $Y=1$ pour montrer que $E(b)=b$ car dans ce cas :  
                $E(bY)=E(0\times X+b\times 1)=0\times E(X)+b\times E(1) = E(X)+b$ car $E(1)=1$
            * $Y=1$ pour montrer que $E(aX+b)=aE(X)+b$ car dans ce cas :  
                $E(aX+b)=E(a\times X+b\times 1)=a\times E(X)+b\times E(1) = aE(X)+b$ car $E(1)=1$
    <bd>Pour les formules sur la Variance</bd>

    * <bd @gris>Démonstration avec « Utilisation de l'Espérance »</bd> Calculs plus simples, mais plus abstraits  
    Rappelons que l'on a $V(Z)=E(\left[Z-E(Z)\right]^2)$ pour toute v.a. $Z$, donc (pour $Z=aX+b$)  
    $\begin{align*}
    V(aX+b) &= E(\left[aX+b-E(aX+b)\right]^2) \\
        &= E(\left[ aX+b-aE(X)-b \right]^2) \quad \text{par linéarité de l'Espérance}\\
        &= E(\left[ aX-aE(X) \right]^2) \\
        &= E(\left[ a(X-E(X)) \right]^2) \\
        &= E( a^2 \left[ X-E(X) \right]^2) \\
        &= a^2 E( \left[ X-E(X) \right]^2) \\
        &= a^2 V(X) \\
    \end{align*}$
    * <bd @gris>Démonstration sans « Utilisation de l'Espérance »</bd> Calculs plus compliqués, mais plus « compréhensibles » (?)  
    La v.a. $X$ est donnée par $(x_i;p_i)$ avec $1\leq i \leq r$  
    La v.a. $Y$ est donnée par $(y_i;p_i)$ avec $y_i=ax_i+b$ et $1\leq i \leq r$  
    Rappelons que pour toute v.a. $Z$ donnée par $(z_i;p_i)$ avec $1\leq i \leq r$, on a : $\displaystyle V(Z) = \sum_{i=1}^r p_i \left( z_i -E(Z)\right)^2$  
    donc en particulier (pour $Z=aX+b$) :  
    $\begin{align*}
    \displaystyle
    V(aX+b) &= \sum_{i=1}^r p_i \left[ ax_i +b -E(aX+b) \right]^2 \\
        &= \sum_{i=1}^r p_i \left[ ax_i +b -aE(X)-b \right]^2 \\
        &= \sum_{i=1}^r p_i \left[ ax_i -aE(X) \right]^2 \\
        &= \sum_{i=1}^r p_i \left[ a(x_i -E(X)) \right]^2 \\
        &= \sum_{i=1}^r p_i a^2 \left[ x_i -E(X) \right]^2 \\
        &= a^2 \sum_{i=1}^r p_i \left[ x_i -E(X) \right]^2 \\
        &= a^2 V(X) \\
    \end{align*}$
    * <bd @gris>Pour Les cas particuliers de la Variance</bd>  
    Ils correspondent aux cas où :
        * $b=0$ pour montrer que $V(aX)=a^2V(X)$ car dans ce cas :  
        $V(aX)=V(aX+0)=a^2V(X)$
        * $a=1$ pour montrer que $V(X+b)=V(X)$ car dans ce cas :  
        $V(X+b)=V(1X+b)=1^2V(X)=V(X)$
        * $a=0$ pour montrer que $V(b)=0$ car dans ce cas :  
        $V(b)=V(0\times X+b)=0^2\times V(X)=0$  

    <bd>Pour les formules sur l'Écart-type</bd>

    * $\sigma(aX+b)=\sqrt {V(aX+b)}=\sqrt {a^2V(aX)}=\sqrt {a^2} \sqrt {V(aX)}=\vert a \vert \sqrt{V(X)} = \vert a \vert \sigma(X)$  
    où $\vert a \vert$ désigne la valeur absolue du réel $a$



## Répétition d'Expériences identiques et indépendantes

### Une répétition d'expériences identiques indépendantes

On dispose d'une pièce truquée dont on sait que pour un lancer la probabilité d'obtenir PILE ($P$) est de $0,6$ et la probabilité d'obtenir FACE ($F$) de $0,4$.  
On lance trois fois de suite cette même pièce, autrement dit nous réalisons trois fois de suite la même expérience : on parle de <red>répétition d'expériences identiques</red>.  
Il semble normal de penser, et nous le supposerons désormais, qu'un lancer d'une telle pièce n'influence pas le résultat d'un autre lancer de cette même pièce lancée dans les mêmes conditions : on parle d'<red>expériences indépendantes</red>.

### Représentation par un arbre pondéré

<clear></clear>

!!! col __60
    On construit un <red>arbre pondéré</red>.
    Il est composé de plusieurs <red>noeuds</red> : ici la <red>racine</red> est un noeud, et tout emplacement de P ou de F est un noeud.  
    Sur chaque <red>branche</red>, **dans le cas d'expériences indépendantes**, on indique la probabilité de l'issue correspondante.  
    Ici, on connait $P(P) = 0,6$ et $P(F) = 0,4$.  
    Un <red>chemin</red> est la succession de plusieurs branches. Chaque chemin correspond à l'événement tel que chaque passage par un noeud correspond à un « ET », i.e. à un « $\cap$ ».
!!! col __40
    ![Arbre Pondéré](./img/img3_arbrePondere.png)

Deux chemins distincts correspondent forcément à deux événements incompatibles.

### Modélisation : Probabilités sur un arbre

Voici trois règles utiles **dans le cas d'une répétition d'expériences identiques et indépendantes (i.i.d.)**, représentée par un arbre pondéré.

!!! pte "Règle des noeuds"
    La somme des probabilités sortantes (i.e à droite) d'un noeud est égale à $1$.

Ici, il faut comprendre que (pour chaque noeud): $0,6 + 0,4 = 1$

!!! def
    On dit que des **expériences identiques** sont <red>indépendantes</red>, lorsque la probabilité d'une liste de résultats est le produit des probabilités de chaque résultat.
    $\ssi
    \begin{cases}
    \text{Pour tout } A_1 \text{ résultat de la 1ère expérience,} \\
    \text{Pour tout } A_2 \text{ résultat de la 2ème expérience,} \quad P(A_1\cap A_2 \cap .. \cap A_k)=P(A_1)P(A_2)...P(A_k)\\
    \text{...} \\
    \text{Pour tout } A_k \text{ résultat de la kème expérience,} \\
    \end{cases}
    $ 

!!! exp
    $P(FFP) = P(F\cap F\cap P)=P(F)P(F)P(P)=0,4\times0,4 \times0,6=0,4^2 \times0,6=0,096=9,6\%$
    En particulier, il découle de cette définition que l'on peut dire que :

!!! pte "Probabilité d'un chemin (/de l'événement correspondant au chemin)"
    Dans un arbre pondéré, la probabilité d'un chemin est égale au produit des probabilités des branches composant ce chemin.

!!! exp
    * La probabilité d'obtenir trois fois FACE, vaut :  
    $P(FFF) = P(F)\times P(F)\times P(F) = 0,4^3 = 0,064 = 6,4\%$
    * La probabilité d'obtenir successivement un PILE, puis un FACE, puis un PILE, vaut :  
    $P(PFP) = 0,6\times 0,4 \times 0,6 = 0,6^2\times 0,4 = 0,144 = 14,4\%$

!!! pte "Probabilité de plusieurs chemins (/de l'événement correspondant à la réunion de plusieurs chemins/(de leurs événements))"
    Dans un arbre pondéré, la probabilité de plusieurs chemins est la somme des probabilités de chaque chemin.

!!! exp
    * La probabilité d'obtenir deux fois FACE exactement, vaut :  
    $\begin{align*}
    P(PFF \cup FFP \cup FPF) &= P(PFF) + P(FFP) + P(FPF) \\
        &= 0,6\times 0,4^2 + 0,6\times 0,4^2 + 0,6\times 0,4^2 \\
        &= 3\times 0,6\times 0,4^2 \\
        &= 0,288 \\
        &= 28,8\%
        \end{align*}$
    * La probabilité d'obtenir un PILE au deuxième lancer vaut :
    $\begin{align*}
    P(PPP \cup PPF \cup FPP \cup FPF) &= P(PPP) + P(PPF) + P(FPP) + P(FPF) \\
        &= 0,6^3 + 0,6^2\times 0,4 + 0,6^2\times 0,4 + 0,4^2\times 0,6 \\
        &= 0,6
    \end{align*}$  
    On retrouve un résultat plutôt normal et prévisible...


## Variables aléatoires  (discrètes) indépendantes (Hors-Programme?)

!!! def
    Soit $X$ et $Y$ deux variables aléatoires (discrètes) sur $\Omega$ (fini).  
    On dit que $X$ et $Y$ sont <red>indépendantes</red>
    $\ssi
    \begin{cases}
    \forall x\in X(\Omega) \quad P(X=x \cap Y=y) \quad \symbover{=}{noté} \quad \text{"} P(X=x;Y=y) \text{"} = P(X=x)P(Y=y)\\
    \forall y\in Y(\Omega)
    \end{cases}$
    
!!! pte
    Si $X$ et $Y$ sont deux v.a. définies sur $\Omega$, et **indépendantes**, alors :

    1. $E(XY)=E(X)E(Y)$
    1. $V(aX+bY)=a^2V(X)+b^2V(Y)$	$\quad$ En particulier : $V(X+Y)=V(X)+V(Y)$

??? demo
    1. Il suffit de considérer la fonction $f: (\m R;\m R) \mapsto \m R$ telle que $f(x;y)=xy$  
    $\begin{align*}
    \displaystyle E(XY) &= E(f(X;Y)) \\
        &= \sum_{x\in X(\Omega); y\in Y(\Omega)} P((X;Y)=(x;y))f(x;y) \\
        &= \sum_{x\in X(\Omega); y\in Y(\Omega)} P((X;Y)=(x;y))xy \\
        &= \sum_{x\in X(\Omega); y\in Y(\Omega)} P(X=x;Y=y)xy \\
        &= \sum_{x\in X(\Omega); y\in Y(\Omega)} P(X=x)P(Y=y)xy \quad \text{ Car } X \text{ et } Y \text{ sont indépendantes}\\
        &= \sum_{x\in X(\Omega)} \sum_{y\in Y(\Omega)} P(X=x)P(Y=y)xy \\
        &= \sum_{x\in X(\Omega)} P(X=x)x \sum_{y\in Y(\Omega)}P(Y=y)y \\
        &= E(X) E(Y) \\
        \end{align*}$  

    1. Rappelons que pour toute v.a. $Z$, on a: $V(Z)=E([Z-E(Z)]^2)$  
    donc en particulier pour $Z=aX+bY$ :  
    $\begin{align*}
    \displaystyle
    V(aX+bY) &= E(\left[ aX+bY -E(aX+bY) \right]^2) \\
        &= E(\left[ aX+bY -aE(X)-bE(Y) \right]^2) \quad \text{par linéarité de l'Espérance} \\
        &= E(\left[ a(X-E(X))+b(Y-E(Y)) \right]^2) \\
        &= E(a^2(X-E(X))^2+2ab(X-E(X))(Y-E(Y)) + b^2(Y-E(Y))^2 ) \\
        &= a^2E([X-E(X)]^2)+2abE([(X-E(X))(Y-E(Y))]) + b^2E([Y-E(Y)]^2 ) \\
        &= a^2V(X)+2abE([XY-XE(Y)-YE(X)+E(X)E(Y)]) + b^2V(Y) \\
        &= a^2V(X)+2ab[E(XY)-E(X)E(Y)-E(Y)E(X)+E(E(X)E(Y))] + b^2V(Y) \\
        &= a^2V(X)+2ab[E(XY)-E(X)E(Y)-E(Y)E(X)+ E(X)E(Y)] + b^2V(Y) \\
        &= a^2V(X)+2ab[E(XY)-E(X)E(Y)] + b^2V(Y) \\
        &= a^2V(X)+2ab\times [0] + b^2V(Y) \quad \text{ car } X \text{ et } Y \text{ indépendantes } \Rightarrow E(XY)=E(X)E(Y)\\
        &= a^2V(X)+b^2V(Y)
    \end{align*}$
        * Le cas particulier correspond à $a=1$ et $b=1$, car :
        $V(X+Y)=V(1\times X+1\times Y)=1^2\times V(X)+1^2\times V(Y)=V(X)+V(Y)$



