# TMATSCO : Probabilités. Loi Binomiale, Échantillonnage.

## Épreuve de Bernoulli, Loi de Bernoulli

!!! def "Épreuve de Bernoulli"
    !!! col __60
        On appelle <red>épreuve de Beroulli</red> de paramètre $p$, toute expérience aléatoire admettant deux issues exactement :

        * L'une, souvent appelée « <red>Succès</red> » notée $S$ dont la probabilité de réalisation est $p\in[0 ;1]$
        * L'autre, souvent appelée « <red>Échec</red> » notée $\overline S$ (quelquefois $E$) dont la probabilité de réalisation est  $q = 1 - p\in[0; 1]$

    !!! col __40
        ![Épreuve de Bernoulli](./img/bernoulli.png)

!!! def "Variable de Bernoulli et Loi de Bernoulli"
    On peut modéliser une épreuve de Bernoulli de paramètre $p$ par la **variable aléatoire** $X$, de $\Omega$ dans $\m R$, telle que : 
    
    * $X$ prend la valeur $1$ si $S$ se produit, et 
    * $X$ la valeur $0$ sinon (si $\overline S$ se produit).  
    
    Dans ce cas, on dit que :
    
    * <red>$X$ est une Variable de Bernoulli de paramètre $p$</red> , ou encore, que
    * <red>$X$ suit la Loi de Bernoulli de paramètre $p$</red>.  
    
    On note parfois <enc>$X \suitloi B(1 ;p)$</enc>, ou quelquefois <enc>$X \simarrow B(1,p)$</enc> ou <enc>$X \sim B(1, p)$</enc>

    On modélise donc une épreuve de Bernoulli comme suit :

    |$k$|$0$|$1$|
    |:-:|:-:|:-:|
    |$P(X=k)$|$q=1-p$|$p$|
    {.nocolor}

!!! remarque
    Ici, $k$ représente une issue $x_k=k$ $\quad$ ($\ssi x_i=i$)

!!! pte
    Soit $X$ une variable de Bernoulli de paramètre $p$, càd $X \suit B(1 ;p)$
    Alors :
    
    * $E(X) = p$
    * $V(X)=p(1-p)$  

!!! demo
    * <bd @gris>$E(X)$</bd> : $E(X)=p_1 x_1 + p_2 x_2 = (1-p)\times 0 + p\times 1 = p$
    * <bd @gris>$V(X)$</bd> : $V(X)=p_1 x_1^2 + p_2 x_2^2 - E(X)^2 = (1-p)\times 0^2 + p\times 1^2 - p^2 = p-p^2=p(1-p)$

## Schéma de Bernoulli

### Exemple 1

On dispose d'un dé non pipé, dont on sait que la probabilité d'obtenir $\{1\}$, appelé **SUCCÈS ($S$)**, pour un lancer est $p =\dfrac 16$  et d'obtenir autre chose, appelé **ÉCHEC ($\overline S$)** est $q = 1 - p = \dfrac 56$.  
On considère l'expérience aléatoire consistant à lancer, de façon indépendante, $n$ fois ce même dé : Il s'agit donc de la **répétition d'Épreuves de BERNOULLI identiques et indépendantes**.
Dans ce cas, On parle d'<red>un Schéma de (n épreuves de) Bernoulli</red>.

Représentation de cette expérience pour $n = 3$ lancers du dé, sous forme d ‘arbre :

![Schéma de 3 épreuves de Bernoulli](./img/schema_bernoulli.png)

Dans ce contexte, un résultat est une liste de $n$ issues $S$ ou $\overline S$ (et non pas une issue comme d'habitude).  
Par exemple, dans un schéma de $3$ épreuves, $(\overline S, S, \overline S)$ est un résultat, qui est  plutôt noté $\overline S S \overline S$, réalisant $1$ SUCCÈS ($S$) et deux ÉCHECS($\overline S$). Remarquons la « Symétrie » ($S/\overline S$) de cet arbre.

### Schéma de Bernoulli

!!! def
    Un <red>Schéma de Bernoulli à $n$ épreuves</red> est une expérience aléatoire consistant à répéter $n$ fois de façon indépendante, une même épreuve de Bernoulli.

De même que dans l'exemple précédent, On peut toujours représenter un Schéma de $n$ épreuves de Bernoulli sous forme d'un arbre.


## Définition & Étude des coefficients binomiaux $\coord nk$

### Définition des Coefficients Binomiaux $\coord nk$

!!! def
    On considère un schéma de $n$ épreuves de Bernoulli, $n\in \m N^*$, représenté par un arbre.  
    Pour tout $k$ entier tel que $0 \leq k \leq n$, on note $\coord nk$, lire &laquo; <red>$k$ parmi $n$</red> &raquo; ou &laquo; <red>Coefficient Binomial de $k$ parmi $n$</red> &raquo;, le nombre de chemins de l'arbre réalisant exactement $k$ SUCCÈS ($S$) lors des $n$ répétitions.

Convention : $\coord 00 = 1$

!!! remarque "Ancienne Notation (Française)"
    $C_n^k= \coord nk$ également appelé le &laquo; <red>Nombre de Combinaisons de $k$ objets parmi $n$</red> &raquo;

!!! exp
    !!! col __60
        Reprenons notre exemple de Schéma de Bernoulli précédent :
        
        * $\coord 30$ : C'est le nombre de chemins de l'arbre réalisant $0$ SUCCÈS parmi $3$ : il n'y en a qu'un, c'est le chemin $\bar S \bar S \bar S$, donc $\coord 30=1$
        * $\coord 31$ : C'est le nombre de chemins de l'arbre réalisant $1$ SUCCÈS parmi $3$ : il y a trois chemins possibles, $S\bar S\bar S$ , $\bar S S \bar S$ et $\bar S \bar S S$, donc $\coord 31=3$
        * $\coord 32$ : C'est le nombre de chemins de l'arbre réalisant $2$ SUCCÈS parmi $3$ : il y a trois chemins possibles, $SS\bar S$ , $\bar S S \bar S$ et $\bar S\bar SS$, donc $\coord 32=3$
        * $\coord 33$ : C'est le nombre de chemins de l'arbre réalisant $3$ SUCCÈS parmi $3$ : il n'y en a qu'un, c'est le chemin $SSS$, donc $\coord 33=1$
    !!! col __40
        ![Schéma de 3 épreuves de Bernoulli](./img/schema_bernoulli.png)


!!! remarque "Interprétation en termes de Dénombrement"
    $\coord nk$ est le nombre de possibilités de placer $k$ **mêmes** éléments (ici « $S$ ») parmi un total de $n$ places disponibles ($n\geq k$) (ici le chemin est de longueur $n$), ou bien $k$ éléments différents parmi $n$ places distinctes, **sans tenir compte de l'ordre**.

### Propriétés des Coefficients Binomiaux

!!! pte
    Pour tout entier $n \geq0$, et pour tout entier $k$ tel que $0 \leq k \leq n$,

    1. $\coord n0=1$ et $\coord nn=1$
    1. $\coord nk=\coord n{n-k}$ $\quad$ « Symétrie des $\coord nk$ »

!!! demo
    1. Un seul chemin conduit à $0$ succès, c'est $\bar S\bar S\bar S...\bar S$  
       Un seul chemin conduit à $n$ succès, c'est $SSS...S$
    1. Si $n = 0$, alors puisque $0 \leq k \leq n$, on a $k = 0$ et $\coord nk=\coord 00=1=\coord 0{0-0}=\coord n{n-k}$  

        Si $n \geq1$, alors :

        * $\coord nk$ désigne le nombre de chemins contenant exactement $k$ SUCCÈS sur l'arbre représentant le Schéma de Bernoulli, donc il y a $(n - k)$ ÉCHECS exactement. En outre,
        * $\coord n{n-k}$ désigne le nombre de chemins contenant exactement $(n - k)$ SUCCÈS sur l'arbre, et donc $k$ ÉCHECS.  

        **Cet arbre est symétrique entre $S$ et $\bar S$** : Détaillons ce que nous entendons par là.  
        Pour chaque chemin, il existe un autre chemin « **complémentaire/symétrique »** au premier, dans lequel :
    
        * chaque SUCCÈS du premier chemin correspond à un ÉCHEC dans le chemin symétrique (càd à la même place dans l'arbre), et 
        * chaque ÉCHEC dans le premier chemin, correspond à un SUCCÈS dans le chemin symétrique (à la même place dans l'arbre).

        Ainsi, le chemin complémentaire d'un chemin a $k$ SUCCÈS est un chemin à $k$ ÉCHECS, et à $(n - k)$ SUCCÈS.  
        Réciproquement, tout chemin à $(n - k)$ SUCCÈS admet un complémentaire à $k$ SUCCÈS, et à $(n-k)$ ÉCHECS.  
        Conclusion : il y a autant de chemins à $k$ SUCCÈS que de chemins à $(n - k)$ SUCCÈS.  
        Autrement dit : $\coord nk=\coord n{n-k}$.

!!! pte
    !!! col __47
        Pour tout entier $n \geq0$, et tout entier $k$ tel que $1 \leq k \leq n-1$,  
        <center><enc>$\coord nk=\coord {n-1}{k-1}+\coord {n-1}{k}$</enc></center>
    !!! col __6
        $\ssi$
    !!! col __47
        Pour tout entier $n \geq0$, et tout entier $k$ tel que $1 \leq k \leq n$,  
        <center><enc>$\coord {n+1}{k+1}=\coord nk+\coord {n}{k+1}$</enc></center>

!!! demo
    Sur l'arbre représentant le Schéma de $n$ épreuves de Bernoulli, les chemins conduisant à $k$ SUCCÈS ($k\geq1$) peuvent venir de deux sources exclusivement :

    * Ceux qui conduisent à $k-1$ SUCCÈS parmi les $n-1$ premières épreuves, et le résultat de la dernière épreuve est un SUCCÈS : il y en a $\coord {n-1}{k-1}$
    * Ceux qui conduisent à $k$ SUCCÈS parmi les $n-1$ premières épreuves, et le résultat de la dernière épreuve est un ÉCHEC : il y en a $\coord {n-1}k$
    
    On en déduit que : $\coord nk=\coord {n-1}{k-1}+\coord {n-1}{k}$

<bd>Utilisation pratique : « Triangle de Pascal »</bd>

|$k$<br/>$n$|$0$|$1$|$2$|$3$|$4$|$5$|$6$|$7$|
|-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$0$|$1$||||||||
|$1$|$1$|$1$|||||||
|$2$|$1$|$2$|$1$||||||
|$3$|$1$|$3$|$3$|$1$|||||
|$4$|$1$|$4$|$6$|$4$|$1$||||
|$5$|$1$|$5$|$10$|$10$|$5$|$1$|||
|$6$|$1$|$6$|$15$|$20$|$15$|$6$|$1$||
|$7$|$1$|$7$|$21$|$35$|$35$|$21$|$7$|$1$|

$\coord nk$ est le nombre situé à la ligne $n$ et la colonne $k$.  
Le 1°) de la propriété $2$ permet de placer tous les $1$, et le 2°) justifie la symétrie des coefficients sur une même ligne. La propriété 3 permet de remplir une ligne suivante lorsqu'on connaît la précédente.


### Quelques autres propriétés utiles (Hors- programme) des $\coord nk$

!!! def
    1. Une <red>Permutation</red> de $n$ éléments $S_1$, $S_2$,..., $S_n$ est une opération consistant à échanger de place (/**permuter**) un nombre quelconque de ces $n$ éléments entre eux : ici **on tient compte de l'ordre**.
    1. Le nombre de permutations de $n$ éléments entre eux est $n!$  
    où $n!=n\times (n-1)\times (n-2)\times ... \times 3\times 2 \times 1$ désigne la factorielle de n.

!!! demo
    Compter le nombre de permutations de ces $n$ éléments revient à compter le nombre de façons de placer $n$ éléments dans $n$ places différentes.  
    Étant données $n$ places numérotées et $n$ éléments différents $S_1$, $S_2$, ..., $S_n$ à placer à ces places numérotées, c'est-à-dire en tenant compte de l'ordre.  
    
    * Le premier élément $S_1$ dispose de $n$ places libres pour être placé, 
    * le deuxième élément $S_2$ (en supposant le premier placé) dispose de ($n-1)$ places libres,
    * etc.., 
    * le dernier $n$-ème élément $S_n$ dispose seulement de $1$ place disponible (il n'a plus le choix, il ne reste plus qu'une seule place). 
    
    En tout, il y a donc $n!=n\times (n-1)\times ...\times 2\times 1$ manières distinctes de placer $n$ éléments $S_1$, $S_2$, ..., $S_n$ à $n$ places numérotées, **en tenant compte de l'ordre**.  
    Tenir compte de l'ordre veut dire, par exemple, que le résultat $(S_1, S_2, S_5,...)$ et le résultat $(S_2, S_1, S_5, ...)$ comptent pour deux résultats différents (au lieu d'un seul).

<bd>Convention</bd>  $0 ! = 1$

!!! exp
    $5!=5\times 4\times 3\times 2\times 1=120$

!!! pte
    $n!=n\times (n-1)!$

!!! pte
    1. $\coord nk=\dfrac {n!}{k!(n-k)!}$ où $n$ et $k$ sont des entiers tels que $0 \leq k \leq n$
    1. Formule du **Binôme de Newton** : $\forall a, b \in \m R$, et $\forall n \in \m N$  
        $\begin{align*}
        \displaystyle
        \left( a+b \right)^n &= \sum_{k=0}^n \coord nk a^k b^{n-k} \\
            &= \coord n0 a^0b^{n} + \coord n1 a ^1b^{n-1} + ... + \coord nk a ^kb^{n-k} + ... + \coord n{n-1} a ^{n-1}b^{1} + \coord nn a ^nb^{0} \\
        \end{align*}$
    1. $\coord nk=\dfrac nk \times \coord {n-1}{k-1}$ $\quad$ pour $1 \leq k \leq n$

!!! demo
    1. Raisonnons par disjonction des cas :

        * Pour $n = 0$ : on sait que, par double convention :
            $\forall k\in \m N$ tel que $0 \leq k \leq 0$, c'est-à-dire pour $k = 0$, $\coord 00=1=\dfrac {0!}{0!(0-0)!}$
        * Pour $n \geq 1$ : Sur l'arbre représentant le Schéma de $n$ épreuves de Bernoulli, $\coord nk$ désigne le nombre de chemins conduisant exactement à $k$ SUCCÈS $S$, donc $(n - k)$ ÉCHECS $\overline S$. Suivant la remarque 3 (bas de la page 3), $\coord nk$ désigne donc aussi le nombre de manières de placer $k$ **mêmes** objets $S$ parmi un total de $n$ places, ou bien ce qui revient au même, $k$ objets **différents** $S_1$, $S_2$, ...,$S_k$ parmi un total de $n$ places, mais sans tenir compte de l'ordre, autrement dit et par exemple on veut que le résultat $(S_1, S_2, S_3, ...)$ et le résultat $(S_2, S_1, S_3, ...)$ ne comptent qu'une seule fois et pas deux.
            * **En tenant compte de l'ordre** des places, un simple raisonnement montre que le premier élément $S_1$ dispose de $n$ places libres pour être placé, puis le second $S_2$ (le premier étant déjà placé) de $(n - 1)$ places libres, le troisième $S_3$ de $(n - 2)$ places libres, etc... et enfin le $k$-ème élément $S_k$ de $(n-k+1)$ places libres. Mais dans ce calcul, les résultats $(S_1,S_2,S_3,...)$ et $(S_2,S_1,S_3)$ comptent deux fois alors que l'on veut qu'il ne compte qu'une seule fois, c'est ce que l'on entend par **ne pas tenir compte de l'ordre**.
            * **Pour ne pas tenir compte de l'ordre des places**, il faut donc diviser le nombre $n\times (n-1)\times ... \times (n-k+1)$ de places précédemment trouvées (avec ordre), par le nombre total de permutations possibles de $k$ éléments $S_1$, $S_2$, ..., $S_k$ entre eux, autrement dit il faut diviser par $k!$.
            Il y a donc en tout :  
            $\coord nk=\dfrac {n\times (n-1) \times ... \times (n-k+1)}{k!}$ manières de placer $k$ mêmes éléments parmi un total de $n$ places.
            Ainsi : $\begin{align*}
            \displaystyle
            \coord nk &= \dfrac {n\times (n-1) \times ... \times (n-k+1)}{k!} \\
                &= \dfrac {n\times (n-1) \times ... \times (n-k+1) \times (n-k) \times (n-k-1) \times 3 \times 2 \times 1}{k!\times (n-k) \times (n-k-1) \times 3 \times 2 \times 1} \\
                &= \dfrac {n!}{k!(n-k)!} \\
            \end{align*}$
    1. Raisonnons encore une fois par disjonction des cas :
        * Pour $n = 0$ : par convention, $(a+b)^0=1=\coord 00 a^0 b^{0-0}=\coord 00 \times 1 \times 1$
        * Pour $n\geq1$, $(a+b)^n = (a+b)\times (a+b)\times ... \times (a+b)$ avec $n$ facteurs dans cette expression.
    Dans la manière de développer cette expression, pour chaque nouveau facteur, il n'y a le choix que d'être multiplié par $a$ (que l'on peut convenir d'appeler SUCCÈS) ou par $b$ (que l'on peut convenir d'appeler ÉCHEC). En développant on trouve donc une somme de termes de la forme $a^k b^{n-k}$ avec $0 \leq k \leq n$ (car la somme des deux puissances doit être égale à $n$, car il y a $n$ facteurs dans cette expression).  
    **Combien de termes de la forme $a^k b^{n-k}$ trouve- t'on en développant ?** Il y en a autant que de chemins distincts dans l'arbre représentant un Schéma de $n$ épreuves de Bernoulli ayant exactement $k$ SUCCÈS (car on doit trouver $a^k$) et $(n-k)$ ÉCHECS (car on doit trouver $b^{n-k}$), i.e. il y en a $\coord nk$.
    Étant donné qu'en outre $k$ peut prendre toutes les valeurs entre $0$ et $n$, on trouve ainsi :  
    $\displaystyle (a+b)^n = \sum_{k=0}^n a^k b^{n-k}$  
    1. On a :   
    $\begin{align*}
    \displaystyle
    \coord nk &= \dfrac {n!}{k!(n-k)!} \\
        &= \dfrac {n \times (n-1)!}{k\times (k-1)! \times (n-1-k+1)!} \\
        &= \dfrac nk \times \dfrac {(n-1)!}{(k-1)! \times \left( (n-1)-(k-1) \right)!} \\
        &= \dfrac nk \times \coord {n-1}{k-1} \\
    \end{align*}$


## Loi Binomiale

### Définition

!!! def "Loi Binomiale"
    Dans un Schéma de $n$ épreuves de Bernoulli, la variable aléatoire $X$ comptant le nombre de SUCCÈS admet pour loi de probabilité :  
    <enc>$P(X = k) =  \coord nk p^k(1-p)^{n-k}$</enc> $\quad$ où $k$ prend les valeurs $0, 1, ..., n$.  
    On dit que <red>$X$ suit la loi Binomiale de paramètres $n$ et $p$</red>, notée <enc>$B(n, p)$</enc>  
    On note parfois <enc>$X \suit B(n, p)$</enc> $\quad$ ou quelquefois $\quad$ <enc>$X \simarrow B(n,p)$</enc> $\quad$ ou $\quad$ <enc>$X \sim B(n, p)$</enc>

### Propriétés de la Loi Binomiale

!!! pte "Espérance, Variance et Écart-Type d'une v.a. suivant la Loi Binomiale"
    Soit $X$ une v.a. suivant la loi Binomiale $B(n,p)$

    1. Espérance : <enc>$E(X)=np$</enc>  
    1. Variance : <enc>$V(X)=np(1-p)$</enc>
    1. Écart-type : <enc>$\sigma(X) = \sqrt {np(1-p)}$</enc>

!!! remarque
    Toute variable aléatoire $X$ suivant une loi binomiale $B(n,p)$ avec $n\in \m N^*$ peut s'écrire comme une somme $X=X_1+X_2+...+X_n$ de $n$ variables de Bernoulli $Xk \suit B(1,p)$, indépendantes entre elles, c'est-à-dire :

    * Pour tout $k\in{0 ;1 ;... ;n}$, $X_k \suit B(1,p)$ (i.e. $X_k$ suit une loi de Bernoulli de paramètre $p$).
    * Par définition : $X_k$ vaut $1$ en cas de SUCCÈS à la $k$-ème réalisation de l'épreuve, et $0$ sinon.
    * Les $X_k$ sont indépendantes entre elles.

!!! demo
    1. Sur l'arbre représentant le Schéma de $n$ épreuves de Bernoulli, il y a $\coord nk$ chemins admettant exactement $k$ SUCCÈS, et donc $(n - k)$ ÉCHECS. Puisque les $n$ épreuves de Bernoulli du Schéma sont indépendantes et identiques, la probabilité d'un chemin est le produit des probabilités des branches qui le composent, or il y a exactement :
    
        * $k$ branches de probabilité $p$ $\quad$ et 
        * $n - k$ branches de probabilité $(1 - p)$ :

        donc :  

        <center>$P(X=k)=\coord nk \underbrace{ P(S)\times ...\times P(S)}_{k \text{ fois}} \times \underbrace{ P(\overline S)\times ...\times P(\overline S)}_{n-k \text{ fois}} = \coord nk p^k (1-p)^{n-k}$</center>

    1. <bd>Espérance</bd> Voici deux techniques de calcul de $E(X)$ lorsque $X \suit B(n,p)$ :

        * <bd @gris>Première Technique</bd> « À la main, en comprenant ce qu'on fait »

        On peut résumer la loi de la variable $X \suit B(n, p)$ comme suit : 

        |$k$|$0$|$1$|$...$|$k$|$...$|$n$|
        |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        |$P(X=k)$|$\coord n0 p^0 (1-p)^{n-0}$|$\coord n1 p^1 (1-p)^{n-1}$|$...$|$\coord nk p^k (1-p)^{n-k}$|$...$|$\coord nn p^n (1-p)^{n-n}$|
        {.petit .nocolor}

        $\begin{align*}
        \displaystyle
        E(X) &= \coord n0 p^0 (1-p)^{n-0}\times 0 + ... + \coord nk p^k (1-p)^{n-k}\times k + ...+ \coord nn p^n (1-p)^{n-n}\times n \\
            &= \sum_{k=0}^n k\times \coord nk p^k (1-p)^{n-k} \\
            &= 0\times \coord n0 p^0 (1-p)^{n-0} + \sum_{k=1}^n k\times \coord nk p^k (1-p)^{n-k} \\
            &= \sum_{k=1}^n k\times \coord nk p^k (1-p)^{n-k} \\
        \end{align*}$

        Or $k\coord nk=n\coord {n-1}{k-1}$ d'après le 4°) de la propriété 5, donc $\displaystyle E(X) = n\times \sum_{k=1}^{n} \coord {n-1}{k-1} p^k (1-p)^{n-k}$  
        On pose alors le « **changement de variables** » : <enc>$i = k - 1$</enc> $\quad$ (c'est-à-dire : $k = i + 1$), alors :  

        $\begin{align*}
        \displaystyle
        E(X) &= n\times \sum_{i=0}^{n-1} \coord {n-1}{i} p^{i+1} (1-p)^{n-(i+1)} \\
            &= n\times \sum_{i=0}^{n-1} \coord {n-1}{i} p\times p^{i} (1-p)^{n-i-1} \\
            &= n\times p \times \sum_{i=0}^{n-1} \coord {n-1}{i} p^{i} (1-p)^{(n-1)-i} \\
            &= n\times p \times (p+1-p)^{n-1} \\
            &= n\times p \times 1^{n-1} \\
            &= n\times p \\
        \end{align*}
        $

        * <bd @gris>Deuxième Technique</bd> En utilisant la décomposition en somme de $n$ v.a.indépendantes chacune suivant $B(1,p)$ :  
        On sait que $X \suit B(n,p)$ peut s'écrire comme somme de $n$ v.a. indépendantes $X_k$ de Bernoulli, i.e. 
        
            * $X_k \suit B(1,p)$ et
            * $X=X_1+X_2+...+X_n$ donc :  
        
        $XE(X)= E(X_1+X_2+...+X_n) = E(X_1) + E(X_2) ...+ E(X_n) = p+p+...+p = n\times p$

    1. <bd>Variance</bd> Encore deux Techniques pour le calcul de $V(X)$ lorsque $X \suit B(n,p)$

        * <bd @gris>Première Technique</bd> « À la main, en comprenant ce qu'on fait... »

        $\begin{align*}
        \displaystyle
        V(X) &= \sum_{k=0}^n P(X=k)k^2 - E(X)^2 \\
            &= \sum_{k=0}^n \coord nk p^k (1-p)^{n-k} k^2 - (np)^2 \\
            &= \sum_{k=1}^n \coord nk p^k (1-p)^{n-k} k^2 - (np)^2 \\
            &= \sum_{k=1}^n k\times \coord nk p^k (1-p)^{n-k} k - (np)^2 \\
            &= \sum_{k=1}^n n\times \coord {n-1}{k-1} p^k (1-p)^{n-k} k - (np)^2 \quad \text{ On pose } i = k - 1 \\
            &= n\sum_{i=0}^{n-1} \coord {n-1}{i} p^{i+1} (1-p)^{n-(i+1)} (i+1) - (np)^2 \\
            &= n\sum_{k=0}^{n-1} \coord {n-1}{k} p\times p^{k} (1-p)^{n-k-1} (k+1) - (np)^2  \quad \text{ On pose } k = i \\
            &= np\sum_{k=0}^{n-1} (k+1) \coord {n-1}{k} p^{k} (1-p)^{(n-1)-k} - (np)^2 \\
            &= np\sum_{k=0}^{n-1} k \coord {n-1}{k} p^{k} (1-p)^{(n-1)-k} + np\sum_{k=0}^{n-1} \coord {n-1}{k} p^{k} (1-p)^{(n-1)-k} - (np)^2 \\
            &= np\sum_{k=1}^{n-1} k \coord {n-1}{k} p^{k} (1-p)^{(n-1)-k} + np(p+1-p)^{n-1} - (np)^2 \\
            &= np\sum_{k=1}^{n-1} (n-1) \coord {n-2}{k-1} p^{k} (1-p)^{(n-1)-k} + np - (np)^2 \quad \text{ On pose } i = k - 1 \\
            &= n(n-1)p\sum_{i=0}^{n-2} \coord {n-2}{i} p^{i+1} (1-p)^{(n-1)-(i+1)} + np - (np)^2 \\
            &= n(n-1)p^2\sum_{i=0}^{n-2} \coord {n-2}{i} p^i (1-p)^{n-2-i} + np - (np)^2 \\
            &= n(n-1)p^2(p+1-p)^{n-2} + np - (np)^2 \\
            &= n(n-1)p^2 + np - (np)^2 \\
            &= (n^2-n)p^2 + np - (np)^2 \\
            &= \cancel{n^2p^2}-np^2 + np - \cancel{(np)^2} \\
            &= np(1-p) \\
        \end{align*}$

        * <bd @gris>Deuxième Technique</bd> En utilisant la décomposition en somme de $n$ v.a. indépendantes $\suit B(1,p)$ :

            $V(X) = V(X_1+X_2+...+X_n)$  $\quad$ or les variables $X_i$ sont indépendantes donc :  
            $V(X)=V(X_1+X_2+...+X_n)=V(X_1)+V(X_2)+...+V(X_n)$  
            (d'après la propriété $1$ du cours Variables Discrètes).
            Donc $\displaystyle V(X)=\sum_{k=1}^n p(1-p)=np(1-p)$

### Simulation de la loi Binomiale $B(n, p)$ sur un Tableur (Calc /Excel)

!!! exp "pour $n = 10$ et $p=0,4$"
    * Créer dans la colonne $A$ les valeurs pour $k$ allant de $0$ à $n$.
    * Avec **[Libre Office Calc](https://fr.libreoffice.org/download/telecharger-libreoffice/)** (Logiciel Libre, Open Source et Gratuit):
        * Dans la cellule $B2$, taper  <enc>= LOI.BINOMIALE(A2 ;10 ;0,4 ;0)</enc> puis faire une recopie vers le bas
        * Puis Aller dans : Insertion $\rightarrow$ Diagramme... $\rightarrow$ (type de diagramme, choisir :) Colonne

    * Avec **Microsoft Excel**
        * Dans la cellule $B2$, taper 
            * <enc>= LOI.BINOMIALE.N(A2 ;10 ;0,4 ;0)</enc> ou 
            * <enc>= LOI.BINOMIALE(A2 ;10 ;0,4 ;0)</enc> puis 
            * faire une recopie vers le bas (remarque : on peut remplacer le dernier $0$ par FAUX)
        * Puis Aller dans : Insérer $\rightarrow$ graphique $\rightarrow$ Histogramme

!!! col __50
    ![Simulation Loi Binomiale, Version 1](./img/simulationBinomiale1.png)
!!! col __50
    ![Simulation Loi Binomiale, Version 2](./img/simulationBinomiale2.png)


    V. Échantillonnage
        1. Exemple 1:
Dans une urne comportant une proportion p = 40% de boules Bleues (B) et le reste de Boules Rouges (R), on tire au hasard une boule et on décide de lui associer 1 (appelé SUCCÈS) si elle a le caractère C = Bleue et 0 (appelé ÉCHEC) sinon. Il s'agit donc d'une épreuve de Bernoulli de paramètre p = 0,4.
En répétant 50 fois cette expérience aléatoire, on obtient une liste de 50 nombres 1 et 0, liste de SUCCÈS et d'ÉCHECS nommée échantillon de taille 50, par exemple (1,0,0,1,1,1,0, ...0,0,1,1,0).
Il s'agit donc d'un chemin d'un Schéma de n = 50 épreuves de Bernoulli de paramètre p = 0,4.
Nous noterons X la variable aléatoire qui compte le nombre de SUCCÈS dans cet échantillon de taille 50, i.e. dans cette répétition de n = 50 épreuves de Bernoulli, ainsi :  X ↪ B(n, p) .
Précisons que pour que X ↪ B(n,p) il faut que le tirage de la boule s'effectue :
    * Avec remise de la boule dans l'urne, par défaut & dans le cas général sauf précision contraire.
    * (éventuellement, au pire) Sans remise de la boule dans l'urne, dans les quelques rares cas exceptionnels où la taille de la population, ici le nombre total de boules, soit jugée suffisamment grande par rapport à la taille de l'échantillon sélectionné.
Définition 1:
Un échantillon (aléatoire) de taille n est la liste des résultats obtenus par n répétitions indépendantes d'une même expérience aléatoire.

Si un échantillon comporte X = 17 fois le 1 (et donc 33 fois le 0), la fréquence f du caractère C = « Bleue » sur cet échantillon vaut donc  f =   .
Si l'on prélève plusieurs échantillons de taille 50, la fréquence f du caractère C = « Bleue » varie d'un échantillon à l'autre, car à priori on n'a pas tiré les mêmes boules entre un tirage et un autre, et cette fréquence f d'échantillonnage n'est donc pas en général exactement égale à la vraie fréquence p = 0,4.
Définition 2:
On dit que f fluctue autour de p. Il s'agit de la fluctuation d'échantillonnage.

        2. Intervalle de Fluctuation et Simulation sur Tableur :
On décide de simuler grâce à un logiciel (EXCEL, CALC ou autre), la situation de l'exemple précédent, c'est à dire de simuler la fluctuation de la fréquence du caractère C = « Bleue » sur un grand nombre (100 par exemple) d'échantillons de taille 50.
OUTIL : la « formule » EXCEL/CALC suivante, à entrer dans une cellule, renvoie 1 avec la probabilité p (avec p[0 ;1]), et 0 sinon (avec une probabilité  1  p) :	 =ENT(ALEA() + p) 
Cette formule permet donc de simuler une loi de Bernoulli de paramètre p dans une cellule.






Sur la figure ci-après, on place sur l'axe des x le numéro de la simulation (de 1 jusqu'à 100) d'un échantillon de taille 50 (= 50 boules tirées au hasard), et sur l'axe des y la fréquence f correspondante trouvée pour le xème échantillon de taille 50.
On remarque que malgré le hasard, la presque totalité des fréquences est comprise entre 
  et  et que celles-ci sont réparties autour de p = 0,4.

Propriété 1 et Définition 3: (Admise, )
    * Si	l'on prélève un échantillon de taille n dans une population où la fréquence d'un caractère C est p,
ET que l'on se trouve sous certaines conditions : [n25 et 0,2  p  0,8] ou [n 30 et np5 et n(1p)5]
Alors	la probabilité que la fréquence f de cet échantillon soit dans l'intervalle  I = 
	est au moins égale à 95% = 0,95
    * L'intervalle I ainsi trouvé s'appelle intervalle de fluctuation de la fréquence f au seuil de 95 %.
Plus généralement :
Définition 4:
Soit X une variable aléatoire qui suit une loi Binomiale B(n, p) et  la variable aléatoire qui représente la fréquence aléatoire de SUCCÈS.
Un intervalle de fluctuation de F au seuil de 95% est un intervalle :
    * de la forme  où a et b sont des entiers compris entre 0 et n.
    * tel que  ce qui est équivalent à dire  
Méthode Pratique : On s'efforce d'obtenir l'intervalle  de plus faible amplitude, pour cela : on effectue une simulation sur Tableur, puis on cherche et choisit :
 le plus petit entier a tel que  i.e. le plus grand a tel que  et
 le plus petit entier b tel que  i.e. le plus petit b tq 
Remarque 1 :
Remarquons que la même définition pourrait s'appliquer pour d'autres valeurs de seuil (90% , 98%, etc..)

Exemple 2 : Détermination d'un intervalle de fluctuation d'une fréquence F par simulation de B(n,p) :
Une urne contient 13 boules Rouges (R) et 37 boules Blanches (B).
La proportion de boules Rouges est donc  p = = 0,26.
On effectue n =  100 tirages au hasard avec remise.
On simule au tableur (EXCEL/CALC) une loi Binomiale B(100 ;0,26).
Cf. Figure ci-après, sur laquelle on a également représenté les probabilités cumulées  :
On voit (regarder la colonne C), que le plus petit a tel que >0,025 vaut  a = 18 .
Donc on peut dire que  et aussi  donc 
On voit (regarder la colonne C), que le plus petit b tel que 0,975 vaut  b = 35 .
Donc on peut dire que , autrement dit 
On s'aperçoit donc que  et donc que 
Conclusion :  I =   est un intervalle de fluctuation pour la fréquence F au seuil de 95%.
Graphiquement, on peut visualiser le principe de cette méthode sur les deux graphiques ci-dessus : on a coupé/enlevé juste un peu moins de 2,5% de chaque côté de ces courbes, afin de conserver au centre juste un peu plus de 95%.



        3. Détermination d'un intervalle de fluctuation à l'aide de la loi binomiale et comparaison avec celui-ci donné en 2de
Exemple 3:
Reprenons l'exemple 2 précédent, avec n = 100 et p = 0,26.
    * L'intervalle obtenu avec le résultat de 2de (remarquons que les conditions d'utilisation sont bien remplies : n25 et 0,2  p 0,8) est l'intervalle centré en p suivant :
I = 
donc  I =  
    * Nous avons vu dans l'exemple 2 comment déterminer un intervalle de fluctuation au seuil de 95% :
donc  J = [0,18 ;0,35] 

    * Comparaison entre les deux méthodes :
    * Les deux méthodes fournissent des intervalles de fluctuation très semblables.
    * On a  : Puisque nous savons que J est un intervalle de fluctuation pour F, au seuil de 95%, cela veut dire que les valeurs de F sont dans cet intervalle J avec une probabilité supérieure ou égale à 95%, donc à plus forte raison dans l'intervalle I, autrement dit on retrouve bien que l'intervalle I est un intervalle de fluctuation de F au seuil de 95%.
    * Le résultat de 2de est donc cohérent avec celui de 1ère, et plus simple à utiliser,
    * Mais : Le résultat de 1ère utilisant la simulation de la loi Binomiale est plus précis.
    * L'intervalle I de 2de est centré en p, tandis que l'intervalle J ne l'est pas en général :
D'ailleurs :   p = 0,26 mais il n'en est pas très éloigné...

        4. Prise de décision à partir d'un échantillon
Exemple 5 :
On cherche à savoir si une pièce est équilibrée.
    *  On fait l'hypothèse que la pièce est équilibrée, donc que la probabilité d'obtenir PILE vaut p = 0,5.
    * On lance n fois cette pièce et on détermine la fréquence d'obtenir PILE sur l'échantillon obtenu.
    * On décide d'un seuil, disons 95%, et on détermine par simulation au TABLEUR l'intervalle de fluctuation I au seuil de 95% à l'aide de la loi B(n, p).
    * On prend une décision :
    * Si f n'est pas dans I, on rejette l'hypothèse que cette pièce soit équilibrée avec un risque de se tromper dans 5% des cas.
Attention : On ne dit pas qu'il y a une probabilité de 5% de se tromper, car il n'y a rien d'aléatoire dans la prise de décision).
    * Si f est dans I, on ne rejette pas l'hypothèse que cette pièce soit équilibrée.
Attention : On ne dit pas dans ce cas qu'on accepte cette hypothèse, car le risque de se tromper en l'acceptant est inconnu.
    * Conclusion :
Attention au vocabulaire et à ce qu'il ne faut pas dire, très précis, quelquefois subtil à comprendre, et pas toujours intuitif ni compréhensible du premier coup. 