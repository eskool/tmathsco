# TMATHSCO : Probabilités. Lois Continues à densité

On considère une expérience aléatoire et un univers associé $\Omega$, muni d'une probabilité $P$.

## Variable aléatoire continue

!!! def "Variable Aléatoire Continue"
    Une <red>variable aléatoire continue</red> $X$ sur un intervalle $I$, est une fonction $X : \m R \to I$, qui à chaque issue de $\Omega$, associe un nombre réel appartenant à $I$.

L'univers image de toutes les issues est donc à priori infini, contrairement à ce qu'il se passait dans le cas des variables aléatoires discrètes connues jusqu'à présent.  
**Par défaut** dans toute la suite, une **variable aléatoire** désigne une **variable aléatoire <u>continue</u>**.

!!! exp
    * La variable aléatoire $X$ correspondant à l'heure d'arrivée au hasard entre $18$h et $20$h, est une variable aléatoire continue sur l'intervalle $[18;20]$.
    * La variable aléatoire $x$ égale à la durée de bon fonctionnement d'un équipement produit en grande série est une variable aléatoire continue sur l'intervalle $[0;+\infty[$.

## Loi de probabilité à densité

!!! def "Densité de Probabilité $f$"
    On se donne une fonction $f$ définie sur un intervalle $I$ telle que :

    * $f$ est continue sur $I$
    * $f$ est positive sur $I$
    * L'aire totale sous la courbe de $f$ sur l'intervalle $I$ vaut $1$

    Une telle fonction $f$ s'appelle une <red>densité de probabilité sur $I$</red>

!!! exp
    !!! col __50
        Sur $I=[a;b]$ (Loi Uniforme) :

        ![Loi Uniforme](./img/img1.png)
    !!! col __50
        Sur $I=\m R = ]-\infty;+\infty[$ (Loi normale) :

        ![Loi Normale](./img/img2.png)

!!! def "Loi à Densité"
    On dit que la <red>loi de probabilité</red> $P$ d'une variable aléatoire $X$ est une <red>loi à densité sur $I$</red>, ou que <red>la variable $X$ est à densité sur $I$</red>, lorsqu'il existe une fonction de densité $f$ sur $I$ telle que :  
    <center>$\forall [c;d] \in I, \quad P(c\leq X \leq d) = \text{Aire sous la courbe de } f, \text{ comprise entre } c \text{ et } d$</center>

!!! exp
    * Lorsque $X$ suit une loi uniforme sur $I=[a;b]$, on a : <enc>$f(x)=\dfrac {1}{b-a}$</enc>  

        !!! col __50
            ![Loi Uniforme P(c≤X≤d)](./img/img3.png)
        !!! col __50 clear
            ![Loi Uniforme P(X>c) ou P(X≥c)](./img/img4.png)

    <clear></clear>

    * Lorsque $X$ suit une loi normale sur $I=\m R=]-\infty;+\infty[$, on a <enc>$f(x)=\dfrac{1}{\sqrt{2\pi}}e^{-\dfrac{1}{2}x^2}$</enc>  

        !!! col __50
            ![Loi Uniforme P(c≤X≤d)](./img/img5.png)
        !!! col __50 clear
            ![Loi Uniforme P(X>c) ou P(X≥c)](./img/img6.png)

!!! pte
    Soit $X$ une variable aléatoire continue, à densité sur $I$. Soit $c$, $d$ deux réels de $I$.

    * $\forall c \in I, \quad P(X=c)=0$ 
    * $P(c \leq X \leq d) = P(c \leq X \lt d) = P(c \lt X \leq d) = P(c \lt X \lt d)$
    * Pour tout réel $e$ tel que $c \leq e \leq d$ : $P(c \leq X \leq d) = P(c \leq X \leq e) + P(e \leq X \leq d)$
    * $P(X \gt c) = 1 - P(X \leq c)$

## Espérance, Variance et Écart-Type de Lois continues

!!! def "Espérance $E(X)$"
    Soit $X$ une variable aléatoire continue.  
    L'<red>Espérance</red> d'une variable aléatoire continue $X$ de densité $f$ sur $[a;b]$ est le nombre réel :  

    <center><enc>$\displaystyle E(X) = \int_a^b tf(t)dt$</enc></center>

Cette définition prolonge au cas continu la définition donnée de l'espérance d'une variable aléatoire discrète.  

<bd>Interprétation de l'Espérance</bd>  
L'espérance d'une variable aléatoire continue $X$ peut encore être interprétée intuitivement comme la valeur moyenne des valeurs prises par $I$.

!!! pte "Linéarité de l'Espérance"
    Soit $X$ et $Y$ deux variables aléatoires continues.  
    Pour tous réels $a$ et $b$ : $E(aX+bY) = aE(X) + bE(Y)$

<bd>Cas particuliers</bd>  

* Pour tout réel $a$ : $E(aX) = aE(X)$
* Pour tout réel $b$ : $E(b) = b$ ( par exemple $E(1) = 1$ )
* Pour tous réels $a$ et $b$ : $E(aX+b) = aE(X) + b$

!!! def "Variance $V(X)$"
    Soit $X$ une variable aléatoire continue.  
    La <red>Variance</red> d'une variable aléatoire continue $X$ de densité $f$ sur $[a;b]$
    <center><enc>$\displaystyle V(X) = E\left( \left[ X - E(X)\right]^2 \right) = E(X^2) - E(X)^2 = \int_a^b t^2f(t)dt - E(X)^2$</enc></center>

!!! pte "Semi-Linéarité de la Variance"
    Soit $X$ et $Y$ deux variables aléatoires continues.
    Pour tous réels $a$ et $b$ : $V(aX+b) = a^2V(X)$

<bd>Cas Particuliers</bd>

* Pour tout réel $a$ : $V(aX) = a^2V(X)$
* Pour tout réel $b$ : $V(b) = 0$ ( par exemple $V(1) = 0$ )
* Pour tout réel $b$ : $V(X+b) = V(X)$

<bd @orange>Attention</bd>  
En général, $V(aX+bY) \neq aV(X) + bV(Y)$ : En effet, pour que cette égalité soit vraie, il faut avoir des conditions supplémentaires sur $X$ et $Y$ (comme par exemple que $X$ et $Y$ sont des variables indépendantes entre elles)

!!! def "Écart-Type $\sigma(X)$"
    Soit $X$ une variable aléatoire continue.  
    L'<red>Écart-Type</red> d'une variable aléatoire continue de densité $f$ sur $[a;b]$ est :  
    <center><enc>$\sigma(X) = \sqrt{V(X)}$</enc>$\,\,\, >0$</center>

<bd>Interprétation de l'Écart-Type</bd>  
Comme dans le cas discret, l'écart-type mesure une dispersion des valeurs prises par $X$ autour de sa moyenne $E(X)$


