# TMATHSCO, Probabilités : Cours Loi Exponentielle

## Densité de Probabilité

!!! def
    $\lambda$ désigne un nombre réel strictement positif.  
    Dire qu'une variable aléatoire <red> $X$ soit la loi exponentielle de paramètre $\lambda$</red> sur $[0;+\infty[$, et on note $X \suitloi \mc E (\lambda)$, signifie que sa densité de probablité $f$ est :  
    
    <center>
    $f(x) = \left\{
    \begin{matrix}
    \lambda e^{-\lambda x} & \text{ si } x\ge 0 \\
    0 & \text{ si } x\lt 0 \\
    \end{matrix}
    \right.
    $

## Fonction de Répartition $F$

!!! pte
    Soit $X$ une variable aléatoire qui suit la loi exponentielle de paramètre $\lambda$.  
    La fonction de répartition $F$ de la variable aléatoire $X$ est définie sur $\m R$ par :  

    <center>
    $F(x) = \left\{
    \begin{matrix}
    1-e^{-\lambda x} & \text{ si } x\ge 0 \\
    0 & \text{ si } x\lt 0 \\
    \end{matrix}
    \right.
    $
    </center>

??? demo
    $X$ prend ses valeurs sur $[0;+\infty[$, donc :  
    
    * pour tout réel $x\lt 0$, $F(x) = P(X\le x) = 0$
    * pour tout réel $x\ge 0$, $\displaystyle F(x) = P(X\le x) = P(0\le X \le x) = \int_0^x \lambda e^{-\lambda t} = \left[ -e^{-\lambda t} \right]_0^x = 1- e^{-\lambda x}$

<bd>Conséquences</bd>  

* Pour tout réel $c\ge 0$, $P(X\ge c) = e^{-\lambda c}$  
En effet, $P(X\ge c) = 1 - P(0 \le X \le  c) = 1 - (1- e^{-\lambda c}) = e^{- \lambda c}$
* Pour tous réels $c\ge 0$ et $d\gt 0$, $P(c\le X \le d) = F(d) - F(c) = (1-e^{- \lambda d}) - (1 - e^{- \lambda c}) = e^{- \lambda c} - e^{- \lambda d}$

## Propriétés

!!! pte "Absence de Mémoire"
    $X$ est une variable aléatoire qui suit la loi exponenentielle de paramètre $\lambda$.  
    Pour tous réels positifs $x$ et $h$, $\quad$ <enc>$\displaystyle P_{(X \ge x)}(X\ge x+h) = P(X\ge h)$</enc>

??? demo
    D'après la définition d'une probabilité conditionnelle, on a $P_A(B) = \dfrac{P(A \cap B)}{P(A)}$, donc  
    $\displaystyle P_{(X\ge x)}(X\ge x+h) = \dfrac{P((X\ge x+h) \cap (X\ge x))}{P(X\ge x)}$  
    De plus, comme $x\ge 0$ et $h\ge 0$, alors $x+h\ge x$, donc l'événement $\{(X\ge x+h) \cap (X\ge x) \}$ est l'événement $\{ (X\ge x) \}$  
    Ainsi :  
    $\begin{align*}
    \displaystyle
    P_{(X\ge x)}(X\ge x+h) &= \dfrac{P((X\ge x+h) \cap (X\ge x))}{P(X\ge x)} \\
        &= \frac{e^{-\lambda (x+h)}}{e^{-\lambda x}} \\
        &= \frac{e^{-\lambda x}e^{-\lambda h}}{e^{-\lambda x}} = \frac{\cancel{e^{-\lambda x}}e^{-\lambda h}}{\cancel{e^{-\lambda x}}} \\
        &= e^{-\lambda h} \\
        &= P(X\ge h)
    \end{align*}
    $

!!! info "Absence de Mémoire"
    On traduit quelquefois cette propriété par la phrase : "Les lois exponentielles n'ont pas de mémoire", ou ont une <red>absence de mémoire</red>.  
    Exemple d'interprétation de l'absence de mémoire : Dans le cas d'un composant électronique, la probabilité qu'un composant tombe en panne au cours d'un intervalle de temps dépend uniquement de la longueur de cet intervalle, et non pas de l'âge de ce composant au début de la période.

## Espérance et Variance

!!! pte "Espérance de la Loi Exponentielle"
    Soit $X$ une variable aléatoire suivant la loi exponentielle de paramètre $\lambda$, Alors :  
    <center><enc>$E(X) = \dfrac{1}{\lambda}$</enc></center>

??? demo
    $\displaystyle E(X) = \lim_{x \to +\infty} \int_0^x tf(t)dt$ avec $f(t) = \lambda e^{-\lambda t}$  
    donc $\displaystyle E(X) = \lim_{x \to +\infty} \int_0^x t \lambda e^{-\lambda t}dt = \lim_{x \to +\infty} \lambda \int_0^x t e^{-\lambda t}dt = \lim_{x \to +\infty} I(x)$ avec $\displaystyle I(x) = \lambda \int_0^x t e^{-\lambda t}dt$  
    <bd @gris> Déterminer une primitive $G$</bd>  
    Il s'agit donc de déterminer une primitive $G$ de la fonction $g(t) = te^{-\lambda t}$, qui doit donc vérifier l'égalité $G'(t)=g(t)$ pour tout $t\in [0;+\infty[$  
    On a l'idée de chercher $G$ sous la forme $G(t) = (at+b)e^{-\lambda t}$ où $a$ et $b$ sont deux réels constants à déterminer.  

    Calculons $G'(t)$

    $\begin{align*}
    \displaystyle
    G'(t) &= \left( (at+b)e^{-\lambda t} \right) ' \\
        &= (at+b)'e^{-\lambda t} + (at+b)(e^{-\lambda t})' \\
        &= ae^{-\lambda t} + (at+b)(-\lambda e^{-\lambda t}) \\
        &= e^{-\lambda t} [a -\lambda (at+b)] \\
        &= [-\lambda at+(a-\lambda b)]e^{-\lambda t} \\
        &= g(t) \\
        & = t e^{-\lambda t} \\
    \end{align*}
    $

    Ainsi, on doit avoir, pour tout réel $t$ :  
    $[-\lambda at+(a-\lambda b)]e^{-\lambda t}=t e^{-\lambda t} \quad \ssi \quad -\lambda at+(a-\lambda b) = t$  
    Par identification des coefficients, on doit donc avoir les égalités :

    $
    \displaystyle
    \left\{
    \begin{matrix*}
    -\lambda a = 1 \\
    a - \lambda b = 0 \\
    \end{matrix*}
    \right.
    $ $\ssi$ $
    \displaystyle
    \left\{
    \begin{matrix*}
    a = \dfrac{-1}{\lambda} \\
    b = \dfrac{a}{\lambda} \\
    \end{matrix*}
    \right.
    $ $\ssi$ $
    \displaystyle
    \left\{
    \begin{matrix*}
    a = \dfrac{-1}{\lambda} \\
    b = \dfrac{\dfrac{-1}{\lambda}}{\lambda} = \dfrac{-1}{\lambda ^2}\\
    \end{matrix*}
    \right.
    $

    Ceci prouve que $G(t) = \left( \dfrac{-1}{\lambda}t+\dfrac{-1}{\lambda^2} \right) e^{-\lambda t}$ est une primitive de $g(t)=t e^{-\lambda t}$

    <bd @gris>Calcul de $E(X)$</bd>  
    On a :
    $\displaystyle E(X) = \lim_{x \to +\infty} \lambda \int_0^x te^{-\lambda t}dt = \lim_{x \to +\infty} I(x)$

    avec :

    $
    \begin{align*}
    \displaystyle
    I(x) &= \lambda \int_0^x te^{-\lambda t}dt \\
        &= \lambda \left[ G(t) \right]_0^x \\
        &= \lambda \left[ G(x) - G(0) \right] \\
        &= \lambda \left[ \left( \dfrac{-1}{\lambda}x+\dfrac{-1}{\lambda^2} \right) e^{-\lambda x} - \left( \dfrac{-1}{\lambda}0+\dfrac{-1}{\lambda^2} \right) e^{-\lambda \times 0} \right] \\
        &= \lambda \left[ \left( \dfrac{-1}{\lambda}x+\dfrac{-1}{\lambda^2} \right) e^{-\lambda x} - \left( \dfrac{-1}{\lambda^2} \right) \times 1 \right] \\
        &= -xe^{-\lambda x} + \dfrac{-1}{\lambda} e^{-\lambda x} + \dfrac{1}{\lambda} \\
    \end{align*}
    $

    De plus :  

    * $\displaystyle \lim_{x \to +\infty} -xe^{-\lambda x} = 0$
    * $\displaystyle \lim_{x \to +\infty} \dfrac{-1}{\lambda}e^{-\lambda x} = 0$
    * $\displaystyle \lim_{x \to +\infty} \dfrac{1}{\lambda} = \dfrac{1}{\lambda}$

    donc :

    $
    \begin{align*}
    \displaystyle
    E(X) &= \lim_{x \to +\infty} \left( -xe^{-\lambda x} + \dfrac{-1}{\lambda} e^{-\lambda x} + \dfrac{1}{\lambda} \right) \\
        &= \displaystyle \underbrace{\lim_{x \to +\infty} -xe^{-\lambda x}}_{0} + \underbrace{\lim_{x \to +\infty} \dfrac{-1}{\lambda}e^{-\lambda x}}_{0} + \underbrace{\lim_{x \to +\infty} \dfrac{1}{\lambda}}_{\dfrac{1}{\lambda}} \\
        &= 0+0+\dfrac{1}{\lambda} \\
        &= \dfrac{1}{\lambda} \\
    \end{align*}
    $  

!!! pte "Variance de la Loi Exponentielle"
    Soit $X$ une variable aléatoire suivant la loi exponentielle de paramètre $\lambda$, Alors :  
    <center><enc>$V(X) = \dfrac{1}{\lambda^2}$</enc></center>

??? demo
    $\displaystyle V(X) = \lim_{x \to +\infty} \int_0^x (t-E(X))^2f(t)dt$ avec $f(t) = \lambda e^{-\lambda t}$  
    donc :  
    $
    \begin{align*}
    \displaystyle V(X) &= \lim_{x \to +\infty} \int_0^x \left( t - \dfrac{1}{\lambda} \right)^2 \lambda e^{-\lambda t}dt \\
        &= \lim_{x \to +\infty} \int_0^x \left( t^2 - \dfrac{2t}{\lambda} + \dfrac{1}{\lambda^2} \right) \lambda e^{-\lambda t}dt \\
        &= \lim_{x \to +\infty} \int_0^x \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}dt \\
        &= \lim_{x \to +\infty} I(x) \text{ avec } I(x) = \int_0^x \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}dt
        \end{align*}
        $  
    <bd @gris> Déterminer une primitive $G$</bd>  
    Il s'agit donc de déterminer une primitive $G$ de la fonction $g(t) = \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}$, qui doit donc vérifier l'égalité $G'(t)=g(t)$ pour tout $t\in [0;+\infty[$  
    On a l'idée de chercher $G$ sous la forme $G(t) = (at^2+bt+c)e^{-\lambda t}$ où $a$, $b$ et $c$ sont deux réels constants à déterminer.  

    Calculons $G'(t)$

    $\begin{align*}
    \displaystyle
    G'(t) &= \left( (at^2+bt+c)e^{-\lambda t} \right) ' \\
        &= (at^2+bt+c)'e^{-\lambda t} + (at^2+bt+c)(e^{-\lambda t})' \\
        &= (2at+b)e^{-\lambda t} + (at^2+bt+c)(-\lambda e^{-\lambda t}) \\
        &= e^{-\lambda t} [2at + b -\lambda (at^2 + bt + c)] \\
        &= e^{-\lambda t} [-\lambda a t^2 + t(2a - \lambda b) + (b - \lambda c) ] \\
        &= g(t) \\
        & = \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t} \\
    \end{align*}
    $

    Ainsi, on doit avoir, pour tout réel $t$ on doit avoir :  
    $[-\lambda at+(a-\lambda b)]e^{-\lambda t}=t e^{-\lambda t} \quad \ssi \quad -\lambda at+(a-\lambda b) = t$  
    Par identification des coefficients, on doit donc avoir les égalités :

    $
    \displaystyle
    \left\{
    \begin{matrix*}
    -\lambda a = \lambda \\
    2a - \lambda b = -2 \\
    b - \lambda c = \dfrac{1}{\lambda} \\
    \end{matrix*}
    \right.
    $ $\ssi$ $
    \displaystyle
    \left\{
    \begin{matrix*}
    a = -1 \\
    -2 -\lambda b = -2 \\
    -\lambda c = \dfrac{1}{\lambda} - b \\
    \end{matrix*}
    \right.
    $ $\ssi$ $
    \displaystyle
    \left\{
    \begin{matrix*}
    a = -1 \\
    b = 0 \\
    -\lambda c = \dfrac{1}{\lambda} - 0 \\
    \end{matrix*}
    \right.
    $  $\ssi$ $
    \displaystyle
    \left\{
    \begin{matrix*}
    a = -1 \\
    b = 0 \\
    c = \dfrac{-1}{\lambda^2} \\
    \end{matrix*}
    \right.
    $

    Ceci prouve que $G(t) = \left( -t^2 - \dfrac{1}{\lambda^2} \right) e^{-\lambda t}$ est une primitive de $g(t) = \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}$

    <bd @gris>Calcul de $V(X)$</bd>  
    On a :
    $\displaystyle V(X) = \lim_{x \to +\infty} \int_0^x \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}dt = \lim_{x \to +\infty} I(x)$

    avec :

    $
    \begin{align*}
    \displaystyle
    I(x) &= \int_0^x \left( \lambda t^2 - 2t + \dfrac{1}{\lambda} \right) e^{-\lambda t}dt \\
        &= \left[ G(t) \right]_0^x \\
        &= \left[ G(x) - G(0) \right] \\
        &= \left[ \left( -x^2 - \dfrac{1}{\lambda^2} \right) e^{-\lambda x} - \left( -0^2 - \dfrac{1}{\lambda^2} \right) e^{-\lambda \times 0} \right] \\
        &= -x^2 e^{-\lambda x} - \dfrac{e^{-\lambda x}}{\lambda^2} + \dfrac{1}{\lambda^2} \times 1 \\
        &= -x^2 e^{-\lambda x} - \dfrac{e^{-\lambda x}}{\lambda^2} + \dfrac{1}{\lambda^2} \\
    \end{align*}
    $

    De plus :  

    * $\displaystyle \lim_{x \to +\infty} -x^2 e^{-\lambda x} = 0$
    * $\displaystyle \lim_{x \to +\infty} \dfrac{-e^{-\lambda x}}{\lambda^2} = 0$
    * $\displaystyle \lim_{x \to +\infty} \dfrac{1}{\lambda^2} = \dfrac{1}{\lambda^2}$

    donc :

    $
    \begin{align*}
    \displaystyle
    V(X) &= \lim_{x \to +\infty} \left( -x^2 e^{-\lambda x} + \dfrac{-e^{-\lambda x}}{\lambda^2} + \dfrac{1}{\lambda^2} \right) \\
        &= \displaystyle \underbrace{\lim_{x \to +\infty} -x^2 e^{-\lambda x}}_{0} + \underbrace{\lim_{x \to +\infty} \dfrac{-e^{-\lambda x}}{\lambda^2}}_{0} + \underbrace{\lim_{x \to +\infty} \dfrac{1}{\lambda^2}}_{\dfrac{1}{\lambda^2}} \\
        &= 0+0+\dfrac{1}{\lambda^2} \\
        &= \dfrac{1}{\lambda^2} \\
    \end{align*}
    $  