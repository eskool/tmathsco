# TMATHSCO, Probabilités : Cours Loi Uniforme (Continue)

<bd>Contexte classique d'utilisation</bd>  

* Un **instant d'arrivée** au hasard dans un intervalle de temps $[a;b]$.
* Un **nombre choisi** au hasard dans un intervalle $I = [a; b]$
* Plus généralement, une **mesure** comprise au hasard entre deux valeurs $a$ et $b$.

!!! def
    On dit qu'une variable aléatoire <red>$X$ suit une loi uniforme sur l'intervalle</red>
    $I = [a; b]$ , et on note $X \suitloi \mc U \left( [a; b] \right)$ , lorsque :  
    Pour tout intervalle $[c; d] \in  [a;b], \quad P(c \leq X \leq d) = \dfrac{d-c}{b-a}$

Une variable aléatoire $X$ suit la loi uniforme sur l'intervalle $[a ; b]$ lorsque la loi de probabilité $P$ de $X$ sur $I$ est une **loi de densité de probabilité constante** sur $[a; b]$ telle que, pour tout réel $x \in [a;b]$ : $f(x) = \dfrac{1}{b-a}$

!!! ex
    Claire passe voir Laurent à un moment quelconque entre $18h30$ et $20h45$.  
    Quelle est la probabilité qu'elle arrive durant le feuilleton préféré de Laurent qui passe de $19h$ à $19h30$ ?

!!! ex
    On choisit un nombre au hasard entre $10$ et $50$.  
    On note $X$ la v.a. qui indique le nombre choisi.  
    Déterminer $P(X = 15)$ , $P (30 \leq X \leq 40 )$ , $P( 10 \lt X \lt 30 )$ , puis $P ( X \gt 20 )$ .

!!! ex
    La durée de communication téléphonique entre Laure et Alicia ne dépasse pas $2h$.  
    Laure appelle Alicia au téléphone.  
    Déterminer la probabilité que la durée de communication soit :

    1. de $30$ min
    1. d'au moins $10$ min
    1. comprise entre $20$ min et $40$ min

!!! pte "Espérance de la loi Uniforme"
    Si $X$ suit une loi uniforme sur $I = [a; b]$, alors son espérance est :  
    <center><enc>$E(X) = \dfrac{a+b}{2}$</enc></center>

??? demo
    $\begin{align*}
    \displaystyle
    E(X) &= \int_a^b tf(t)dt \\
        &= \int_a^b \dfrac{t}{b-a}dt \\
        &= \dfrac{1}{b-a}\int_a^b tdt \\
        &= \dfrac{1}{b-a} \left[ \dfrac{t^2}{2}\right]_a^b \text{ car } F(t)=\dfrac{t^2}{2} \text{ est une primitive de } f(t)=t \\
        &= \dfrac{1}{b-a} \left[ \dfrac{b^2}{2} - \dfrac{a^2}{2} \right] \\
        &= \dfrac{1}{b-a} \left[ \dfrac{b^2-a^2}{2} \right] \\
        &= \dfrac{b^2-a^2}{2(b-a)} \\
        &= \dfrac{(b-a)(b+a)}{2(b-a)} = \dfrac{\cancel{(b-a)}(b+a)}{2\cancel{(b-a)}} \\
        &= \dfrac{b+a}{2} = \dfrac{a+b}{2} \\
    \end{align*}$

<bd>Traduction intuitive</bd> $\dfrac{a+b}{2}$ est la moyenne de **tous** les nombres compris entre $a$ et $b$  
(pas seulement des deux extrémités $a$ et $b$ ).

!!! pte "Variance de la loi Uniforme"
    Si $X$ suit une loi uniforme sur $I = [a; b]$, alors sa <red>Variance</red> est :  
    <center><enc>$V(X) = \dfrac{(b-a)^2}{12}$</enc></center>

!!! demo
    On sait que $X$ est une v.a. de densité $f(t)=\dfrac{1}{b-a}$
    $\begin{align*}
    \displaystyle
    V(X) &= \int_a^b (t-E(X))^2f(t)dt \text{ avec } f(t) = \dfrac{1}{b-a} \\
        &= \int_a^b (t-E(X))^2 \times \dfrac{1}{b-a} dt \\
        &= \dfrac{1}{b-a} \int_a^b (t-E(X))^2dt \\
        &= \dfrac{1}{b-a} \left[ \dfrac{(t-E(X))^3}{3} \right]_a^b  \text{ car } G(t) = \dfrac{(t-E(X))^3}{3} \text{ est une primitive de } g(t)=(t-E(X))^2 \\
        &= \dfrac{1}{b-a} \dfrac{(b-E(X))^3}{3} -  \dfrac{(a-E(X))^3}{3} \\
        &= \dfrac{1}{b-a} \dfrac{(b-E(X))^3-(a-E(X))^3}{3} \\
        &= \dfrac{1}{3(b-a)} \left[ (b-E(X))^3-(a-E(X))^3 \right] \\
        & \text{ Or } \quad B=b-E(X) = b-\dfrac{a+b}{2} = \dfrac{2b}{2} - \dfrac{a+b}{2} = \dfrac{b-a}{2} \\
        & \text{ Et } \quad A=a-E(X) = a-\dfrac{a+b}{2} = \dfrac{2a}{2} - \dfrac{a+b}{2} = \dfrac{a-b}{2} \\
        &\text{Rappelons l'identité remarquable : } B^3 - A^3 = (B-A)(B^2+BA+A^2) \text{ donc} \\
        V(X) &= \dfrac{1}{3(b-a)} \left[ \left(\dfrac{b-a}{2} - \dfrac{a-b}{2} \right) \left( \left(\dfrac{b-a}{2} \right)^2 + \left(\dfrac{b-a}{2} \right) \left(\dfrac{a-b}{2} \right) + \left(\dfrac{a-b}{2} \right)^2 \right) \right] \\
        V(X) &= \dfrac{1}{3(b-a)} \left( \dfrac{\cancel{2}(b-a)}{\cancel{2}} \right) \left( \left(\dfrac{b-a}{2} \right)^2 + \left(\dfrac{b-a}{2} \right) \left(\dfrac{a-b}{2} \right) + \left(\dfrac{a-b}{2} \right)^2 \right) \\
        V(X) &= \dfrac{\cancel{(b-a)}}{3\cancel{(b-a)}} \left[ \left(\dfrac{b-a}{2} \right)^2 + \left(\dfrac{b-a}{2} \right) \left(\dfrac{a-b}{2} \right) + \left(\dfrac{a-b}{2} \right)^2 \right] \\
        V(X) &= \dfrac{1}{3} \left[ \dfrac{(b-a)^2}{4} - \dfrac{(b-a)(b-a)}{4} + \dfrac{(a-b)^2}{4} \right] \\
        V(X) &= \dfrac{1}{3} \times \dfrac{1}{4} \times \left[ \cancel{(b-a)^2} - \cancel{(b-a)^2} + (b-a)^2 \right] \text{ car } (a-b)^2=(b-a)^2 \\
        V(X) &= \dfrac{1}{12} \times (b-a)^2 \\
        V(X) &= \dfrac{(b-a)^2}{12} \\
    \end{align*}$

!!! ex
    Déterminer les Espérances, les Variances et les Écarts-Types pour les exercices $1$, $2$ et $3$ précédents.