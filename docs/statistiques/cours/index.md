# TMATHSCO : Statistiques à 2 Variables

## Nuage de points/ Ajustement Affine

### Série Statistique à 2 Variables quantitatives

Sur une population, on étudie deux caractères quantitatifs $x$ et $y$ (par exemple, la taille et le poids des joueuses d'une Équipe de Basket féminine). Pour chacun des $n$ individus de cette population, on note $x_i$ et $y_i$ les valeurs prises par chacun de ces caractères et on présente les données à l'aide de la série statistique à deux varaibles ci-dessous:

<center>

|>|>|>|>|Série Statistique à 2 Variables|
|:-:|:-:|:-:|:-:|:-:|
|Valeur $x_i$|$x_1$|$x_2$|...|$x_n$|
|Valeur $y_i$|$y_1$|$y_2$|...|$y_n$|  

</center>

* Dans un repère, le **nuage de points** associé à la série statistique ci-dessus est l'ensemble des points $M_i$ de coordonnées $(x_i, y_i)$
  * Le **point moyen** de ce nuage est le point $G$ de coordonnées $\left( \overline{x}, \overline{y} \right)$ où :
    * $\overline x$ est la moyenne des valeurs $x_i$
    * $\overline y$ est la moyenne des valeurs $y_i$


!!! pte
    * Dans un repère, le **nuage de points** associé à la série statistique ci-dessus est l'ensemble des points $M_i$ de coordonnées $(x_i, y_i)$
    * Le **point moyen** de ce nuage est le point $G$ de coordonnées $\left( \overline{x}, \overline{y} \right)$ où :
        * $\overline x$ est la moyenne des valeurs $x_i$
        * $\overline y$ est la moyenne des valeurs $y_i$

### Ajustement d'un nuage de points

On peut se demander: &laquo; Existe-t-il une dépendance entre les valeurs $x_i$ et $y_i$ prises par les deux variables ? &raquo;

Selon la forme du nuage associé, on peut être tenté de :

* voir un certain type de dépendance entre $x$ et $y$:
  * voir un type de **dépendance affine**: c'est à dire penser que les points se répartissent autour d'une certaine droite (figure 1)
  * ou bien voir un type de **dépendance fonctionnelle**: c'est à dire penser que les points se répartissent autour d'une certaine courbe $\mathcal C_f$ d'une certaine fonction $f$ (figure 2)
* ou bien, ne pas voir de dépendance du tout (figure 3)

!!! def
    Pratiquer un <red>ajustement affine</red> d'un nuage de points consiste à tracer une droite qui passe le plus près possible des points nuage

Des questions viennent à l'esprit:

* Quelle droite tracer?
* Y en -a-til une meilleure que l'autre?
* meilleure selon quel critère?
* ...

La *méthode des moindre carrés* apportera quelques réponses

!!! exp
    Six élèves etc..

## Droite des Moindres Carrés

### Méthode des Moindres Carrés

!!! mth
    Le problème de l'ajustement consiste à estimer les coefficients $a$ et $b$ qui fournissent la "*meilleure*" droite $d$ selon un certain crière.  
    Le critère choisi par méthode des moindres carrés est le suivant:  
    La droite $d$ doit minimiser la somme des $M_iA_i^2$ où $A_i$ est le point de $d$ de même abscisse $x_i$ que le point $M_i$.  
    Ainsi, les nombres réels $a$ et $b$ de l'équation réduite $d:y=ax+b$ de la droite $d$ des moindres carrés, sont choisis de sorte à **minimiser la <red>distance</red>**:

    <center>$\displaystyle \sum_{i=1}^n M_iA_i^2 = \sum_{i=1}^n \left( y_i-(ax_i+b)^2 \right)$</center>

    Cette <red>distance</red> représente la somme des carrés des distances verticales des points $M_i$ à la droite $d$:

### Équation de la droite des moindes carrés

Notations : $V(x)=\dfrac 1n \displaystyle \sum_{i=1}^n (x_i - \overline x)^2$ et $V(y)=\dfrac 1n \displaystyle \sum_{i=1}^n (y_i - \overline y)^2$ sont les variances respectives des séries $(x_i)$ et $(y_i)$

!!! def
    On appelle <red>covariance de la série $(x_i;y_i)$</red>, le nombre réel noté <red>$cov(x;y)$</red>, tel que:

    <center>$\displaystyle cov(x;y)=\dfrac 1n \sum_{i=1}^n (x_i - \overline x)(y_i - \overline y)$</center>

!!! pte
    Dans un repère, la ***droite des moindres carrés*** (ou **droite de régression**) de $y$ en $x$ associé au nuage de points $M_i(x_i;y_i)$ avec $1\le i \le n$:

    * passe par le point moyen $G(\overline x; \overline y)$ du nuage
    * a pour équation <encadre style="margin-right:0.8em;">$d:y=a(x - \overline x)+\overline y$</encadre> avec <encadre>$a=\dfrac{cov(x;y)}{V(x)}$</encadre>

??? demo
    :one: Soit $S=\displaystyle \sum_{i=1}^n A_iM_i^2 = \sum_{i=1}^n (y_i-(ax_i+b))^2 = \sum_{i=1}^n ((y_i-ax_i)-b))^2 = \sum_{i=1}^n \left((y_i-ax_i)^2-2b(y_i-ax_i)+b^2 \right)$  
    $\displaystyle = \sum_{i=1}^n (y_i-ax_i)^2 - \sum_{i=1}^n 2b(y_i-ax_i) + \sum_{i=1}^n b^2 = \sum_{i=1}^n (y_i-ax_i)^2 - 2b\sum_{i=1}^n (y_i-ax_i) + nb^2$  
    donc $S=  \displaystyle nb^2 - 2b\sum_{i=1}^n (y_i-ax_i) + \sum_{i=1}^n (y_i-ax_i)^2$  
    donc $S$ peut être vue comme une fonction $S(b)$ trinôme du second degré, qui dépend de la variable $b$. 

    On suppose que $a$ est fixe.
    $S$ admet un minimum (car $n\ge 0$), qui est atteint lorsque sa dérivée $S'(b)=0$.
    or $S'(b) = \displaystyle 2nb -2\sum_{i=1}^n (y_i-ax_i)$
    donc $S'(b)=0 \Leftrightarrow \displaystyle 2nb -2\sum_{i=1}^n (y_i-ax_i) = 0 \Leftrightarrow 2nb = 2\sum_{i=1}^n (y_i-ax_i) \Leftrightarrow nb = \sum_{i=1}^n (y_i-ax_i)$
    $\Leftrightarrow b = \displaystyle \dfrac {1}{n}\times \sum_{i=1}^n (y_i-ax_i) = \dfrac 1n \sum_{i=1}^n y_i - a\times \dfrac 1n \sum_{i=1}^n x_i = \overline y - a\overline x \Leftrightarrow$ <encadre>$b=\overline y - a\overline x$</encadre>  
    En outre, $b=\overline y -a\overline x \Leftrightarrow \overline y = a\overline x + b$
    Ceci prouve déjà le premier point de la propriété: la droite $d$ des moindres carrés passe par le point moyen $G(\overline x;\overline y)$, puisque ses coordonnées $(\overline x;\overline y)$ vérifient l'équation réduite de $d:y=ax+b$  
    :two: Ensuite, on connaît l'équation réduite de $d:y=ax+b \Leftrightarrow y=ax+(\overline y - a \overline x) \Leftrightarrow y=ax - a \overline x +\overline y$  
    donc on a l'équation réduite de <encadre>$d:y=a(x-\overline x)+\overline y$</encadre>  
    :three: Il manque juste à déterminer la formule de $a$ avec la covariance.  
    On sait déjà que $S=\displaystyle \sum_{i=1}^n (y_i-ax_i-b)^2$ avec $b=\overline y - a\overline x$  
    donc $S=\displaystyle \sum_{i=1}^n (y_i-ax_i- (\overline y - a\overline x))^2 = \sum_{i=1}^n (y_i-ax_i- \overline y + a\overline x)^2 = \sum_{i=1}^n \left((y_i - \overline y) - a(x_i - \overline x)\right)^2$  
    donc $S= \displaystyle \sum_{i=1}^n (y_i - \overline y)^2 - \sum_{i=1}^n  2a(y_i - \overline y)(x_i - \overline x) + \sum_{i=1}^n a^2(x_i - \overline x)^2$  
    donc $S= \displaystyle a^2 \sum_{i=1}^n (x_i - \overline x)^2 - 2a\sum_{i=1}^n (x_i - \overline x)(y_i - \overline y) + \sum_{i=1}^n (y_i - \overline y)^2$  
    donc $S$ peut être vue comme une fonction $S(a)$ trinôme du second degré, qui dépend de $a$.
    De plus, on a les 3 égalités:

    * $\displaystyle \sum_{i=1}^n (x_i - \overline x)^2 = nV(x)$ et 
    * $\displaystyle \sum_{i=1}^n (y_i - \overline y)^2 = nV(y)$
    * $\displaystyle \sum_{i=1}^n (x_i - \overline x)(y_i - \overline y) = n .cov(x;y)$

    donc $S(a) = nV(x)a^2-2an.cov(x;y) + nV(y)$  
    $S(a)$ admet un minimum car $nV(x)\gt 0$, qui est atteint lorsque $S'(a)=0$
    or $S'(a)=2nV(x)a-2n.cov(x;y)$  
    donc $S'(a)=0 \Leftrightarrow 2nV(x)a-2n.cov(x;y)=0 \Leftrightarrow 2nV(x)a = 2n.cov(x;y) \Leftrightarrow V(x)a = cov(x;y)$  
    donc $S'(a)=0 \Leftrightarrow$ <encadre>$a = \dfrac{cov(x;y)}{V(x)}$</encadre>

!!! ex
    (à faire)

## Coefficient de Corrélation $r$

### Coefficient de corrélation linéaire $r$

La décision d'ajuster un nuage de points par une droite s'est prise jusqu'à présent à la seule "*vue*" du nuage de points, selonn que sa forme soit linéairement allongée, ou pas.
Le coefficient de corrélation suivant correspond à une forme de quantification de cette intuition:

!!! def
    On appelle <red>coefficient de corrélation linéaire entre $x$ et $y$</red> le nombre réel noté $r$:

    <center>

    <encadre style="padding-top:0.5em;padding-bottom:0.5em;">$r = \dfrac{cov(x;y)}{\sigma(x)\sigma(y)}$</encadre1>

    </center>

    où $\sigma(x)$ et $\sigma(y)$ désignent resp. l'écart-type des séries $(x_i)$ et $(y_i)$

!!! pte
    <center>$r = \dfrac{\displaystyle \sum_{i=1}^n (x_i - \overline x)(y_i - \overline y)}{\sqrt{\displaystyle \sum_{i=1}^n (x_i - \overline x)^2}\sqrt{\displaystyle \sum_{i=1}^n (y_i - \overline y)^2}}$</center>

!!! pte "(Admise :cry:)"
    Pour toute série statistique à deux variables $x$ et $y$,
    <center>$-1\le r \le 1$</center>

<bd>Conséquences</bd>

* **$r$ a toujours le même signe que $cov(x;y)$** car $\sigma(x)>0$ et $\sigma(y)>0$
* Si $r=1$ ou $r=-1$, Alors la corrélation entre $x$ et $y$ est maximale: les points du nuage sont parfaitement alignés
* Si $r>0$, Alors la droite de régression a une **pente positive** dans un repère orthonormé
* Si $r<0$, Alors la droite de régression a une **pente négative** dans un repère orthonormé

<bd>Remarque</bd>

* Une très forte corrélation ***peut*** traduire un lien de cause à effet entre $x$ et $y$, **néanmoins, ATTENTION, ce n'est pas toujours le cas**.  
On peut trouver des exemples totalement improbables de deux variables étudiées $x$ et $y$ qui sont fortement corrélées, mais qui n'ont pourtant rien à voir l'une avec l'autre (à priori...)
* Le site : https://www.tylervigen.com/spurious-correlations recense quelques exemples... dont certains sont clairement et volontairement loufoques. Par exemple:

<center>

<img src="img/spuriousCorrelation.png" class="box">
<figcaption>

Très Forte Corrélation (r &asymp; 0,99) entre le nombre de divorces dans le Maine 🇺🇸 et la consommation de Margarine...

</figcaption>

</center>

### Ajustement se ramenant à un ajustement affine, par changement de variable

Dans certains cas, les points du nuage semblent se répartir autour d'une courbe $\mathcal C_f$ autre que celle d'une droite. Il est quelquefois possible de ramener l'étude à celui d'un ajustement affine **à l'aide d'un changement de variables**.

!!! exp
    Dans le cas suivant, on peut penser qu'il existe une relation du type $y=ax^2+b$ entre $x$ et $y$.  
    Le changement de variables $u=x^2$ permet de ramener l'étude à $y=au+b$ et on peut déterminer $a$ et $b$ avec la méthode des moindres carrés.

!!! exp
    Dans le cas suivant, on peut penser qu'il existe une relation du type $y=ae^x+b$ entre $x$ et $y$.  
    Le changement de variables $u=e^x$ permet de ramener l'étude à $y=au+b$ et on peut déterminer $a$ et $b$ avec la méthode des moindres carrés.




