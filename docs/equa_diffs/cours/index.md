# TMaths Co : Équations Différentielles

## Notion d'Équation Différentielle

### Un Exemple Introductoire

On peut se demander quelle fonction dérivable $f$ vérifie l'équation : pour tout réel $x\in I=\mathbb R$,

<center>$f'(x)=e^x+2 \quad (E)$</center>

La question est donc de trouver une fonction $f$ dont la dérivée $f'(x)$ vérifie une certaine égalité (elle doit être égale à $e^x+2$ dans notre cas particulier) pour tout réel $x$ dans un certain intervalle $I$.  
Il se pourrait même qu'il existe plusieurs fonctions $f$ distinctes vérifiant cette même égalité.  
Nous avons donc affaire à *une équation fonctionnelle, càd dont l'inconnue est une fonction $f$*, et dans laquelle intervient également *une fonction dérivée $f'$*.
Une telle équation fonctionnelle (faisant intervenir $f$ et sa dérivée $f'$) sur un intervalle $I$ est appelée <red>une équation différentielle sur l'intervalle $I$</red> (dans notre cas $I=\mathbb R)$.  
Comme d'habitude pour les équations, le but est de trouver non seulement **une** solution particulière $f$ à une telle équation, mais **toutes** les solutions $f$ vérifiant l'équation:  
C'est ce que l'on appelle <red>Résoudre l'équation différentielle sur $I$</red>

<bd>Notation des Équations Différentielles</bd>
Puisque l'on a l'habitude de noter &laquo; $y=f(x)$ &raquo;, alors on décide de noter aussi &laquo; $y'=f'(x)$ &raquo;, ce qui justifie qu'on note plutôt l'équation différentielle souvent de la manière suivante:

<center>

<enc>$y'=e^x+2\quad (E)$</enc>

</center>

<bd>Une Idée de Solution particulière?</bd>

* On sait qu'une primitive de $e^x$ est $e^x$
* On sait qu'une primitive de $2$ est $2x$
* donc une primitive de la fonction $f(x)=e^x+2$ est $F(x)=e^x+2x$

On a donc l'égalité: $F'(x)=f(x)=e^x+2$

Cela prouve que **la fonction $F$ définie par <enc>$F(x)=e^x+2x$</enc> $\,\,$, est UNE solution particulière de l'équation différentielle sur $I=\mathbb R$**

<bd>Existe-t-il d'autres solutions?</bd>

Notons $G$ une solution quelconque de l'équation différentielle $(E)$ sur $I$, c'est-à-dire $G'(x)=f(x)=e^x+2$, alors:

* $(G-F)'(x)=G'(x)-F'(x)=f(x)-f(x)=0$ sur $I$
* $G-F$ est donc une fonction dont la dérivée vaut $0$ sur tout l'intervalle $I$
* donc $G-F$ est une fonction constante sur $I$,
* Il existe donc une constante $k$ telle que, pour tout réel $x$, $G(x)-F(x)=k$
Autrement dit, Il existe une constante $k$ telle que $G(x)=F(x)+k$
* Pour chaque valeur de $k$ distincte, la fonction $G$ ainsi définie est une solution distincte de l'équation différentielle $(E)$

**Conclusion:**
La forme générale des solutions de l'équation différentielle $(E)$ est $G(x)=F_k(x)=F(x)+k$ où $k$ décrit $\mathbb R$

<bd>Généralisation ?</bd>

Le même exemple, mais en version plus générale, pourrait se formuler ainsi: 
On considère une certaine fonction $f$ donnée explicitement en fonction de $x$,

Quelles sont les fonctions $y=F(x)$ vérifiant l'égalité:

<center>

<enc>$y'=f \quad (E)$</enc>

</center>

Notre méthode a bien fonctionné, car nous avons été capables de déterminer explicitement une primitive $F$ de $f$ sur $I$. Néanmoins, cela n'est pas toujours simple, et même pire : on n'y parvient pas toujours. Lorsque déterminer explicitement une primitive $F$ de $f$ s'avère difficile, on utilise des méthodes approximatives, par exemple la **méthode d'Euler** (mais ceci est une autre histoire).


### Définitions 

!!! def
    * Une <red>équation différentielle $(E)$ sur un intervalle $I$</red> est une équation où l'inconnue est une fonction $y=f$, et où intervient sa dérivée $y'=f'$.
    * <red>Résoudre une équation différentielle sur un intervalle $I$</red>, c'est déterminer toutes les fonctions $f$, définies et dérivables sur $I$ qui sont solutions de cette équation $(E)$

Plus généralement, peuvent intervenir dans l'équation différentielle : 

* la fonction $y=f$
* la dérivée $y'=f'$
* des fonctions explicites en fonction de $x$
* des constantes


## Équations Différentielles $y'=f$

!!! pte
    On se donne une fonction $f$ connue explicitement en fonction de $x$.
    Les solutions sur $I$ de l'équation différentielle $y'=f$, sont les primitives $F_k$ de la fonction $f$, définies sur $I$

!!!ex
    Soit $(E)$ l'équation différentielle $y'=5+e^{-x}$  
    Dans chaque cas suivant, vérifier (en justifiant) si la fonction $f$ définie sur $\mathbb R$ est solution de $(E)$ (ou pas).

    1. $f(x)=\dfrac{5xe^{x}+1}{e^x}$
    2. $f(x)=5x-e^{-x}$

## Résolution des équations différentielles $y'=ay$

!!! pte
    Soit $a$ un nombre réel donné.
    Les solutions sur $\mathbb R$ de l'équation différentielle $y'=ay$ sont les fonctions $f_k$ définies sur $\mathbb R$ par:

    <center>

    <enc>$f_k(x)=ke^{ax}$</enc>  $\quad$ où $k$ est un nombre réel

    </center>

??? demo
    * <env>$\Leftarrow$</env> La fonction $f_k$ définie sur $\mathbb R$ par $f_k(x)=ke^{ax}$ avec $k$ un nombre réel, est dérivable sur $\mathbb R$, et pour tout réel $x$:
    $$f_k'(x)=kae^{ax}=a(ke^{kx})=af_k(x)$$
    Donc $f_k$ est bien une solution sur $\mathbb R$ de l'équation différentielle $y'=ay$
    * <env>$\Rightarrow$</env> Réciproquement, soit $g$ une solution quelconque sur $\mathbb R$ de $y'=ay$. 
    $g$ est dérivable sur $\mathbb R$, et pour tout rél $x$, $g'(x)=ag(x) \quad \Leftrightarrow \quad g'(x)-ag(x)=0$.
    On pose, pour tout réel $x$, $\varphi(x)=\dfrac{g(x)}{e^{ax}}=e^{-ax}g(x)$
    La fonction $\varphi$ est dérivable sur $\mathbb R$, et pour tout réel $x$,
    $\varphi'(x)=-ae^{ax}g(x)+e^{ax}g'(x)=e^{ax}\left[ g'(x) - ag(x)\right] = e^{ax}\times 0= 0$
    Donc $\varphi$ est une fonction constante sur $\mathbb R$, et il existe un réel $k$ tel que, pour tout réel $x$, $\varphi(x)=k$
    Ainsi, pour tout réel $x$, $e^{-ax}g(x)=k \quad \Leftrightarrow \quad g(x)=ke^{ax}$ (en multipliant à gauche et à droite par $e^{ax}$)

<env>Allure des courbes des fonctions solutions selon les signes de $a$ et $k$</env>

(à faire)

!!!ex
    Résoudre sur $\mathbb R$ l'équation différentielle $5y'+2y=0$

## Résolution des équations différentielles $y'=ay+b$

!!! pte
    On se donne $a$ et $b$ deux nombres réels non nuls.
    Les solutions sur $\mathbb R$ de l'équation différentielle $y'=ay+b$ sont les fonctions $f_k$ définies sur $\mathbb R$ par:

    <center>

    <enc>$f_k(x)=ke^{ax}-\dfrac ba$</enc> $\quad$ où $k$ est un nombre réel

    </center>

??? demo
    * <env>Solution particulière</env> On a l'idée de déterminer d'abord une solution particulière à l'équation $y'=ay+b\quad (E)$, sous la forme d'une fonction constante $g$ définie sur $\mathbb R$. Notons par exemple $g:x\mapsto c$ avec $c\in \mathbb R$ et l'on veut déterminer la ou les valeurs possibles pour $c$ de sorte que $g$ soit une solution de $(E)$. 
    Puisque $g$ est une fonction constante, on a $g'(x)=0$ pour tout réel $x$.
    Ainsi, $g$ est une solution de l'équation $0=ay+b \Leftrightarrow 0=ac+b \Leftrightarrow c=\dfrac{-b}{a}$
    Il existe donc une unique fonction *constante* qui soit solution de $(E)$: pour tout réel $x$, $g(x)=\dfrac{-b}{a}$
    * <env>Solution Générale</env> Soit $f$ une solution Générale quelconque de $y'=ay+b \quad (E)$
    donc $f'=af+b$. D'autre part, on sait que $g'=ag+b$ où $g$ est la fonction constante particulière ci-dessus.
    donc, en soustrayant membre à membre ces deux égalités, on a:
    $f'-g'=(af+b)-(ag+b)=af+b-ag-b=af-ag=a(f-g)$
    de plus, $(f-g)'=f'-g'$ donc $(f-g)'=a(f-g)$
    Ainsi, $f-g$ est une solution de $y'=ay$ donc il existe une constante $k$ telle que, pour tout $x$,
    $f(x)-g(x)=ke^{ax}$
    Conclusion: les solutions sur $\mathbb R$ de $y'=ay+b$ sont les fonctions $f_k$ définies par:
    $$f_k(x)=ke^{ax}-\dfrac ba$$

!!! ex
    Soit $(E)$ l'équation différentielle $y'=2y-3$

    1. Déterminer la solution constante de $(E)$
    
        ??? corr
            Soit $g$ la solution constante de $(E)$.
            Pour tout réel $x$, $g(x)=c$, donc $g'(x)=0$. Donc $g'=2g-3$ donc $0=2c-3$ donc $c=\dfrac{3}{2}$
            La solution constante est donc la fonction $g$ définie sur $\mathbb R$ par $g(x)=\dfrac{3}{2}$

    2. En déduire toutes les solutions de $(E)$
    
        ??? corr
            Soit $f$ une solution quelconque (générale) de $(E)$, donc $f'=2f-3$. De même, on sait que $g'-2g-3$.
            Donc $f'-g'=(2f-3)-(2g-3)=2f-3-2g+3=2f-2g=2(f-g)$
            De plus, $(f-g)'=f'-g'$, donc $(f-g)'=2(f-g)$
            Ainsi, $f-g$ est une solution de l'équation $y'=2y$
            dont on sait que les solutions générales sont les fonctions $f_k$ définies sur $\mathbb R$ par $f_k(x)=ke^{2x}$ avec $k\in\mathbb R$, 
            donc $f-g=f_k$ donc, $f(x)-\dfrac 32=ke^{2x}$, donc $f(x)=ke^{2x}+\dfrac 32$ avec $k\in\mathbb R$

    3. Déterminer la solution $h$ de $(E)$ telle que $h(-1)=2$
    
        ??? corr
            On sait que $h=f$ où est l'une des fonctions de la solution générale de (E).
            Donc $h(x)=ke^{2x}+\dfrac 32$ pour une certaine valeur $k\in\mathbb R$.
            Déterminer la fonction $h$ qui vérifie $h(-1)=2$, c'est donc déterminer la constante $k$ telle que :
            $h(-1)=ke^{2\times (-1)}+\dfrac 32=2 \Leftrightarrow ke^{-2}+\dfrac 32=0 \Leftrightarrow ke^{-2}=-\dfrac 32$
            donc $k=\dfrac{-\dfrac 32}{e^{-2}}$ donc $k=-\dfrac{3}{2e^{-2}}$ donc $k=-\dfrac{3e^2}{2}$
            Conclusion : La solution $h$ de $(E)$ vérifiant $h(-1)=2$ est donc définie par $h(x)=-\dfrac{3e^2}{2}e^{2x}+\dfrac 32$
            $h(x)=-\dfrac{3}{2}e^{2x+2}+\dfrac 32$, donc <enc>$h(x)=\dfrac{3}{2} \left( 1-e^{2x+2}  \right)$</enc>

