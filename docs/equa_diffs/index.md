# TMaths Co : Équations Différentielles

Les équations différentielles sont des équations dans lesquelles les inconnues ne sont pas des nombres réels $x$ vérifiant une certaine égalité, comme usuellement.
Dans une équation différentielle, les inconnues sont des fonctions $f$ vérifiant une certaine égalité reliant la fonction $f$, sa dérivée $f'$, quelquefois également certains nombres réels $x$.