# TMATHSCO : Cours Convexité

## Fonctions Convexes et Fonctions Concaves

### Définitions

!!! def
    Soit $f$ une fonction dérivable sur un intervalle $I$ et $C$ est sa courbe représentative dans un repère.

    * On dit que $f$ est <red>convexe</red> sur $I$ lorsque la courbe $C$ est entièrement <red>au-dessus</red> de chacune de ses tangentes, sur l'intervalle $I$.
    * On dit que $f$ est <blue>concave</blue> sur $I$ lorsque sur , la courbe  est entièrement <blue>au-dessous</blue> de chacune de ses tangentes, sur l'intervalle $I$

!!! exp
    |Convexe| Concave|
    |:-:|:-:|
    |La fonction $x \mapsto x^2$ est convexe sur $\m R$<br>![](img/x2.png) | La fonction $x \mapsto \sqrt x$ est concave sur $[0;+\infty[$<br>![](img/racineX.png)|
    |La fonction $x \mapsto e^x$ est convexe sur $\m R$<br>![](img/exponentielle.png) | La fonction $x \mapsto \ln(x)$ est concave sur $]0;+\infty[$<br>![](img/logarithme.png)|

## Point d’inflexion

!!! def
    $f$ est une fonction dérivable sur un intervalle $I$.
    $C$ est sa courbe représentative dans un repère, et $a\in I$.
    Dire que $A \coord{a}{f(a)}$ est un point d’inflexion de $C$ signifie qu’en $A$ la courbe $C$ traverse sa tangente.

<bd>Conséquence 1</bd>
En l’abscisse $a$ d’un point d’inflexion, $f$ passe de convexe à concave, ou réciproquement, de concave à convexe.

!!! exp
    Soit $f$ la fonction définie sur $\m R$, par $f(x)=x^3$.  
    L’origine $O$ du repère est un point est un point d’inflexion de la courbe $C$ représentant $f$.  
    $f$ est concave sur $]-\infty;0]$, et convexe sur $[0;+\infty[$.  
    Le point $O(0;0)$ est un point d’inflexion.

    ![](img/inflexion.png)

## Convexité et Dérivées

### Convexité et sens de variation de $f'$

!!! pte
    Soit $f$ une fonction dérivable sur un intervalle $I$.

    * La fonction $f$ est <red>convexe</red> sur $I \ssi$ $f'$ est <red>croissante</red> sur $I$
    * La fonction $f$ est <blue>concave</blue> sur $I \ssi$ $f'$ est <blue>décroissante</blue> sur $I$

!!! exp
    |Convexe| Concave|
    |:-:|:-:|
    |La fonction $x \mapsto x^2$ est convexe sur $\m R$<br/>![](img/x2.png)<br/>En effet $f'(x)=2x$<br/>et la fonction $f'$ est strictement croissante sur $\m R$ (car $2>0$) | La fonction $x \mapsto \sqrt x$ est concave sur $[0;+\infty[$<br/>![](img/racineX.png)<br/>En effet $f'(x)=\dfrac {1}{2\sqrt x}$<br/>et la fonction $f'$ est strictement décroissante sur $[0;+\infty[$ |
    |La fonction $x \mapsto e^x$ est convexe sur $\m R$<br/>![](img/exponentielle.png)<br/>En effet $f'(x)=e^x$<br/>et la fonction $f'$ est strictement croissante sur $\m R$ (car $2>0$) | La fonction $x \mapsto \ln(x)$ est concave sur $]0;+\infty[$<br/>![](img/logarithme.png) <br/>En effet $f'(x)=\dfrac 1x$<br/>et la fonction $f'$ est strictement décroissante sur $]0;+\infty[$|


### Convexité et signe de $f''$

!!! def
    Soit $f$ une fonction dérivable sur un intervalle $I$.

    * On dit que $f$ est <red>deux fois dérivable sur $I$</red> lorsque la fonction dérivée $f'$ est elle-même dérivable sur $I$.
    * La fonction dérivée de $f'$, notée $f''$, est appelée <red>dérivée seconde de $f$</red>.

!!! exp
    Pour tout réel $x$, on pose $f(x)=x^2$, alors $f'(x)=2x$ et $f''(x)=2$

!!! pte "de la propriété précédente"
    Soit $f$ une fonction dérivable sur un intervalle $I$.

    * La fonction $f$ est <red>convexe</red> sur $I \ssi$ pour tout réel $x$ de $I$, $f''(x) \color{red}{\ge 0}$
    * La fonction $f$ est <blue>concave</blue> sur $I \ssi$ pour tout réel $x$ de $I$, $f''(x) \color{blue}{\le 0}$

!!! exp
    !!! col __70
        Soit $f$ la fonction définie sur $\m R$ par $f(x)=1-e^x$  
        Pour tout réel $x$, $f'(x)=-e^x$ et $f''(x)=-e^x$  
        Donc pour tout réel $x$, on a $f''(x)\lt 0$ et donc $f$ est concave sur $\m R$.
    !!! col __30
        ![](img/exempleUnMoinsExpo.png)

### Point d’inflexion et dérivée seconde

!!! pte
    Soit $f$ une fonction deux fois dérivable sur un intervalle $I$ et $C$ la courbe représentative de $f$ dans un repère et $a\in I$.  
    Le point $A \coord{a}{f(a)}$ est un point d’inflexion de $C \ssi f''$ s’annule en $a$ en changeant de signe.

!!! exp
    Soit $f$ la fonction définie sur $\m R$ par $f(x)=x^3$. On a $f'(x)=3x^2$ et $f''(x)=6x$.  
    D’où le tableau de signes :
    
    (tableau)

    On retrouve ainsi le fait que le point $O\coord{0}{0}$ est un point d’inflexion de la courbe de $f$.

## Positions relatives de courbes

### Position relative de $exp$ et de la droite $y=x$

!!! pte
    Pour tout nombre réel $x$, $\quad e^x \gt x$

??? demo
    !!! col __70
        La fonction exponentielle est définie sur $\m R$ par $f(x)=e^x$ et donc $f'(x)=e^x$.
        La fonction exponentielle est convexe sur $\m R$, donc, sa courbe $C$ est au-dessus de toutes ses tangentes.  
        En particulier, La courbe $C$ est au-dessus de sa tangente $T$ au point d’abscisse $0$ dont on sait qu’une équation est donnée par :
        
        <center>$T:y=f'(0)(x-0)+f(0)$</center>

        avec $f(x)=e^x$ et $f'(x)=e^x$.  
        Donc $T:y=e^0(x-0)+e^0$, donc $T:y=1x+1$, donc $T:y=x+1$  
        Conclusion : Pour tout réel $x$, $e^x \ge x+1 \gt x$
    !!! col __30
        ![](img/demoExpoX.png)

### Position relative de LN et de la droite $y=x$

!!! pte
    Pour tout nombre réel $x$, $\quad \ln x \lt x$

??? demo
    !!! col __70
        La fonction $\ln$ est définie sur $]0;+\infty[$ par $f(x)=\ln x$ et donc $f'(x)=\dfrac 1x$.
        La fonction $\ln$ est concave sur $\m R$, donc, sa courbe $C$ est en-dessous de toutes ses tangentes.  En particulier, La courbe $C$ est en-dessous de sa tangente $T$ au point d’abscisse $1$ dont on sait qu’une équation est donnée par :  
        <center>$T:y=f'(1)(x-1)+f(1)$</center>
        avec $f(x)=\ln x$ et $f'(x)= \dfrac 1x$.  
        Donc $T:y=\dfrac 11 (x-1)+ \ln 1$, donc $T:y=1 (x-1)+ 0$, donc $T:y=x-1$  
        Conclusion : Pour tout réel $x$, $\ln x \le x-1 \lt x$
    !!! col __30
        ![](img/demoLNX.png)

### Conséquences graphiques

!!! col __70
    On déduit des deux points précédents que :

    * La courbe représentative de la fonction exponentielle est au-dessus de la droite d’équation $y=x$ sur $\m R$
    * La courbe représentative de la fonction $\ln$ est en-dessous de la droite d’équation $y=x$ sur $\m R$

!!! col __30
    ![](img/demoExpoLN.png)








