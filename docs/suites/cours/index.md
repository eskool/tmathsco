# TMATHSCO : Cours Suites

## Approche Intuitive de la Limite d'une Suite

### Suites de limite infinie

!!! exp "Introduction graphique pour Limite $+\infty$"
    <img src="img/suite_2nplus3.png" style="width:40%; float:right;margin-left:3px;">

    Soit $(u_n)$ la suite définie par $u_n=2n+3$

    Il semble que le terme $u_n$ soit aussi *grand* (comprendre proche de $+\infty$) que l'on veut lorque $n$ est *suffisamment grand* (comprendre proche de $+\infty$):

    * $u_n>1000$ pour $n\ge499$
    * $u_n>10^6$ pour $n\ge499999$

    <bd>Language Humain</bd> plus on va vers la droite, et plus la *hauteur* monte haut (aussi *haut* que l'on veut)...

!!! def
    On dit dans ce cas que la suite $(u_n)$ a pour limite $+\infty$, quand $n$ tend vers $+\infty$, et on note:

    * $\displaystyle \lim_{n\to +\infty} u_n = +\infty$
    * ou encore que: $u_n \rightarrow +\infty$ quand $n \rightarrow +\infty$

!!! exp "Introduction graphique pour Limite $-\infty$"
    ![](img/suite_moinsZero5n2.png) {.right}
    Soit $(u_n)$ la suite définie par $u_n=-0,5n^2$

    Il semble que le terme $u_n$ soit aussi *petit* (comprendre proche de $-\infty$) que l'on veut lorque $n$ est *suffisamment grand* (comprendre proche de $+\infty$):

    * $u_n< -1000$ pour $n\ge45$
    * $u_n<-10^6$ pour $n\ge1415$

    <bd>Language Humain</bd> plus on va vers la droite, et plus la *hauteur* descend bas (aussi *bas* que l'on veut)...

!!! def
    On dit dans ce cas que la suite $(u_n)$ a pour limite $-\infty$, quand $n$ tend vers $+\infty$, et on note:

    * $\displaystyle \lim_{n\to +\infty} u_n = -\infty$
    * ou encore que: $u_n \rightarrow -\infty$ quand $n \rightarrow +\infty$

!!! pte "Suites de Référence de Limite $\infty$"
    Soit un réel fixé $k>0$ (resp. $k<0$), et $p>0$ un entier.
    Les suites $(kn)$, $(kn^2)$, $(kn^3)$, ...,  $(kn^p)$, $(k\sqrt{n})$, $(ke^n)$ ont pour limite $+\infty$ (resp. $-\infty$), lorsque $n\rightarrow +\infty$

### Suites de limite $0$

!!! exp "Introduction graphique"
    <img src="img/suite_2surn.png" style="width:40%; float:right;margin-left:3px;">

    Soit $(u_n)$ la suite définie par $u_n=\dfrac 2 n$

    Il semble que le terme $u_n$ soit aussi proche de $0$ que l'on veut lorque $n$ est *suffisamment grand*:

    * $u_n<0,01$ pour $n\ge200$
    * $u_n<10^{-6}$ pour $n\ge2000000$

    <bd>Language Humain</bd> plus on va vers la droite, et plus la *hauteur* est proche de $0$ (donc de l'axe des $x$)...

!!! def
    On dit dans ce cas que la suite $(u_n)$ a pour limite $0$, quand $n$ tend vers $+\infty$, et on note:

    * $\displaystyle \lim_{n\to +\infty} u_n = 0$
    * ou encore que: $u_n \rightarrow 0$ quand $n \rightarrow +\infty$

!!! pte "Suites de Référence de Limite $0$"
    Soit un réel fixé $k\in \mathbb R$, et $p>0$ un entier.
    Les suites $\left( \dfrac k n \right)$, $\left( \dfrac k {n^2} \right)$, $\left( \dfrac k {n^3} \right)$, ...,  $\left( \dfrac k {n^p} \right)$, $\left( \dfrac k {\sqrt{n}} \right)$, $(ke^{-n})$ ont pour limite $0$, lorsque $n\rightarrow +\infty$

### Suites de limite $\ell$

!!! exp "Introduction graphique"
    <img src="img/suite_2plus1surn.png" style="width:40%; float:right;margin-left:3px;">

    Soit $(u_n)$ la suite définie par $u_n=2+\dfrac 1 n$

    Il semble que le terme $u_n$ soit aussi proche de $\ell=2$ que l'on veut lorque $n$ est *suffisamment grand*:

    * $u_n<2,01$ pour $n\ge101\quad$ ($\Leftrightarrow u_n-2\lt 0,01$)
    * $u_n<2+10^{-6}$ pour $n\ge1000001\quad $ ($\Leftrightarrow u_n-2\lt 0,000001$)

    <bd>Language Humain</bd> plus on va vers la droite, et plus la *hauteur* est proche de $\ell=2$...

!!! def
    On dit dans ce cas que la suite $(u_n)$ a pour limite $\ell$, quand $n$ tend vers $+\infty$, et on note:

    * $\displaystyle \lim_{n\to +\infty} u_n = \ell$
    * ou encore que: $u_n \rightarrow \ell$ quand $n \rightarrow +\infty$

!!! pte "On ramène l'étude à une limite $0$"
    Soit $(u_n)$ une suite.
    $\displaystyle \lim_{n\to +\infty} u_n = \ell \Leftrightarrow \lim_{n\to +\infty} \left( u_n - \ell \right) = 0$

!!! exp
    Soit $(u_n)$ la suite définie par $u_n=\dfrac 1 n +2$
    Pout tout $n$, $\quad u_n-2=\dfrac 1 n \quad $  qui est une suite de référence de limite $0$,
    donc $\displaystyle \lim_{n\to +\infty} \left( u_n - 2 \right) = 0 \Leftrightarrow \lim_{n\to +\infty} u_n = 2$

### Suites n'ayant pas de limite

:boom: **ATTENTION : PAS TOUTES** les suites n'admettent de limite quand $n\rightarrow +\infty$ :boom:

!!! exp "Suite n'ayant pas de limite"
    <img src="img/suite_moinsunpuissancen.png" style="width:35%; float:right;margin-left:3px;">

    La suite $(u_n)$ définie par <encadre style="margin-left:0.3em;margin-right:0.6em;">$u_n=(-1)^n$</encadre> n'admet pas de limite. 
    En efet, les valeurs successives des termes de la suite $(u_n)$ sont: $1; -1; 1; -1; 1; -1; 1; -1; ...$
    Cela veut dire que la suite définie par $u_n=(-1)^n$ n'admet:

    * **Pas de Limite réelle $\ell$ :** il n'existe aucun nombre réel $\ell$ dont (TOUS) les termes $u_n$ ne s'approchent indéfinitivement, aussi proche que l'on veut. Certes, la moitié d'entre eux (pour les rangs $n$ pairs) s'approchent de $+1$, et l'autre moitié (rangs $n$ impairs) s'approchent de $-1$, mais ils ne s'approchent PAS (comprendre PAS TOUS SIMULTANÉMENT) du même nombre réel $\ell$. Donc pas de limite réelle $\ell$.
    * **Pas de Limite $+\infty$ :** Cette suite $(u_n)$ ne tend pas non plus vers $+\infty$, car par exemple, tous les termes $u_n$ sont bornés: $-1\le u_n\le1$
    * **Pas de Limite $-\infty$ :** Cette suite $(u_n)$ ne tend pas non plus vers $-\infty$. Idem.

## Suites Convergentes vs Suites Divergentes

### Suites Convergentes

!!! def "Suite Convergente"
    Si une suite admet une limite $\ell\in \mathbb R$, i.e. que $\displaystyle \lim_{n \to +\infty} u_n=\ell \in \mathbb R$, alors on dit que :
    * la suite $(u_n)$ est **convergente (vers $\ell\in \mathbb R)$**
    * la suite $(u_n)$ **converge (vers $\ell\in \mathbb R$)**,

!!! pte "Unicité de la Limite"
    Si une suite $(u_n)$ admet une limite $\ell\in \mathbb R$, Alors **cette limite $\ell$ est unique**.

### Suites Divergentes

!!! def "Suite Divergente"
    Une suite $(u_n)$ **diverge**, ou **est divergente**, lorsqu'elle n'est **pas convergente** (vers $\ell\in \mathbb R$)

!!! pte
    Une suite $(u_n)$ **diverge**, ou **est divergente** lorsqu'elle se trouve dans l'une des 3 situations distinctes suivantes (qui s'excluent l'une l'autre):

    * Ou bien $\displaystyle \lim_{n \to +\infty} u_n=+\infty$, auquel cas, on dit que :
        * la suite $(u_n)$ est **divergente (vers $+\infty$)**.
        * la suite $(u_n)$ **diverge (vers $+\infty$)**
    * Ou bien $\displaystyle \lim_{n \to +\infty} u_n=-\infty$, auquel cas, on dit que :
        * la suite $(u_n)$ est **divergente (vers $-\infty$)**.
        * la suite $(u_n)$ **diverge (vers $-\infty$)**
    * Ou bien la suite **n'admet pas de limite** (ni un réel $\ell$, ni $+\infty$, ni $-\infty$), auquel cas on dit que :
        * la suite $(u_n)$ **diverge**
        * la suite $(u_n)$ **est divergente**

## Opérations sur les Limites

### Formes indéterminées (FI)

#### Contexte et Notion de Forme indéterminée (FI)

Dans toute la suite, nous supposerons que l'on connaît deux suites $(u_n)$ et $(v_n)$, pour lesquelles :

* $\displaystyle \lim_{n \to +\infty} u_n$ existe et est connue
* $\displaystyle \lim_{n \to +\infty} v_n$ existe et est connue
* **et seulement ces informations** (en particulier, on ne suppose pas connus de détails supplémentaires sur les suites $(u_n)$ ni $(v_n)$)

Nous appelerons ***cas général*** cette situation (où l'on dispose d'aucune autre information sur les suites $(u_n)$ et $(v_n)$)
 
#### Question : Peut-on prédire/en déduire l'existence et la valeur des limites suivantes:

* $\displaystyle \lim_{n \to +\infty} \left(u_n+ v_n \right)$ ?
* $\displaystyle \lim_{n \to +\infty} \left(u_n- v_n \right)$ ?
* $\displaystyle \lim_{n \to +\infty} \left(u_n\times v_n \right)$ ?
* $\displaystyle \lim_{n \to +\infty} \left( \dfrac{u_n}{v_n} \right)$ ?

Le problème est que la réponse dépend des situations...

##### La réponse peut être OUI

Dans certaines situations, heureusement, les informations connues précédemment **dans le cas général** (existence et valeur de $\displaystyle \lim_{n \to +\infty} u_n$ et $\displaystyle \lim_{n \to +\infty} v_n$) **et seulement ces informations-là, peuvent être suffisantes pour donner une réponse positive** à la question posée:
Il peut arriver que l'on puisse déduire l'Existence et Valeur de la limite d'une opération entre les suites $(u_n)$ et $(v_n)$ en connaissant ***seulement*** l'existence et la valeur de $\displaystyle \lim_{n \to +\infty} u_n$ et $\displaystyle \lim_{n \to +\infty} v_n$. Voici un exemple:

!!! exp "de limite d'une soustraction 'sans problèmes'"
    Supposons que nous sachions que $\displaystyle \lim_{n \to +\infty} u_n=2$ et $\displaystyle \lim_{n \to +\infty} v_n=3$
    On peut prédire *intuitivement* (pour le moment) sans trop de problèmes que $\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right)=2-3=-1$

    On conclut que, pour des limites réelles de $(u_n)$ et $(v_n)$, "**la limite d'une soustraction est la soustraction des limites**".

Remarquons que nous avons été capables de prédire $\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right)$ **sans autre information d'aucune sorte sur $(u_n)$ et $(v_n)$, mais seulement en connaissant l'existence et la valeur des limites de $(u_n)$ et $(v_n)$**.
Pour résumer toutes ces situations, nous renseignerons ci-après dans le cours, un tableau décrivant ce que l'on peut prédire avec certitude.

##### La réponse peut être NON

Malheureusement, dans certaines autres situations, **il peut arriver** que les informations connues précédemment **dans le cas général** (existence et valeur de $\displaystyle \lim_{n \to +\infty} u_n$ et $\displaystyle \lim_{n \to +\infty} v_n$) **et seulement ces informations-là**, ne soient pas suffisantes pour donner une **même** réponse à la question posée...
Plus précisément, et pire que cela: dans certaines situations, et **selon le choix très particulier des suites $(u_n)$ et $(v_n)$**, on peut montrer que la question de l'existence et de la valeur de la limite d'une opération entre les suites $(u_n)$ et $(v_n)$ peut admettre chacune des réponses suivantes (pour des suites $(u_n)$ et $(v_n)$ différentes bien sûr):

* la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **$+\infty$**
* la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **$-\infty$**
* la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **n'importe quel réel $\ell\in \mathbb R$**
* la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut **ne même pas exister**

Dans ce cas, on dit que la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ est une **Forme Indéterminée (FI)**.
Cela ne veut pas dire que la question n'admet pas de réponse, mais que nous avons **besoin d'un complément d'information, et d'une étude plus précise** qui dépend du choix très particulier que nous avons fait pour les suites $(u_n)$ et $(v_n)$.
Cela veut dire aussi qu'il n'existe **pas de *même* réponse immédiate et générale**, et qu'**on ne peut <red>provisoirement<black> pas répondre** avec les seules informations sur l'existence et la valeur de $\displaystyle \lim_{n \to +\infty} u_n$ et $\displaystyle \lim_{n \to +\infty} v_n$. 
Par contre, en étudiant précisément les suites $(u_n)$ et $(v_n)$ choisies, on pourra toujours dire dans laquelle des 4 situations précédentes nous sommes (limite $+\infty$, limite $-\infty$, limite réelle $\ell\in \mathbb R$, ou pas de limite du tout)
La réponse dépendra alors directement des choix très particuliers pour les suites $(u_n)$ et $(v_n)$. 

Je retiens:

!!! def
    Une **forme indéterminée (FI)** est une situation dans laquelle la théorie ne donne <red>pas de réponse dans le cas général</red> pour la limite d'une opération ($+, -, \times, /$) entre deux suites. 
    On ne peut donc **provisoirement** pas répondre.
    Par contre, on peut toujours répondre en étudiant étudier plus précisément le cas particulier des suites $(u_n)$ et $(v_n$) choisies.

!!! pte
    Lorsqu'on est dans une situation d'une forme indéterminée, la théorie prouve que chacun des cas suivants peut véritablement se produire (pour des suites $(u_n)$ et $(v_n)$ différentes bien sûr)

    * la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **$+\infty$**
    * la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **$-\infty$**
    * la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut être égale à **n'importe quel réel $\ell\in \mathbb R$**
    * la limite de l'opération entre les suites $(u_n)$ et $(v_n)$ peut **ne même pas exister**

!!! exp "Exemples d'une soustraction 'avec problèmes' (avec l'infini) "
    Dans cet exemple, nous allons illustrer le fait que <encadre style="margin-left:0.6em;margin-right:0.5em;">$\infty-\infty$</encadre> est une **Forme Indéterminée**.

    Reprenons notre exemple sur la soustraction, mais supposons cette fois-ci que les limites des $(u_n)$ et de $(v_n)$ sont infinies, plus précisément supposons que :

    * $\displaystyle \lim_{n \to +\infty} u_n=+\infty$
    * $\displaystyle \lim_{n \to +\infty} v_n=+\infty$

    &#x2BA9; <bd>Situation 1</bd> $u_n=2n$ et $v_n=n$
    Nous sommes donc bien dans le cas où

    * $\displaystyle \lim_{n \to +\infty} u_n=\lim_{n \to +\infty} 2n=+\infty$
    * $\displaystyle \lim_{n \to +\infty} v_n=\lim_{n \to +\infty} n=+\infty$

    De plus, dans le cas particulier des suites que nous avons choisies:
    Pour tout entier $n$, $u_n-v_n=2n-n=n$, 
    donc on peut dire que:

    <center>

    <encadre>$\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right) =+\infty$</encader>

    </center>

    &#x2BA9; <bd>Situation 2</bd> $u_n=n$ et $v_n=2n$
    Nous sommes donc bien dans le cas où

    * $\displaystyle \lim_{n \to +\infty} u_n=\lim_{n \to +\infty} n=+\infty$
    * $\displaystyle \lim_{n \to +\infty} v_n=\lim_{n \to +\infty} 2n=+\infty$

    De plus, dans le cas particulier des suites que nous avons choisies:
    Pour tout entier $n$, $u_n-v_n=n-2n=-n$, 
    donc on peut dire que:

    <center>

    <encadre>$\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right) =-\infty$</encadre>

    </center>

    &#x2BA9; <bd>Situation 3</bd> $u_n=n+4$ et $v_n=n$
    Nous sommes donc bien dans le cas où

    * $\displaystyle \lim_{n \to +\infty} u_n=\lim_{n \to +\infty} n+4=+\infty$
    * $\displaystyle \lim_{n \to +\infty} v_n=\lim_{n \to +\infty} n=+\infty$

    De plus, dans le cas particulier des suites que nous avons choisies:
    Pour tout entier $n$, $u_n-v_n=n+4-n=4$, 
    donc on peut dire que:

    <center>

    <encadre>$\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right) =4$</encadre>

    </center>

    Remarque (généralisation):
    On pourait généraliser ce raisonnement en définissant les suites $(u_n)$ et $(v_n)$ par:

    * $u_n=n+\ell$ où $\ell\in \mathbb R$ désigne un nombre réel constant mais quelconque, 
    * $v_n=n$, 

    On montrerait facilement que dans le cas particulier des suites que nous avons choisies: 
    $$\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right) =\ell$$

    &#x2BA9; <bd>Situation 4</bd> $u_n=n+(-1)^n$ et $v_n=n$
    Nous sommes donc bien dans le cas où

    * $\displaystyle \lim_{n \to +\infty} u_n=\lim_{n \to +\infty} n+(-1)^n=+\infty$ car $\quad \forall n\in \mathbb N, \quad u_n\ge n-1$
    * $\displaystyle \lim_{n \to +\infty} v_n=\lim_{n \to +\infty} n=+\infty$

    De plus, dans le cas particulier des suites que nous avons choisies:
    Pour tout entier $n$, $u_n-v_n=n+(-1)^n-n=(-1)^n$, 
    donc on peut dire que:

    <center>

    <encadre>la limite de $(u_n-v_n)$ n'existe pas</encadre>

    </center>

    <bd @vert>Conclusion de l'Exemple</bd>

    Nous avons donc bien trouvé $4$ exemples différents ($2$ suffisaient..), mais néanmoins possibles, pour des suites $(u_n)$ et $(v_n)$ telles que :  
    D'une part:

    * $\displaystyle \lim_{n \to +\infty} u_n=+\infty$
    * $\displaystyle \lim_{n \to +\infty} v_n=+\infty$

    D'autre part, néanmoins, l'existence et la valeur de $\displaystyle \lim_{n \to +\infty} \left( u_n-v_n \right)$ n'admet jamais la même réponse, et chacune des $4$ situations précitées est possible (limite $+\infty$, limite $-\infty$, limite réelle $\ell \in \mathbb R$, et pas de limite du tout)

    **Nous sommes donc bien en présence d'une Forme Indéterminée *de type* &laquo; $\infty-\infty$ &raquo;**

### Règles opératoires sur les Limites

#### Somme

<center>

|>|>|>|>|>|>|>|>|>|Limite d'une Somme de Suites $u_n+v_n$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{n \to +\infty} u_n=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|
|et si $\displaystyle \lim_{n \to +\infty} v_n=...$|$\ell'$|$+\infty$|$-\infty$|$\ell'$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|$\ell'$|
|<nobr>alors  $\displaystyle \lim_{n \to +\infty} (u_n+v_n)=...$</nobr>|<nobr>$\ell+\ell'$</nobr>|$+\infty$|$-\infty$|$+\infty$|$+\infty$|$-\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|$-\infty$|
{.petit}

</center>

**Résumé:**

* Une seule FI pour l'Addition: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)+(-\infty)$ ou $(-\infty)+(+\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour l'addition
* la Règle des signes pour l'addition est préservée: $+(+\infty)=+\infty$ et $+(-\infty)=-\infty$

#### Différence

<center>

|>|>|>|>|>|>|>|>|>|Limite d'une Différence de Suites $u_n-v_n$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{n \to +\infty} u_n=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$+\infty$|$-\infty$|$-\infty$|$-\infty$|
|et si $\displaystyle \lim_{n \to +\infty} v_n=...$|$\ell'$|$+\infty$|$-\infty$|$\ell'$|$+\infty$|$-\infty$|$\ell'$|$+\infty$|$-\infty$|
|<nobr>alors  $\displaystyle \lim_{n \to +\infty} (u_n-v_n)=...$</nobr>|<nobr>$\ell-\ell'$</nobr>|$-\infty$|$+\infty$|$+\infty$|<span style="color:red;">$FI$</span>|$+\infty$|$-\infty$|$-\infty$|<span style="color:red;">$FI$</span>|
{.petit}

</center>

**Résumé:**

* Une seule FI pour la Soutraction: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)-(+\infty)$ ou $(-\infty)-(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Soustraction
* la Règle des signes pour la soustraction est préservée: $-(+\infty)=-\infty$ et $-(-\infty)=+\infty$

#### Produit

<center>

|>|>|>|>|>|>|>|>|>|Limite d'un Produit de Suites $u_n \times v_n$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{n \to +\infty} u_n=...$|$\ell$|<nobr>$\ell>0$</nobr>|<nobr>$\ell>0$</nobr>|<nobr>$\ell<0$</nobr>|<nobr>$\ell<0$</nobr>|$+\infty$|$+\infty$|$-\infty$|$0$|
|et si $\displaystyle \lim_{n \to +\infty} v_n=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|<nobr>alors  $\displaystyle \lim_{n \to +\infty} (u_n\times v_n)=...$</nobr>|<nobr>$\ell \times \ell'$</nobr>|$+\infty$|$-\infty$|$-\infty$|$+\infty$|$+\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|
{.petit}

</center>

**Résumé:**

* Une seule FI pour la Multiplication: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$0\times \infty$</red></encadre> $\quad($comprendre : $0\times(+\infty)$ ou $0\times(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Multiplication, **SAUF SUR $\ell=0$!!**
* la Règle des signes pour le produit est préservée: $(+)\times(+)=+$, $(-)\times(-)=+$, $(+)\times(-)=-$, $(-)\times(+)=-$

#### Quotient

<center>

|>|>|>|>|>|>|>|<bd red blue>Cas I :</bd> Limite d'un Quotient de Suites $\dfrac {u_n}{v_n}$ avec <div style="display: inline-block; padding: 0.3em; background-color:#f3cbcb;">$\displaystyle \lim_{n \to +\infty} v_n \ne 0$</div> (et $\forall n \in \mathbb{N} , v_n \ne 0$)|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{n \to +\infty} u_n=...$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|et si $\displaystyle \lim_{n \to +\infty} v_n=...$|$\ell' \neq 0$|$+\infty$ ou $-\infty$|$\ell'>0$|$\ell'<0$|$\ell'>0$|$\ell'<0$|$+\infty$ ou $-\infty$|
|<nobr>alors  $\displaystyle \lim_{n \to +\infty} \left( \frac{u_n}{ v_n} \right)=...$</nobr>|$\dfrac \ell {\ell'}$|$0$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|
{.petit}

|>|>|>|>|>|<span style="background-color:#F00;">Cas II :</span> Limite d'un Quotient de Suites $\dfrac {u_n}{v_n}$ avec <div style="display: inline-block; padding: 0.3em; background-color:#f3cbcb;">$\displaystyle \lim_{n \to +\infty} v_n = 0$</div> (et $\forall n \in \mathbb{N} , v_n \ne 0$)|
|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{n \to +\infty} u_n=...$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$0$|
|et si $\displaystyle \lim_{n \to +\infty} v_n=...$|$0^{+}$<br/>$=0$ en restant positif|$0^{+}$<br/>$=0$ en restant positif|$0^{-}$<br/>$=0$ en restant négatif|$0^{-}$<br/>$=0$ en restant négatif|$0$|
|<nobr>alors  $\displaystyle \lim_{n \to +\infty} \left( \frac{u_n}{ v_n} \right)=...$</nobr>|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|
{.petit}

</center>

**Résumé des deux tableaux précédents sous la forme d'un tableau à double entrée:** (si vous préférez)

<center>

|>|>|>|>|>|>|>|Tableau résumant la Limite d'un Quotient $\dfrac {u_n}{v_n}$ avec $\forall n \in \mathbb{N} , v_n \ne 0$|
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|<span style="position:relative;top:0.6em;right:0.4em;">$\displaystyle \lim_{n \to +\infty}u_n$</span><span style="position:relative;top:-1em;right:-0.5em;">$\displaystyle \lim_{n \to +\infty}v_n$</span>|>|>|$0$|$\ell'>0$|$\ell'<0$|$+\infty$|$-\infty$|
|^|$0^{+}$|$0^{-}$|$0^{+/-}$|^|^|^|^|
|$\ell=0$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|$0$|$0$|$0$|$0$|
|$\ell>0$|$+\infty$|$-\infty$|<span style="color:blue;">$PDL$</span>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{+}$|$0^{-}$|
|$\ell<0$|$-\infty$|$+\infty$|<span style="color:blue;">$PDL$</span>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{-}$|$0^{+}$|
|$+\infty$|$+\infty$|$-\infty$|<span style="color:blue;">$PDL$</span>|$+\infty$|$-\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|
|$-\infty$|$-\infty$|$+\infty$|<span style="color:blue;">$PDL$</span>|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|

</center>

<bd blue>$PDL$</bd> voulant dire que l'on est sûr que la suite $\left( \dfrac {u_n}{v_n} \right)$ n'admet <blue>P</blue>as <blue>D</blue>e <blue>L</blue>imite (ni réelle $\ell$, ni infinie).

**Résumé: (Idem pour les Suites)**

* Deux FI pour le Quotient: <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{0}{0}$</red></encadre> $\,\,\,$ et $\,\,$ <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{\infty}{\infty}$</red></encadre>
comprendre $\dfrac{0^+}{0^+}$, $\dfrac{0^-}{0^-}$, $\dfrac{0^+}{0^-}$, $\dfrac{0^-}{0^+}$, $\dfrac{0^{+/-}}{0^+}$, $\dfrac{0^{+/-}}{0^-}$, $\dfrac{0^{+}}{0^{+/-}}$, etc...$\,\,$ et $\,\,$ $\dfrac{+\infty}{+\infty}$, $\dfrac{-\infty}{-\infty}$, $\dfrac{+\infty}{-\infty}$, $\dfrac{-\infty}{+\infty}$, 
* Pour le quotient, l'$\infty$ **AU NUMÉRATEUR** :
  * ne l'emporte pas sur $\ell'=0^{+/-}$ **AU DÉNOMINATEUR**. Dans ce cas: PAS DE LIMITE
  * l'emporte sur tout réel $\ell'\ne 0$ **AU DÉNOMINATEUR**, 
  * ne l'emporte PAS sur l'$\infty$ **AU DÉNOMINATEUR** !! Dans ce cas : on a une FI, 
* Pour le quotient, $0$ **AU NUMÉRATEUR** l'emporte sur tout réel $\ell$ **AU DÉNOMINATEUR**, **SAUF SUR $\ell=0$ !!** $\rightarrow$ On a une FI,
* la Règle des signes pour le quotient est préservée: $\dfrac{+}{+}=+$, $\dfrac{-}{-}=+$, $\dfrac{+}{-}=-$, $\dfrac{-}{+}=-$

### Résumé des Formes indéterminées (FI)

<center>

|>|>|>|>|Résumé des Formes indéterminées usuelles pour les Suites|
|:-:|:-:|:-:|:-:|:-:|
|$\infty-\infty$|$0\times \infty$|$\dfrac{0}{0}$|$\dfrac{\infty}{\infty}$|$1^{\infty}$|

</center>

La forme indéterminée $1^{\infty}$ peut sembler moins naturelle pour le moment, mais pourra être mieux comprise durant cette année, grâce à :

* l'étude de la fonction exponentielle $exp$, vue en 1ère,
* et l'étude de la fonction $ln$ qui sera étudiée en peu plus tard, 
* et grâce à la formule <encadre>$x^y=e^{y ln(x)}$</encadre>

## Propriétés sur les Limites

### Passage à la limite dans les inégalités

!!! pte
    Si $(u_n)$ et $(v_n)$ sont deux suites convergentes telles que, $\quad \forall n \in \mathbb N, \quad u_n \lt v_n$
    Alors $\displaystyle \lim_{n \to +\infty} u_n \le \lim_{n \to +\infty} v_n$

!!! exp
    Si $(u_n)$ est convergente vesr un réel $\ell$, et que pour tout entier $n$ $,\quad u_n\lt3$
    Alors $\ell \le 3$

### Théorème des Gendarmes

!!! thm "des gendarmes"
    Soient $(u_n)$, $(v_n)$ et $(w_n)$ trois suites, et $\ell$ un nombre réel.

    Si :one: à partir d'un certain rang $n_0$, $\quad v_n\le u_n \le w_n$, $\quad$ et que  
    $\,\,\,\,$ :two: $\displaystyle \lim_{n \to +\infty} v_n=\lim_{n \to +\infty} w_n=\ell$  
    Alors $\displaystyle \lim_{n \to +\infty} u_n=\ell$

Remarque: le théorème des gendarmes est un théorème d'existence, il sert à démontrer que $\displaystyle \lim_{n \to +\infty} u_n$ existe **ET** vaut $\ell$

!!! exp
    Soit $(u_n)$ la suite définie par $u_n=\dfrac{(-1)^n}{n^2}$

    On sait que pour tout entier $n\ge 1, \quad -1\le (-1)^n \le 1$,
    donc 

    * $\forall n \ge1, \quad \dfrac{-1}{n^2}\le \dfrac{(-1)^n}{n^2} \le \dfrac{1}{n^2}$
    De plus, on sait que:
    * $\displaystyle \lim_{n \to +\infty} \dfrac{-1}{n^2}=0$ et $\displaystyle \lim_{n \to +\infty} \dfrac{1}{n^2}=0$ (ce sont des suites de référence pour $0$)
    Donc d'après le **théorème des gendarmes** (à citer), on peut dire que :
    * $\displaystyle \lim_{n \to +\infty} \dfrac{(-1)^n}{n^2}$ existe, et $\displaystyle \lim_{n \to +\infty} \dfrac{(-1)^n}{n^2}=0$