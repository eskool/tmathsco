# TMATHSCO: Cours Suites Arithmético-Géométriques

## Suites du type $u_{n+1}=au_n+b$

!!! def "Suite Arithmético-Géométrique"
    Soit $a,b \in \mathbb R$ deux réels.
    Une suite $(u_n)$ définie par la relation $u_{n+1}=au_n+b$ est appelée une <red>suite arithmético-géométrique</red>.

!!! Remarque
    Contrairement à ce que l'on pourrait croire à cause de son nom faussement tendencieux, *sauf cas très particulier* (cf ci-dessous), une suite arithmético-géométrique n'est en général:

    * ni arithmétique
    * ni géométrique

<bd>Cas particuliers</bd>

* Si $a= 0$, $(u_n)$ est définie par $u_n=b$, donc $(u_n)$ est une suite constante (égale à $b$)
* Si $a\ne 0$ et $b= 0$, $(u_n)$ est définie par $u_{n+1}=au_n$, donc $(u_n)$ est une suite géométrique de raison $a$
* Si $a= 1$, $(u_n)$ est définie par $u_{n+1}=u_n+b$, donc $(u_n)$ est une suite arithmétique de raison $b$

## Représentation Graphique

Pour représenter graphiquement les termes d'une suite arithmético-géométrique, on pourrait bien sûr calculer les termes un par un, mais cela serait long et laborieux.
Le **procédé géométrique** décrit dans la méthode ci-dessous ne demande **aucun calcul**:

* on place simplement un premier point, pour le terme initial $u_0$
* puis, on place les points suivants en traçant une sorte d'escalier à partir de premier point.

!!! mth
    $(u_n)$ est une suite définie par $u_0$ et par la relation $u_{n+1}=au_n+b$

    :one: on trace deux droites:

    * la droite $\Delta$ d'équation $y=x$ (la *1ère bissectrice*)
    * la droite $d$ d'équation $y=ax+b$

    :two: on place $u_0$ sur l'axe des abscisses  
    :three: On place $u_1$ sur l'axe des abscisses en deux temps:

    * d'abord, on utilise la droite $d$ pour placer $u_1=au_0+b$ sur l'axe des ordonnées
    * ensuite, on utilise la droite $\Delta$ pour reporter $u_1$ sur l'axe des abscisses

    :four: on recommence l'étape 3°) pour placer $u_2$, $u_3$, $u_4$, ... sur l'axe des abscisses

!!! col __50
    ![](img/suiteArithmGéom_Croissante.png)

    <center>

    <figcaption style="margin-top:0.7em;">

    Suite Arithmético Géométrique :<br/>$u_{n+1}=\dfrac{1}{2}u_n+3$ et $u_0\approx0,5$<br/>Deux Conjectures: <br/>$ (u_n)$ <b>croissante</b> et $\displaystyle \lim_{n \to +\infty} u_n=6$
    </figcaption>

    </center>

!!! col __50 clear
    ![](img/suiteArithmGéom_Décroissante.png)

    <center>

    <figcaption style="margin-top:0.7em;">

    Suite Arithmético Géométrique :<br/>$u_{n+1}=\dfrac{1}{2}u_n+3$ et $u_0\approx 9,5$<br/>Deux Conjectures: <br/>$ (u_n)$ <b>décroissante</b> et $\displaystyle \lim_{n \to +\infty} u_n=6$
    </figcaption>

    </center>

<bd>Remarques</bd>

1. **Procédé géométrique:**  **Le *procédé géométrique* précédent** de construction des premiers termes consécutifs $u_n$, **se généralise très bien à des suites récurrentes *quelconques***, c'est-à-dire définies par $u_{n+1}=f(u_n)$ avec $f$ quelconque (affine, ou pas). Il suffit pour cela de remplacer la droite $d$ d'équation $y=f(x)=\dfrac12x+3$, par la courbe $\mathcal{C}$ d'équation $y=f(x)$ avec $f$ quelconque. Le reste du procédé est inchangé.

2. **Conjecture de la Limite $\ell$:**

    * <red>Graphiquement</red>, **la conjecture de *la limite* $\ell$ est très simple, et se généralise également à des suites récurrentes *quelconques*** : c'est **l'abscisse du point d'intersection** entre $\Delta$ et la droite $d$ (plus généralement entre $\Delta$ et la courbe $\mathcal C: y=f(x)$).
    * <red>À la calculatrice</red>, **la conjecture de la limite $\ell$ se fait très simplement aussi, et se généralise également à des suites récurrentes *quelconques***, avec la touche <num ans></num> sur NumWorks ou  <ti seconde></ti><ti signe></ti> sur TI-83 Premium CE.

3. <encadre style="font-weight:700;">Tracé des Suites à la Calculatrice (p38)</encadre>
![](img/calculatrices.jpg)

!!! ex
    Soit $(u_n)$ une suite arithmético-géométrique définie par la relation de récurrence $(R)$ suivante :  

    <center><enc>$u_{n+1}=\dfrac 12 u_n+3 \quad (R)$</enc></center>

    1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$  

        ??? corr
            Notons $k\in \mathbb R$ la constante définissant la suite $(a_n)$, autrement dit: pour tout entier $n$, on a: $a_n=k$

            Puisque la suite $(a_n)$ doit vérifier l'égalité $(R)$, alors :
            pour tout entier $n$, $\quad a_{n+1}=\dfrac 12 a_n+3 \Leftrightarrow k=0,5k+3 \Leftrightarrow k-0,5k=3 \Leftrightarrow 0,5k=3$
            donc $\Leftrightarrow k=\dfrac {3}{0,5}=6$
            <b>Conclusion: la suite constante $(a_n)$ définie par $a_n=6$ pour tout entier $n$ est la seule suite constante vérifiant la relation $(R)$</b>.

    2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$

        ??? corr
            Soit $(u_n)$ une suite quelconque vérifiant $(R)$, alors:

            * $u_{n+1}=0,5u_n+3$ pour tout entier $n$
            * $a_{n+1}=0,5a_n+3$ pour tout entier $n$

            donc, en soustrayant ces deux égalités membre à membre, on trouve que pour tout entier $n$:

            $u_{n+1}-a_{n+1}=0,5u_n-0,5a_n+3-3$
            $u_{n+1}-a_{n+1}=0,5u_n-0,5a_n$
            $u_{n+1}-a_{n+1}=0,5(u_n-a_n)$

            Cette dernière égalité prouve que la suite $(v_n)$ définie par $v_n=u_n-a_n$ pour tout entier $n$, est une suite géométrique de raison $q=0,5$.
            En effet : pour tout entier $n$, $v_{n+1}=u_{n+1}-a_{n+1}=0,5(u_n-a_n)=0,5v_n$

            De plus, on connaît l'expression explicite de $v_n$ en fonction de $n$ car $(v_n)$ est géométrique, donc, pour tout entier $n$:
            $v_n=v_0\times q^n=(u_0-6)\times 0,5^n$
            donc $v_n=u_n-a_n=(u_0-6)\times 0,5^n$
            donc $u_n-6=(u_0-6)\times 0,5^n$ car $(a_n)$ est une suite constante égale à $6$.
            <b>Conclusion: pour tout entier $n$, <encadre>$u_n=(u_0-6)\times 0,5^n+6$</encadre> $\quad$ est la forme générale des suites $(u_n)$ vérifiant $(R)$</b>

    3. Montrer que la suite $(u_n)$ n'est ni arithmétique, ni géométrique

        ??? corr
            Calculons $u_0=\dfrac 12$
            <br/>

            Calculons $u_1=\dfrac 12\times u_0+3=\dfrac 12\times \dfrac 12+3=\dfrac 14+3=\dfrac {13}4=3,25$
            <br/>

            Calculons $u_2=\dfrac 12\times u_1+3=\dfrac 12\times \dfrac {13}{4}+3=\dfrac {13}{8}+3=\dfrac {37}8=4,625$

            * --> Pour montrer qu'elle n'est PAS arithmétique:

            Calculons $u_1-u_0=3,25-0,5=2,75$
            Calculons $u_2-u_1=4,625-3,25=1,375$
            donc $u_1-u_0 \ne u_2-u_1$
            ceci prouve que PAS toutes les différences $u_{n+1}-u_n$ ne sont égales entre elles, donc en particulier 
            <b>Conclusion:la suite $(u_n)$ n'est pas arithmétique</b>

            * --> Pour montrer qu'elle n'est PAS géométrique:

            Calculons $\dfrac {u_1}{u_0}=\dfrac {3,25}{0,5}= 6,5$
            Calculons $\dfrac {u_2}{u_1}=\dfrac {4,625}{3,25}\approx 1,42$ au centième
            donc $\dfrac{u_1}{u_0} \ne \dfrac{u_2}{u_1}$
            ceci prouve que PAS tous les quotients $\dfrac{u_{n+1}}{u_n}$ ne sont égales entre elles, donc en particulier 
            <b>Conclusion: la suite $(u_n)$ n'est pas géométrique</b>

    5. En déduire la limite de $u_n$

        ??? corr
            On sait que $\displaystyle \lim_{n \to +\infty} 0,5^n=0$ car $-1<q=0,5<1$, donc $\displaystyle \lim_{n \to +\infty} (u_0-6)\times 0,5^n=(u_0-6)\times 0=0$ d'après le TPL (Théorème sur le Produit des Limites)
            donc $\displaystyle \lim_{n \to +\infty} \left( (u_0-6)\times 0,5^n+6 \right) =0+6=6$ d'après le Théorème sur l'Addition des Limites (TAL)
            donc $\displaystyle \lim_{n \to +\infty} u_n =6$

!!! ex "NON corrigé en cours.."
    Soit $(u_n)$ la suite arithmético-géométrique définie par la relation $u_{n+1}=\dfrac 14 u_n +2$ $\quad (R)$

    1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$
    2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$
    3. Avec $u_0=2$, En déduire l'expression de $u_n$ en fonction de $n$
    4. En déduire la limite de $u_n$
