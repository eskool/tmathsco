<titre>DS Gendarmes</titre>

<ex>

Grâce au Théorème des Gendarmes, déterminer les limites des suites suivantes:

* $u_n=\dfrac {5+2\times (-1)^n}{4+\sqrt n}$
<br/>
* $u_n=\dfrac {2+(-1)^n}{2n^2+3n+1}$
<br/>
* $u_n=\dfrac {4\sin(n)+5}{2n+1}$

</ex>

<ex>

Soit $(u_n)$ la suite définie par:
$$u_n=\dfrac {1}{n^3}+\dfrac {2}{n^3}+\cdots+\dfrac {n}{n^3}$$

1. Montrer que, pour tout entier $n\ge1$, $u_n=\dfrac {n(n+1)}{2n^3}$
2. Déterminer la limite de $u_n$ lorsque $n \rightarrow +\infty$

</ex>

