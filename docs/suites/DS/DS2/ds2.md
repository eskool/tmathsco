<titre>DS Maths Co : Suites</titre>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=3n+4$$

1. Montrer que la suite $(u_n)$ est arithmétique. On déterminera la raison $r$ et le premier terme $u_0$
2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$
3. Calculer la Somme $S=u_0+u_1+...+u_{50}$
4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$
5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

</exo>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=4\times \left( \dfrac 13 \right)^n$$

1. Montrer que la suite $(u_n)$ est géométrique. On déterminera la raison $q$ et le premier terme $u_0$
2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$
3. Calculer la Somme $S=u_0+u_1+...+u_{30}$
4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$
5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

</exo>

<newpage></newpage>

<exo>

Soit $(u_n)$ la suite arithmético-géométrique définie par :
$$u_{n+1}=\dfrac 23 u_n+2 \quad \text{et} \quad u_0=1$$

1. Montrer que la suite $(u_n)$ n'est ni arithmétique, ni géométrique
2. Représenter graphiquement les premiers termes de la suite $(u_n)$ sur le graphique ci-dessous.

<center>

<img src="ds_exo3.png" style="width:60%;margin-bottom:0.5em;">

</center>

3. Conjecturer le sens de variation de la suite $(u_n)$
4. Conjecturer la limite de la suite $(u_n)$

</exo>

<exo>

Soit $(u_n)$ une suite arithmético-géométrique définie par la relation de récurrence $(R)$ suivante :
$$u_{n+1}=\dfrac 38 u_n+2 \quad (R)$$

1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$
2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$
4. A partir de cette question, on sait que $u_0=1$, en déduire une expression explicite de $u_n$ en fonction de $n$
3. En déduire la limite de $u_n$

</exo>

