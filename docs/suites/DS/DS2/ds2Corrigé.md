<titre><red>Corrigé DS Maths Co : Suites</red></titre>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=3n+4$$

1. Montrer que la suite $(u_n)$ est arithmétique. On déterminera la raison $r$ et le premier terme $u_0$

<rep live>

Il s'agit de montrer que toutes les différences $u_{n+1}-u_n$ sont constantes, pour tout entier $n$:
Or, Pour tout entier $n$, $u_{n+1}=3(n+1)+4=3n+3+4=3n+7$, donc
Pour tout entier $n$, $u_{n+1}-u_n=(3n+7)-(3n+4)=3n+7-3n-4=7-4=3$
Ceci prouve que toutes les différences $u_{n+1}-u_n$ sont bien constantes, et sont mêmes égales à $3$.
Ceci prouve que:
* La suite $(u_n)$ est arithmétique de raison $r=3$
* de terme initial $u_0=3\times0 + 4=4$

</rep>

2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} 3n=+\infty$ car il s'agit d'une suite de référence pour $+\infty$.
Donc $\displaystyle \lim_{n \to +\infty} u_n=+\infty$ d'après le théorème sur l'Addition des Limites.

</rep>

3. Calculer la Somme $S=u_0+u_1+...+u_{50}$

<rep live>

<env>Méthode 1: "À la main"</env>
On sait que pour tout entier $n$, $u_n=3n+4$
$S=u_0+u_1+...+u_{100}=(3\times 0 + 4) + (3\times 1 + 4)+...+ (3\times 50 + 4)= (3\times 0 + 3\times 1 + ...3\times 50) + (4+4+...+4)$ où le nombre $4$ apparaît $51$ fois dans cette addition
$S=3\times (0+1+...+50) + 4\times 51$
$S=3\times (1+...+50) + 204$
$S=3\times \dfrac {50\times 51}{2} + 204$
$S=3\times 25 \times 51+ 204$
$S=3825+ 204$
$S=4029$

<env>Méthode 2: Avec la Formule du Cours</env>
On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=(b-a+1)\times \dfrac {u_a+u_b}{2}$ dans notre cas, on obtient:
$S=u_0+u_1+...+u_{50}=(50-0+1)\times \dfrac {u_0+u_{50}}{2}$ 
avec $u_0=3\times0+4=4$ et $u_{50}=3\times50 +4 = 154$
$S=51\times \dfrac {4+154}{2}$
$S=51\times \dfrac {158}{2}$
$S=51\times 79$
$S=4029$

</rep>

4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$

<rep live>

On sait que pour tout entier $n$,
$S_n=u_a+u_{a+1}+...+u_{b}=(b-a+1)\times \dfrac {u_a+u_b}{2}$ dans notre cas, on obtient pour tout entier $n$,
$S_n=u_0+u_1+...+u_n=(n-0+1)\times \dfrac {u_0+u_n}{2}$ 
avec $u_0=3\times0+4=4$ et $u_n=3\times n +4$
donc $S_n=(n+1)\times \dfrac {4+3n+4}{2}$
donc $S_n=(n+1)\times \dfrac {3n+8}{2}$
donc $S_n=\dfrac {(n+1)(3n+8)}{2}$

</rep>

5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $S_n=\dfrac {(n+1)(3n+8)}{2}$
$S_n = \dfrac {3n^2+8n+3n+8}{2}$
$S_n = \dfrac {3n^2+11n+8}{2}$
$S_n = \dfrac {3n^2}{2} + \dfrac {11n}{2} + \dfrac {8}{2}$
$S_n = \dfrac {3n^2}{2} + \dfrac {11n}{2} + 4$

Or $\displaystyle \lim_{n \to +\infty} \dfrac {3n^2}{2}=\lim_{n \to +\infty} \dfrac {3}{2}n^2=+\infty$ car $\displaystyle \lim_{n \to +\infty} n^2=+\infty$ est une suite de référence en $+\infty$ et $\dfrac 32>0$
De même, $\displaystyle \lim_{n \to +\infty} \dfrac {11n}{2}=\lim_{n \to +\infty} \dfrac {11}{2}n=+\infty$ car $\displaystyle \lim_{n \to +\infty} n=+\infty$ est une suite de référence en $+\infty$ et $\dfrac {11}{2}>0$
Donc d'après le théorème sur l'addition des limites:
$\displaystyle \lim_{n \to +\infty} \dfrac {3n^2}{2} + \dfrac {11n}{2} + 4 = +\infty$
Conclusion: $\displaystyle \lim_{n \to +\infty} S_n = +\infty$

</rep>

</exo>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=4\times \left( \dfrac 13 \right)^n$$

1. Montrer que la suite $(u_n)$ est géométrique. On déterminera la raison $q$ et le premier terme $u_0$

<rep live>

Il s'agit de montrer que tous les quotients $\dfrac {u_{n+1}}{u_n}$ sont constants, pour tout entier $n$:
Or, Pour tout entier $n$, $u_{n+1}=3\times \left( \dfrac 12 \right)^{n+1}$, donc
Pour tout entier $n$, $\dfrac {u_{n+1}}{u_n} = \dfrac {4\times \left( \dfrac 13 \right)^{n+1}}{4\times \left( \dfrac 13 \right)^n} = \dfrac {4\times \left( \dfrac 13 \right)^{n}\times \dfrac 13}{4\times \left( \dfrac 13 \right)^n} = \dfrac 13$
Ceci prouve que toutes les quotients $\dfrac {u_{n+1}}{u_n}$ sont bien constants, et sont mêmes égaux à $\dfrac 13$.
Ceci prouve que:
* La suite $(u_n)$ est géométrique de raison $q=\dfrac 13$
* de terme initial $u_0=4\times \left( \dfrac 13 \right) ^0=4\times 1=4$

</rep>

2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} \left( \dfrac 13 \right)^n=0$ car il s'agit d'une suite de référence pour $0$.
Donc $\displaystyle \lim_{n \to +\infty} 4\times \left( \dfrac 13 \right)^n=4\times 0 = 0$ d'après le théorème sur le Produit des Limites.
Donc $\displaystyle \lim_{n \to +\infty} u_n = 0$

</rep>

3. Calculer la Somme $S=u_0+u_1+...+u_{30}$

<rep live>

<env>Méthode 1: "À la main"</env>
On sait que pour tout entier $n$, $u_n=4\times \left( \dfrac 13 \right)^n$
$S=u_0+u_1+...+u_{30}=4\times \left( \dfrac 13 \right)^0 + 4\times \left( \dfrac 13 \right)^1+...+ 4\times \left( \dfrac 13 \right)^{30} = 4\times \left( \left( \dfrac 13 \right)^0+ \left( \dfrac 13 \right)^1 + ...+ \left( \dfrac 13 \right)^{30} \right)$
$S = 4\times \left( 1 + \left( \dfrac 13 \right)^1 + ...+ \left( \dfrac 13 \right)^{30} \right)= 4\times \dfrac {1-q^{n+1}}{1-q}$ où $q=\dfrac 13$ et $n=30$
$S = 4\times \dfrac {1-\left( \dfrac 13 \right)^{30+1}}{1-\dfrac 13} = 4\times \dfrac {1-\left( \dfrac 13 \right)^{31}}{\dfrac 23} = 4\times \dfrac 32 \times \left( 1-\left( \dfrac 13 \right)^{31} \right)$
$S = 6\times \left( 1-\dfrac {1}{3^{31}} \right)$

<env>Méthode 2: Avec la Formule du Cours</env>
On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=u_a\times \dfrac {1-q^{b-a+1}}{1-q}$ dans notre cas, on obtient:
$S=u_0+u_{1}+...+u_{30}=u_0\times \dfrac {1-q^{30-0+1}}{1-q}$
$S=u_0\times \dfrac {1-q^{31}}{1-q}$ avec $u_0=4\times \left( \dfrac 13 \right)^0=4\times 1 = 4$
$S=u_0\times \dfrac {1-q^{31}}{1-q} = 4\times \dfrac {1-\left( \dfrac 13 \right)^{31}}{1-\left( \dfrac 13 \right)}=4\times \dfrac {1-\left( \dfrac 13 \right)^{31}}{\dfrac 23} = 4\times \dfrac 32 \times \left( 1-\left( \dfrac 13 \right)^{31} \right)$
$S=6\times \left( 1- \dfrac {1}{3^{31}} \right)$

</rep>

4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$

<rep live>

On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=u_a\times \dfrac {1-q^{b-a+1}}{1-q}$ donc, dans notre cas, on obtient pour tout entier $n$,
$S_n=u_0+u_1+...+u_n=u_0\times \dfrac {1-q^{n-0+1}}{1-q}$
avec $u_0=4\times \left( \dfrac 13 \right)^0=4\times1 = 4$
donc $S_n = 4\times \dfrac {1-\left( \dfrac 13 \right)^{n+1}}{1-\left( \dfrac 13 \right)}$
donc $S_n = 4\times \dfrac {1-\left( \dfrac 13 \right)^{n+1}}{\dfrac 23}$
donc $S_n = 4\times \dfrac 32 \times \left( 1-\left( \dfrac 13 \right)^{n+1} \right)$
donc $S_n = 6\times \left( 1-\left( \dfrac 13 \right)^{n+1} \right)$

</rep>

5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

<rep live>

Or $\displaystyle \lim_{n \to +\infty} \left( \dfrac {1}{3} \right)^{n+1} =0\quad $ car pour $-1<q=\dfrac 13<1$, on a $\displaystyle \lim_{n \to +\infty} q^{n+1} =0$
donc $\displaystyle \lim_{n \to +\infty} 1-\left( \dfrac {1}{3} \right)^{n+1} = 1-0 = 1$ d'après le théorème sur la Soustraction des Limites
donc $\displaystyle \lim_{n \to +\infty} 6\times \left( 1-\left( \dfrac {1}{3} \right)^{n+1} \right) = 6\times 1 = 6$ d'après le théorème sur le Produit des Limites
Conclusion : $\displaystyle \lim_{n \to +\infty} S_n=6$

</rep>

</exo>

<newpage></newpage>

<exo>

Soit $(u_n)$ la suite arithmético-géométrique définie par :
$$u_{n+1}=\dfrac 23 u_n+2 \quad \text{et} \quad u_0=1$$

1. Montrer que la suite $(u_n)$ n'est ni arithmétique, ni géométrique

<rep live>

  On commence par calculer les premiers termes:
  $u_0=1$
  $u_1=\dfrac 23 u_0 + 2 = \dfrac 23 \times 1 + 2 = \dfrac 23 + 2 = \dfrac {8}{3} \approx 2,67$
  $u_2=\dfrac 23 u_1 + 2 = \dfrac 23 \times \dfrac 83 + 2 = \dfrac {16}{9} + 2 = \dfrac {34}{9} \approx 3,78 $

* Montrons que la suite n'est PAS arithmétique:
  Pour cela on calcule les deux différences:
    * $u_1-u_0 = \dfrac 83 - 1 = \dfrac 53 \approx 1,67$
    * $u_2-u_1 = \dfrac {34}{9} - \dfrac {8}{3} = \dfrac {10}{9} \approx 1,11$
  On en déduit que <enc>$u_1-u_0 \ne u_2-u_1$</enc>
  <b>Conclusion:</b> La suite $(u_n)$ n'est PAS arithmétique

* Montrons que la suite n'est PAS géométrique:
  Pour cela on calcule les deux quotients:
    * $\dfrac {u_1}{u_0} = \dfrac {\dfrac 83}{1}=\dfrac 83 \approx 2,67$
    * $\dfrac {u_2}{u_1} = \dfrac {\dfrac {34}{9}}{\dfrac 83} = \dfrac {17}{12} \approx 1,42$
  On en déduit que <enc>$\dfrac {u_1}{u_0} \ne \dfrac {u_2}{u_1}$</enc>
  <b>Conclusion:</b> La suite $(u_n)$ n'est PAS géométrique

</rep>

2. Représenter graphiquement les premiers termes de la suite $(u_n)$ sur le graphique ci-dessous.

<center>

<img src="ds2_exo3Corrigé.png" style="width:60%;margin-bottom:0.5em;">

</center>

3. Conjecturer le sens de variation de la suite $(u_n)$

<rep live>

La suite $(u_n)$ semble croissante

</rep>

4. Conjecturer la limite de la suite $(u_n)$

<rep live>

La Limite de la suite $(u_n)$ semble être égale à $6$

</rep>

</exo>

<exo>

Soit $(u_n)$ une suite arithmético-géométrique définie par la relation de récurrence $(R)$ suivante :
$$u_{n+1}=\dfrac 38 u_n+2 \quad (R)$$

1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$

<rep live>

Notons $k\in \mathbb R$ la constante définissant la suite $(a_n)$, autrement dit: pour tout entier $n$, on a: $a_n=k$

Puisque la suite $(a_n)$ doit vérifier l'égalité $(R)$, alors :
pour tout entier $n$, $\quad a_{n+1} = \dfrac 38 a_n + 2 \Leftrightarrow k = \dfrac 38 k+2 \Leftrightarrow k - \dfrac 38 k = 2 \Leftrightarrow \left( 1-\dfrac 38 \right)k = 2 \Leftrightarrow \dfrac 58 k = 2 \Leftrightarrow k = \dfrac 85 \times 2 = \dfrac {16}{5}$
donc $\Leftrightarrow k=\dfrac {16}{5}$
<b>Conclusion: la suite constante $(a_n)$ définie par $a_n=\dfrac {16}{5}$ pour tout entier $n$ est la seule suite constante vérifiant la relation $(R)$</b>.

</rep>

2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$

<rep live>

Soit $(u_n)$ une suite quelconque vérifiant $(R)$, alors:

* $u_{n+1} = \dfrac 38 u_n + 2$ pour tout entier $n$
* $a_{n+1} = \dfrac 38 a_n + 2$ pour tout entier $n$

donc, en soustrayant ces deux égalités membre à membre, on trouve que pour tout entier $n$:

$u_{n+1}-a_{n+1}=\dfrac 38 u_n - \dfrac 38 a_n+2-2$
$u_{n+1}-a_{n+1}=\dfrac 38 u_n - \dfrac 38 a_n$
$u_{n+1}-a_{n+1}=\dfrac 38 (u_n - a_n)$


Cette dernière égalité prouve que la suite $(v_n)$ définie par $v_n=u_n-a_n$ pour tout entier $n$, est une suite géométrique de raison $q=\dfrac 38$.
En effet : pour tout entier $n$, $v_{n+1}=u_{n+1}-a_{n+1}= \dfrac 38(u_n-a_n)= \dfrac 38v_n$

De plus, on connaît l'expression explicite de $v_n$ en fonction de $n$ car $(v_n)$ est géométrique, donc, pour tout entier $n$:
$v_n=v_0\times q^n=\left( u_0-\dfrac {16}{5} \right)\times \left( \dfrac 38 \right)^n$
donc $v_n=u_n-a_n = \left( u_0-\dfrac {16}{5} \right)\times \left( \dfrac 38 \right)^n$
donc $u_n- \dfrac {16}{5} = \left( u_0-\dfrac {16}{5} \right)\times \left( \dfrac 38 \right)^n$ car $(a_n)$ est une suite constante égale à $\dfrac {16}{5}$.
<b>Conclusion: pour tout entier $n$, <encadre>$u_n=\left( u_0-\dfrac {16}{5} \right) \times \left( \dfrac 38 \right)^n+\dfrac {16}{5}$</encadre> $\quad$ est la forme générale des suites $(u_n)$ vérifiant $(R)$</b>

</rep>

3. A partir de cette question, on sait que $u_0=1$, en déduire une expression explicite de $u_n$ en fonction de $n$

<rep live>

On a donc $u_n=\left( 1-\dfrac {16}{5} \right) \times \left( \dfrac 38 \right)^n+\dfrac {16}{5}$
donc <enc>$u_n=-\dfrac {11}{5} \times \left( \dfrac 38 \right)^n+\dfrac {16}{5}$</enc>

</rep>

4. En déduire la limite de $u_n$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} \left( \dfrac 38 \right)^n=0$ car pour tout réel $q$ vérifiant $-1<q=\dfrac 38<1$ on a $\displaystyle \lim_{n \to +\infty} q^n=0$
donc $\displaystyle \lim_{n \to +\infty} -\dfrac {11}{5} \times \left( \dfrac 38 \right)^n=-\dfrac {11}{5}\times 0=0$ d'après le théorème sur le Produit des Limites
donc $\displaystyle \lim_{n \to +\infty} -\dfrac {11}{5} \times \left( \dfrac 38 \right)^n+\dfrac {16}{5}=0+\dfrac {16}{5}=\dfrac {16}{5}$ d'après le théorème sur la Somme des Limites.
<env>Conclusion</env> $\displaystyle \lim_{n \to +\infty} u_n=\dfrac {16}{5}$

</rep>

</exo>

