<titre><red>Corrigé DS Maths Co : Suites</red></titre>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=4n+7$$

1. Montrer que la suite $(u_n)$ est arithmétique. On déterminera la raison $r$ et le premier terme $u_0$

<rep live>

Il s'agit de montrer que toutes les différences $u_{n+1}-u_n$ sont constantes, pour tout entier $n$:
Or, Pour tout entier $n$, $u_{n+1} = 4(n+1)+7=4n+4+7=4n+11$, donc
Pour tout entier $n$, $u_{n+1}-u_n=(4n+11)-(4n+7)=4n+11-4n-7=11-7=4$
Ceci prouve que toutes les différences $u_{n+1}-u_n$ sont bien constantes, et sont mêmes égales à $4$.
Ceci prouve que:
* La suite $(u_n)$ est arithmétique de raison $r=4$
* de terme initial $u_0=4\times0 + 7=7$

</rep>

2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} 4n=+\infty$ car il s'agit d'une suite de référence pour $+\infty$.
Donc $\displaystyle \lim_{n \to +\infty} u_n = \lim_{n \to +\infty} 4n+7 = +\infty$ d'après le théorème sur l'Addition des Limites.

</rep>

3. Calculer la Somme $S=u_0+u_1+...+u_{60}$

<rep live>

<env>Méthode 1: "À la main"</env>
On sait que pour tout entier $n$, $u_n=4n+7$
$S=u_0+u_1+...+u_{60}=(4\times 0 + 7) + (4\times 1 + 7)+...+ (4\times 60 + 7)= (4\times 0 + 4\times 1 + ...+ 4\times 60) + (7+7+...+7)$ où le nombre $7$ apparaît $61$ fois dans cette addition
$S=4\times (0+1+...+60) + 7\times 61$
$S=4\times (1+...+60) + 427$
$S=4\times \dfrac {60\times 61}{2} + 427$
$S=4\times 30 \times 61+ 427$
$S=7320 + 427$
$S=7747$

<env>Méthode 2: Avec la Formule du Cours</env>
On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=(b-a+1)\times \dfrac {u_a+u_b}{2}$ dans notre cas, on obtient:
$S=u_0+u_1+...+u_{60}=(60-0+1)\times \dfrac {u_0+u_{60}}{2}$ 
avec $u_0=4\times0+7=7$ et $u_{60}=4\times 60 +7 = 247$
$S=61\times \dfrac {7+247}{2}$
$S=61\times \dfrac {254}{2}$
$S=61\times 127$
$S=7747$

</rep>

4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$

<rep live>

On sait que pour tout entier $n$,
$S_n=u_a+u_{a+1}+...+u_{b}=(b-a+1)\times \dfrac {u_a+u_b}{2}$ dans notre cas, on obtient pour tout entier $n$,
$S_n=u_0+u_1+...+u_n=(n-0+1)\times \dfrac {u_0+u_n}{2}$ 
avec $u_0=4\times 0+7=7$ et $u_n=4\times n +7$
donc $S_n=(n+1)\times \dfrac {7+4n+7}{2}$
donc $S_n=(n+1)\times \dfrac {4n+14}{2}$
donc $S_n=(n+1)\times \left[ \dfrac {4n}{2} + \dfrac {14}{2} \right]$
donc $S_n=(n+1)(2n+7)$

</rep>

5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $S_n=(n+1)(2n+7)$
$S_n = 2n^2+7n+2n+7$
$S_n = 2n^2+9n+7$

Or $\displaystyle \lim_{n \to +\infty} 2n^2 = +\infty$ car $\displaystyle \lim_{n \to +\infty} n^2=+\infty$ est une suite de référence en $+\infty$ et d'après le T.P.L (Théorème sur le Produit des Limites) 
De même, $\displaystyle \lim_{n \to +\infty} 9n = +\infty$ car $\displaystyle \lim_{n \to +\infty} n=+\infty$ est une suite de référence en $+\infty$ et toujours d'après le T.P.L.
Donc d'après le Théorème sur l'Addition des Limites (T.A.L.):
$\displaystyle \lim_{n \to +\infty} 2n^2+9n+7 = +\infty$
Conclusion: $\displaystyle \lim_{n \to +\infty} S_n = +\infty$

</rep>

</exo>

<exo>

Soit $(u_n)$ la suite définie, pour tout entier $n$, par
$$u_n=5\times \left( \dfrac 14 \right)^n$$

1. Montrer que la suite $(u_n)$ est géométrique. On déterminera la raison $q$ et le premier terme $u_0$

<rep live>

Il s'agit de montrer que tous les quotients $\dfrac {u_{n+1}}{u_n}$ sont constants, pour tout entier $n$:
Or, Pour tout entier $n$, $u_{n+1}=5\times \left( \dfrac 14 \right)^{n+1}$, donc
Pour tout entier $n$, $\dfrac {u_{n+1}}{u_n} = \dfrac {5\times \left( \dfrac 14 \right)^{n+1}}{5\times \left( \dfrac 14 \right)^n} = \dfrac {5\times \left( \dfrac 14 \right)^{n}\times \dfrac 14}{5\times \left( \dfrac 14 \right)^n} = \dfrac 14$
Ceci prouve que toutes les quotients $\dfrac {u_{n+1}}{u_n}$ sont bien constants, et sont mêmes égaux à $\dfrac 14$.
Ceci prouve que:
* La suite $(u_n)$ est géométrique de raison $q=\dfrac 14$
* de terme initial $u_0=5\times \left( \dfrac 14 \right) ^0=5\times 1=5$

</rep>

2. Déterminer la limite de $u_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} \left( \dfrac 14 \right)^n=0$ car il s'agit d'une suite de référence pour $0$.
Donc $\displaystyle \lim_{n \to +\infty} 5\times \left( \dfrac 14 \right)^n=5\times 0 = 0$ d'après le Théorème sur le Produit des Limites.
Donc $\displaystyle \lim_{n \to +\infty} u_n = 0$

</rep>

3. Calculer la Somme $S=u_0+u_1+...+u_{40}$

<rep live>

<env>Méthode 1: "À la main"</env>
On sait que pour tout entier $n$, $u_n=5\times \left( \dfrac 14 \right)^n$
$S=u_0+u_1+...+u_{40}=5\times \left( \dfrac 14 \right)^0 + 5\times \left( \dfrac 14 \right)^1+...+ 5\times \left( \dfrac 14 \right)^{40} = 5\times \left( \left( \dfrac 14 \right)^0+ \left( \dfrac 14 \right)^1 + ...+ \left( \dfrac 14 \right)^{40} \right)$
$S = 5\times \left( 1 + \left( \dfrac 14 \right)^1 + ...+ \left( \dfrac 14 \right)^{40} \right)= 5\times \dfrac {1-q^{n+1}}{1-q}$ où $q=\dfrac 14$ et $n=40$
$S = 5\times \dfrac {1-\left( \dfrac 14 \right)^{40+1}}{1-\dfrac 14} = 5\times \dfrac {1-\left( \dfrac 14 \right)^{41}}{\dfrac 34} = 5\times \dfrac 43 \times \left( 1-\left( \dfrac 14 \right)^{41} \right)$
$S = \dfrac {20}{3}\times \left( 1-\dfrac {1}{4^{41}} \right)$

<env>Méthode 2: Avec la Formule du Cours</env>
On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=u_a\times \dfrac {1-q^{b-a+1}}{1-q}$ dans notre cas, on obtient:
$S=u_0+u_{1}+...+u_{30}=u_0\times \dfrac {1-q^{40-0+1}}{1-q}$
$S=u_0\times \dfrac {1-q^{41}}{1-q}$ avec $u_0=5\times \left( \dfrac 14 \right)^0=5\times 1 = 5$
$S=u_0\times \dfrac {1-q^{41}}{1-q} = 5\times \dfrac {1-\left( \dfrac 14 \right)^{41}}{1 - \dfrac 14 } = 5\times \dfrac {1-\left( \dfrac 14 \right)^{41}}{\dfrac 34} = 5\times \dfrac 43 \times \left( 1-\left( \dfrac 14 \right)^{41} \right)$
$S=\dfrac {20}{3}\times \left( 1- \dfrac {1}{4^{41}} \right)$

</rep>

4. Donner une formule explicite de $S_n=u_0+u_1+...+u_n$ en fonction de $n$

<rep live>

On sait que pour tout entier $n$,
$S=u_a+u_{a+1}+...+u_{b}=u_a\times \dfrac {1-q^{b-a+1}}{1-q}$ donc, dans notre cas, on obtient pour tout entier $n$,
$S_n=u_0+u_1+...+u_n=u_0\times \dfrac {1-q^{n-0+1}}{1-q}$
avec $u_0 = 5\times \left( \dfrac 14 \right)^0=5\times1 = 5$
donc $S_n = 5\times \dfrac {1-\left( \dfrac 14 \right)^{n+1}}{1-\left( \dfrac 134\right)}$
donc $S_n = 5\times \dfrac {1-\left( \dfrac 14 \right)^{n+1}}{\dfrac 34}$
donc $S_n = 5\times \dfrac 43 \times \left( 1-\left( \dfrac 14 \right)^{n+1} \right)$
donc $S_n = \dfrac {20}{3} \times \left( 1-\left( \dfrac 14 \right)^{n+1} \right)$

</rep>

5. Déterminer la limite de $S_n$ lorsque $n$ tend vers $+\infty$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} \left( \dfrac {1}{4} \right)^{n+1} = 0\quad $ car pour tout $-1<q=\dfrac 14<1$, on a $\displaystyle \lim_{n \to +\infty} q^{n+1} =0$
donc $\displaystyle \lim_{n \to +\infty} 1-\left( \dfrac {1}{4} \right)^{n+1} = 1-0 = 1$ d'après le Théorème sur la Soustraction des Limites
donc $\displaystyle \lim_{n \to +\infty} \dfrac {20}{3} \times \left( 1-\left( \dfrac {1}{4} \right)^{n+1} \right) = \dfrac {20}{3} \times 1 = \dfrac {20}{3}$ d'après le Théorème sur le Produit des Limites
Conclusion : $\displaystyle \lim_{n \to +\infty} S_n=\dfrac {20}{3}$

</rep>

</exo>

<newpage></newpage>

<exo>

Soit $(u_n)$ la suite arithmético-géométrique définie par :
$$u_{n+1}=\dfrac 25 u_n + 3 \quad \text{et} \quad u_0=2$$

1. Montrer que la suite $(u_n)$ n'est ni arithmétique, ni géométrique

<rep live>

  On commence par calculer les premiers termes:
  $u_0=2$
  $u_1=\dfrac 25 u_0 + 3 = \dfrac 25 \times 2 + 3 = \dfrac 45 + 3 = \dfrac {19}{5} = 3,8$
  $u_2=\dfrac 25 u_1 + 3 = \dfrac 25 \times \dfrac {19}{5} + 3 = \dfrac {38}{25} + 3 = \dfrac {113}{25} = 4,52 $

* Montrons que la suite n'est PAS arithmétique:
  Pour cela on calcule les deux différences:
    * $u_1-u_0 = \dfrac {19}{5} - 2 = \dfrac 95 = 1,8$
    * $u_2-u_1 = \dfrac {113}{25} - \dfrac {19}{5} = \dfrac {18}{25} = 0,72$
  On en déduit que <enc>$u_1-u_0 \ne u_2-u_1$</enc>
  <b>Conclusion:</b> La suite $(u_n)$ n'est PAS arithmétique

* Montrons que la suite n'est PAS géométrique:
  Pour cela on calcule les deux quotients:
    * $\dfrac {u_1}{u_0} = \dfrac {\dfrac {19}{5}}{2}=\dfrac {19}{10} = 1,9$
    * $\dfrac {u_2}{u_1} = \dfrac {\dfrac {113}{25}}{\dfrac {19}{5}} = \dfrac {113}{95} \approx 1,19$
  On en déduit que <enc>$\dfrac {u_1}{u_0} \ne \dfrac {u_2}{u_1}$</enc>
  <b>Conclusion:</b> La suite $(u_n)$ n'est PAS géométrique

</rep>

2. Représenter graphiquement les premiers termes de la suite $(u_n)$ sur le graphique ci-dessous.

<center>

<img src="ds3_exo3Corrigé.png" style="width:60%;margin-bottom:0.5em;">

</center>

3. Conjecturer le sens de variation de la suite $(u_n)$

<rep live>

La suite $(u_n)$ semble croissante

</rep>

4. Conjecturer la limite de la suite $(u_n)$

<rep live>

La Limite de la suite $(u_n)$ semble être égale à $5$

</rep>

</exo>

<exo>

Soit $(u_n)$ une suite arithmético-géométrique définie par la relation de récurrence $(R)$ suivante :
$$u_{n+1}=\dfrac 45 u_n + 2 \quad (R)$$

1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$

<rep live>

Notons $k\in \mathbb R$ la constante définissant la suite $(a_n)$, autrement dit: pour tout entier $n$, on a: $a_n=k$

Puisque la suite $(a_n)$ doit vérifier l'égalité $(R)$, alors :
pour tout entier $n$, $\quad a_{n+1} = \dfrac 45 a_n + 2 \Leftrightarrow k = \dfrac 45 k+2 \Leftrightarrow k - \dfrac 45 k = 2 \Leftrightarrow \left( 1-\dfrac 45 \right)k = 2 \Leftrightarrow \dfrac 15 k = 2 \Leftrightarrow k = \dfrac 51 \times 2 = 10$
donc $\Leftrightarrow k = 10$
<b>Conclusion: la suite constante $(a_n)$ définie par $a_n=10$ pour tout entier $n$ est la seule suite constante vérifiant la relation $(R)$</b>.

</rep>

2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$

<rep live>

Soit $(u_n)$ une suite quelconque vérifiant $(R)$, alors:

* $u_{n+1} = \dfrac 45 u_n + 2$ pour tout entier $n$
* $a_{n+1} = \dfrac 45 a_n + 2$ pour tout entier $n$

donc, en soustrayant ces deux égalités membre à membre, on trouve que pour tout entier $n$:

$u_{n+1}-a_{n+1}=\dfrac 45 u_n - \dfrac 45 a_n+2-2$
$u_{n+1}-a_{n+1}=\dfrac 45 u_n - \dfrac 45 a_n$
$u_{n+1}-a_{n+1}=\dfrac 45 (u_n - a_n)$


Cette dernière égalité prouve que la suite $(v_n)$ définie par $v_n=u_n-a_n$ pour tout entier $n$, est une suite géométrique de raison $q=\dfrac 45$.
En effet : pour tout entier $n$, $v_{n+1}=u_{n+1}-a_{n+1}= \dfrac 45(u_n-a_n)= \dfrac 45v_n$

De plus, on connaît l'expression explicite de $v_n$ en fonction de $n$ car $(v_n)$ est géométrique, donc, pour tout entier $n$:
$v_n=v_0\times q^n=\left( u_0-10 \right)\times \left( \dfrac 45 \right)^n$
donc $v_n=u_n-a_n = \left( u_0-10 \right)\times \left( \dfrac 45 \right)^n$
donc $u_n- 10 = \left( u_0-10 \right)\times \left( \dfrac 45 \right)^n$ car $(a_n)$ est une suite constante égale à $10$.
<b>Conclusion: pour tout entier $n$, <encadre>$u_n=\left( u_0-10 \right) \times \left( \dfrac 45 \right)^n+10$</encadre> $\quad$ est la forme générale des suites $(u_n)$ vérifiant $(R)$</b>

</rep>

3. A partir de cette question, on sait que $u_0=2$, en déduire une expression explicite de $u_n$ en fonction de $n$

<rep live>

On a donc $u_n = \left( 2 - 10 \right) \times \left( \dfrac 45 \right)^n + 10$
donc <enc>$u_n = -8 \times \left( \dfrac 45 \right)^n + 10$</enc>

</rep>

4. En déduire la limite de $u_n$

<rep live>

On sait que $\displaystyle \lim_{n \to +\infty} \left( \dfrac 45 \right)^n=0$ car pour tout réel $q$ vérifiant $-1<q=\dfrac 45<1$ on a $\displaystyle \lim_{n \to +\infty} q^n=0$
donc $\displaystyle \lim_{n \to +\infty} -8 \times \left( \dfrac 45 \right)^n = -8 \times 0=0$ d'après le Théorème sur le Produit des Limites
donc $\displaystyle \lim_{n \to +\infty} -8 \times \left( \dfrac 45 \right)^n + 10 = 0 + 10 = 10$ d'après le Théorème sur la Somme des Limites.
<env>Conclusion</env> $\displaystyle \lim_{n \to +\infty} u_n = 10$

</rep>

</exo>

