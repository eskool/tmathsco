# TMATHSCO : Leçon sur les Suites

Commençons par des rappels de 1ère, durant lesquels nous ferons des rappels sur les :

* définition d'une suite
* les différents modes de génération d'une suite
* suites Arithmétiques
* suites Géométriques