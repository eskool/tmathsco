<titre>Exercices Limites de Suites</titre>

<ex>

Déterminer les limites de la Somme, de la Différence, du Produit et du Quotient de $u_n$ par $v_n$, pour chacune des suites suivantes:

1. $u_n=2n+3$ et $v_n=n$
Autrement dit, on demande de déterminer (l'existence et) la valeur de:
* $\displaystyle \lim_{n \to +\infty} u_n$
* $\displaystyle \lim_{n \to +\infty} v_n$
* $\displaystyle \lim_{n \to +\infty} u_n+v_n$
* $\displaystyle \lim_{n \to +\infty} u_n-v_n$
* $\displaystyle \lim_{n \to +\infty} u_n\times v_n$
* $\displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}$
2. $u_n=\sqrt n$ et $v_n=4n$
3. $u_n=2n+1$ et $v_n=3n^2+n-1$

</ex>

<ex>

Déterminer les limites de la Somme, de la Différence, du Produit et du Quotient de $u_n$ par $v_n$, pour chacune des suites suivantes:

1. $u_n=3n+5$ et $v_n=2n$
Autrement dit, on demande de déterminer (l'existence et) la valeur de:
* $\displaystyle \lim_{n \to +\infty} u_n$
* $\displaystyle \lim_{n \to +\infty} v_n$
* $\displaystyle \lim_{n \to +\infty} u_n+v_n$
* $\displaystyle \lim_{n \to +\infty} u_n-v_n$
* $\displaystyle \lim_{n \to +\infty} u_n\times v_n$
* $\displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}$
2. $u_n=\sqrt n$ et $v_n=3n$
3. $u_n=-2n+4$ et $v_n=2n^2+n-3$

</ex>

<ex>

Déterminer les limites de la Somme, de la Différence, du Produit et du Quotient de $u_n$ par $v_n$, pour chacune des suites suivantes:

1. $u_n=-3n+2$ et $v_n=\dfrac 1n$
Autrement dit, on demande de déterminer (l'existence et) la valeur de:
* $\displaystyle \lim_{n \to +\infty} u_n$
* $\displaystyle \lim_{n \to +\infty} v_n$
* $\displaystyle \lim_{n \to +\infty} u_n+v_n$
* $\displaystyle \lim_{n \to +\infty} u_n-v_n$
* $\displaystyle \lim_{n \to +\infty} u_n\times v_n$
* $\displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}$

</ex>

<ex>

Grâce au Théorème des Gendarmes, déterminer les limites des suites suivantes:

* $u_n=\dfrac {3+(-1)^n}{\sqrt n}$
* $u_n=\dfrac {1+(-1)^n}{4n^2}$
* $u_n=\dfrac {sin(n)+cos(n)}{2n+1}$

</ex>

<ex>

Soit $(u_n)$ la suite définie par:
$$u_n=\dfrac {1}{n^2}+\dfrac {2}{n^2}+\cdots+\dfrac {n}{n^2}$$

1. Montrer que, pour tout entier $n\ge1$, $u_n=\dfrac {n(n+1)}{2n^2}$
2. Déterminer la limite de $u_n$ lorsque $n \rightarrow +\infty$

</ex>

<ex>

Soit $(u_n)$ la suite définie par:
$$u_n=\dfrac {n}{n^2+1}+\dfrac {n}{n^2+2}+\dfrac {n}{n^2+3}+\cdots+\dfrac {n}{n^2+n}$$

1. Montrer que, pour tout entier $n\ge1$ et pour tout entier $1\le k \le n$:
$$\dfrac {n}{n^2+n} \le \dfrac {n}{n^2+k}\le \dfrac {n}{n^2+1}$$

2. En déduire un encadrement de $u_n$, pour tout entier naturel $n\ge 1$,
3. Déterminer la limite de $u_n$ lorsque $n \rightarrow +\infty$

</ex>


<br/>
<br/>
<br/>
