<titre>TMATHCO : DM Suites </titre>
A rendre **au plus tard le Jeudi 17 Décembre**:
* **en main propre au Professeur**, pour le groupe ayant cours **le Jeudi 17 Décembre de 16h30 à 18h30**
* **Dans le casier de M SCHWENCKE**, pour le groupe n'ayant pas cours ce jour là, **au plus tard Jeudi 17 Décembre à 12h** (ATTENTION: au plus tard à midi pour ce groupe SVP)

<ex>

Étudier les limites suivantes :

1. $u_n=n^2+3n-2$, $\quad v_n=4n-1$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$
2. $u_n=5n+4$, $\quad v_n=n^2+3n+1$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$
3. $u_n=n^2+6n+2$, $\quad v_n=3n^2+n+4$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$
3. $u_n=3n+2$, $\quad v_n=\sqrt n+5$, $\quad \displaystyle \lim_{n \to +\infty} u_n-v_n=?$

</ex>

<ex>

Soit $(u_n)$ la suite définie par:
$$u_n=3+\dfrac {(-1)^n+4}{2n+1}$$

Montrer que $\displaystyle \lim_{n \to +\infty} u_n$ existe et déterminer sa valeur (on pourra utliser le Théorème des Gendarmes)

</ex>