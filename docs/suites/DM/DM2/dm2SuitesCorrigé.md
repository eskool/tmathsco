<titre>TMATHCO : DM Suites </titre>

<ex>

Étudier les limites suivantes :

1. $u_n=n^2+3n-2$, $\quad v_n=4n-1$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$

<rep live>

Intuivivement, $\displaystyle \lim_{n \to +\infty} u_n = +\infty$ et $\displaystyle \lim_{n \to +\infty} v_n = +\infty$, donc nous avons affaire à une <bred>Forme Indéterminée (F.I.)</bred> du type $\displaystyle \frac {+\infty}{+\infty}$. Il faut donc préalablement modifier l'expression $\displaystyle \frac {u_n}{v_n}$ pour la simplifier au maximum AVANT passage à la limite:

$\displaystyle \frac {u_n}{v_n} = \frac {n^2+3n-2}{4n-1} = \frac {n^2 \left( 1 + \frac{3n}{n^2}-\frac {2}{n^2} \right)}{n(4 - \frac{1}{n})} = \frac{n^2}{n} \times \frac{1 + \frac{3n}{n^2}-\frac {2}{n^2}}{4 - \frac 1n} = n \times \frac{1 + \frac{3}{n}-\frac {2}{n^2}}{4 - \frac 1n} $

De plus:
* D'une part $\displaystyle \lim_{n \to +\infty} n=+\infty$ (Limite de Cours)
* D'autre part : $\displaystyle \lim_{n \to +\infty} 1 + \frac{3}{n}-\frac {2}{n^2} = 1+0-0=1$ (Théorème d'<bred>Addition</bred> des Limites, T.A.L. et Théorème de <bred>Soustraction</bred> des Limites, T.S.L.) et $\displaystyle \lim_{n \to +\infty} 4- \frac 1n = 4-0=4$ (T.A.L. + T.S.L.)
donc $\displaystyle \lim_{n \to +\infty} \frac{1 + \frac{3}{n}-\frac {2}{n^2}}{4 - \frac 1n} = \frac 14$ (Théorème sur les <bred>Quotients</bred> des Limites, T.Q.L.)

donc $\displaystyle \lim_{n \to +\infty} \frac{u_n}{v_n} = +\infty$ d'après le Théorème sur le <bred>Produit</bred> des Limites, T.P.L. $\left(  \text{intuitivement : } \,\displaystyle "+\infty \times \frac 14 = +\infty " \right)$

</rep>

2. $u_n=5n+4$, $\quad v_n=n^2+3n+1$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$

<rep live>

Intuivivement, $\displaystyle \lim_{n \to +\infty} u_n = +\infty$ et $\displaystyle \lim_{n \to +\infty} v_n = +\infty$, donc nous avons affaire à une <bred>Forme Indéterminée (F.I.)</bred> du type $\displaystyle \frac {+\infty}{+\infty}$. Il faut donc préalablement modifier l'expression $\displaystyle \frac {u_n}{v_n}$ pour la simplifier au maximum AVANT passage à la limite:

$\displaystyle \frac {u_n}{v_n} = \frac {5n+4}{n^2+3n+1} = \frac {n(5+\frac 4n)}{n^2 \left( 1 + \frac{3n}{n^2}+\frac {1}{n^2} \right)} = \frac{n}{n^2} \times \frac{5 + \frac 4n}{1 + \frac{3n}{n^2}+\frac {1}{n^2}} = \frac{1}{n} \times \frac{5 + \frac 4n}{1 + \frac{3}{n}+\frac {1}{n^2}} $

De plus:
* D'une part $\displaystyle \lim_{n \to +\infty} \frac 1n=0$ (Limite de Cours)
* D'autre part : $\displaystyle \lim_{n \to +\infty} 5+ \frac 4n = 5+0 = 5$ (T.A.L) et $\displaystyle \lim_{n \to +\infty} 1 + \frac{3}{n}+\frac {1}{n^2} = 1+0+0=1$ (T.A.L).
donc $\displaystyle \lim_{n \to +\infty} \frac {5 + \frac 4n}{1 + \frac{3}{n}+\frac {1}{n^2}} = \frac 51 = 5$ (T.Q.L.)

donc $\displaystyle \lim_{n \to +\infty} \frac{u_n}{v_n} = 0$ d'après le T.P.L. $\left(  \text{intuitivement : } \,\displaystyle "0 \times 5 = 0 " \right)$

</rep>

3. $u_n = n^2+6n+2$, $\quad v_n = 3n^2+n+4$, $\quad \displaystyle \lim_{n \to +\infty} \dfrac {u_n}{v_n}=?$

<rep live>

Intuivivement, $\displaystyle \lim_{n \to +\infty} u_n = +\infty$ et $\displaystyle \lim_{n \to +\infty} v_n = +\infty$, donc nous avons affaire à une <bred>Forme Indéterminée (F.I.)</bred> du type $\displaystyle \frac {+\infty}{+\infty}$. Il faut donc préalablement modifier l'expression $\displaystyle \frac {u_n}{v_n}$ pour la simplifier au maximum AVANT passage à la limite:

$\displaystyle \frac {u_n}{v_n} = \frac {n^2+6n+2}{3n^2+n+4} = \frac {n^2 \left( 1 + \frac{6n}{n^2}+\frac {2}{n^2} \right)}{n^2 \left(  3 + \frac{n}{n^2} +\frac {4}{n^2} \right)} = \frac{n^2}{n^2} \times \frac {1 + \frac{6n}{n^2}+\frac {2}{n^2}}{3 + \frac{n}{n^2} +\frac {4}{n^2}} = 1 \times \frac {1 + \frac{6}{n}+\frac {2}{n^2}}{3 + \frac{1}{n} +\frac {4}{n^2}} $

De plus:
* D'une part $\displaystyle \lim_{n \to +\infty} 1=1$ (Limite d'une Constante...)
* D'autre part : $\displaystyle \lim_{n \to +\infty} 1 + \frac{6}{n} + \frac {2}{n^2} = 1+0+0=1$ (T.A.L.) et $\displaystyle \lim_{n \to +\infty} 3 + \frac{1}{n} +\frac {4}{n^2} = 3+0+0=3$ (T.A.L.)
donc $\displaystyle \lim_{n \to +\infty} \frac{1 + \frac{6}{n}+\frac {2}{n^2}}{3 + \frac{1}{n} +\frac {4}{n^2}} = \frac 13$ (Théorème sur les <bred>Quotients</bred> des Limites, T.Q.L.)

donc $\displaystyle \lim_{n \to +\infty} \frac{u_n}{v_n} = \frac 13$ d'après le Théorème sur le <bred>Produit</bred> des Limites, T.P.L. $\left(  \text{intuitivement : } \,\displaystyle "1 \times \frac 13 = \frac 13 " \right)$

</rep>

4. $u_n=3n+2$, $\quad v_n=\sqrt n+5$, $\quad \displaystyle \lim_{n \to +\infty} u_n-v_n=?$

<rep live>

Intuivivement, $\displaystyle \lim_{n \to +\infty} u_n = +\infty$ et $\displaystyle \lim_{n \to +\infty} v_n = +\infty$, donc nous avons affaire à une <bred>Forme Indéterminée (F.I.)</bred> du type $\displaystyle (+\infty) - (+\infty)$. Il faut donc préalablement modifier l'expression $\displaystyle u_n - v_n$ pour la simplifier au maximum AVANT passage à la limite:

$u_n - v_n = (3n+2) - (\sqrt n + 5) = 3n - \sqrt n  + 2 - 5 = n- \sqrt n - 3$
Puis, on factorise par le terme dominant (ici $n$ est dominant par rapport à $\sqrt n$) :
$\displaystyle u_n - v_n = n\times \left( 1 - \frac{\sqrt n}{n} - \frac 3n\right) = n\times \left( 1 - \frac{\sqrt n}{\sqrt n \times \sqrt n} - \frac 3n\right)$ car $n = \sqrt n \times \sqrt n$
donc $\displaystyle u_n - v_n = n\times \left( 1 - \frac{1}{\sqrt n} - \frac 3n\right)$

De plus:
* D'une part $\displaystyle \lim_{n \to +\infty} n=+\infty$ (Limite de Cours)
* D'autre part : $\displaystyle \lim_{n \to +\infty} \left( 1 - \frac{1}{\sqrt n} - \frac 3n\right) = 1-0-0=1$ (T.Q.L + T.S.L.) car $\displaystyle \lim_{n \to +\infty} \frac {1}{\sqrt n} = 0$ d'après le T.Q.L. $\left(  \text{intuitivement : } \,\displaystyle "\frac {1}{+\infty} = 0 " \right)$

donc $\displaystyle \lim_{n \to +\infty} u_n - v_n = \lim_{n \to +\infty} n \times \left( 1 - \frac{1}{\sqrt n} - \frac 3n\right) = +\infty$ d'après le T.P.L. $\left(  \text{intuitivement : } \,\displaystyle "+\infty \times 1 = +\infty " \right)$

</rep>

</ex>

<newpage></newpage>

<ex>

Soit $(u_n)$ la suite définie par:
$$u_n=3+\dfrac {(-1)^n+4}{2n+1}$$

Montrer que $\displaystyle \lim_{n \to +\infty} u_n$ existe et déterminer sa valeur (on pourra utliser le Théorème des Gendarmes)

<rep live>

* <bred>D'une part, trouvons un encadrement de $u_n$, pour tout entier $n$ :</bred>
Commençons par remarquer que, pour tout entier $n, \quad -1 \le (-1)^n \le 1$
donc pour tout entier $n$:
$-1+4 \le (-1)^n+4 \le 1+4$
$\Leftrightarrow 3 \le (-1)^n+4 \le 5$
$\Leftrightarrow 3 \le (-1)^n+4 \le 5$
$\Leftrightarrow \displaystyle \frac {3}{2n+1} \le \frac {(-1)^n+4}{2n+1} \le \frac {5}{2n+1}$
$\Leftrightarrow \displaystyle 3+\frac {3}{2n+1} \le 3+\frac {(-1)^n+4}{2n+1} \le 3+\frac {5}{2n+1}$

$\Leftrightarrow $ <enc>$\displaystyle 3+\frac {3}{2n+1} \le u_n \le 3+\frac {5}{2n+1}$</enc>

* D'autre part étudions la limite de chacune des 2 bornes de cet encadrement (la limite du terme de Gauche et la limite du terme de Droite):

  * Limite du terme de Gauche : On a $\displaystyle \lim_{n \to +\infty} 2n+1 = +\infty$ (T.A.L), donc $\displaystyle \lim_{n \to +\infty} \frac {3}{2n+1} = 0$ (T.Q.L) $\left(  \text{intuitivement : } \,\displaystyle "\frac {3}{+\infty} = 0 " \right)$
  * Limite du terme de Droite : On a $\displaystyle \lim_{n \to +\infty} 2n+1 = +\infty$ (T.A.L), donc $\displaystyle \lim_{n \to +\infty} \frac {5}{2n+1} = 0$ (T.Q.L) $\left(  \text{intuitivement : } \,\displaystyle "\frac {5}{+\infty} = 0 " \right)$
  * on en déduit que les limites des termes de Gauche et de de Droite existent et valent la même valeur $0$

* <bred>Conclusion :</bred> D'après le <bred>Théorème des Gendarmes</bred>, on peut donc en déduire que la limite du terme du milieu, à savoir $u_n$, existe et vaut $0$ également, donc :

<center>

<enc>$$\displaystyle \lim_{n \to +\infty} u_n = 0$$</enc>

</center>

</rep>

</ex>