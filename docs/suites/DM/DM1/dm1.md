<titre>Devoir Maison, pour la rentrée Octobre 2021</titre>

<exo>

Soit $(u_n)$ la suite arithmético-géométrique définie par :
$$u_{n+1}=\dfrac 25 u_n+3 \quad \text{et} \quad u_0=1,5$$

1. Montrer que la suite $(u_n)$ n'est ni arithmétique, ni géométrique
2. Représenter graphiquement les premiers termes de la suite $(u_n)$ sur le graphique ci-dessous.

<center>

<img src="exo1.png" style="width:40%;margin-bottom:0.5em;">
<figcaption>

Procédé Graphique Suite $u_{n+1}=\dfrac 25 u_n+3$<br/> et $u_0=1,5$

</figcaption>

</center>

2. Conjecturer le sens de variation de la suite $(u_n)$
3. Conjecturer la limite de la suite $(u_n)$

<exo>

Soit $(u_n)$ une suite arithmético-géométrique définie par la relation de récurrence $(R)$ suivante :
$$u_{n+1}=\dfrac 13 u_n+5 \quad (R)$$

1. Déterminer une suite $(a_n)$ constante qui vérifie la relation de récurrence $(R)$
2. En déduire toutes les suites $(u_n)$ qui vérifient la relation de récurrence $(R)$
4. A partir de cette question, on sait que $u_0=4$, en déduire une expression explicite de $u_n$ en fonction de $n$
3. En déduire la limite de $u_n$