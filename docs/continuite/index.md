# TMATHSCO : Continuité & Théorème des Valeurs Intermédiaires

Cette leçon définit formellement ce qu'est une fonction continue sur un intervalle $I$, et étudie quelques propriétés des fonctions continues, dont le très célèbre théorème des valeurs intérmédiaires, souvent abrégé en "TVI".

