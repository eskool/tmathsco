# TMATHSCO : Fonctions Continues & Théorème des Valeurs Intermédiaires (TVI)

## Notion de Continuité

### Définitions

!!! def
    Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ un nombre réel **de $I$**.

    * Dire que <red>$f$ est continue en $a$</red> signifie que $f$ admet une limite en $a$ égale à $f(a)$, i.e. que: 
    <center><enc>$\displaystyle \lim_{x\to a} f(x) = f(a)$</enc></center>
    * Dire que <red>$f$ est continue sur l'intervalle $I$</red> signifie que $f$ est continue en tout réel $a$ de $I$

<bd>Graphiquement</bd> La continuité d'une fonction $f$ sur un intervalle $I$ se traduit par le fait que l'on peut dessiner la courbe représentative de $f$ sur $I$ **sans lever le crayon**.

### Différents Cas de Figure

!!! exp "Discontinuité de type $1$"
    !!! col __60
        La fonction $f$ représentée ci-contre :

        * est discontinue en $a=1$, car :
            * <bd>Limite à gauche en $1$</bd> $\displaystyle \lim\limits_{\substack{x \to 1 \\ x<1}} f(x) = \lim_{x\to 1^-} f(x) = 1$
            * <bd>Limite à droite en $1$</bd> $\displaystyle \lim\limits_{\substack{x \to 1 \\ x>1}} f(x) = \lim_{x\to 1^+} f(x) = -1$
            * <bd>PAS de Limite en $1$</bd>  
            car Limite à gauche en $1$ $\ne$ Limite à droite en $1$  
            <bd>Conclusion</bd>  
            $f$ est discontinue en $a=1$, car il y a une coupure en $1$  
            Autrement dit: dans ce cas, la limite en $1$ n'existe même pas  
            En particulier, la fonction n'est PAS continue sur $I=[-2;3]$  
        * est continue sur $[-2;1[$ et aussi continue sur $]1;3]$
    !!! col __40
        <center>
        <encadre>Exemple de discontinuité de type $1$</encadre>
        <figure>
        <img src="img/discontinuite.png">
        <figcaption>
        fonction donnée graphiquement
        </figcaption>
        </figure>
        </center>

!!! exp "Discontinuité de type $2$"
    !!! col __60
        La fonction $f$ représentée ci-contre :

        * est discontinue en $a=1$, car :
            * <bd>Limite à gauche en $1$</bd> $\displaystyle \lim\limits_{\substack{x \to 1 \\ x<1}} f(x) = \lim_{x\to 1^-} f(x) = 1$
            * <bd>Limite à droite en $1$</bd> $\displaystyle \lim\limits_{\substack{x \to 1 \\ x>1}} f(x) = \lim_{x\to 1^+} f(x) = 1$
            * <bd>Il existe une Limite en $a=1$ qui vaut $\ell=1$</bd>$\quad$ 
            En effet, Limite à gauche $=$ Limite à droite en $a=1$. 
            Donc La limite en $1$ existe et vaut $\ell=1$ : $\displaystyle \lim_{x\to 1} f(x) = 1$
            * Par contre, par lecture graphique, $f(1)=2$, donc  
            <center><enc>$\displaystyle \lim_{x\to 1} f(x) \ne f(1)$</enc></center>  
            <bd>Conclusion</bd>  
            $f$ est discontinue en $1$, car la limite en $1$ existe, mais $\ne f(1)$
            En particulier, la fonction n'est PAS continue sur $I=[-2;3]$ 
        * est continue sur $[-2;1[$ et aussi continue sur $]1;3]$
    !!! col __40
        <center>
        <encadre>Exemple de discontinuité de type $2$</encadre>
        <figure>
        <img src="img/discontinuite1.png">
        <figcaption>
        fonction donnée graphiquement
        </figcaption>
        </figure>
        </center>

### Convention dans les tableaux de variation

Par convention, dorénavant, dans un tableau de variation, sur un intervalle $I$, une flèche :

* Une **flèche vers le haut** représente une fonction strictement croissante **et continue** sur $I$
* Une **flèche vers le bas** représente une fonction strictement décroissante **et continue** sur $I$

## Propriétés des Fonctions Continues

!!! thm "Dérivable $\Rightarrow$ Continue (Admis)"
    Soit $f$ une fonction dérivable sur un intervalle $I$, et $a$ un nombre réel **de $I$**.

    * Si $f$ est dérivable en $a$, Alors $f$ est continue en $a$
    * Si $f$ est dérivable sur $I$, Alors $f$ est continue sur $I$

!!! pte "Conséquence"
    Toutes les fonctions usuelles sont continues sur tout intervalle inclus dans leur ensemble de définition $D$.

!!! pte "Opérations sur les Fonctions Continues"
    Soient $f$ et $g$ deux fonctions définies sur un intervalle $I$, et $a\in I$.
    Si $f$ et $g$ sont continues en $a$, Alors:

    * $\lambda f$ est continue en $a$ (pour $\lambda \in \mathbb R$)
    * $f+g$ est continue en $a$
    * $f-g$ est continue en $a$
    * $f\times g$ est continue en $a$
    * $\dfrac fg$ est continue en $a$ lorsque $g(a)\ne0$
    $\dfrac fg$ est discontinue en $a$ lorsque $f(a)\ne0$ et $g(a)=0$
    * Si une fonction $g$ est continue en $a$ et $f$ est continue en $g(a)$,
    Alors la fonction $f\circ g$ est continue en $a$

## TVI pour les Fonctions Continues

Rappel Intuitif : Une fonction continue sur un intervalle $I$ est une fonction sans coupures, sans sauts sur l'intervalle $I$, **dont on peut tracer la courbe sur $I$ sans lever le crayon**.

### Introduction Graphique pour $k=0$

<clear></clear>

!!! col __28
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kegal0_f_croissante.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ strictement croissante<br/>sur $[a;b]$
    </figcaption>
    </figure>

    On a $f(a)<0$ et $f(b)>0$ donc :  
    <center><enc>$f(a)<0<f(b)$</enc></center>  
    Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que :  
    <center>$f(c)=0\,(=k)$</center>  
    On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
    **<red>une unique</red> valeur $c$ exactement**.
    </center>
!!! col __29
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kegal0_f_decroissante.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ strictement décroissante<br/>sur $[a;b]$
    </figcaption>
    </figure>

    On a $f(a)>0$ et $f(b)<0$ donc :  
    <center><enc>$f(b)<0<f(a)$</enc></center>
    Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$  
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que :  
    <center>$f(c)=0\,(=k)$</center>  
    On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
    **<red>une unique</red> valeur $c$ exactement**.
    </center>
!!! col __43 clear
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kegal0_f_quelconque.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ quelconque<br/>sur $I=[a;b]$
    </figcaption>
    </figure>

    On a $f(a)<0$ et $f(b)>0$ donc :  
    <center><enc>$f(a)<0<f(b)$</enc></center>  
    Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$  
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que:  
    <center>$f(c)=0\,(=k)$</center>  
    On voit bien qu'on NE PEUT PAS améliorer le résultat dans ce cas général:  
    Il existe <red>au moins</red> une valeur $c$ vérifiant l'égalité $f(c)=0$ si $f$ est quelconque.
    **MAIS, il <red>PEUT</red> exister plusieurs valeurs $c$** vérifiant l'égalité $f(c)=0$ si $f$ est une fonction quelconque.
    </center>

### Introduction Graphique pour $k$ réel quelconque

On se donne maintenant un réel $k$ quelconque mais fixé.

!!! col __29
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kquelconque_f_croissante.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ strictement croissante<br/>sur $[a;b]$
    </figcaption>
    </figure>

    On a $f(a)<k$ et $f(b)>k$ donc :  
    <center><enc>$f(a)<k<f(b)$</enc></center>  
    Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$  
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que $f(c)=k$  
    On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
    **<red>une unique</red> valeur $c$ exactement**.
    </center>
!!! col __29
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kquelconque_f_decroissante.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ strictement décroissante<br/>sur $[a;b]$
    </figcaption>
    </figure>

    On a $f(a)>k$ et $f(b)<k$ donc :  
    <center><enc>$f(b)<k<f(a)$</enc></center>  
    Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$  
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que $f(c)=k$  
    On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
    **<red>une unique</red> valeur $c$ exactement**.
    </center>
!!! col __42 clear
    <center>
    <figure style="width:95%;">
    <img src="img/tvi_kquelconque_f_quelconque.png" style="margin-bottom:1em;width:100%;">
    <figcaption style="white-space:nowrap;">
    $f$ quelconque<br/>sur $I=[a;b]$
    </figcaption>
    </figure>

    On a $f(a)<k$ et $f(b)>k$ donc :  
    <center><enc>$f(a)<k<f(b)$</enc></center>  
    Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$  
    donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe <red>au moins</red> un réel $c$ tel que $f(c)=k$  
    On voit bien qu'on NE PEUT PAS améliorer le résultat dans ce cas général:  
    Il existe <red>au moins</red> une valeur $c$ vérifiant l'égalité $f(c)=k$ si $f$ est quelconque.  
    **MAIS, il <red>PEUT</red> exister plusieurs valeurs $c$** vérifiant l'égalité $f(c)=k$ si $f$ est une fonction quelconque.
    </center>

### Le Théorème des Valeurs Intermédiaires (TVI) pour les fonctions continues

!!! thm "des Valeurs Intermédiaires (TVI) pour les Fonctions Continues"
    Soit $f$ une fonction **continue** sur un intervalle $I=[a;b]$ (borné), *avec* **$a<b$**.
    Pour tout nombre réel $k$ **compris entre $f(a)$ et $f(b)$**,
    Il existe **au moins** un réel $c$ compris entre $a$ et $b$ tel que $f(c)=k$

<bd>Interprétation Graphique</bd>  
En notant $\mathcal C_f$ la courbe représentative de $f$ dans le repère. Pour tout réel $k$ compris entre $f(a)$ et $f(b)$, la droite horizontale d'équation $y=k$, **coupe au moins une fois** la courbe $\mathcal C_f$ en un point d'abscisse $c$ comprise entre $a$ et $b$.

<bd>Interprétation en Termes d'Équation</bd>  
Pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l’équation <enc>$f(x)=k$</enc> $\,$ admet **au moins une solution** $c$ comprise entre $a$ et $b$.

### Généralisation à des intervalles $I$ ouverts et/ou non bornés

!!! def "Intervalles Bornés, ou Non Bornés"
    * Si $a$ et $b$ sont deux **réels finis**:
        * chacun des intervalles suivant est appelé <red>borné</red> :
            * $[a;b]$
            * $]a;b]$
            * $[a;b[$
            * $]a;b[$
        * l'intervalle $[a;b]$ est <red>fermé et borné</red>
        * l'intervalle $]a;b]$ est <red>semi-ouvert et borné</red>: ouvert à gauche, fermé à droite (et borné)
        * l'intervalle $[a;b[$ est <red>semi-ouvert et borné</red>: fermé à gauche, ouvert à droite (et borné)
    * Si $a$ et/ou $b$ sont **infinis**, 
        * chacun des intervalles suivants est appelé <red>non borné</red> :
            * $]-\infty;b]$ (pour $b$ réel)
            * $]-\infty;b[$ (pour $b$ réel) 
            * $[a;+\infty[$ (pour $a$ réel)
            * $]a;+\infty[$ (pour $a$ réel) 
            * $]-\infty;+\infty[$
        * $]-\infty;b[$ est <red>non borné à gauche, ouvert à droite</red>, 
        * $]-\infty;b]$ est <red>non borné à gauche, fermé à droite</red>,
        * $]a;+\infty[$ est <red>ouvert à gauche, non borné à droite</red>,
        * $[a;+\infty[$ est <red>fermé à gauche, non borné à droite</red>,
        * $]-\infty;+\infty[$ est <red>non borné ni à gauche ni à droite, et ouvert à gauche et à droite</red>

Ce **théorème se généralise bien pour des intervalles $I$ (semi-) ouverts et/ou non bornés**, en adaptant l'énoncé de la manière suivante :

* Si $a=-\infty$ ou bien si $I$ est ouvert en $a$, alors remplacer $f(a)$ par $\quad \displaystyle \lim_{x\to a} f(x)$
* Si $b=+\infty$ ou bien si $I$ est ouvert en $b$, alors remplacer $f(b)$ par $\quad \displaystyle \lim_{x\to b} f(x)$

!!! exp
    L'équation $e^x=0,2$ admet au moins une solution sur l'intervalle $I=]-2;-1[$

## TVI pour les Fonctions Continues ET Strictement monotones

!!! thm "des Valeurs Intermédiaires (TVI) pour les Fonctions Continues ET Strictement Monotones"
    Soit $f$ une fonction **continue** et **strictement monotone** sur un intervalle $I=[a;b]$ (borné), *avec* **$a<b$**.  
    Pour tout nombre réel $k$ **compris entre $f(a)$ et $f(b)$**,  
    Il existe **un unique** un réel $c$ compris entre $a$ et $b$ tel que $f(c)=k$

<bd>Extension à des Intervalles non bornés</bd>  
De même que pour le précédent, **Ce théorème s'étend à des intervalles (semi-) ouverts et/ou non bornés**, en adaptant $f(a)$ et $f(b)$ de la manière décrite ci-dessus.

!!! exp
    L'équation $e^x=2$ admet une unique solution sur l'intervalle $I=[0;+\infty[$

## Fonction Réciproque d'une fonction continue

!!! def "Fonctions Réciproques (Admis)"
    Soit $f$ une fonction **continue strictement monotone** sur un intervalle $I=[a;b]$
    Il existe une **unique** fonction définie sur $J=[f(a);f(b)]$ (ou $[f(b);f(a)]$) et à valeurs dans $I$, appelée **fonction réciproque de $f$**, et notée $f^{-1}$ telle que:

    $$
    \left\{
        \begin{array}{ll}
            f(x)=y\\
            x\in I
        \end{array}
    \right.
    \Leftrightarrow
    \left\{
        \begin{array}{ll}
            x=f^{-1}(y)\\
            y\in J
        \end{array}
    \right.
    $$

!!! pte
    Dans un repère orthonormé, les courbes représentatives de $f$ et $f^{-1}$ sont symétriques par rapport à la droite d'équation $y=x$ (1ère bissectrice)

!!! exp "Deux Cas de Figure"
    !!! col __48
        <center>
        <figure style="width:95%;">
        <img src="img/fonctionsreciproques_x2sqrt.png" style="margin-bottom:1em;width:100%;">
        <figcaption style="white-space:nowrap;">
        $x\mapsto x^2$ et $x\mapsto \sqrt x$<br/>sont réciproques l'une de l'autre
        </figcaption>
        </figure>

        La fonction $f$ définie sur $I=[0;+\infty[$ par $f(x)=x^2$ est continue (car dérivable) et strictement croissante sur $I$.  
        De plus, $f$ prend ses valeurs dans $J=[0;+\infty[$.  
        Sa fonction réciproque $f^{-1}$ de $J$ dans $I$ est la fonction racine carrée.  
        En effet:

        $
        \left\{
            \begin{array}{ll}
                x^2=y\\
                x\in I=[0;+\infty[
            \end{array}
        \right.
        \Leftrightarrow
        \left\{
            \begin{array}{ll}
                x=\sqrt y\\
                y\in J=[0;+\infty[
            \end{array}
        \right.
        $

        </center>
    !!! col __48
        <center>
        <figure style="width:95%;">
        <img src="img/fonctionsreciproques_expln.png" style="margin-bottom:1em;width:100%;">
        <figcaption style="white-space:nowrap;">
        $x\mapsto e^x$ et $x\mapsto ln(x)$<br/>sont réciproques l'une de l'autre
        </figcaption>
        </figure>

        La fonction $f$ définie sur $I=]-\infty;+\infty[$ par $f(x)=e^x$ est continue (car dérivable) et strictement croissante sur $I$.  
        De plus, $f$ prend ses valeurs dans $J=]0;+\infty[$.  
        Sa fonction réciproque $f^{-1}$ de $J$ dans $I$ est la fonction Logarithme Népérien (LN).  
        En effet:

        $
        \left\{
            \begin{array}{ll}
                e^x=y\\
                x\in I=]-\infty;+\infty[
            \end{array}
        \right.
        \Leftrightarrow
        \left\{
            \begin{array}{ll}
                x=ln(y)\\
                y\in J=]0;+\infty[
            \end{array}
        \right.
        $

        </center>

