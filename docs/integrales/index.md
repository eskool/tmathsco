# TMATHSCO : Intégrales

Cette leçon définit ce qu'est l'intégrale d'une fonction continue.
Grâce aux intégrales, on calcule l'aire sous la courbe d'une fonction entre deux points/valeurs $a$ et $b$.

Cette leçon dépend de la notion de Primitive d'une fonction.