# TMATHSCO : Intégration

## Intégrale d'une fonction continue positive :

!!! col __70
    Dans tout le chapitre, on munit le plan d'un repère .
    L'aire du rectangle OIKJ, est appelée <red>unité d'aire</red> ou « <red>u.a.</red> »  
    Certaines parties du plan admettent une aire que l'on peut mesurer dans cette unité d'aire.

!!! col __30
    ![](img/img1_unites_aires.png)

!!! exp
    Le triangle ABC a pour aire $\dfrac 12 \times 2 \, u.a. + \dfrac 12 \times 2 \, u.a.= 1 \, u.a. + 1 \, u.a.=2 \, u.a.$  
    En supposant que $OI=2\, cm$ en abscisses et $OJ = 1\, cm$ en ordonnées, donc $1 \, u.a.=1 \, cm\times 2\, cm=2\, cm^2$ . Ainsi, $Aire(ABC)=2\, u.a.=2\times 2 \, cm^2=4\, cm^2$.

!!! def
    On dit que $f$ est <red>positive</red> sur un intervalle $I$, et on note $f\ge 0$ sur $[a;b]$  
    $\ssi \forall x \in [a;b] \quad f(x) \ge 0$  
    $\ssi$ La courbe $C$ de $f$ reste au-dessus de l'axe des $x$ sur l'intervalle $[a;b]$.

!!! pte "et Définition"
        Soit $f$ une fonction **continue** et **positive** sur un intervalle $[a;b]$.  
        Soit $C$ la courbe représentative de $f$ dans un repère orthogonal du plan. Alors :  

    !!! col __70
        * On considère la partie $E$ suivante comprise entre :
            * la courbe $C$, 
            * l'axe des $x$, et 
            * la droite verticale d'équation $x=a$
            * la droite verticale d'équation $x=b$  
        * Cette partie $E$ **admet une Aire**, en unités d'aires (u.a.), qui est usuellement appelée : 
            * <red>Aire sous la courbe (représentative) de $f$</red>
            * <red>Intégrale de $a$ à $b$ de la fonction $f$</red>, ou quelquefois 
            * <red>somme de $a$ à $b$ de $f$</red>   
        On la note :  
        <center><enc>$Aire(E)=\displaystyle \int_a^b f(x)dx \quad u.a.$</enc></center>  

    !!! col __30 clear
        ![](img/img2_aire_sous_courbe.png)

<bd>Variable muette</bd>  
Dans cette notation , la variable $x$ de cette notation peut être remplacée par n'importe quelle autre variable (lettre), et ce sans changer ni la signification de la notation, ni (donc) la valeur de l'intégrale : On dit que la variable est muette. On a donc les égalités :  
<center><enc>$\displaystyle \int_a^b f(x)dx=\int_a^b f(t)dt=\int_a^b f(u)du=...$</enc></center>

!!! exp "de Calcul d'une Aire sous la courbe d'une fonction $f$, en $cm^2$"
    Si $\displaystyle \int_a^b f(x)dx=3$ et que $1 \, u.a. = 2\, cm^2$ (cf. exemple 1)  
    Alors $Aire(E) = 3\times 2cm^2=6\, cm^2$

!!! remarque 
    * L'intégrale d'une fonction $f$ **positive** <u>ET</u> **continue** sur $[a;b]$ est un **nombre positif**.
    * Si $a=b$, alors la partie $E$ est réduite à un segment vertical donc $\displaystyle \int_a^a f(x)dx=0$.  

!!! pte
    Certaines des propriétés des Aires peuvent se traduire instantanément en termes d'Intégrales :

    !!! col __70
        * Additivité des Aires :
        Pour tout $c\in [a;b]$ :  
        <center><enc>$\displaystyle \int_a^b f(x)dx=\int_a^c f(x)dx+\int_c^b f(x)dx$</enc></center>
    !!! col __30 clear
        ![](img/img3_additivite_aires.png)

    !!! col __70
        * Conservation par symétrie :  
        Si la courbe $C$ de $f$ est symétrique par rapport à l'axe $(Oy)$ (cas des fonctions paires)  
        Alors $\displaystyle \int_{-a}^0 f(x)dx=\int_0^a f(x)dx$  
        donc  
        <center><enc>$\displaystyle \int_{-a}^a f(x)dx=2\int_0^a f(x)dx$</enc></center>
    !!! col __30 clear
        ![](img/img4_conservation_par_symetrie.png)

    !!! col __70
        * Conservation par translation :  
        Par exemple :  
        La courbe de la fonction $\sin$ est périodique de période $T=2\pi$, donc invariante par translation de vecteur $2\pi \overrightarrow{OI}$, donc  
        <center><enc>$\displaystyle \int_{0}^{\pi} \sin(x)dx=\int_{2\pi}^{3\pi} \sin(x)dx$</enc></center>

    !!! col __30 clear
        ![](img/img5_conservation_par_translation.png)

## Primitive d'une fonction continue sur un intervalle. 

### Définition d'une Primitive

!!! thm
    Soit $f$ une fonction **continue** et **positive** sur $[a;b]$.
    La fonction $F$ définie sur $[a;b$ par $F({\color{red} x})=\displaystyle \int_{a}^{\color{red} x} f(t)dt$ est :

    * dérivable sur $[a;b]$
    * $F'(x) = \left( \displaystyle \int_{a}^{x} f(t)dt \right)' = f(x)$
    * $F(a)  = \displaystyle \int_{a}^{a} f(t)dt = 0$

!!! def
    Une fonction $F$ dérivable sur un intervalle $I$ tq $F'=f$ sur $I$, est appelée <red>UNE primitive</red> de $f$ sur $I$.

!!! remarque
    En d'autres termes, le théorème précédent affirme que :
    Si $f$ est une fonction **continue** et **positive** sur $[a;b]$,
    alors la fonction $F$ définie par $F(x) = \displaystyle \int_a^x f(t)dt$ est une primitive de $f$ sur $[a;b]$ (qui s'annule en $a$).

!!! exp
    Soit $f$ la fonction définie par $f(x)=2x+3$ sur $\m R$.  
    Alors :  

    1. La fonction $F$ définie par $F(x) = x^2+3x$ est clairement une primitive de $f$ sur $\m R$, car :
        * $F$ est dérivable sur $\m R$
        * $F'(x)=2x+3=f(x)$
    1. La fonction $F_1(x)=x^2+3x+1$ est une (autre) primitive de $f$ sur $\m R$ (pour les mêmes raisons) 
    1. Toutes les fonctions $F_k(x)=x^2+3x+k$ définies par   sont (encore) des primitives de $f$ (pour les mêmes raisons).

!!! pte
    * Toute fonction $f$ **continue** sur un intervalle $I$ admet des primitives $F$ sur $I$.
    * Si $F$ est **l'une** des primitives de $f$ sur $I$, Alors **les autres primitives** de $f$ sur $I$ sont les fonctions $x \mapsto F(x)+k$ où $k$ est une constante réelle.
    * Étant données deux valeurs réelles $x_0$ et $y_0$ quelconques :  
    Il existe une unique primitive $F$ de $f$ sur l'intervalle $I$ qui vérifie l'égalité : <enc>$F(x_0)=y_0$</enc>

!!! exp
    1. Les primitives de $f: x \mapsto 2x+3$ sur $\m R$ sont les fonctions $F: x \mapsto x^2+3x+k$ avec $k$ réel.
    1. Parmi elles, il existe une unique primitive $F$ de $f$ telle que $F(1)=2$ :  
    En effet $F(1)=1^2+3\times 1+k=2 \ssi 4+k=2 \ssi k=2-4=-2$  
    Conclusion : L'unique primitive $F$ de $f : x \mapsto 2x+3$ sur $\m R$ telle que $F(1)=2$ est la fonction $F$ définie par $F(x)=x^2+3x-2$.
    1. Si $f$ est continue et positive, alors la fonction $F$ définie par $F(x)= \displaystyle \int_a^x f(t)dt$ est donc l'unique primitive de $f$ **qui s'annule en $a$**.

### Techniques de Calculs de primitives

Par lecture inverse du tableau des dérivées.
Le tableau suivant récapitule UNE primitive $F$ possible pour chacune des fonctions $f$ suivantes :

| $f(x)=$ | $F(x)=$ | $f$ définie sur ... |
|:-:|:-:|:-:|
|$a$<br/>(constante) | $ax {\color{red} +k}$ | $\m R$ |
|$x$ | $\dfrac {x^2}{2} {\color{red} +k}$| $\m R$ |
|$\dfrac 1x$ | $\ln (x) {\color{red} +k}$| $]0; +\infty[$ |
|^ | $\ln (-x) {\color{red} +k}$| $]-\infty;0[$ |
|$\dfrac 1{x^2}$ | $-\dfrac 1x {\color{red} +k}$| $]-\infty; 0[$ ou $]0; +\infty[$ |
|$x^n$<br/>($n\in \m Z, n\neq -1$) | $\dfrac {x^{n+1}}{n+1} {\color{red} +k}$| <ul><li>$\m R$</li><li>$]-\infty; 0[$ ou $]0; +\infty[$ lorsque $n$ est entier, et $n\leq -2$</li></ul> |
|$\dfrac {1}{\sqrt x}$ | $2\sqrt x {\color{red} +k}$| $]0;+\infty[$ |
|$e^x$ | $e^x {\color{red} +k}$| $\m R$ |
|$e^{kx}$ | $\dfrac {e^{kx}}{k} {\color{red} +k}$| $\m R$ |

!!! remarque
    * Ne jamais oublier qu'une primitive ***générale*** de $f$ est $F+k$. De plus :
    * Une primitive de $\dfrac {1}{2\sqrt x}$ est $\sqrt x$ (énoncé équivalent)
    * Certaines fonctions, comme par exemple $x \mapsto e^{-x^2}$, sont continues donc on sait qu'elles admettent bien des primitives, mais on ne connaît pas de formule « explicite » pour leurs primitives. On ne sait donc pas calculer de façon exacte aucune de leurs intégrales.

!!! ex
    Déterminer les primitives des fonctions suivantes :
    !!! col __25
        :one: $5x^2+2x+\dfrac 13$
    !!! col __25
        :two: $-\dfrac {3}{x^2}$
    !!! col __25
        :three: $7\sqrt x$
    !!! col __25 clear
        :four: $5\sin(3x)$
    !!! col __25
        :five: $e^{-2x}$
    !!! col __25
        :six: $xe^{-x^2}$
    !!! col __25
        :seven: $\dfrac {1}{x+3}$
    !!! col __25 clear
        :eight: $\dfrac {3}{5x-2}$

### Opérations sur les primitives

| $f=$ | $F=$ | $f$ et $F$ définies sur ... |
|:-:|:-:|:-:|
| $u'+v'$ | $u+v$ | Si $u$ et $v$ sont des primitives de $u'$ et $v'$ sur $I$,<br/>Alors $u+v$ est une primitive de $u'+v'$ sur $I$ |
| $u'-v'$ | $u-v$ | Si $u$ et $v$ sont des primitives de $u'$ et $v'$ sur $I$,<br/>Alors $u-v$ est une primitive de $u'-v'$ sur $I$ |
| $ku'$ | $ku$ | Si $u$ est une primitive de $u'$ sur $I$, et $k=C^{te}$,<br/>Alors $k\times u$ est une primitive de $ku'$ sur $I$ |
| $\dfrac {u'}{u^2}$ | $-\dfrac {1}{u}$ | Si $u$ est une primitive de $u'$ sur $I$,<br/>Alors $-\dfrac 1u$ est une primitive de $\dfrac {u'}{u^2}$ sur $I$ privé des $x\in I$ tq $u(x)=0$|
| $u'\times u^n$<br/>($n\in \m Z, n \neq -1$) | $\dfrac {u^{n+1}}{n+1}$ | Si $u$ est une primitive de $u'$ sur $I$,<br/>Alors la fonction $\dfrac {u^{n+1}}{n+1}$ est une primitive de $u' \times u^n$ sur <br/><ul><li>$I$ lorsque $n \ge 0$</li><li>$I$ privé des $x\in I$ tq $u(x)=0$ lorsque $n\leq -2$</li></ul>|
| $u'e^u$ | $e^u$ | Si $u$ est une primitive de $u'$ sur $I$,<br/>Alors $e^u$ est une primitive de $u'e^u$ sur $I$ |

!!! remarque
    * Ne **jamais** oublier qu'une primitive ***générale*** de $f$ est $F+k$. De plus :
    * Une primitive de $\dfrac {u'}{u^2}$ est $-\dfrac 1u$ (énoncé équivalent)
    * Une primitive de $u'\times u$ est $\dfrac 12 u^2$ (cas particulier de $u'\times u^n$ pour $n=1$)


## Intégrale d'une fonction de signe quelconque :

### Lien entre Intégrale d'une fonction continue positive et Primitives

!!! pte
    Soit $f$ **continue** et **positive** sur un intervalle $[a;b]$ (donc $a\leq b$) :  
    $\displaystyle \int_a^b f(x)dx=F(b)-F(a)$ $\quad$ Quelle que soit la primitive de $F$ de $f$.

Cela suppose que ce nombre soit indépendant de la primitive $F$ choisie...

### Intégrale d'une fonction de signe quelconque :

!!! def
    Pour toute fonction $f$ **continue** sur un intervalle $I$. Pour tous $a$, $b$ de $I$:
    L'intégrale de $a$ à $b$ de $f$ est définie par :
    $\displaystyle \int_a^b f(x)dx = F(b)-F(a) \quad \symbover{=}{noté} \quad  [F]_a^b$ $\quad$ Quelle que soit la primitive $F$ de $f$

    Ce nombre est donc indépendant de la primitive $F$ choisie.

!!! remarque
    * Lorsque $f$ est continue **et positive sur $[a;b]$, avec $a\leq b$**, cette définition est la même que la définition initialement adoptée en termes d'aire.
    * Cette définition est aussi indépendante de la primitive $F$ choisie.
    * La fonction $H$ définie par $H(x)= \displaystyle \int_a^x f(t)dt=F(x)-F(a)$ est l'unique primitive de $f$ qui s'annule en $a$. (Quelle que soit la primitive $F$ de $f$).

!!! exp "(de calculs d'intégrale)"
    1. $I=\displaystyle \int_0^1 (x^2+5x)dx=\left[ \dfrac {x^3}{3}+5\dfrac {x^2}{2} \right]_0^1=\left[ \left( \dfrac {1^3}{3} +5\dfrac {1^2}{2}\right) -\left( \dfrac {0^3}{3} +5\dfrac {0^2}{2}\right) \right] = \dfrac 13+ \dfrac 52 = \dfrac {17}{6}$
    1. $I=\displaystyle \int_1^3 e^xdx=\left[ e^x\right]_1^3=[e^3-e^1]=e^3-e$
    1. $I=\displaystyle \int_1^2 \dfrac {dx}{x} = \int_1^2 \dfrac 1x dx = \left[ \ln x\right]_1^2=[\ln 2- \ln 1] = \ln 2$

!!! pte
    Soit $f$ et $g$ deux fonctions **continues** sur un intervalle $I$, et $a$, $b$, $c$ trois réels de $I$.

    * $\displaystyle \int_b^a f(x)dx = - \int_a^b f(x)dx$
    * **Relation de Chasles** : $\displaystyle \int_a^b f(x)dx = \int_a^c f(x)dx + \int_c^b f(x)dx$
    * **Linéarité** : 
        * $\displaystyle \int_a^b (f(x)+g(x))dx = \int_a^b f(x)dx + \int_a^b g(x)dx$
        * Pour tout réel $\lambda$ : $\displaystyle \int_a^b \lambda f(x)dx = \lambda \int_a^b f(x)dx$

En particulier : $\lambda$ : $\displaystyle \int_a^b (f(x)-g(x))dx = \int_a^b f(x)dx - \int_a^b g(x)dx$ (cas où $\lambda=-1$)

!!! pte "Positivité de l'intégrale"
    Soit $f$ et $g$ deux fonctions continues sur un intervalle $I$.  
    Soit $a,b$ deux réels de l'intervalle $I$ tels que <enc>$a\leq b$</enc>  

    * Si $f \geq 0$ sur $I$, alors $\displaystyle \int_a^b f(x)dx \geq 0$ 
    * Si $f \leq 0$ sur $I$, alors $\displaystyle \int_a^b f(x)dx \leq 0$ 
    * Si $f \leq g$ sur $I$, alors $\displaystyle \int_a^b f(x)dx \leq \int_a^b g(x)dx$  

## Techniques de Calculs d'Intégrales :

### Pour calculer une intégrale de $f$, il suffit de connaître une primitive $F$

!!! pte "Rappel"
    Pour toute fonction continue $f$ sur $I$, pour tous $a,b$ de $I$.  
    $\displaystyle \int_a^b f(x)dx = \left[ F\right]_a^b = \left[F(b)-F(a) \right]$ $\quad$ Quelle que soit la primitive $F$ de $f$ sur $I$.

!!! ex
    Calculer les intégrales suivantes :

    1. $\displaystyle \int_0^1 \dfrac 5xdx$
    1. $\displaystyle \int_{-1}^1 xe^{-x^2}dx$
    1. $\displaystyle \int_1^2 \ln x dx$	(On pourra démontrer que $F(x)=x\ln x -x$ est une primitive de $f(x)=\ln x$) 

## Calculs d'Aire à l'aide d'une Intégrale

!!! pte
    Soit $f$ une fonction continue sur un intervalle $I$.  
    Soit $a,b$ deux réels de $I$ tels que $a\leq b$.  
    Soit $A$ l'**aire sous la courbe**, de la partie $D$ comprise entre la courbe, l'axe des $x$, $x=a$ et $x=b$.  
    Alors :  

    | Si $f\geq 0$ et $a\leq b$ | Si $f\geq 0$ et $a\geq b$ |
    |:-:|:-:|
    | ![](img/img6.png)<br/>$A=+\displaystyle \int_a^b f(x)dx\quad u.a.$ | ![](img/img7.png)<br/>$A=-\displaystyle \int_a^b f(x)dx\quad u.a.$ |
    | Si $f\leq 0$ et $a\leq b$ | Si $f\leq 0$ et $a\geq b$ |
    | ![](img/img8.png)<br/>$A=-\displaystyle \int_a^b f(x)dx\quad u.a.$ | ![](img/img9.png)<br/>$A=+\displaystyle \int_a^b f(x)dx\quad u.a.$ |

<bd>Moyen Mnémotechnique</bd>  
On veut établir un lien entre l'aire  et l'intégrale de  à , donc il faut **suivre la flèche de $a$ vers $b$** puis regarder le sens où l'on tourne pour faire le tour de la courbe :

* Si c'est le sens trigo (sens contraire des aiguilles d'une montre)  : C'est <enc>$+$</enc>
* Si c'est le sens anti-trigo (sens horaire) : C'est <enc>$-$</enc>
* Si $f$ change de signe sur $I$

    ![](img/img10.png)

    1. Aire Totale en fonction des intégrales :  
    $A=A_1+A_2+A_3=\displaystyle \int_a^c f(x)dx-\int_c^d f(x)dx+\int_d^b f(x)dx$

    2. Intégrale Totale en fonction des Aires :
    L'intégrale est la somme des **aires algébriques** :
    $\displaystyle \int_a^b f(x)dx=A_1-A_2+A_3$

!!! pte "(Admis, :cry:) Aire entre deux courbes"
    !!! col __60
        Soit $f$ et $g$ deux fonctions continues sur un intervalle $I$.  
        Soit $a,b$ deux réels de $I$ tels que $a\leq b$.  
        Si $f\leq g$ sur $I$,  
        Alors l'aire $A$ comprise entre les courbes de $f$ et de $g$,
        et les droites verticales $x=a$ et $x=b$ vaut  
        <center><enc>$A=\displaystyle \int_a^b (g(x)-f(x))dx \quad u.a.$</enc></center>
    !!! col __40
            ![](img/img11.png)

## Valeur Moyenne d'une fonction $f$

!!! def
    !!! col __60
        Soit $f$ une fonction continue sur $[a;b]$, avec $a\neq b$,  
        On appelle <red>valeur moyenne</red> de $f$ sur $[a;b]$, le réel :  
        <center><enc>$\mu=\displaystyle \frac {1}{b-a}\int_a^b f(x)dx \quad u.a.$</enc></center>
    !!! col __40
            ![](img/img12.png)
 
On a : $\displaystyle (b-a) \mu = \int_a^b f(x)dx$
Si $f$ est continue et positive sur $[a;b]$,  
Alors on peut comprendre cette égalité en termes d'aires :  
Aire « sous la courbe de $f$»  entre $a$ et $b$ = Aire « sous la courbe de la fonction constante $\mu$ » entre $a$ et $b$. Autrement dit :  
Aire sous la courbe de $f$ entre $a$ et $b$, égale à l'Aire du Rectangle $(ABCD)$ (de hauteur $\mu$).


