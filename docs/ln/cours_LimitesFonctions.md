<titre>TMaths Co: Limites de Fonctions</titre>


# Limite d'une fonction en l'infini

## Limite $+\infty$ en l'infini ( $\pm \infty$ )

!!! exp "Introduction graphique pour Limite $+\infty$ en $+\infty$"
<exp data="Introduction graphique pour Limite +infini en +infini">

<div style="width:40%; float:right;margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $+\infty$ en $+\infty$</encadre>

<figure>

<img src="img/x2.png">

<figcaption>

fonction $x\mapsto x^2$

</figcaption>

</figure>

<figure>

<img src="img/x3.png">

<figcaption>

fonction $x\mapsto x^3$

</figcaption>

</figure>

</center>

</div>

:one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=x^2$

Il semble que les images $f(x)$ soient aussi ***grandes*** (comprendre proches de $+\infty$) que l'on veut, lorque $x$ est *suffisamment **grand*** (comprendre proche de $+\infty$):

* $f(x)>10^6$ pour $x>1000$
* $f(x)>10^{10}$ pour $x>10^5$

**Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **monte haut** (aussi *haut* que l'on veut)...

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} x^2=+\infty$</enc>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=x^3$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} x^3=+\infty$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $+\infty$, quand $x$ tend vers $+\infty$**, et on note:

* $\displaystyle \lim_{x\to +\infty} f(x) = +\infty$
* ou encore que: $f(x) \rightarrow +\infty$ quand $x \rightarrow +\infty$

</def>


<exp data="Introduction graphique pour Limite +infini en -infini">

<div style="width:40%; float:right;margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $+\infty$ en $-\infty$</encadre>

<figure>

<img src="img/x2.png">

<figcaption>

fonction $x\mapsto x^2$

</figcaption>

</figure>

<figure>

<img src="img/moinsx3.png">

<figcaption>

fonction $x\mapsto -x^3$

</figcaption>

</figure>

</center>

</div>

:one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=x^2$

Il semble que les images $f(x)$ soient aussi ***grandes*** (comprendre proches de $+\infty$) que l'on veut, lorque $x$ est *suffisamment **petit*** (comprendre proche de $-\infty$):

* $f(x)>10^6$ pour $x< -1000$
* $f(x)>10^{12}$ pour $x< -10^6$

**Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **monte haut** (aussi *haut* que l'on veut)...

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} x^2=+\infty$</enc>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=-x^3$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -x^3=+\infty$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $+\infty$, quand $x$ tend vers $-\infty$**, et on note:

* $\displaystyle \lim_{x\to -\infty} f(x) = +\infty$
* ou encore que: $f(x) \rightarrow +\infty$ quand $x \rightarrow -\infty$

</def>


## Limite $-\infty$ en l'infini ( $\pm \infty$ )

<exp data="Introduction graphique pour Limite -infini en +infini">

<div style="width:40%; float:right;margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $-\infty$ en $+\infty$</encadre>

<figure>

<img src="img/moinsx2.png">

<figcaption>

fonction $x\mapsto -x^2$

</figcaption>

</figure>

<figure>

<img src="img/moinsx3.png">

<figcaption>

fonction $x\mapsto -x^3$

</figcaption>

</figure>

</center>

</div>

:one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=-x^2$

Il semble que les images $f(x)$ soient aussi ***petites*** (comprendre proches de $-\infty$) que l'on veut, lorque $x$ est *suffisamment **grand*** (comprendre proche de $+\infty$):

* $f(x)< -10^6$ pour $x>1000$
* $f(x)< -10^{12}$ pour $x>10^6$

**Language Humain :** plus on va vers la **droite**, et plus la *"hauteur"* **descend bas** (aussi *bas* que l'on veut)...

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -x^2=-\infty$</enc>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=-x^3$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -x^3=-\infty$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $+\infty$**, et on note:

* $\displaystyle \lim_{x\to +\infty} f(x) = -\infty$
* ou encore que: $f(x) \rightarrow -\infty$ quand $x \rightarrow +\infty$

</def>


<exp data="Introduction graphique pour Limite -infini en -infini">

<div style="width:40%; float:right;margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $-\infty$ en $-\infty$</encadre>

<figure>

<img src="img/moinsx2.png">

<figcaption>

fonction $x\mapsto -x^2$

</figcaption>

</figure>

<figure>

<img src="img/x3.png">

<figcaption>

fonction $x\mapsto x^3$

</figcaption>

</figure>

</center>

</div>

:one: Soit $f$ la fonction définie sur $\mathbb R$ par $f(x)=-x^2$

Il semble que les images $f(x)$ soient aussi ***petites*** (comprendre proches de $-\infty$) que l'on veut, lorque $x$ est *suffisamment ***petit*** (comprendre proche de $-\infty$):

* $f(x)< -10^6$ pour $x< -1000$
* $f(x)< -10^{12}$ pour $x< -10^6$

**Language Humain :** plus on va vers la **gauche**, et plus la *"hauteur"* **descend bas** (aussi *bas* que l'on veut)...

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -x^2=-\infty$</enc>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R$ par $f(x)=x^3$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} x^3=-\infty$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $-\infty$**, et on note:

* $\displaystyle \lim_{x\to -\infty} f(x) = -\infty$
* ou encore que: $f(x) \rightarrow -\infty$ quand $x \rightarrow -\infty$

</def>


<pte data="Fonctions de Référence de Limite infinie en l'infini">

<center>

<div style="font-size:0.9em;">

|Fonctions de Référence de Limite Infinie en l'infini||||||
|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x\to +\infty} ln(x)=+\infty$|$\displaystyle \lim_{x\to +\infty} \sqrt x=+\infty$|$\displaystyle \lim_{x\to +\infty} x^2=+\infty$|$\displaystyle \lim_{x\to +\infty} x^3=+\infty$|$\displaystyle \lim_{x\to +\infty} x^n=+\infty$<br>pour $n$ entier $>0$|$\displaystyle \lim_{x\to +\infty} e^x=+\infty$|
||$\,$|$\displaystyle \lim_{x\to -\infty} x^2=+\infty$|$\displaystyle \lim_{x\to -\infty} x^3=+\infty$|$\displaystyle \lim_{x\to -\infty} x^n=+\infty$<br>pour $n$ entier <b>pair</b> $>0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{e^x}=+\infty$|
|^|^|^|^|$\displaystyle \lim_{x\to -\infty} x^n=-\infty$<br>pour $n$ entier <b>impair</b> $>0$|^|

</div>

</center>

</pte>

## Limite $0$ en l'infini ( $\pm \infty$ )

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $0$ en $+\infty$</encadre>

<figure>

<img src="img/1surx_enplusinfini.png">

<figcaption>

fonction $x\mapsto \dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite 0 en +infini">

:one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=\dfrac 1x$

Il semble que les images $f(x)$ soient aussi ***"petites", au sens proches de $0$*** que l'on veut, lorque $x$ est *suffisamment **grand*** (comprendre proche de $+\infty$):

* $0<f(x)<10^{-3}$ pour $x>1000$
* $0<f(x)<10^{-5}$ pour $x>10^5$

**Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **est proche de $0$** (aussi *proche* que l'on veut)...
$\Leftrightarrow$ plus on va vers la **droite**, et plus ***la (hauteur de la) courbe s'approche de l'axe des $x$*** (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} \dfrac 1x = 0$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/moins1surx_enplusinfini.png">

<figcaption>

fonction $x\mapsto -\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=-\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} -\dfrac 1x = 0$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/sinxsurx_enplusinfini.png">

<figcaption>

fonction $x\mapsto \dfrac {sin(100x)}{x}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= \dfrac {sin(100x)}{x}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} \dfrac {sin(100x)}{x} = 0$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $0$, quand $x$ tend vers $+\infty$,** et on note:

* $\displaystyle \lim_{x\to +\infty} f(x) = 0$
* ou encore que: $f(x) \rightarrow 0$ quand $x \rightarrow +\infty$

</def>


<!-- Exemple suivant -->

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $0$ en $-\infty$</encadre>

<figure>

<img src="img/moins1surx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto -\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite 0 en -infini">

:one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=-\dfrac 1x$

Il semble que les images $f(x)$ soient aussi ***"petites", au sens proches de $0$*** que l'on veut, lorque $x$ est *suffisamment **petit*** (comprendre proche de $-\infty$):

* $0<f(x)<10^{-3}$ pour $x< -1000$
* $0<f(x)<10^{-5}$ pour $x< -10^5$

**Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **est proche de $0$** (aussi *proche* que l'on veut)...
$\Leftrightarrow$ plus on va vers la **gauche**, et plus ***la (hauteur de la) courbe s'approche de l'axe des $x$*** (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} -\dfrac 1x = 0$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/1surx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto \dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} \dfrac 1x = 0$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/sinxsurx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto \dfrac {sin(100x)}{x}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= \dfrac {sin(100x)}{x}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} \dfrac {sin(100x)}{x} = 0$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que la fonction $f(x)$ a pour limite $0$, quand $x$ tend vers $-\infty$, et on note:

* $\displaystyle \lim_{x\to -\infty} f(x) = 0$
* ou encore que: $f(x) \rightarrow 0$ quand $x \rightarrow -\infty$

</def>


<pte data="Fonctions de Référence de Limite 0 en l'infini">

<center>

<div style="font-size:0.9em;">

|Fonctions de Référence de Limite $0$ en l'infini|||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{ln(x)}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{\sqrt x}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac 1x=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^2}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^3}=0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{x^n}=0$<br/>pour $n$ entier $>0$|$\displaystyle \lim_{x\to +\infty} \dfrac {1}{e^x}=0$|
||$\,$|$\displaystyle \lim_{x\to -\infty} \dfrac 1x=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^2}=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^3}=0$|$\displaystyle \lim_{x\to -\infty} \dfrac {1}{x^n}=0$<br/>pour $n$ entier $>0$|$\displaystyle \lim_{x\to -\infty} {e^x}=0$|

</div>

</center>

</pte>


## Limite $\ell$ en l'infini ( $\pm\infty$ )

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $\ell$ en $+\infty$</encadre>

<figure>

<img src="img/2plus1surx_enplusinfini.png">

<figcaption>

fonction $x\mapsto 2+\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite L en +infini">

:one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+\dfrac 1x$

Il semble que les images $f(x)$ soient ***aussi proches de $2$*** que l'on veut, lorque $x$ est *suffisamment **grand*** (comprendre proche de $+\infty$):

* $2<f(x)<2+10^{-3}$ pour $x>1000$
* $2<f(x)<2+10^{-5}$ pour $x>10^5$

**Language Humain :** plus on va vers la **droite**, et plus la *hauteur* **est proche de $2$** (aussi *proche* que l'on veut)...
$\Leftrightarrow$ plus on va vers la **droite**, et plus ***la (hauteur de la) courbe s'approche de la droite horizontale d'équation $y=2$*** (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2+\dfrac 1x = 2$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/2moins1surx_enplusinfini.png">

<figcaption>

fonction $x\mapsto 2-\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2-\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2-\dfrac 1x = 2$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/2plussinxsurx_enplusinfini.png">

<figcaption>

fonction $x\mapsto 2+\dfrac {sin(100x)}{x}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+ \dfrac {sin(100x)}{x}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to+\infty} 2+\dfrac {sin(100x)}{x} = 2$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $\ell$, quand $x$ tend vers $+\infty$**, et on note:

* $\displaystyle \lim_{x\to +\infty} f(x) = \ell$
* ou encore que: $f(x) \rightarrow \ell$ quand $x \rightarrow +\infty$

</def>


<def>

On dit aussi dans ce cas que la droite $d$ d'équation $y=\ell$ est une **asymptote horizontale en $+\infty$** à la courbe $\mathcal C_f$ de $f$.

</def>

<envaleur>Un exemple</envaleur>

La droite $d$ d'équation $y=2$ est une asymptote horizontale en $+\infty$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=2+\dfrac 1x$

<!-- Exemple suivant -->

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $\ell$ en $-\infty$</encadre>

<figure>

<img src="img/2moins1surx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto 2-\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite L en -infini">

:one: Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2-\dfrac 1x$

Il semble que les images $f(x)$ soient **aussi proches de $2$** que l'on veut, lorque $x$ est *suffisamment **petit*** (comprendre proche de $-\infty$):

* $2<f(x)<2+10^{-3}$ pour $x< -1000$
* $2<f(x)<2+10^{-5}$ pour $x< -10^5$

**Language Humain :** plus on va vers la **gauche**, et plus la *hauteur* **est proche de $2$** (aussi *proche* que l'on veut)...
$\Leftrightarrow$ plus on va vers la **gauche**, et plus ***la (hauteur de la) courbe s'approche de la droite horizontale d'équation $y=2$*** (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2-\dfrac 1x = 2$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/2plus1surx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto 2+\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)=2+\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2+\dfrac 1x = 2$</enc>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/2plussinxsurx_enmoinsinfini.png">

<figcaption>

fonction $x\mapsto 2+\dfrac {sin(100x)}{x}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $f$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $f(x)= 2+\dfrac {sin(100x)}{x}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to-\infty} 2+\dfrac {sin(100x)}{x} = 2$</enc>

</exp>

<clear></clear>

<def>

On dit dans ce cas que **la fonction $f(x)$ a pour limite $\ell$, quand $x$ tend vers $-\infty$**, et on note:

* $\displaystyle \lim_{x\to -\infty} f(x) = \ell$
* ou encore que: $f(x) \rightarrow \ell$ quand $x \rightarrow -\infty$

</def>

<def>

On dit aussi dans ce cas que la droite $d$ d'équation $y=\ell$ est une **asymptote horizontale en $-\infty$** à la courbe $\mathcal C_f$ de $f$.

</def>

<envaleur>Un exemple</envaleur>

La droite $d$ d'équation $y=2$ est une asymptote horizontale en $-\infty$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=2+\dfrac 1x$


## Lien entre Limite $\ell$ et limite $0$ en l'infini

L'étude des Limites $\ell$ se ramène en quelque sorte à l'étude des limites $0$, grâce à la propriété suivante:

<pte data="Lien entre Limite L et Limite 0 en l'infini">

<center>

|Lien entre Limite $\ell$ et Limite $0$|
|:-:|
|$\displaystyle \lim_{x\to +\infty} f(x)=\ell \Leftrightarrow \displaystyle \lim_{x\to +\infty} (f(x)-\ell)=0$|
|$\displaystyle \lim_{x\to -\infty} f(x)=\ell \Leftrightarrow \displaystyle \lim_{x\to -\infty} (f(x)-\ell)=0$|

</center>

</pte>

<mth data="Montrer qu'une limite vaut L en l'infini">

* Pour démontrer que la limite de $f(x)$ vaut $\ell $ en l'infini ($\pm \infty$), il suffit de démontrer que la limite de $f(x)-\ell$ vaut $0$ en l'infini ($\pm \infty$)
* Pour cela on dispose de fonctions de références de limite $0$ en l'infini ($\pm \infty$)

</mth>



<exp>

Soit $f$ la fonction définie sur $\mathbb R^*=]-\infty;0[\cup ]0;+\infty[$, par $f(x)=3-\dfrac {1}{x^2}$

On a $f(x)-3=-\dfrac {1}{x^2}$ donc : 

* Limite en $+\infty$:
  On sait que la fonction $x\mapsto \dfrac {1}{x^2}$ est une fonction de référence pour les limites $0$ en $+\infty$.
  donc $\displaystyle \lim_{x\to +\infty} (f(x)-3)= \lim_{x\to +\infty} \left( -\dfrac {1}{x^2} \right) = -0 = 0$
  Ceci prouve que $\displaystyle \lim_{x\to +\infty} f(x)=3$
* Limite en $-\infty$:
  On sait que la fonction $x\mapsto \dfrac {1}{x^2}$ est une fonction de référence pour les limites $0$ en $-\infty$.
  donc $\displaystyle \lim_{x\to -\infty} (f(x)-3)= \lim_{x\to -\infty} \left( -\dfrac {1}{x^2} \right) = -0 = 0$
  Ceci prouve que $\displaystyle \lim_{x\to -\infty} f(x)=3$

</exp>

# Limite d'une fonction en un réel $a$ fini

## Limite infinie en $0$

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $+\infty$ en $0$</encadre>

<figure>

<img src="img/1surx_en0plus.png">

<figcaption>

fonction $x\mapsto \dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite +infini en 0">

:one: Soit $f$ la fonction définie sur $D=]0;+\infty[$ par $f(x)=\dfrac 1x$

Il semble que les images $f(x)$ soient ***aussi grandes*** que l'on veut (comprendre proches de $+\infty$), lorque $x$ est *suffisamment **"petit", au sens de proche de $0$ (tout en restant $>0$, à cause de l'ensemble de définition $D$ )*** :

* $f(x)>10^{3}$ pour $0<x<0,001$
* $f(x)>10^{5}$ pour $0<x<10^{-5}$

**Language Humain :** plus on **s'approche de $0$, tout en restant $>0$**, et plus la *hauteur* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut)
$\Leftrightarrow$ plus on **s'approche de zéro en venant de la droite**, et plus ***la (hauteur de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to0} \dfrac 1x = +\infty$</enc>

<val>Remarques</val>

* La notation précédente est équivalente à <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} \dfrac 1x=+\infty$</enc> $\,\,$ qui est sous-entendue, plus précise, mais plus lourde.
* De manière plus générale, on supposera toujours quand on détermine la limite de $f(x)$ quand $x$ tend vers un réel comme $0$ que:
 **$x$ tend vers $0$ *tout en restant dans l'ensemble de définition $D$ de $f$***. 
Cela suppose néanmoins que ce soit possible... Remarquons que c'est en particulier possible lorsque $D$ est un intervalle ou une réunion d'intervalles, et que :
  * ou bien, $0\in D$ c'est-à-dire lorsque $0$ est à l'intérieur de l'ensemble $D$ (ce qui n'est PAS le cas dans cet exemple)
  * ou bien, $0$ est l'une des extrémités de $D$, sans pour autant appartenir à $D$ (c'est qui EST le cas dans cet exemple)

  Ainsi, il faudrait noter pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in D}} \dfrac 1x=+\infty$</enc> $\,\,$ qui est sous-entendue, mais que nous n'utiliserons pas, sauf besoin supplémentaire de précision.

<val style="margin-bottom:0.3em;margin-top:0.3em;">Notion de Limite <b>à droite</b> en $0$ et <b>Notation $0^+$</b></val>
<enc style="display:block;">On note quelquefois, pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac 1x=+\infty$</enc> $\,\,$ ou encore <enc>$\displaystyle \lim_{x\to 0^+} \dfrac {1}{x}=+\infty$</enc>
On dit dans ce cas que **la limite à droite en $0$ de $f$ vaut $+\infty$**
Ce qui exprime que la fonction $x\mapsto \dfrac 1x$ tend vers $+\infty$ quand **$x$ tend vers $0$ *par la droite*** (i.e. tout en restant positif)</enc>

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**

<clear></clear>

<div style="width:35%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/moins1surx_en0moins.png">

<figcaption>

fonction $x\mapsto -\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $g$ définie sur $D=]-\infty;0[$ par $g(x)=-\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to0} -\dfrac 1x = +\infty$</enc>

<val>Remarques</val>

* Idem, la notation précédente est sous-entendu équivalente à <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} -\dfrac 1x=+\infty$</enc>
* Idem, de manière plus générale, il faudrait noter pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in D}} -\dfrac 1x=+\infty$</enc> $\,\,$ qui est sous-entendue, mais que nous n'utiliserons pas, sauf besoin supplémentaire de précision.

<val style="margin-bottom:0.3em;">Notion de Limite <b>à gauche</b> en $0$ et <b>Notation $0^-$</b></val>
<enc style="display:block;">On note quelquefois, pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} -\dfrac 1x=+\infty$</enc> $\,\,$ ou encore <enc>$\displaystyle \lim_{x\to 0^-} -\dfrac {1}{x}=+\infty$</enc>
On dit dans ce cas que **la limite à gauche en $0$ de $f$ vaut $+\infty$**
Ce qui exprime que la fonction $x\mapsto -\dfrac 1x$ tend vers $+\infty$ quand **$x$ tend vers $0$ *par la gauche*** (i.e. tout en restant négatif)</enc>

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une **asymptote verticale à la courbe $\mathcal C_f$**

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/1surx2_en0.png">

<figcaption>

fonction $x\mapsto \dfrac {1}{x^2}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $h$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $h(x)=\dfrac {1}{x^2}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to0} \dfrac {1}{x^2} = +\infty$</enc>

<val>Remarques</val>

* Idem, la notation précédente est sous-entendu équivalente à <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\ne0}} \dfrac {1}{x^2}=+\infty$</enc>
* Idem, de manière plus générale, il faudrait noter pour être plus précis: <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in D}} \dfrac {1}{x^2}=+\infty$</enc> $\,\,$ qui est sous-entendue, mais que nous n'utiliserons pas, sauf besoin supplémentaire de précision.

<val style="margin-bottom:0.3em;">Limite en $0$ vs Limite <b>à gauche</b> en $0$ vs Limite <b>à droite</b> en $0$</val>
  <div style="display:block;" class="encadre">

  * Il peut arriver que la <b>limite à gauche</b> en $0$ <b>ET</b> <b>à droite</b> en $0$ existent toutes les deux simultanément, c'est le cas dans cet exemple:

    <center>

     <enc>$\displaystyle \lim_{x\to 0^-} \dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^2}=+\infty$</enc> $\,\,$ et $\,\,$ <enc>$\displaystyle \lim_{x\to 0^+} \dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^2}=+\infty$</enc>$\,\,$

    </center>

    Mais tous les cas de figure sont possibles: seule la limite à gauche existe, ou bien, seule la limite à droite existe, ou aucune des deux n'existe.
    **Dans le cas où aucune des deux limites à gauche et à droite n'existe, on dit que la limite en $0$ n'existe pas.**

  * Il peut même arriver que la limite à gauche en $0$ soit égale à la limite à droite en $0$, comme c'est le cas dans cet exemple.
    **Dans le cas où les deux limites à gauche et à droite existent et sont égales entre elles, on dit que la limite en $0$ existe** et qu'elle est égale à la limite à gauche et à la limite à droite. 
    
    <center>

    <enc>$\displaystyle \lim_{x\to 0} \dfrac {1}{x^2} = \displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^2} = +\infty$</enc>

    </center>
  * Néanmoins, il peut aussi arriver (voir le point 4 ci-après), que la limite à gauche et à droite existent en $0$, mais qu'elles ne soient pas égales: **Dans ce cas, nous dirons que la limite en $0$ N'existe PAS** car les limites à gauche et à droite (existent mais) sont différrentes.

  </div>

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**

<div style="width:35%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/1surx_en0.png">

<figcaption>

fonction $x\mapsto \dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:four: <b>Contre-Exemple</b> : La fonction $k$ définie sur $D=]-\infty;0[\cup ]0;+\infty[$ par $k(x)=\dfrac 1x$

<env>Limite à gauche en $0$</env> $\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac 1x = \lim_{x\to 0^-} \dfrac 1x = \displaystyle \lim\limits_{\substack{x \to 0 \\ x\in ]-\infty;0[}} \dfrac 1x = -\infty$</enc>
$\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad$La limite à gauche en $0$ existe et vaut $-\infty$
<env>Limite à droite en $0$</env> $\,\,\,\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac 1x = \lim_{x\to 0^+} \dfrac 1x = \displaystyle \lim\limits_{\substack{x \to 0 \\ x\in ]0;+\infty[}} \dfrac 1x = +\infty$</enc>
$\quad\quad\quad\quad\quad\quad\quad\quad\quad\quad$La limite à droite en $0$ existe et vaut $+\infty$
<env>PAS de Limite en $0$</env> $\,$ En effet, cet exemple prouve que : 
  * **la limite en zéro n'existe pas**, et donc que 
  * la notation $\displaystyle "\lim_{x\to 0} \dfrac 1x"$ est ambigüe dans ce cas particulier, et que 
  * il faut quelquefois utiliser la notation $\displaystyle \lim\limits_{\substack{x \to 0 \\ x\in I}} \dfrac 1x$, où $I\subseteq D$ désigne un ensemble inclus ou égal à l'ensemble de définition $D$, pour lever les ambigüités

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une **asymptote verticale à la courbe $\mathcal C_f$**

<clear></clear>

</exp>

<clear></clear>

<def>

On dit dans les cas précédents (non ambigüs) que la fonction $f(x)$ a pour limite $+\infty$, quand $x$ tend vers $0$, et on note:

* $\displaystyle \lim_{x\to 0} f(x) = +\infty$
* ou encore que: $f(x) \rightarrow +\infty$ quand $x \rightarrow 0$

</def>

<def>

Dans n'importe lequel des trois cas suivants:

* ou bien la limite en $0$ existe et vaut $+\infty$ (donc la limite à gauche et à droite existent aussi)
* ou bien seulement la limite à gauche en $0$ existe et vaut $+\infty$
* ou bien seulement la limite à droite en $0$ existe et vaut $+\infty$


On dit que la **droite verticale** $d$ d'équation $x=0$ est une **asymptote verticale en $0$** (resp. en $0^-$, ou en $0^+$, quand on veut être plus précis) à la courbe $\mathcal C_f$ de $f$.

</def>

<envaleur>Un exemple</envaleur>

La droite $d$ d'équation $x=0$ est une asymptote verticale en $0$ à la courbe $\mathcal C_f$ de la fonction $f$ définie sur $D=]0;+\infty[$ par $f(x)=\dfrac {1}{\sqrt x}$

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemples de fonctions $f$ de limite $-\infty$ en $0$</encadre>

<figure>

<img src="img/1surx_en0moins.png">

<figcaption>

fonction $x\mapsto \dfrac 1x$

</figcaption>

</figure>

</center>

</div>

<exp data="Introduction graphique pour Limite -infini en 0">

:one: Soit $f$ la fonction définie sur $D=]-\infty;0[$ par $f(x)=\dfrac 1x$

Il semble que les images $f(x)$ soient ***aussi petites*** que l'on veut (comprendre proches de $-\infty$), lorque $x$ est *suffisamment **"petit", au sens de proche de $0$ tout en restant $<0$*** :

* $f(x)< -10^{3}$ pour $-0,001<x<0$
* $f(x)< -10^{5}$ pour $-10^{-5}<x<0$

**Language Humain :** plus on **s'approche de $0$, tout en restant $<0$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut)
$\Leftrightarrow$ plus on **s'approche de zéro en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut)

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac 1x = \lim_{x\to 0^-} \dfrac 1x = \lim_{x\to 0} \dfrac 1x = -\infty$</enc></enc>

<val>Remarques</val> $\,$mêmes remarques sur les notations que pour $+\infty$ (etc... pour toute la suite)

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**

<clear></clear>

<div style="width:35%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/moins1surx_en0plus.png">

<figcaption>

fonction $x\mapsto -\dfrac 1x$

</figcaption>

</figure>

</center>

</div>

:two: on pourrait dire de même pour la fonction $g$ définie sur $D=]0;+\infty[$ par $g(x)=-\dfrac 1x$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} -\dfrac 1x = \lim_{x\to 0^+} -\dfrac 1x =\lim_{x\to 0} -\dfrac 1x=-\infty$</enc></enc>

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est une **asymptote verticale à la courbe $\mathcal C_f$**

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<figure>

<img src="img/moins1surx2_en0.png">

<figcaption>

fonction $x\mapsto -\dfrac {1}{x^2}$

</figcaption>

</figure>

</center>

</div>

:three: on pourrait dire de même pour la fonction $h$ définie sur $\mathbb R^*=]-\infty;0[\cup]0;+\infty[$ par $h(x)=-\dfrac {1}{x^2}$

<env>Notation</env> $\,\,$ <enc>$\displaystyle \lim_{x\to 0} -\dfrac {1}{x^2}=-\infty$</enc></enc>

<val style="margin-bottom:0.3em;">Limite en $0$ vs Limite <b>à gauche</b> en $0$ vs Limite <b>à droite</b> en $0$</val>

  <center>

  <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} -\dfrac {1}{x^2} = \lim\limits_{\substack{x \to 0 \\ x>0}} -\dfrac {1}{x^2} = \lim_{x\to 0} -\dfrac {1}{x^2} = -\infty$</enc>

  </center>

  La limite à gauche et à droite en $0$ existent et valent la même valeur $-\infty$, **donc** la limite en $0$ existe et vaut $-\infty$

<env>Asymptote Verticale</env> On dit dans ce cas que la **droite verticale** d'équation $x=0$ est **une asymptote verticale à la courbe $\mathcal C_f$**


<clear></clear>

</exp>

<clear></clear>

<def>

On dit dans les cas précédents (non ambigüs) que **la fonction $f(x)$ a pour limite $-\infty$, quand $x$ tend vers $0$**, et on note:

* $\displaystyle \lim_{x\to 0} f(x) = -\infty$
* ou encore que: $f(x) \rightarrow -\infty$ quand $x \rightarrow 0$

</def>

<def>

Dans n'importe lequel des trois cas suivants:

* ou bien la limite en $0$ existe et vaut $-\infty$ (donc la limite à gauche et à droite existent aussi)
* ou bien seulement la limite à gauche en $0$ existe et vaut $-\infty$
* ou bien seulement la limite à droite en $0$ existe et vaut $-\infty$


On dit que la **droite verticale** $d$ d'équation $x=0$ est une **asymptote verticale en $0$** (resp. en $0^-$, ou en $0^+$, quand on veut être plus précis) à la courbe $\mathcal C_f$ de $f$.

</def>

<envaleur>Un exemple</envaleur>

La droite verticale $d$ d'équation $x=0$ est une asymptote verticale en $0$ à la courbe $\mathcal C_f$ de la fonction $f$ définie par $f(x)=-\dfrac {1}{\sqrt x}$

### Fonctions de Référence de Limite infinie en $0$


<pte data="Fonctions de Référence de Limite infinie en 0">

<center>

<div style="font-size:1em;">

|Fonctions de Référence de Limite $\pm\infty$ en $0$|||
|:-:|:-:|:-:|
|Limite à gauche en $0$|Limite à droite en $0$|Limite en $0$|
|$\varnothing$|$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{\sqrt{x}} = \lim_{x\to 0^+} \dfrac {1}{\sqrt {x}} = \lim_{x\to 0} \dfrac {1}{\sqrt {x}} = +\infty$|$\displaystyle \lim_{x\to 0} \dfrac {1}{\sqrt {x}} = +\infty$||
|$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x} = \lim_{x\to 0^-} \dfrac {1}{x} = -\infty$|$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x} = \lim_{x\to 0^+} \dfrac {1}{x} = +\infty$|(n'existe pas)|
|Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^n} = \lim_{x\to 0^-} \dfrac {1}{x^n} = -\infty$|Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^n} = \lim_{x\to 0^+} \dfrac {1}{x^n} = +\infty$|(n'existe pas)|
|$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^2} = \lim_{x\to 0^-} \dfrac {1}{x^2} = +\infty$|$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^2} = \lim_{x\to 0^+} \dfrac {1}{x^2} = +\infty$|$\displaystyle \lim_{x\to 0} \dfrac {1}{x^2} = +\infty$|
|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \dfrac {1}{x^n} = \lim_{x\to 0^-} \dfrac {1}{x^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \dfrac {1}{x^n} = \lim_{x\to 0^+} \dfrac {1}{x^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim_{x\to 0} \dfrac {1}{x^n} = +\infty$|

</div>

</center>

</pte>

## Limite infinie en un réel $a$

Les mêmes cas de figure existent, lorsque l'on calcule la limite de $f(x)$, quand $x$ tend vers un réel $a$ avec $x$ restant dans l'ensemble de définition $D$ de $f$.

### Différents Cas de Figure

<div>

<!-- ATTENTION : CONSERVER LE LÉGER DÉCALAGE À GAUCHE DES IMGS POUR AJUSTEMENT À L'EXPORT EN PDF -->

<div style="width:24%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/1surxmoins2plus1_en2.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

fonction $f(x) = \dfrac {1}{x-2}+1$

</figcaption>

</figure>

<env>Limite à gauche en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} f(x) = \lim_{x\to2^-} f(x) = -\infty$
<env>Limite à droite en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} f(x) = \lim_{x\to2^+} f(x) = +\infty$
<env>Limite en $2$</env>
Limite à gauche $\ne$ Limite à droite
donc $\displaystyle \lim_{x\to2} f(x)$ n'existe pas
Asymptote verticale : $d:x=2$

</center>

</div>

<div style="width:24%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/moins1surxmoins2plus1_en2.png" style="margin-bottom:1em;">

<figcaption>

fonction $g(x) = -\dfrac {1}{x-2}+1$

</figcaption>

</figure>

<env>Limite à gauche en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} g(x) = \lim_{x\to2^-} g(x) = +\infty$
<env>Limite à droite en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} g(x) = \lim_{x\to2^+} g(x) = -\infty$
<env>Limite en $2$</env>
Limite à gauche $\ne$ Limite à droite
donc $\displaystyle \lim_{x\to2} g(x)$ n'existe pas
Asymptote verticale : $d:x=2$

</center>

</div>

<div style="width:24%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/1surxmoins2carreplus1_en2.png" style="margin-bottom:1em;">

<figcaption>

fonction $h(x) = \dfrac {1}{(x-2)^2}+1$

</figcaption>

</figure>

<env>Limite à gauche en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} h(x) = \lim_{x\to2^-} h(x) = +\infty$
<env>Limite à droite en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} h(x) = \lim_{x\to2^+} h(x) = +\infty$
<env>Limite en $2$</env>
Limite à gauche $=$ Limite à droite
donc $\displaystyle \lim_{x\to2} h(x)$ existe et vaut $+\infty$
Asymptote verticale : $d:x=2$

</center>

</div>

<div style="width:24%; float:left; margin-left:3px;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/moins1surxmoins2carreplus1_en2.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

fonction $k(x) = -\dfrac {1}{(x-2)^2}+1$

</figcaption>

</figure>

<env>Limite à gauche en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x<2}} k(x) = \lim_{x\to2^-} k(x) = -\infty$
<env>Limite à droite en $2$</env>
$\displaystyle \lim\limits_{\substack{x \to 2 \\ x>2}} k(x) = \lim_{x\to2^+} k(x) = -\infty$
<env>Limite en $2$</env>
Limite à gauche $=$ Limite à droite
donc $\displaystyle \lim_{x\to2} k(x)$ existe et vaut $-\infty$
Asymptote verticale : $d:x=2$

</center>

</div>

</div>

<clear></clear>

### Définitions

<def data="Limite infinie à gauche ou à droite en un réel a">

<env>Limite $+\infty$ à gauche en $a$</env> plus on **s'approche de $a$, tout en restant $<a$**, et plus la *"hauteur"* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut). On note : <enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = +\infty$</enc>
<env>Limite $+\infty$ à droite en $a$</env> plus on **s'approche de $a$, tout en restant $>a$**, et plus la *"hauteur"* **est grande** (comprendre proche de $+\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la droite**, et plus ***la ("hauteur" de la) courbe est grande*** (comprendre proche de $+\infty$) (aussi proche que l'on veut). On note : <enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x) = +\infty$</enc>
<env>Limite $-\infty$ à gauche en $a$</env> plus on **s'approche de $a$, tout en restant $<a$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la gauche**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut). On note : <enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = -\infty$</enc>
<env>Limite $-\infty$ à droite en $a$</env> plus on **s'approche de $a$, tout en restant $>a$**, et plus la *"hauteur"* **est petite** (comprendre proche de $-\infty$) (aussi *proche* que l'on veut) $\Leftrightarrow$ plus on **s'approche de $a$ en venant de la droite**, et plus ***la ("hauteur" de la) courbe est petite*** (comprendre proche de $-\infty$) (aussi proche que l'on veut). On note : <enc>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x) = -\infty$</enc>

</def>

<def data="Asymptote verticale d'équation (d) : x=a ">

Lorsqu'au moins l'une des 4 limites précédentes existe et est infinie, on dit que la droite verticale **$d:x=a$ est une asymptote verticale à la courbe $\mathcal C_f$**

</def>

<def data="Limite infinie en a et lien avec la Limite (infinie) à gauche (resp. à droite) en a ">

Soit $a$ un réel tel que $x$ puisse tendre vers $a$ tout en restant dans $D$.

:one: $\,$ **ou bien $a$ est une extrémité de $D$**, auquel cas:
  * Si $a$ est une extrémité gauche de $D$ (donc $x\rightarrow a$ par la droite...) et que la limite infinie à droite en $a$ existe
  Alors dans ce cas on dit que la limite infinie en $a$ existe, et vaut la limite de droite en $a$
  On note <enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x)$</enc>
  * Si $a$ est une extrémité droite de $D$ (donc $x\rightarrow a$ par la gauche...) et que la limite infinie à gauche en $a$ existe
  Alors dans ce cas on dit que la limite infinie en $a$ existe, et vaut la limite de gauche en $a$
  On note <enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x)$</enc>

:two: $\,$ **ou bien $a\in D$** (c'est-à-dire $a$ est à l'intérieur de $D$), auquel cas:
  La limite inifinie en $a$ existe $\Leftrightarrow$ :
  * Les limites infinies à gauche <b>ET</b> à droite en $a$ existent toutes les deux, et
  * Limite à gauche en $a$ = Limite à droite en $a$
  Dans ce cas on dit que la limite en $a$ existe, et on pose: Limite en $a=$ Limite à gauche en $a = $ Limite à droite en $a$
  On note <enc>$\displaystyle \lim_{x\to a} f(x) =  \lim\limits_{\substack{x \to a \\ x<a}} f(x) = \lim_{x\to a^-} f(x) = \lim\limits_{\substack{x \to a \\ x>a}} f(x) = \lim_{x\to a^+} f(x)$</enc>

</def>


### Fonctions de Référence de Limite infinie en $a$

<pte data="Fonctions de Référence de Limite infinie en a">

<center>

<div style="font-size:0.9em;">

|Fonctions de Référence de Limite $\pm\infty$ en $a$|||
|:-:|:-:|:-:|
|Limite à gauche en $a$|Limite à droite en $a$|Limite en $a$|
|$\varnothing$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{\sqrt{x-a}} = \lim_{x\to a^+} \dfrac {1}{\sqrt {x-a}} = \lim_{x\to a} \dfrac {1}{\sqrt {x-a}} = +\infty$|$\displaystyle \lim_{x\to a} \dfrac {1}{\sqrt {x-a}} = +\infty$||
|$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{x-a} = \lim_{x\to a^-} \dfrac {1}{x-a} = -\infty$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{x-a} = \lim_{x\to a^+} \dfrac {1}{x-a} = +\infty$|(n'existe pas)|
|Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^-} \dfrac {1}{(x-a)^n} = -\infty$|Généralisation pour $n$ entier **impair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^+} \dfrac {1}{(x-a)^n} = +\infty$|(n'existe pas)|
|$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^2} = \lim_{x\to a^-} \dfrac {1}{(x-a)^2} = +\infty$|$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^2} = \lim_{x\to a^+} \dfrac {1}{(x-a)^2} = +\infty$|$\displaystyle \lim_{x\to a} \dfrac {1}{(x-a)^2} = +\infty$|
|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x<a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^-} \dfrac {1}{(x-a)^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim\limits_{\substack{x \to a \\ x>a}} \dfrac {1}{(x-a)^n} = \lim_{x\to a^+} \dfrac {1}{(x-a)^n} = +\infty$|Généralisation pour $n$ entier **pair**$>0$<br/>$\displaystyle \lim_{x\to a} \dfrac {1}{(x-a)^n} = +\infty$|

</div>

</center>

</pte>

## Limite finie en un réel $a$

Soit $f$ une fonction définie sur un intervalle $I$, et $\ell\in \mathbb R$ un nombre réel.
Soit un réel $a$ tel que:
* ou bien $a\in I$ est intérieur à $I$, 
* ou bien $a$ est une extrémité de $I$ si $a\notin I$

<def>

Dire que : plus $x$ **s'approche de $a$**, et plus la *"hauteur"* $f(x)$ **est proche de $\ell$** (aussi *proche* que l'on veut) 
$\Leftrightarrow$ plus $x$ **s'approche de $a$**, et plus ***la "hauteur" $f(x)$ de la courbe est proche de $\ell$*** (aussi proche que l'on veut). 
Dans ce cas on dit que la limite de $f(x)$ en $a$ existe et vaut le réel fini $\ell$.
On note : <enc>$\displaystyle \lim_{x\to a} f(x) = \ell$</enc>

</def>

<env>Remarque </env> Cette notation sous-entend que <enc>$\displaystyle \lim_{x\to a} f(x) = \lim\limits_{\substack{x \to a \\ x\ne a}} f(x)$</enc> $\,$et plus généralement que <enc>$\,$ $\displaystyle \lim_{x\to a} f(x) = \lim\limits_{\substack{x \to a \\ x\in I, x\ne a}} f(x)$</enc>

<pte>

Dire que la limite de $f$ en $a$ existe et vaut $\ell$ est équivalent à dire que:
* la limite à gauche $a$ existe et vaut $\ell$
* la limite à droite $a$ existe et vaut $\ell$

</pte>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemple d'une fonction $f$ n'ayant pas de limite finie : <br/>ni en $0$, ni en $1$, etc...</encadre>

<figure>

<img src="img/partieentiere.png">

<figcaption>

fonction partie Entière $x\mapsto E(x)=\lfloor x \rfloor$

</figcaption>

</figure>

</center>

</div>

<exp data="Fonction pour laquelle Limite en a n'existe pas">

Soit $f$ la fonction définie sur $D=\mathbb R$ par $f(x)=E(x)=\lfloor x \rfloor$

<env>Limites</env> $\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x<0}} \lfloor x \rfloor = \lim_{x\to 0^-} \lfloor x \rfloor = -1$</enc> $\,$ et $\,$ <enc>$\displaystyle \lim\limits_{\substack{x \to 0 \\ x>0}} \lfloor x \rfloor = \lim_{x\to 0^+} \lfloor x \rfloor = 0$</enc>
$\quad\quad\quad\quad\quad$ Limite à gauche existe et vaut $-1$, Limite à droite existe et vaut $0$
$\quad\quad\quad\quad\quad$ donc la limite en $0$ n'existe pas.
De la même manière, il n'existe pas de limite de $f$ en $a$ lorsque $a$ est un nombre entier.

<clear></clear>



</exp>



# Opérations sur les Limites de fonctions

## Notion de Forme Indéterminée (FI)

C'est exactement la même notion appiquée aux limites de fonctions

## Limite d'une Somme

<center>

|Limite d'une Somme de fonctions|||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x)+g(x) \right)=...$|$\ell+\ell'$|$+\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|$-\infty$|

</center>

**Résumé: (Idem que les Suites)**

* Une seule FI pour l'Addition: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)+(-\infty)$ ou $(-\infty)+(+\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour l'addition
* la Règle des signes pour l'addition est préservée: $+(+\infty)=+\infty$ et $+(-\infty)=-\infty$

## Limite d'une Soustraction

<center>

|Limite d'une Soustraction de fonctions|||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x)-g(x) \right)=...$|$\ell-\ell'$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|$+\infty$|<span style="color:red;">$FI$</span>|

</center>

**Résumé: (Idem que les Suites)**

* Une seule FI pour la Soutraction: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$\infty-\infty$</red></encadre> $\quad($comprendre : $(+\infty)-(+\infty)$ ou $(-\infty)-(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Soustraction
* la Règle des signes pour la soustraction est préservée: $-(+\infty)=-\infty$ et $-(-\infty)=+\infty$

## Limite d'un Produit

<center>

|Limite d'un Produit de fonctions||||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|$\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell>0$|$\ell>0$|$\ell<0$|$\ell<0$|$+\infty$|$+\infty$|$-\infty$|$0$|
|$\displaystyle \lim_{x \to a} g(x)=...$|$\ell'$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|$\displaystyle \lim_{x \to a} \left( f(x) \times g(x) \right)=...$|$\ell \times \ell'$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|$+\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

</center>

**Résumé: (Idem pour les Suites)**

* Une seule FI pour la Multiplication: <encadre style="padding:0.2em 0.4em;"><red style="font-size:1.5em;">$0\times \infty$</red></encadre> $\quad($comprendre : $0\times(+\infty)$ ou $0\times(-\infty)$ $)$
* l'infini l'emporte sur tout réel $\ell$ pour la Multiplication, **SAUF SUR $\ell=0$!!**
* la Règle des signes pour le produit est préservée: $(+)\times(+)=+$, $(-)\times(-)=+$, $(+)\times(-)=-$, $(-)\times(+)=-$

## Limite d'un Quotient

<center>

|<span style="background-color:#F00;">Cas I :</span> Limite d'un Quotient de Fonctions $\dfrac {f(x)}{g(x)}$ avec <div style="display: inline-block; padding: 0.3em; background-color:#f3cbcb;">$\displaystyle \lim_{x \to a} g(x) \ne 0$</div> (et $\forall x \in \mathcal{V(a)} , g(x) \ne 0$)||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{x \to a} f(x)=...$|$\ell$|$\ell$|$+\infty$|$+\infty$|$-\infty$|$-\infty$|$+\infty$ ou $-\infty$|
|et si $\displaystyle \lim_{x \to a} g(x)=...$|$\ell' \neq 0$|$+\infty$ ou $-\infty$|$\ell'>0$|$\ell'<0$|$\ell'>0$|$\ell'<0$|$+\infty$ ou $-\infty$|
|<nobr>alors  $\displaystyle \lim_{x \to a} \left( \frac{f(x)}{g(x)} \right)=...$</nobr>|$\dfrac \ell {\ell'}$|$0$|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

|<span style="background-color:#F00;">Cas II :</span> Limite d'un Quotient de Suites $\dfrac {f(x)}{g(x)}$ avec <div style="display: inline-block; padding: 0.3em; background-color:#f3cbcb;">$\displaystyle \lim_{x \to a} g(x) = 0$</div> (et $\forall x \in \mathcal{V(a)} , g(x) \ne 0$)||||||
|:-:|:-:|:-:|:-:|:-:|:-:|
|Si $\displaystyle \lim_{x \to a} f(x)=...$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$\ell>0$ ou $+\infty$|$\ell<0$ ou $-\infty$|$0$|
|et si $\displaystyle \lim_{x \to a} g(x)=...$|$0^{+}$<br/>$=0$ en restant positif|$0^{+}$<br/>$=0$ en restant positif|$0^{-}$<br/>$=0$ en restant négatif|$0^{-}$<br/>$=0$ en restant négatif|$0$|
|<nobr>alors  $\displaystyle \lim_{x \to a} \left( \frac{f(x)}{g(x)} \right)=...$</nobr>|$+\infty$|$-\infty$|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|

</center>

**Résumé des deux tableaux précédents sous la forme d'un tableau à double entrée:** (si vous préférez)

<center>

<div class="double">

|Tableau résumant la Limite d'un Quotient $\dfrac {f(x)}{g(x)}$ avec $\forall x \in \mathcal{V(a)} , g(x) \ne 0$||||||||
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|<span style="position:relative;top:0.6em;right:0.4em;">$\displaystyle \lim_{x \to a} f(x)$</span><span style="position:relative;top:-1em;right:-0.5em;">$\displaystyle \lim_{x \to a} g(x)$</span>|>|>|$0$|$\ell'>0$|$\ell'<0$|$+\infty$|$-\infty$|
|^|$0^{+}$|$0^{-}$|$0^{+/-}$|^|^|^|^|
|$\ell=0$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|$0$|$0$|$0$|$0$|
|$\ell>0$|$+\infty$|$-\infty$|<span style="color:blue;">$PDL$</span>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{+}$|$0^{-}$|
|$\ell<0$|$-\infty$|$+\infty$|<span style="color:blue;">$PDL$</span>|$\dfrac \ell {\ell'}$|$\dfrac \ell {\ell'}$|$0^{-}$|$0^{+}$|
|$+\infty$|$+\infty$|$-\infty$|<span style="color:blue;">$PDL$</span>|$+\infty$|$-\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|
|$-\infty$|$-\infty$|$+\infty$|<span style="color:blue;">$PDL$</span>|$-\infty$|$+\infty$|<span style="color:red;">$FI$</span>|<span style="color:red;">$FI$</span>|

<div>

</center>

<span style="color:blue;">$PDL$<span><black> voulant dire que l'on est sûr que la fonction $\left( \dfrac {f(x)}{g(x)} \right)$ n'admet <blue><b>P</b><black>as <blue><b>D</b><black>e <blue><b>L</b><black>imite (ni réelle $\ell$, ni infinie).

**Résumé: (Idem pour les Suites)**

* Deux FI pour le Quotient: <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{0}{0}$</red></encadre> $\,\,\,$ et $\,\,$ <encadre style="padding:0.7em 0.4em;"><red style="font-size:1.5em;">$\dfrac{\infty}{\infty}$</red></encadre>
comprendre $\dfrac{0^+}{0^+}$, $\dfrac{0^-}{0^-}$, $\dfrac{0^+}{0^-}$, $\dfrac{0^-}{0^+}$, $\dfrac{0^{+/-}}{0^+}$, $\dfrac{0^{+/-}}{0^-}$, $\dfrac{0^{+}}{0^{+/-}}$, etc...$\,\,$ et $\,\,$ $\dfrac{+\infty}{+\infty}$, $\dfrac{-\infty}{-\infty}$, $\dfrac{+\infty}{-\infty}$, $\dfrac{-\infty}{+\infty}$, 
* Pour le quotient, l'$\infty$ **AU NUMÉRATEUR** :
  * ne l'emporte pas sur $\ell'=0^{+/-}$ **AU DÉNOMINATEUR**. Dans ce cas: PAS DE LIMITE
  * l'emporte sur tout réel $\ell'\ne 0$ **AU DÉNOMINATEUR**, 
  * ne l'emporte PAS sur l'$\infty$ **AU DÉNOMINATEUR** !! Dans ce cas : on a une FI, 
* Pour le quotient, $0$ **AU NUMÉRATEUR** l'emporte sur tout réel $\ell$ **AU DÉNOMINATEUR**, **SAUF SUR $\ell=0$ !!** Dans ce cas: on a une FI,
* la Règle des signes pour le quotient est préservée:
 $\dfrac{+}{+}=+$, $\dfrac{-}{-}=+$, $\dfrac{+}{-}=-$, $\dfrac{-}{+}=-$

## Résumé des Formes indéterminées (FI)

<center>

|Résumé des Formes indéterminées usuelles pour les Fonctions|||||
|:-:|:-:|:-:|:-:|:-:|
|$\infty-\infty$|$0\times \infty$|$\dfrac{0}{\infty}$|$\dfrac{\infty}{\infty}$|$1^{\infty}$|

</center>

En résumé, ce sont les mêmes que pour les suites.
La forme indéterminée $1^{\infty}$ peut sembler moins naturelle pour le moment, mais pourra être mieux comprise durant cette année, grâce à :
* l'étude de la fonction exponentielle $exp$, vue en 1ère,
* et l'étude de la fonction $ln$ qui sera étudiée en peu plus tard, 
* et grâce à la formule <encadre>$x^y=e^{y ln(x)}$</encadre>

# Fonctions Continues & Théorème des Valeurs Intermédiaires (TVI)

## Notion de Continuité

### Définitions

<def>

Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ un nombre réel **de $I$**.

* Dire que **$f$ est continue en $a$** signifie que $f$ admet une limite en $a$ égale à $f(a)$, i.e. que <enc>$\displaystyle \lim_{x\to a} f(x) = f(a)$</enc>
* Dire que **$f$ est continue sur l'intervalle $I$** signifie que $f$ est continue en tout réel $a$ de $I$

</def>

<env>Graphiquement</env> La continuité d'une fonction $f$ sur un intervalle $I$ se traduit par le fait que l'on peut dessiner la courbe représentative de $f$ sur $I$ **sans lever le crayon**.

### Différents Cas de Figure

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemple de discontinuité de type $1$</encadre>

<figure>

<img src="img/discontinuite.png">

<figcaption>

fonction donnée graphiquement

</figcaption>

</figure>

</center>

</div>

<exp data="Discontinuité de type 1">

La fonction $f$ représentée ci-contre :
* est discontinue en $a=1$, car :
  * <env>Limite à gauche en $1$</env>$\quad \displaystyle \lim\limits_{\substack{x \to 1 \\ x<1}} f(x) = \lim_{x\to 1^-} f(x) = 1$
  * <env>Limite à droite en $1$</env>$\quad \displaystyle \lim\limits_{\substack{x \to 1 \\ x>1}} f(x) = \lim_{x\to 1^+} f(x) = -1$
  * <env>PAS de Limite en $1$</env>$\quad$ car Limite à gauche $\ne$ Limite à droite en $1$
  <env>Conclusion</env> $\, f$ est discontinue en $a=1$, car il y a une coupure en $1$
  Autrement dit: dans ce cas, la limite en $1$ n'existe même pas
  En particulier, la fonction n'est PAS continue sur $I=[-2;3]$ 
* est continue sur $[-2;1[$ et aussi continue sur $]1;3]$

</exp>

<clear></clear>

<div style="width:40%; float:right; margin-left:3px;">

<center>

<encadre>Exemple de discontinuité de type $2$</encadre>

<figure>

<img src="img/discontinuite1.png">

<figcaption>

fonction donnée graphiquement

</figcaption>

</figure>

</center>

</div>

<exp data="Discontinuité de type 2">

La fonction $f$ représentée ci-contre :
* est discontinue en $a=1$, car :
  * <env>Limite à gauche en $1$</env>$\quad \displaystyle \lim\limits_{\substack{x \to 1 \\ x<1}} f(x) = \lim_{x\to 1^-} f(x) = 1$
  * <env>Limite à droite en $1$</env>$\quad \displaystyle \lim\limits_{\substack{x \to 1 \\ x>1}} f(x) = \lim_{x\to 1^+} f(x) = 1$
  * <env>Il existe une Limite en $a=1$ qui vaut $\ell=1$</env>$\quad$ 
  En effet, Limite à gauche $=$ Limite à droite en $a=1$. 
  Donc La limite en $1$ existe et vaut $\ell=1$ : $\displaystyle \lim_{x\to 1} f(x) = 1$
  * Par contre, par lecture graphique, $f(1)=2$, donc <enc>$\displaystyle \lim_{x\to 1} f(x) \ne f(1)$</enc>
  <env>Conclusion</env> $f$ est discontinue en $1$, car la limite en $1$ existe, mais $\ne f(1)$
  En particulier, la fonction n'est PAS continue sur $I=[-2;3]$ 
* est continue sur $[-2;1[$ et aussi continue sur $]1;3]$

</exp>

### Convention dans les tableaux de variation

Par convention, dorénavant, dans un tableau de variation, sur un intervalle $I$, une flèche :

* Une **flèche vers le haut** représente une fonction strictement croissante **et continue** sur $I$
* Une **flèche vers le bas** représente une fonction strictement décroissante **et continue** sur $I$

## Propriétés des Fonctions Continues

<thm data="Dérivable => Continue. Admis">

Soit $f$ une fonction dérivable sur un intervalle $I$, et $a$ un nombre réel **de $I$**.
* Si $f$ est dérivable en $a$, Alors $f$ est continue en $a$
* Si $f$ est dérivable sur $I$, Alors $f$ est continue sur $I$

</thm>

<pte data="Conséquence">

Toutes les fonctions usuelles sont continues sur tout intervalle inclus dans leur ensemble de définition $D$.

</pte>

<pte data="Opérations sur les Fonctions Continues">

Soient $f$ et $g$ deux fonctions définies sur un intervalle $I$, et $a\in I$.
Si $f$ et $g$ sont continues en $a$, Alors:
* $\lambda f$ est continue en $a$ (pour $\lambda \in \mathbb R$)
* $f+g$ est continue en $a$
* $f-g$ est continue en $a$
* $f\times g$ est continue en $a$
* $\dfrac fg$ est continue en $a$ lorsque $g(a)\ne0$
$\dfrac fg$ est discontinue en $a$ lorsque $f(a)\ne0$ et $g(a)=0$
* Si une fonction $g$ est continue en $a$ et $f$ est continue en $g(a)$,
Alors la fonction $f\circ g$ est continue en $a$


</pte>



## TVI pour les Fonctions Continues

Rappel Intuitif : Une fonction continue sur un intervalle $I$ est une fonction sans coupures, sans sauts sur l'intervalle $I$, **dont on peut tracer la courbe sur $I$ sans lever le crayon**.

### Introduction Graphique pour $k=0$

<div>

<!-- ATTENTION : CONSERVER LE LÉGER DÉCALAGE À GAUCHE DES IMGS POUR AJUSTEMENT À L'EXPORT EN PDF -->

<div style="width:26.5%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/tvi_kegal0_f_croissante.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ strictement croissante sur $[a;b]$

</figcaption>

</figure>

On a $f(a)<0$ et $f(b)>0$ donc :
<enc>$f(a)<0<f(b)$</enc>
Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que :
$f(c)=0\,(=k)$
On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
**une unique valeur $c$ exactement**.

</center>

</div>

<div style="width:27%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/tvi_kegal0_f_decroissante.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ strictement décroissante sur $[a;b]$

</figcaption>

</figure>

On a $f(a)>0$ et $f(b)<0$ donc :
<enc>$f(b)<0<f(a)$</enc>
Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que :
$f(c)=0\,(=k)$
On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
**une unique valeur $c$ exactement**.

</center>

</div>

<div style="width:43%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-11%;top:-0.8em;width:95%;">

<img src="img/tvi_kegal0_f_quelconque.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ quelconque sur $I=[a;b]$

</figcaption>

</figure>

On a $f(a)<0$ et $f(b)>0$ donc :
<enc>$f(a)<0<f(b)$</enc>
Autrement dit: le nombre $k=0$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que:
$f(c)=0\,(=k)$
On voit bien qu'on NE PEUT PAS améliorer le résultat dans ce cas général:
Il existe **au moins** une valeur $c$ vérifiant l'égalité $f(c)=0$ si $f$ est quelconque.
**MAIS, il *PEUT* exister plusieurs valeurs $c$** vérifiant l'égalité $f(c)=0$ si $f$ est une fonction quelconque.

</center>

</div>

</div>

### Introduction Graphique pour $k$ réel quelconque

On se donne maintenant un réel $k$ quelconque mais fixé.

<div>

<!-- ATTENTION : CONSERVER LE LÉGER DÉCALAGE À GAUCHE DES IMGS POUR AJUSTEMENT À L'EXPORT EN PDF -->

<div style="width:28.5%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/tvi_kquelconque_f_croissante.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ strictement croissante sur $[a;b]$

</figcaption>

</figure>

On a $f(a)<k$ et $f(b)>k$ donc :
<enc>$f(a)<k<f(b)$</enc>
Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que $f(c)=k$
On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
**une unique valeur $c$ exactement**.

</center>

</div>

<div style="width:27%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-20%;top:-0.8em;width:95%;">

<img src="img/tvi_kquelconque_f_decroissante.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ strictement décroissante sur $[a;b]$

</figcaption>

</figure>

On a $f(a)>k$ et $f(b)<k$ donc :
<enc>$f(b)<k<f(a)$</enc>
Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que $f(c)=k$
On voit bien qu'on pourrait améliorer le résultat dans ce cas particulier: 
**une unique valeur $c$ exactement**.

</center>

</div>

<div style="width:41%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-12%;top:-0.8em;width:95%;">

<img src="img/tvi_kquelconque_f_quelconque.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$f$ quelconque sur $I=[a;b]$

</figcaption>

</figure>

On a $f(a)<k$ et $f(b)>k$ donc :
<enc>$f(a)<k<f(b)$</enc>
Autrement dit: le nombre $k$ est compris entre $f(a)$ et $f(b)$
donc s'il n'y a pas de "coupure"/"sauts" dans la courbe, alors il existe **au moins** un réel $c$ tel que $f(c)=k$
On voit bien qu'on NE PEUT PAS améliorer le résultat dans ce cas général:
Il existe **au moins** une valeur $c$ vérifiant l'égalité $f(c)=k$ si $f$ est quelconque.
**MAIS, il *PEUT* exister plusieurs valeurs $c$** vérifiant l'égalité $f(c)=k$ si $f$ est une fonction quelconque.

</center>

</div>

</div>



### Le Théorème des Valeurs Intermédiaires (TVI) pour les fonctions continues

<thm data="des Valeurs Intermédiaires (TVI) pour les Fonctions Continues">

Soit $f$ une fonction **continue** sur un intervalle $I=[a;b]$ (borné), *avec* **$a<b$**.
Pour tout nombre réel $k$ **compris entre $f(a)$ et $f(b)$**,
Il existe **au moins** un réel $c$ compris entre $f(a)$ et $f(b)$ tel que $f(c)=k$

</thm>

<env>Interprétation Graphique</env> En notant $\mathcal C_f$ la courbe représentative de $f$ dans le repère.Pour tout réel $k$ compris entre $f(a)$ et $f(b)$, la droite horizontale d'équation $y=k$, **coupe au moins une fois** la courbe $\mathcal C_f$ en un point d'abscisse $c$ comprise entre $a$ et $b$.

<env>Interprétation en Termes d'Équation</env>
Pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l’équation <enc>$f(x)=k$</enc> $\,$ admet **au moins une solution** $c$ comprise entre $a$ et $b$.

### Généralisation à des intervalles $I$ ouverts et/ou non bornés

<def data="Intervalles Bornés, ou Non Bornés">

* Si $a$ et $b$ sont deux réels finis:
  * chacun des intervalles $[a;b]$, $]a;b]$, $[a;b[$ et $]a;b[$ est appelé **borné**
  * l'intervalle $[a;b]$ est **fermé et borné**
  * l'intervalle $]a;b]$ est **semi-ouvert et borné**: ouvert à gauche, fermé à droite (et borné)
  * l'intervalle $[a;b[$ est **semi-ouvert et borné**: fermé à gauche, ouvert à droite (et borné)
* Si $a$ et/ou $b$ sont infinis, 
  * chacun des intervalles $]-\infty;b]$, $]-\infty;b[$ (pour $b$ réel) $[a;+\infty[$, $]a;+\infty[$ (pour $a$ réel) et $]+\infty;+\infty[$, est appelé **non borné**
  * $]-\infty;b[$ non borné à gauche, ouvert à droite, ou 
  * $]-\infty;b]$ non borné à gauche, fermé à droite,
  * $]a;+\infty[$ ouvert à gauche, non borné à droite,
  * $[a;+\infty[$ fermé à gauche, non borné à droite,
  * $]-\infty;+\infty[$ non borné ni à gauche ni à droite, et ourvert à dauche et à droite

</def>

Ce **théorème se généralise bien pour des intervalles $I$ (semi-) ouverts et/ou non bornés**, en adaptant l'énoncé de la manière suivante : 
* Si $a=-\infty$ ou bien si $I$ est ouvert en $a$, alors remplacer $f(a)$ par $\quad \displaystyle \lim_{x\to a} f(x)$
* Si $b=+\infty$ ou bien si $I$ est ouvert en $b$, alors remplacer $f(b)$ par $\quad \displaystyle \lim_{x\to b} f(x)$

<exp>

L'équation $e^x=0.2$ admet au moins une solution sur l'intervalle $I=]-2;-1[$

</exp>

## TVI pour les Fonctions Continues et Strictement monotones

<thm data="des Valeurs Intermédiaires (TVI) pour les Fonctions Continues ET Strictement Monotones">

Soit $f$ une fonction **continue** et **strictement monotone** sur un intervalle $I=[a;b]$ (borné), *avec* **$a<b$**.
Pour tout nombre réel $k$ **compris entre $f(a)$ et $f(b)$**,
Il existe **un unique** un réel $c$ compris entre $f(a)$ et $f(b)$ tel que $f(c)=k$

</thm>

<env>Extension</env> De même que pour le précédent, **Ce théorème s'étend à des intervalles (semi-) ouverts et/ou non bornés**, en adaptant $f(a)$ et $f(b)$ de la manière décrite ci-dessus.

<exp>

L'équation $e^x=2$ admet une unique solution sur l'intervalle $I=[0;+\infty[$

</exp>

## Fonction Réciproque d'une fonction continue

<def data="Fonctions Réciproques - Admis">

Soit $f$ une fonction **continue strictement monotone** sur un intervalle $I=[a;b]$
Il existe une **unique** fonction définie sur $J=[f(a);f(b)]$ (ou $[f(b);f(a)]$) et à valeurs dans $I$, appelée **fonction réciproque de $f$**, et notée $f^{-1}$ telle que:

$$
\left\{
    \begin{array}{ll}
        f(x)=y\\
        x\in I
    \end{array}
\right.
\Leftrightarrow
\left\{
    \begin{array}{ll}
        x=f^{-1}(y)\\
        y\in J
    \end{array}
\right.
$$

</def>

<pte>

Dans un repère orthonormé, les courbes représentatives de $f$ et $f^{-1}$ sont symétriques par rapport à la droite d'équation $y=x$ (1ère bissectrice)

</pte>

<exp data="Deux Cas de Figure">

<div>

<!-- ATTENTION : CONSERVER LE LÉGER DÉCALAGE À GAUCHE DES IMGS POUR AJUSTEMENT À L'EXPORT EN PDF -->

<div style="width:48%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-10%;top:-0.8em;width:95%;">

<img src="img/fonctionsreciproques_x2sqrt.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$x\mapsto x^2$ et $x\mapsto \sqrt x$ sont réciproques l'une de l'autre

</figcaption>

</figure>

La fonction $f$ définie sur $I=[0;+\infty[$ par $f(x)=x^2$ est continue (car dérivable) et strictement croissante sur $I$.
De plus, $f$ prend ses valeurs dans $J=[0;+\infty[$.
Sa fonction réciproque $f^{-1}$ de $J$ dans $I$ est la fonction racine carrée.
En effet:

$$
\left\{
    \begin{array}{ll}
        x^2=y\\
        x\in I=[0;+\infty[
    \end{array}
\right.
\Leftrightarrow
\left\{
    \begin{array}{ll}
        x=\sqrt y\\
        y\in J=[0;+\infty[
    \end{array}
\right.
$$

</center>

</div>

<div style="width:48%; float:left; margin-left:3px;border-right: solid #999;">

<center>

<figure style="position:relative; left:-10%;top:-0.8em;width:95%;">

<img src="img/fonctionsreciproques_expln.png" style="margin-bottom:1em;">

<figcaption style="white-space:nowrap;">

$x\mapsto e^x$ et $x\mapsto ln(x)$ sont réciproques l'une de l'autre

</figcaption>

</figure>

La fonction $f$ définie sur $I=]-\infty;+\infty[$ par $f(x)=e^x$ est continue (car dérivable) et strictement croissante sur $I$.
De plus, $f$ prend ses valeurs dans $J=]0;+\infty[$.
Sa fonction réciproque $f^{-1}$ de $J$ dans $I$ est la fonction Logarithme Népérien (LN).
En effet:

$$
\left\{
    \begin{array}{ll}
        e^x=y\\
        x\in I=]-\infty;+\infty[
    \end{array}
\right.
\Leftrightarrow
\left\{
    \begin{array}{ll}
        x=ln(y)\\
        y\in J=]0;+\infty[
    \end{array}
\right.
$$

</center>

</div>

</div>





