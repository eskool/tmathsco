# TMATHSCO : Fonctions Réciproques

Dans toute la suite, on se donne une fonction $f$ définie sur $D$, à valeurs dans l'ensemble $E=f(D)$, donc $f: D \rightarrow E$

!!! def
    Une fonction $f:D\rightarrow E$ est dite <red>strictement monotone</red> sur $D$ $\Leftrightarrow$

    * ou bien, $f$ strictement croissante sur $D$
    * ou bien, $f$ strictement décroissante sur $D$

De manière très générale en mathématiques, on dispose de la propriété suivante:

!!! pte "(Fonctions Réciproques)"
    Si $f:D\rightarrow E=f(D)$ est continue et strictement monotone de $D$ sur $E$,
    Alors :

    * **Existence et Unicité de $f^{-1}$:** il existe une **unique** fonction, notée $f^{-1}:E \rightarrow D$, appelée **fonction réciproque** de $f$, qui est définie sur $E$ et à valeurs dans $D=f^{-1}(E)$, et qui vérifie les propriétés suivantes:

        <center>$\forall x\in D, \forall y\in E, \quad y=f(x) \Leftrightarrow f^{-1}(y)=x$</center>
    
        En particulier:

        <center>$\forall x \in D, \quad f^{-1}(f(x))=x$</center>

        <center>$\forall x \in E, \quad f(f^{-1}(x))=x$</center>

    * **Sens de variation de $f^{-1}$ :** La fonction $f^{-1}:E \rightarrow D$ est strictement monotone sur $E$.
    Plus précisément, Les fonctions $f$ et $f^{-1}$ ont le même sens de variation:
        * Dire que $f:D \rightarrow E$ est strictement croissante sur $D \Leftrightarrow f^{-1}:E \rightarrow D$ est strictement croissante sur $E$
        * Dire que $f:D \rightarrow E$ est strictement décroissante sur $D \Leftrightarrow f^{-1}:E \rightarrow D$ est strictement décroissante sur $E$

!!! exp
    Soit $f:x\mapsto x^2$ définie sur $D=\mathbb R^+=[0;+\infty[$, et à valeurs dans $E=\mathbb R^+=[0;+\infty[=f(D)$. Cette fonction $f$ est continue et strictement croissante sur $\mathbb R^+$ donc, d'après la propriété précédente, on a bien les propriétés suivantes:

    * Il existe une fonction réciproque $f^{-1}$, notée aussi $x\mapsto \sqrt{x}$
    * $\forall x,y \in \mathbb R^+, \quad \left( y=f(x) \Leftrightarrow f^{-1}(y)=x\right)$ se traduit par:
    $\forall x,y \in \mathbb R^+, \quad \left( y=x^2 \Leftrightarrow x=\sqrt{y}\right)$

    * $\forall x \in \mathbb R^+, \quad f(f^{-1}(x))=x \Leftrightarrow (\sqrt{x})^2=x$
    * $\forall x \in \mathbb R^+, \quad f^{-1}(f(x))=x \Leftrightarrow (\sqrt{x^2})=x$