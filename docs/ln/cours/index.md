# Logarithme Népérien

## Existence et Unicité de la fonction $ln(x)$

!!! def "(Existence et Unicité de LN)"
    La fonction $f:x\mapsto e^x$ est continue et strictement croissante de  $D=\mathbb R$ dans $E=\mathbb R^+_*=]0;+\infty[$, 
    donc **il existe** une **unique** fonction réciproque $f^{-1}$ de $E=]0;+\infty[$ dans $\mathbb R$, notée $ln$, et appelée <red>fonction logarithme népérien</red>.

!!! pte
    Par définition même, on a donc $\quad ln: \,]0;+\infty[ \rightarrow \mathbb R$, et:

    * $\forall x \in \mathbb R^+_*, \forall y\in \mathbb R, \quad y=e^x \Leftrightarrow ln(y)=x$
    * $\forall x \in \mathbb R, \quad ln(e^x)=x$
    * $\forall x \in ]0;+\infty[, \quad e^{ln(x)}=x$

!!! ex
    === "Question 1."
        Calculer $ln(1)$

        ???- "Réponse"
            $e^0=1 \Leftrightarrow 0=ln(1)$

    === "Question 2."
        Résoudre l'équation $e^{2x+1}=3$

        ???- "Réponse"
            $\Leftrightarrow 2x+1=ln(3)$
            $\Leftrightarrow 2x=ln(3)-1$
            $\Leftrightarrow x=\dfrac {ln(3)-1}{2}$

## Formulaire

!!! pte
    * $ln(1)=0$
    * $ln(e)=1$
    * la fonction $ln$ transforme un produit en somme:
    
        <center>$\forall x,y \in ]0;+\infty[, \quad ln(x\times y)=ln(x)+ln(y)$</center>
        ce résultat peut être étendu au cas où l'on a plusieurs termes comme facteurs :
    
        <center>$\forall x_1,x_2,...,x_p \in ]0;+\infty[, \quad ln(x_1\times x_2\times ... \times x_p)=ln(x_1)+ln(x_2)+...+ln(x_p)$</center>

    * la fonction $ln$ transforme une puissance en produit:
    
        <center>$\forall x \in ]0;+\infty[, \forall n \in \mathbb Z, \quad ln(x^n)=n\times ln(x)$</center>
        ce résultat peut être étendu au cas où $y\in \mathbb R$:
    
        <center>$\forall x \in ]0;+\infty[, \forall y \in \mathbb R, \quad ln(x^y)=y\times ln(x)$</center>

    * la fonction $ln$ transforme un inverse en opposé:
    
        <center>$\forall x,y \in ]0;+\infty[, \quad ln \left( \dfrac {1}{x} \right)=-ln(x)$</center>

    * la fonction $ln$ transforme un quotient en soustraction:

        <center>$\forall x,y \in ]0;+\infty[, \quad ln \left( \dfrac {x}{y} \right)=ln(x)-ln(y)$</center>

## Étude de la fonction $ln$

### Symétrie de la courbe de la fonction Réciproque

<center><iframe src="https://www.geogebra.org/classic/pjn7tdbf?embed" width="90%" height="600" allowfullscreen style="border: 1px solid #e4e4e4;border-radius: 4px;" frameborder="0"></iframe></center>

### Signe de la fonction $ln$

!!! pte
    On a les équivalences suivantes:

    * $ln(x)=0 \Leftrightarrow x=1$
    * $ln(x)>0 \Leftrightarrow x>1$
    * $ln(x)<0 \Leftrightarrow 0<x<1$

### Dérivabilité de la fonction $ln$

#### Dérivée de $x\mapsto ln(x)$

!!! pte
    La fonction $x\mapsto ln(x)$ est définie et dérivable sur $D=]0;+\infty[$, et sa dérivée vaut:

    <center>$\left( ln(x)\right)'=\dfrac {1}{x}$</center>

#### Dérivée de $x\mapsto ln(ax+b)$

!!! pte
    La fonction $g:x\mapsto ln(ax+b)$ est définie et dérivable sur $D=]0;+\infty[$, et sa dérivée vaut:

    <center>$g'(x)= \left( ln(ax+b) \right)'=\dfrac {a}{(ax+b)}$</center>

#### Dérivée de $x\mapsto ln(u(x))$

!!! pte
    Soit $u:D \rightarrow ]0;+\infty[$, une fonction définie et dérivable sur l'intervalle $D$, et à valeurs dans $\mathbb R^+_*=]0;+\infty[$.
    Alors la fonction $g:x\mapsto ln(u(x))$ est définie et dérivable sur $D$, et sa dérivée vaut:

    <center>$g'(x)= \left( ln(u(x)) \right)'=\dfrac {u'(x)}{u(x)}$</center>

### Variations de la fonction $ln$

!!! pte
    La fonction $x\mapsto ln(x)$ est strictement croissante sur $D=]0;+\infty[$

D'où le Tableau de Variations de la fonction $\ln$ sur $]-\infty; +\infty[$ :

```vartable
\begin{tikzpicture}
	\tkzTabInit[lgt=3, espcl=6]
		{$x$/1 , Signe de $f'(x) =\dfrac 1x$/1.5 , Variations de $f(x)=\ln x$/2.5}
		{$0$ , $+\infty$}
	\tkzTabLine{d,+,}
	\tkzTabVar{D- / $-\infty$,  + / $+\infty$ }
	\tkzTabVal[draw]{1}{2}{0.35}{$1$}{$0$}
	\tkzTabVal[draw]{1}{2}{0.7}{$e$}{$1$}
\end{tikzpicture}
```

### Application à la Résolution d'Équations & Inéquations

!!! pte
    $\forall x,y \in ]0;+\infty[,$

    * $ln(x)=ln(y) \Leftrightarrow x=y$
    * $ln(x)<ln(y) \Leftrightarrow x<y$
    * $ln(x)\le ln(y) \Leftrightarrow x\le y$
    * $ln(x)>ln(y) \Leftrightarrow x>y$
    * $ln(x)\ge ln(y) \Leftrightarrow x\ge y$

!!! ex
    Résoudre sur $\mathbb R$, les équations et inéquations suivantes:

    * $ln(2x+1)=ln(3x-1)$
    * $ln(2x^2+6x)=ln(x-1)$
    * $ln(4x+5)> ln(-x+2)$
    * $ln(4x+5)\le ln(x+3)$
    * $ln(5x+2)\ge ln(2x+6)$

