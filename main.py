import os
import datetime

def define_env(env):
  "Hook function"

  @env.macro
  def basthon(exo: str, hauteur: int) -> str:
    "Renvoie du HTML pour embarquer un fichier `exo` dans Basthon"
    return f"""<iframe src="https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo}" width="100%;" height="{hauteur};"></iframe>

[Lien dans une autre page](https://console.basthon.fr/?from={env.variables.io_url}{env.variables.page.url}../{exo})
"""

  @env.macro
  def script(lang: str, nom: str) -> str:
    "Renvoie le script dans une balise bloc avec langage spécifié"
    return f"""```{lang}
--8<---  "docs/""" + os.path.dirname(env.variables.page.url.rstrip('/')) + f"""/{nom}"
```"""

  @env.macro
  def py(nom: str) -> str:
    "macro python rapide"
    return script('python', "scripts/" + nom + ".py")

  @env.macro
  def html_fig(num: int) -> str:
    "Renvoie le code HTML de la figure n° `num`"
    return f'--8<-- "docs/' + os.path.dirname(env.variables.page.url.rstrip('/')) + f'/figures/fig_{num}.html"'

  @env.filter
  def year():
    now = datetime.datetime.now()
    return now.year

  @env.filter
  def formatWithDate(s:str)->str:
    """Replaces {date} by the current Year"""
    print("-------------TYPE of s = ", type(s))
    i0 = s.index("{date}")
    return s[:i0]+str(year())+s[i0+6:]
    # return s[:i0]+str(datetime.datetime.now().year)+s[i0+6:]
